#! /bin/bash

function dim {
  echo -e "\e[2m$1\e[0m"
}

function ok {
  echo -e "\e[1m\e[92m$1\e[0m"
}

function err {
  echo -e "\e[1m\e[91m$1\e[0m"
}

function h1 {
  echo ""
  echo -e "\e[1m# $1\e[0m"
}

function h2 {
  echo "-> $1"
}


function h2ok {
  echo -e "\e[1m\e[92m-> $1\e[0m"
}

function h2err {
  echo -e "\e[1m\e[91m-> $1\e[0m"
}

function h3 {
  echo "--> $1"
}

function h3err {
  echo -e "\e[1m\e[91m--> $1\e[0m"
}
function h3ok {
  echo -e "\e[1m\e[92m--> $1\e[0m"
}

function check_result {
  exit_status=$1
  log_fc="$2ok"
  err_log_fc="$2err"
  ok_msg=$3
  error_msg=$4
  if [ $exit_status -ne 0 ]; then
    if [ ! -z "$error_msg" ]; then
      eval "$err_log_fc \"$error_msg\""
    fi
    exit 2
  else
    if [ ! -z "$ok_msg" ]; then
      eval "$log_fc \"$ok_msg\""
    fi
  fi
}


##
## Checks if the Shell is interactive
##
function isInteractiveShell() {
    if [ -t ]; then
        return true
    fi
    return false
}

##
## Pring a message to console using ANSI Colors if the terminal is interactive, otherwhise will print a
## plain text message without ANSI Colors
##
function printMsgWithType() {
   if [ $# -eq 0 ]; then
       echo "USAGE: $0 'text message' [TYPE]"
       echo "Note: TYPE:can be INFO, WARN, ERROR or TRACE, where TRACE will be the default type if it is not specified"
       exit 1
   fi
   MSG=$1
   PRE_MSG=""
   MSG_TYPE="TRACE"
   RESET="\e[0m"
   if [ $# -eq 2 ]; then
        if [ "$2" = "INFO" ]; then
            PRE_MSG="\e[32m[INFO]$RESET"
            MSG_TYPE="[INFO]"
        elif [ "$2" = "WARN" ]; then
            PRE_MSG="\e[33m[WARNING]$RESET"
            MSG_TYPE="[WARNING]"
        elif [ "$2" = "ERROR" ]; then
            PRE_MSG="\e[31m[ERROR]$RESET"
            MSG_TYPE="[ERROR]"
        fi
   fi
   if [ isInteractiveShell ]; then
        echo -e "$PRE_MSG $1"
   else
        echo "$MSG_TYPE $1"
   fi
}

