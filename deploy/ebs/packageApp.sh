#! /bin/bash

OUT_DIR=${1:-/tmp} 
OUT_FILE="vcm.zip"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PARENT_DIR="$(dirname "${DIR}")" 

. ${PARENT_DIR}/commons/common-functions.sh

cd ${DIR}
h1 "Creating Application ZIP file"
zip ${OUT_DIR}/${OUT_FILE} -r -@ < ${DIR}/files.lst
check_result $? "h2" "Done. File generated at ${OUT_DIR}/${OUT_FILE}" "Failed!"
cd -
