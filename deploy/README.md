# Elastic Beanstalk (EBS) application

This section explains how to create an Elastic Beanstalk (EBS) application to deploy VCM  

## Dockerfile
The Dockerfile just reference to the ECR image, below how it should be:
[Dockerfile](./ebs/Dockerfile)


## Websockets

To allow websockets connections be operative for VCM is necessary to add some extra configuration to the nginx server 
that exists on the VCM Elastic Beanstalk application. This configuration should be specified as a descriptor in the 
deploy source zip file.

###Structure
The structure is important, is necessary to create a zip file with this format:

```
.
├── .ebextensions
│   ├── cloudwatch.config
│   ├── nginx-instance.config
├── Dockerfile  
```

### Nginx Configuration 
The file should have at least this content
[nginx-instance.config](./ebs/.ebextensions/nginx-instance.config)


### Cloudwatch Configuration (optional)
This configuration enables some metrics that are useful to monitor like memory consumption, disk space, etc.
The file should have at least this content
[cloudwatch.config](./ebs/.ebextensions/cloudwatch.config)

## Zip file creation
In order to simplify this process there is a script that can be used. It's on the `deploy/ebs` folder.

##ebs folder
On this folder exists all the configuration, the Dockerfile explained above and one script called `packageApp.sh`.  
 

###Running the script

Example 1:
```bash
./deploy/ebs/packageApp.sh  
```
The zip file will be generated in `/tmp/vcm.zip`.

or specifying an existing directory like '/home/user1'

Example 2:

```bash
./deploy/ebs/packageApp.sh /home/user1  
```
The zip file will be generated in `/home/user1/vcm.zip`.

The zip file is the deployment descriptor used by Elastic Beanstalk Application. 

#IMPORTANT
New Advanced environment customization with configuration files (.ebextensions) for VCM must be committed and pushed 
on the `deploy/ebs/.ebextensions` directory.

You can add AWS Elastic Beanstalk configuration files (.ebextensions) to your web application's source code to configure your environment and customize the AWS resources that it contains. Configuration files are YAML- or JSON-formatted documents with a .config file extension that you place in a folder named .ebextensions and deploy in your application source bundle.

#REFERENCES
https://docs.aws.amazon.com/elastic-beanstalk/index.html

https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/ebextensions.html
