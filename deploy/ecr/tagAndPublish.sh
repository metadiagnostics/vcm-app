#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PARENT_DIR="$(dirname "${DIR}")"
. ${PARENT_DIR}/commons/common-functions.sh

##
## Checks if aws-google-auth tool is the PATH
##
if type -P aws-google-auth > /dev/null; then
  printMsgWithType "'aws-google-auth' is installed and available in the PATH." INFO
else
  printMsgWithType "You need to install 'aws-google-auth' to run this script" ERROR
  echo "https://github.com/cevoaustralia/aws-google-auth"
  exit 1
fi

##
## Checks the correct script usage
##
if [ $# -eq 0 ]; then
    printMsgWithType "Please specify the <google username> to log in using the aws-google-auth tool" ERROR
    echo "Usage: $0 <google username> [AWS_PROFILE [AWS_REGION]]"
    echo -e "Usage Example: $0 jdo ventdemo us-east-2\n"
    echo -e "Note: The username is not the full email address, for instance jdoe is the username for jdoe@saperi.io\n\n"
    exit 1
fi

GOOGLE_USERNAME=$1

##
## Checks if AWS_PROFILE was specified
##
if [ $# -eq 1 ]; then
    printMsgWithType "AWS_PROFILE was not specified, using default profile 'ventdemo'" WARN
    AWS_PROFILE=ventdemo
elif [ $# -gt 1 ]; then
    AWS_PROFILE=$2
fi

##
## Checks if AWS_REGION was specified
##
if [ $# -lt 3 ]; then
    printMsgWithType "AWS_REGION was not specified, using region 'us-east-2'" WARN
    AWS_REGION=us-east-2
elif [ $# -gt 2 ]; then
    AWS_REGION=$3
fi

# Using AWS CLI commands with SAML Federated Logins
# Please refer to this page to further details: https://saperisys.atlassian.net/wiki/spaces/EN/pages/351338540/Saperi+AWS+Login+Usage+via+Saperi+GSuite+GMail#Using-AWS-CLI-commands-with-SAML-Federated-Logins
aws-google-auth --username $GOOGLE_USERNAME@saperi.io  \
--region $AWS_REGION \
--idp-id C02ks349j \
--sp-id 85430879393  \
--role-arn arn:aws:iam::527978306071:role/Saperi_GA@VentilatorWebAppDemo  \
--duration 14400 \
--profile $AWS_PROFILE

IMG_NAME="vcm"
IMG_VERSION="latest"
AWS_ECR_ID=${AWS_ECR_ID:-527978306071.dkr.ecr.us-east-2.amazonaws.com}
AWS_TAG="${AWS_ECR_ID}/vcm"
AWS_REGION='us-east-2'

h1 "Authenticating with AWS ECR using '${AWS_PROFILE}' profile"
aws --profile $AWS_PROFILE --region $AWS_REGION ecr get-login-password | docker login --username AWS --password-stdin ${AWS_ECR_ID}
check_result $? "h2" "Done" "Failed!"

IMG=`docker images | grep "^${IMG_NAME}\s*${IMG_VERSION}" | awk '{ print $3}'`
h1 "Tagging Image $IMG (${IMG_NAME}/${IMG_VERSION}) as ${AWS_TAG}"
docker tag ${IMG} ${AWS_TAG}:${IMG_VERSION}
check_result $? "h2" "Done" "Failed!"

h1 "Pushing ${IMG} to AWS ECR"
docker push ${AWS_TAG}:${IMG_VERSION}
check_result $? "h2" "Done" "Failed!"
echo ""


