# AWS ESB Deployment

For this application, the following EBS Application was created: https://us-east-2.console.aws.amazon.com/elasticbeanstalk/home?region=us-east-2#/application/overview?applicationName=VCM

For now, the application contains a single environment called `VcmEnv`. The environment was configured to use a RDS instance to host the application's MySQL database. The environment is configured to support Docker images to be deployed.

## Changes required in the Application (Deprecated)

**This section is deprecated because the application is now using HTTP and running behind a LB that deals with HTTPS connections comming from
the clients**

Because the application will be running inside a docker container behind an Nginx instance (this is automatically done by AWS), and because Nginx will be configured to use HTTPS, we need to tell our
application to understand the `x-forwarded-for` and `x-forwarded-proto` headers that Nginx will be sending.

To add support to these headers, we need to modify our `application-prod.yml` file and add the following configuration option in the `server` section:

```yaml
server:
  use-forward-headers: true
```

Source: https://stackoverflow.com/questions/33812471/spring-oauth-redirect-uri-not-using-https

## Deploying the Application

The deployment strategy is the following:

- We package the application inside a docker image **(1)**
- We tag the image with the tags required for the image to be published in AWS' ECR **(2)**
- We push the image into ECR **(2)**
- We then deploy a `Dockefile` in AWS EBS that creates a new image based on the one we have published to ECR. **(3)**

**(1)** To package the application inside a docker image we use Maven and JIB:

```sh
./mvnw -DskipTests -Pprod verify jib:dockerBuild
```

We can verify that we now have an image in our local registry:

```sh
docker images | grep "vcm"
```

**(2)** For this step we will need to have [AWS CLI](https://aws.amazon.com/cli/) installed and configured in our system. We can check the installation by running the following command:

```sh
aws configure list
```

Once we are sure the AWS CLI is installed and configured, and once we have the Docker image of the application in our local registry, we can execute the following command to tag the image and to push it to ECR:

```sh
./deploy/ecr/tagAndPublish.sh
```

**(3)** The deployment of the application in EBS is a manual process that includes the following steps:

- Create a zip file with a `Dockerfile` and some ESB specific configurations **(3.1)**
- Upload the zip file to EBS using the browser. **(3.2)**

**(3.1)** To create the zip file that will be deployed in EBS, we can execute the following commands:

```sh
./deploy/ebs/packageApp.sh
```

The zip file will be generated in `/tmp/vcm.zip`.

**(3.2)** To deploy the zip file created in the previous step we will need to login into the EBS Console and browse into the VCM application and `VcmEnv` environment (or the environment we want). For example, [this URL](https://us-east-2.console.aws.amazon.com/elasticbeanstalk/home?region=us-east-2#/environment/dashboard?applicationName=VCM&environmentId=e-btsp9kpshv) is for the `VcmEnv` environment.

In there, we can use the "Upload and deploy" button to upload the zip file we generated before.

**Tip**: When uploading a new version of the application, if the only thing that changed was the docker image itself, the only thing we need to do for EBS to take the changes is to restart the environment after having pushed the new version of the docker image into ECR. This is only possible because EBS is using the _latest_ version of the docker image, and the image is pulled every time the environment is started. The following AWS CLI command can be used to restart the `VcmEnv` environment:

```sh
aws elasticbeanstalk restart-app-server --environment-name 	VcmEnvironment
```

# ESB Configuration

There are 2 layers of configurations involving EBS: the files that are using during the deployment of the application to configure the docker image that ends up being deployed and the configuration of the environment itself.

## Deployment Configuration files (Deprecated)

**This section is deprecated because the application is now using HTTP and running behind a LB that deals with HTTPS connections comming from
the clients**

The deployment configuration files are located in the `deploy/ebs` directory.

![deploy/ebs directory](docs/assets/deploy-ebs-dir.png)

The following is a brief description of each of the files present in this directory:

- `Dockerfile`: This is the file that EBS uses to create the docker image to be deployed. Note that this is not the Dockerfile that we used when we created the docker image of the application itself. In fact, this Dockerfile is based on the other. There is not much to see or change in this file other than the base docker image and the port our application will be listening to.
- `privatekey.confg`: When deployed, the application will be configured to use HTTPs. This file contains the configuration used by EBS to fetch a certificate and its private key from an S3 Bucket in amazon. More information about how these files were created can be found [here](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/configuring-https-ssl.html). Information about how this file can be configured can be found [here](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/https-storingprivatekeys.html). More information about the default S3 Bucket that is automatically created for each EBS application can be found [here](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/AWSHowTo.S3.html).
- `http-instance-single.config`: Given that the application is not using a load balancer we need to configure it in a way that the proxy server passes the HTTPS traffic to the application instance. In other words, it is the instance (the Nginx server in the instance) the one that will deal with HTTPS and not EBS itself. This file contains the configuration to do exactly that. More information about this file can be found [here](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/https-singleinstance.html).
- `https-instance.config`: This file is used by ESB to tell the Nginx instance used to receive the traffic to use SSL and to set the HTTP headers that the proxy will pass to the application. This file will use the certificate and private key configured in the `privatekey.config` file. More information about this file can be found [here](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/https-singleinstance-docker.html) and [here](https://www.serverlab.ca/tutorials/linux/web-servers-linux/how-to-proxy-wss-websockets-with-nginx/).

## Environment Configuration

The application uses [Spring Boot's Configuration Externalization](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-external-config) meaning that all its configuration options can be modified in multiple ways including system properties, environment variables and so on. In EBS, we can define environment variables for an environment and in this way configure our application. The available configuration options can be extracted from `src/main/resources/config/application.yml` and `src/main/resources/config/application-prod.yml`. In order to set the values of these configuration options in EBS, we can use the EBS GUI. Navigate to the environment dashboard (i.e. https://us-east-2.console.aws.amazon.com/elasticbeanstalk/home?region=us-east-2#/environment/dashboard?applicationName=VCM&environmentId=e-btsp9kpshv) and then _Configuration_ -> _Software_ -> _Edit_. In the "Environment properties" section, we can include the value for the application's configuration options.

![EBS Configuration](./docs/assets/ebs-env-config.png)

# OAuth Server Configuration
