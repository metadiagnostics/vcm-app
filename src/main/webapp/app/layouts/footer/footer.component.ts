/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, OnInit } from '@angular/core';
import { VERSION } from 'app/app.constants';
import { COMMITHASH } from 'app/app.constants';

@Component({
  selector: 'jhi-footer',
  styleUrls: ['footer.scss'],
  templateUrl: './footer.component.html'
})
export class FooterComponent {
  version: string;
  commitHash: string;
  constructor() {
    // eslint-disable-next-line no-console
    this.version = VERSION ? (VERSION.toLowerCase().startsWith('v') ? VERSION : 'v' + VERSION) : '';
    this.commitHash = '' + COMMITHASH;
  }
}
