/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Pagination } from 'app/shared/util/request-util';
import { ILabelModel } from 'app/monitor/label.model';
import { IHWModel } from 'app/monitor/hardware.model';
import { IVentModel } from 'app/monitor/ventilator.model';
import { IVentilatorMode } from 'app/monitor/ventilator.mode';

@Injectable({ providedIn: 'root' })
export class VentilatorService {
  public resourceUrl = SERVER_API_URL + 'api/ventilator';

  constructor(private http: HttpClient) {}

  getLabels(req?: Pagination): Observable<HttpResponse<ILabelModel>> {
    const options = createRequestOption(req);
    return this.http.get<ILabelModel>(this.resourceUrl, { params: options, observe: 'response' });
  }

  getModes(hwId: string, req?: Pagination): Observable<HttpResponse<IVentilatorMode[]>> {
    const options = createRequestOption(req);
    return this.http.get<IVentilatorMode[]>(this.resourceUrl + '/mode/' + hwId, { params: options, observe: 'response' });
  }

  getVentilatorRoDefinitions(hwId: string, req?: Pagination): Observable<HttpResponse<IHWModel[]>> {
    const options = createRequestOption(req);
    return this.http.get<IHWModel[]>(this.resourceUrl + '/definition/ro/' + hwId, { params: options, observe: 'response' });
  }

  /*getVentilatorRwDefinitions(hwId: string, req?: Pagination): Observable<HttpResponse<IVentModel[]>> {
    const options = createRequestOption(req);
    return this.http.get<IVentModel[]>(this.resourceUrl + '/definition/rw/' + hwId, { params: options, observe: 'response' });
  }*/
}
