/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Injectable } from '@angular/core';
import { Location } from '@angular/common';

import { AuthServerProvider } from 'app/core/auth/auth-session.service';
import { Logout } from './logout.model';

@Injectable({ providedIn: 'root' })
export class LoginService {
  constructor(private location: Location, private authServerProvider: AuthServerProvider) {}

  login(): void {
    // If you have configured multiple OIDC providers, then, you can update this URL to /login.
    // It will show a Spring Security generated login page with links to configured OIDC providers.
    location.href = `${location.origin}${this.location.prepareExternalUrl('oauth2/authorization/oidc')}`;
  }

  logout(): void {
    this.authServerProvider.logout().subscribe((logout: Logout) => {
      let logoutUrl = logout.logoutUrl;
      const redirectUri = `${location.origin}${this.location.prepareExternalUrl('/')}`;

      if(logout.implementation === "cognito"){
        logoutUrl = `${logout.logoutUrl}?client_id=${logout.id}&logout_uri=${redirectUri}`;
      } else {
        // if Keycloak, uri has protocol/openid-connect/token
        if (logoutUrl.includes('/protocol')) {
          logoutUrl = logoutUrl + '?redirect_uri=' + redirectUri;
        } else {
          // Okta
          logoutUrl = logoutUrl + '?id_token_hint=' + logout.id + '&post_logout_redirect_uri=' + redirectUri;
        }
      }
      window.location.href = logoutUrl;
    });
  }
}
