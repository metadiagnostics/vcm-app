/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Pagination } from 'app/shared/util/request-util';
import { ITeamMember } from 'app/monitor/team-member.model';

@Injectable({ providedIn: 'root' })
export class CareTeamService {
  public resourceUrl = SERVER_API_URL + 'api/';

  constructor(private http: HttpClient) {}

  getLocalTeamItems(req?: Pagination): Observable<HttpResponse<ITeamMember[]>> {
    const options = createRequestOption(req);
    return this.http.get<ITeamMember[]>(this.resourceUrl + 'localteam', { params: options, observe: 'response' });
  }

  getRemoteTeamItems(req?: Pagination): Observable<HttpResponse<ITeamMember[]>> {
    const options = createRequestOption(req);
    return this.http.get<ITeamMember[]>(this.resourceUrl + 'remoteteam', { params: options, observe: 'response' });
  }
}
