/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Pagination } from 'app/shared/util/request-util';
import { IAlarmModel, IAlarmWarningModel } from 'app/monitor/alarm.model';

@Injectable({ providedIn: 'root' })
export class AlarmService {
  public resourceUrl = SERVER_API_URL + 'api/';

  private alarmsSubject = new Subject<IAlarmWarningModel[]>();

  private alarms: IAlarmWarningModel[];

  private audio = new Audio();

  private forceMute = false;

  constructor(private http: HttpClient) {
    this.alarms = [];
    this.audio.src = '/content/beep.mp3';
    this.audio.volume = 0.05; // sets the volume to 5%
    this.audio.loop = true;
  }


  getAlarmWarnings(patientId: string, req?: Pagination): Observable<HttpResponse<IAlarmWarningModel[]>> {
    const options = createRequestOption(req);
    return this.http.get<IAlarmWarningModel[]>(this.resourceUrl + 'alarm/warning/patient/' + patientId, {
      params: options,
      observe: 'response'
    });
  }

  getAlarmWarning(id: string, patientId: string, req?: Pagination): Observable<HttpResponse<IAlarmWarningModel>> {
    const options = createRequestOption(req);
    return this.http.get<IAlarmWarningModel>(this.resourceUrl + 'alarm/warning/' + id + '/patient/' + patientId, {
      params: options,
      observe: 'response'
    });
  }

  ackAllAlarmWarnings(patientId: string, req?: Pagination): Observable<HttpResponse<IAlarmWarningModel[]>> {
    const options = createRequestOption(req);
    return this.http.put<IAlarmWarningModel[]>(this.resourceUrl + 'alarm/warning/patient/' + patientId, [], {
      params: options,
      observe: 'response'
    });
  }

  ackAlarmWarning(id: string, patientId: string, req?: Pagination): Observable<{}> {
    const options = createRequestOption(req);
    return this.http.put<{}>(this.resourceUrl + 'alarm/warning/' + id + '/patient/' + patientId, { params: options, observe: 'response' });
  }

  public alarmsSubscription(): Subject<IAlarmWarningModel[]> {
    return this.alarmsSubject;
  }

  public notify(alarms: IAlarmWarningModel[]): void {
    this.alarms = alarms;
    this.alarms.forEach( a => a.muted= a.muted || this.forcedMute());
    this.alarmsSubject.next(alarms);
  }

  public getAudio(): HTMLAudioElement {
    return this.audio;
  }

  public setAudioVolume(volume: number): void {
    if (this.audio !== undefined) {
      this.audio.volume = volume;
    }
  }

  public getAlarms(): IAlarmWarningModel[] {
    return this.alarms;
  }

  public areAllAlarmsMuted(): boolean {
    return this.alarms.filter(a => a.muted === true).length === this.alarms.length;
  }

  public forcedMute(): boolean {
    return  this.forceMute;
  }

  public forceMuteSound(): void {
    this.forceMute = true;
  }

  public forceUnmuteSound(): void {
    this.forceMute = false;
  }
}
