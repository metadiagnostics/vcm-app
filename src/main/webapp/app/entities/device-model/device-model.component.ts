/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IDeviceModel } from 'app/shared/model/device-model.model';
import { DeviceModelService } from './device-model.service';
import { DeviceModelDeleteDialogComponent } from './device-model-delete-dialog.component';

@Component({
  selector: 'jhi-device-model',
  templateUrl: './device-model.component.html'
})
export class DeviceModelComponent implements OnInit, OnDestroy {
  deviceModels?: IDeviceModel[];
  eventSubscriber?: Subscription;

  constructor(
    protected deviceModelService: DeviceModelService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.deviceModelService.query().subscribe((res: HttpResponse<IDeviceModel[]>) => (this.deviceModels = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInDeviceModels();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IDeviceModel): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInDeviceModels(): void {
    this.eventSubscriber = this.eventManager.subscribe('deviceModelListModification', () => this.loadAll());
  }

  delete(deviceModel: IDeviceModel): void {
    const modalRef = this.modalService.open(DeviceModelDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.deviceModel = deviceModel;
  }
}
