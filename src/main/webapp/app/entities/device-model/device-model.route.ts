/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IDeviceModel, DeviceModel } from 'app/shared/model/device-model.model';
import { DeviceModelService } from './device-model.service';
import { DeviceModelComponent } from './device-model.component';
import { DeviceModelDetailComponent } from './device-model-detail.component';
import { DeviceModelUpdateComponent } from './device-model-update.component';

@Injectable({ providedIn: 'root' })
export class DeviceModelResolve implements Resolve<IDeviceModel> {
  constructor(private service: DeviceModelService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDeviceModel> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((deviceModel: HttpResponse<DeviceModel>) => {
          if (deviceModel.body) {
            return of(deviceModel.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new DeviceModel());
  }
}

export const deviceModelRoute: Routes = [
  {
    path: '',
    component: DeviceModelComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'vcmApp.deviceModel.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: DeviceModelDetailComponent,
    resolve: {
      deviceModel: DeviceModelResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'vcmApp.deviceModel.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: DeviceModelUpdateComponent,
    resolve: {
      deviceModel: DeviceModelResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'vcmApp.deviceModel.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: DeviceModelUpdateComponent,
    resolve: {
      deviceModel: DeviceModelResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'vcmApp.deviceModel.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
