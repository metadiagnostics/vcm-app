/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAssignment, Assignment } from 'app/shared/model/assignment.model';
import { AssignmentService } from './assignment.service';
import { IAssignedDevice, AssignedDevice } from 'app/shared/model/assigned-device.model';
import { IDeviceCapability } from 'app/shared/model/device-capability.model';
import { DeviceCapabilityService } from '../device-capability/device-capability.service';
import { IDevice } from 'app/shared/model/device.model';
import { ExtDeviceService } from '../device/ext-device.service';
import { DeviceCapabilityType } from 'app/shared/model/enumerations/device-capability-type.model';

@Component({
  selector: 'jhi-assignment-update',
  templateUrl: './assignment-update.component.html'
})
export class AssignmentUpdateComponent implements OnInit {
  assignment?: IAssignment;
  isSaving = false;

  deviceCapabilities: IDeviceCapability[] = [];
  devicesForSelectedCapability: IDevice[] = [];

  editForm = this.fb.group({
    id: [],
    patientId: [null, [Validators.required]],
    mrn: [null, [Validators.required]],
    selectedNewCapability: [],
    selectedNewDevice: [],
    active: [],
    ventilatorMode: [],
    ventilatorRecommenderType: [],
    ventilatorControlType: []
  });

  constructor(
    protected assignmentService: AssignmentService,
    protected extDeviceService: ExtDeviceService,
    protected deviceCapabilityService: DeviceCapabilityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ assignment }) => {
      this.assignment = assignment;
      this.updateForm(assignment);
    });

    this.deviceCapabilityService.query().subscribe((res: HttpResponse<IDeviceCapability[]>) => (this.deviceCapabilities = res.body || []));
  }

  updateForm(assignment: IAssignment): void {
    this.editForm.patchValue({
      id: assignment.id,
      patientId: assignment.patientId,
      mrn: assignment.mrn,
      active: assignment.active,
      ventilatorMode: assignment.ventilatorMode,
      ventilatorRecommenderType: assignment.ventilatorRecommenderType,
      ventilatorControlType: assignment.ventilatorControlType
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const assignment = this.createFromForm();

    assignment.devices = this.assignment?.devices;
    assignment.devices?.forEach(ad => {
      ad.assignment!.devices = [];
    });

    if (assignment.id !== undefined) {
      this.subscribeToSaveResponse(this.assignmentService.update(assignment));
    } else {
      this.subscribeToSaveResponse(this.assignmentService.create(assignment));
    }
  }

  trackDeviceId(index: number, item: IAssignedDevice): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  private createFromForm(): IAssignment {
    return {
      ...new Assignment(),
      id: this.editForm.get(['id'])!.value,
      patientId: this.editForm.get(['patientId'])!.value,
      mrn: this.editForm.get(['mrn'])!.value,
      active: this.editForm.get(['active'])!.value,
      ventilatorMode: this.editForm.get(['ventilatorMode'])!.value,
      ventilatorRecommenderType: this.editForm.get(['ventilatorRecommenderType'])!.value,
      ventilatorControlType: this.editForm.get(['ventilatorControlType'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAssignment>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  // Assignment Data Sources (Devices)
  onDataSourceChange(): void {
    this.editForm.patchValue({ selectedNewDevice: undefined });

    const capability = this.editForm.get(['selectedNewCapability'])?.value;
    if (!capability || !capability.type) {
      this.devicesForSelectedCapability = [];
    } else {
      this.extDeviceService
        .available(this.assignment ? this.assignment.id : undefined, capability.type)
        .subscribe((res: HttpResponse<IDevice[]>) => {
          this.devicesForSelectedCapability = res.body || [];

          if (this.devicesForSelectedCapability.length > 0) {
            this.editForm.patchValue({ selectedNewDevice: this.devicesForSelectedCapability[0] });
          }
        });
    }
  }

  onDataSourceAdd(): void {
    const capability: IDeviceCapability = this.editForm.get(['selectedNewCapability'])?.value;
    const device: IDevice = this.editForm.get(['selectedNewDevice'])?.value;

    if (!capability || !device) {
      return;
    }

    // add the device to the assignment
    if (!this.assignment!.devices) {
      this.assignment!.devices = [];
    }

    if (
      this.assignment!.devices.find(ad => {
        return ad.device?.hardwareId === device.hardwareId && ad.capability?.type === capability.type;
      })
    ) {
      // We already have this assignment
      return;
    }

    const assignedDevice = new AssignedDevice(undefined, this.assignment, device, capability);
    this.assignment!.devices?.push(assignedDevice);
  }

  onDataSourceDeleted(hardwareId: string, capabilityType: DeviceCapabilityType): void {
    // Remove the desired device
    this.assignment!.devices =
      this.assignment!.devices?.filter(ad => {
        return ad.device?.hardwareId !== hardwareId || ad.capability?.type !== capabilityType;
      }) || [];
  }
}
