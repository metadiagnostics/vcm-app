/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { IDevice } from 'app/shared/model/device.model';
import { DeviceCapabilityType } from 'app/shared/model/enumerations/device-capability-type.model';

type EntityArrayResponseType = HttpResponse<IDevice[]>;

@Injectable({ providedIn: 'root' })
export class ExtDeviceService {
  public resourceUrl = SERVER_API_URL + 'api/ext';

  constructor(protected http: HttpClient) {}


   unassigned(capability?: DeviceCapabilityType): Observable<EntityArrayResponseType> {
    let options: HttpParams = new HttpParams();
    if(capability){
      options = options.set("capability", capability);
    }

    return this.http.get<IDevice[]>(this.resourceUrl+"/unassigned-devices", { params: options, observe: 'response' });
   }

   available(assignmentId?: number, capability?: DeviceCapabilityType): Observable<EntityArrayResponseType> {
    let options: HttpParams = new HttpParams();
    if (assignmentId){
      options = options.set("assignmentId", assignmentId.toString());
    }
    if(capability){
      options = options.set("capability", capability);
    }

    return this.http.get<IDevice[]>(this.resourceUrl+"/available-devices", { params: options, observe: 'response' });
  }
}
