/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IDeviceCapability, DeviceCapability } from 'app/shared/model/device-capability.model';
import { DeviceCapabilityService } from './device-capability.service';
import { DeviceCapabilityComponent } from './device-capability.component';
import { DeviceCapabilityDetailComponent } from './device-capability-detail.component';

@Injectable({ providedIn: 'root' })
export class DeviceCapabilityResolve implements Resolve<IDeviceCapability> {
  constructor(private service: DeviceCapabilityService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDeviceCapability> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((deviceCapability: HttpResponse<DeviceCapability>) => {
          if (deviceCapability.body) {
            return of(deviceCapability.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new DeviceCapability());
  }
}

export const deviceCapabilityRoute: Routes = [
  {
    path: '',
    component: DeviceCapabilityComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'vcmApp.deviceCapability.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: DeviceCapabilityDetailComponent,
    resolve: {
      deviceCapability: DeviceCapabilityResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'vcmApp.deviceCapability.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
