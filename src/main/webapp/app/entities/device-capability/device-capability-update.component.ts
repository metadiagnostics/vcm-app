/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IDeviceCapability, DeviceCapability } from 'app/shared/model/device-capability.model';
import { DeviceCapabilityService } from './device-capability.service';

@Component({
  selector: 'jhi-device-capability-update',
  templateUrl: './device-capability-update.component.html'
})
export class DeviceCapabilityUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    type: []
  });

  constructor(
    protected deviceCapabilityService: DeviceCapabilityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ deviceCapability }) => {
      this.updateForm(deviceCapability);
    });
  }

  updateForm(deviceCapability: IDeviceCapability): void {
    this.editForm.patchValue({
      id: deviceCapability.id,
      type: deviceCapability.type
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const deviceCapability = this.createFromForm();
    if (deviceCapability.id !== undefined) {
      this.subscribeToSaveResponse(this.deviceCapabilityService.update(deviceCapability));
    } else {
      this.subscribeToSaveResponse(this.deviceCapabilityService.create(deviceCapability));
    }
  }

  private createFromForm(): IDeviceCapability {
    return {
      ...new DeviceCapability(),
      id: this.editForm.get(['id'])!.value,
      type: this.editForm.get(['type'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDeviceCapability>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
