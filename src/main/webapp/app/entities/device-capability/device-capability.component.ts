/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { IDeviceCapability } from 'app/shared/model/device-capability.model';
import { DeviceCapabilityService } from './device-capability.service';

@Component({
  selector: 'jhi-device-capability',
  templateUrl: './device-capability.component.html'
})
export class DeviceCapabilityComponent implements OnInit, OnDestroy {
  deviceCapabilities?: IDeviceCapability[];
  eventSubscriber?: Subscription;

  constructor(protected deviceCapabilityService: DeviceCapabilityService, protected eventManager: JhiEventManager) {}

  loadAll(): void {
    this.deviceCapabilityService.query().subscribe((res: HttpResponse<IDeviceCapability[]>) => (this.deviceCapabilities = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInDeviceCapabilities();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IDeviceCapability): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInDeviceCapabilities(): void {
    this.eventSubscriber = this.eventManager.subscribe('deviceCapabilityListModification', () => this.loadAll());
  }
}
