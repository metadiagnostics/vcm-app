/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAssignedDevice, AssignedDevice } from 'app/shared/model/assigned-device.model';
import { AssignedDeviceService } from './assigned-device.service';
import { AssignedDeviceComponent } from './assigned-device.component';
import { AssignedDeviceDetailComponent } from './assigned-device-detail.component';
import { AssignedDeviceUpdateComponent } from './assigned-device-update.component';

@Injectable({ providedIn: 'root' })
export class AssignedDeviceResolve implements Resolve<IAssignedDevice> {
  constructor(private service: AssignedDeviceService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAssignedDevice> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((assignedDevice: HttpResponse<AssignedDevice>) => {
          if (assignedDevice.body) {
            return of(assignedDevice.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AssignedDevice());
  }
}

export const assignedDeviceRoute: Routes = [
  {
    path: '',
    component: AssignedDeviceComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'vcmApp.assignedDevice.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: AssignedDeviceDetailComponent,
    resolve: {
      assignedDevice: AssignedDeviceResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'vcmApp.assignedDevice.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: AssignedDeviceUpdateComponent,
    resolve: {
      assignedDevice: AssignedDeviceResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'vcmApp.assignedDevice.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: AssignedDeviceUpdateComponent,
    resolve: {
      assignedDevice: AssignedDeviceResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'vcmApp.assignedDevice.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
