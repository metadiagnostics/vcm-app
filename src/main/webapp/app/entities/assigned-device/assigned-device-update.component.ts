/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAssignedDevice, AssignedDevice } from 'app/shared/model/assigned-device.model';
import { AssignedDeviceService } from './assigned-device.service';
import { IAssignment } from 'app/shared/model/assignment.model';
import { AssignmentService } from 'app/entities/assignment/assignment.service';
import { IDevice } from 'app/shared/model/device.model';
import { DeviceService } from 'app/entities/device/device.service';
import { IDeviceCapability } from 'app/shared/model/device-capability.model';
import { DeviceCapabilityService } from 'app/entities/device-capability/device-capability.service';

type SelectableEntity = IAssignment | IDevice | IDeviceCapability;

@Component({
  selector: 'jhi-assigned-device-update',
  templateUrl: './assigned-device-update.component.html'
})
export class AssignedDeviceUpdateComponent implements OnInit {
  isSaving = false;
  assignments: IAssignment[] = [];
  devices: IDevice[] = [];
  devicecapabilities: IDeviceCapability[] = [];

  editForm = this.fb.group({
    id: [],
    assignment: [],
    device: [],
    capability: [null, Validators.required]
  });

  constructor(
    protected assignedDeviceService: AssignedDeviceService,
    protected assignmentService: AssignmentService,
    protected deviceService: DeviceService,
    protected deviceCapabilityService: DeviceCapabilityService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ assignedDevice }) => {
      this.updateForm(assignedDevice);

      this.assignmentService.query().subscribe((res: HttpResponse<IAssignment[]>) => (this.assignments = res.body || []));

      this.deviceService.query().subscribe((res: HttpResponse<IDevice[]>) => (this.devices = res.body || []));

      this.deviceCapabilityService
        .query()
        .subscribe((res: HttpResponse<IDeviceCapability[]>) => (this.devicecapabilities = res.body || []));
    });
  }

  updateForm(assignedDevice: IAssignedDevice): void {
    this.editForm.patchValue({
      id: assignedDevice.id,
      assignment: assignedDevice.assignment,
      device: assignedDevice.device,
      capability: assignedDevice.capability
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const assignedDevice = this.createFromForm();
    if (assignedDevice.id !== undefined) {
      this.subscribeToSaveResponse(this.assignedDeviceService.update(assignedDevice));
    } else {
      this.subscribeToSaveResponse(this.assignedDeviceService.create(assignedDevice));
    }
  }

  private createFromForm(): IAssignedDevice {
    return {
      ...new AssignedDevice(),
      id: this.editForm.get(['id'])!.value,
      assignment: this.editForm.get(['assignment'])!.value,
      device: this.editForm.get(['device'])!.value,
      capability: this.editForm.get(['capability'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAssignedDevice>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
