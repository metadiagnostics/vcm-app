/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAssignedDevice } from 'app/shared/model/assigned-device.model';
import { AssignedDeviceService } from './assigned-device.service';
import { AssignedDeviceDeleteDialogComponent } from './assigned-device-delete-dialog.component';

@Component({
  selector: 'jhi-assigned-device',
  templateUrl: './assigned-device.component.html'
})
export class AssignedDeviceComponent implements OnInit, OnDestroy {
  assignedDevices?: IAssignedDevice[];
  eventSubscriber?: Subscription;

  constructor(
    protected assignedDeviceService: AssignedDeviceService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.assignedDeviceService.query().subscribe((res: HttpResponse<IAssignedDevice[]>) => (this.assignedDevices = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAssignedDevices();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAssignedDevice): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAssignedDevices(): void {
    this.eventSubscriber = this.eventManager.subscribe('assignedDeviceListModification', () => this.loadAll());
  }

  delete(assignedDevice: IAssignedDevice): void {
    const modalRef = this.modalService.open(AssignedDeviceDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.assignedDevice = assignedDevice;
  }
}
