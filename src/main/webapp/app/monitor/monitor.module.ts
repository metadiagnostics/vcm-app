/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { NgModule } from '@angular/core';

import { AccordionModule } from 'primeng/accordion';
import { ButtonModule } from 'primeng/button';
import { DragDropModule } from 'primeng/dragdrop';
import { FieldsetModule } from 'primeng/fieldset';
import { PanelModule } from 'primeng/panel';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { SelectButtonModule } from 'primeng/selectbutton';
import { RadioButtonModule } from 'primeng/radiobutton';
import { SliderModule } from 'primeng/slider';
import { DialogModule } from 'primeng/dialog';

import { DrawerModule } from 'app/drawer/drawer.module';
import { PulseControllerModule } from 'app/pulse-controller/pulse-controller.module';
import { VcmSharedModule } from 'app/shared/shared.module';
import { FlowsheetDataBlockModule } from 'app/shared/components/flowsheet-data-block/flowsheet-data-block.module';
import { HardwareDataBlockModule } from 'app/shared/components/hardware-data-block/hardware-data-block.module';
import { NumericDataBlockModule } from 'app/shared/components/numeric-data-block/numeric-data-block.module';
import { TabularDataBlockModule } from 'app/shared/components/tabular-data-block/tabular-data-block.module';
import { StatusBadgeModule } from 'app/shared/components/status-badge/status-badge.module';
import { VentilatorRecommenderInputDataBlockModule } from 'app/shared/components/ventilator-recommender-input-data-block/ventilator-recommender-input-data-block.module';
import { MonitorAssignmentsComponent } from './monitor-assignments.component';
import { MonitorDrawerComponent } from './monitor-drawer.component';
import { MonitorViewComponent } from './monitor-view.component';
import { VentilatorParameterDataBlockComponent } from './ventilator-parameter-data-block/ventilator-parameter-data-block.component';
import { VentilatorRecommenderDrawerComponent } from './ventilator-recommender-drawer/ventilator-recommender-drawer.component';
import { MonitorRoute } from './monitor.route';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faEye, faPhone } from '@fortawesome/free-solid-svg-icons';
import { HeaderDataBlockModule } from 'app/shared/components/header-data-block/header-data-block.module';
import { AlarmDataBlockModule } from 'app/shared/components/alarm-data-block/alarm-data-block.module';
import {TabViewModule} from "primeng/tabview";
@NgModule({
    imports: [
        VcmSharedModule,
        AccordionModule,
        ButtonModule,
        DragDropModule,
        DrawerModule,
        FieldsetModule,
        FlowsheetDataBlockModule,
        HardwareDataBlockModule,
        MonitorRoute,
        NumericDataBlockModule,
        PanelModule,
        PulseControllerModule,
        ScrollPanelModule,
        SelectButtonModule,
        RadioButtonModule,
        SliderModule,
        FontAwesomeModule,
        DialogModule,
        StatusBadgeModule,
        TabularDataBlockModule,
        VentilatorRecommenderInputDataBlockModule,
        HeaderDataBlockModule,
        AlarmDataBlockModule,
        TabViewModule
    ],
  declarations: [
    MonitorAssignmentsComponent,
    MonitorDrawerComponent,
    MonitorViewComponent,
    VentilatorParameterDataBlockComponent,
    VentilatorRecommenderDrawerComponent
  ]
})
export class MonitorModule {
  constructor() {
    library.add(faEye, faPhone);
  }
}
