/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

export interface ITeamMember {
  name?: string;
  phone?: string;
  gender?: IGender;
  status?: IMemberStatus;
}

export class TeamMemberModel implements ITeamMember {
  constructor(public name?: string, public phone?: string, public gender?: IGender, public status?: IMemberStatus) {}
}
export const enum IGender {
  MALE = 'MALE',
  FEMALE = 'FEMALE',
  OTHER = 'OTHER'
}

export const enum IMemberStatus {
  PRESENT = 'Present',
  AWAY = 'Away',
  ABSENT = 'Absent'
}
