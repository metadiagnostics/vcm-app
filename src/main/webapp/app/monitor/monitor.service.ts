/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { EventEmitter, Injectable, Output } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Pagination } from 'app/shared/util/request-util';
import { IMonitorApplyRecomendation, IMonitorAssignment } from 'app/shared/model/ext/monitor-assignment.models';
import { IVentilatorConfigurationChangeRequest } from 'app/shared/model/ext/ventilator-configuration.models';
import { VentilatorModeType } from 'app/shared/model/enumerations/ventilator-mode-type.model';
import { VentilatorRecommenderType } from 'app/shared/model/enumerations/ventilator-recommender-type.model';
import { VentilatorRecommenderControlType } from 'app/shared/model/enumerations/ventilator-recommender-control.model';
import { IHWModel } from 'app/monitor/hardware.model';

type MonitorAssignmentArrayResponseType = HttpResponse<IMonitorAssignment[]>;
type MonitorAssignmentResponseType = HttpResponse<IMonitorAssignment>;
type MonitorApplyRecomendationType = HttpResponse<IMonitorApplyRecomendation>;
type BlobType = HttpResponse<Blob>;
type MonitorVentilatorRecommenderType = VentilatorRecommenderType;
type MonitorVentilatorRecommenderControlType = VentilatorRecommenderControlType;

@Injectable({ providedIn: 'root' })
export class MonitorService {
  public resourceUrl = SERVER_API_URL + 'api/ext/monitor';

  @Output() ventilatorRecommenderType: EventEmitter<MonitorVentilatorRecommenderType> = new EventEmitter();
  @Output() ventilatorRecommenderControl: EventEmitter<MonitorVentilatorRecommenderControlType> = new EventEmitter();

  constructor(protected http: HttpClient) {}
  queryAssignments(req?: any): Observable<MonitorAssignmentArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMonitorAssignment[]>(`${this.resourceUrl}/assignments`, { params: options, observe: 'response' });
  }

  findAssignment(id: number): Observable<MonitorAssignmentResponseType> {
    return this.http.get<IMonitorAssignment>(`${this.resourceUrl}/assignments/${id}`, { observe: 'response' });
  }

  changeVentilatorRecommendationMode(id: number, mode: VentilatorModeType): Observable<{}> {
    return this.http.put<{}>(`${this.resourceUrl}/assignments/${id}/ventilator-recommendation/${mode}`, { observe: 'response' });
  }

  changeVentilatorRecommenderType(id: number, type: VentilatorRecommenderType): Observable<{}> {
    this.ventilatorRecommenderType.emit(type);
    return this.http.put<{}>(`${this.resourceUrl}/assignments/${id}/ventilator-recommender-type/${type}`, { observe: 'response' });
  }

  changeVentilatorRecommenderMode(id: number, control: MonitorVentilatorRecommenderControlType): Observable<{}> {
    this.ventilatorRecommenderControl.emit(control);
    return this.http.put<{}>(`${this.resourceUrl}/assignments/${id}/ventilator-recommender-control/${control}`, { observe: 'response' });
  }

  applyVentilatorChanges(id: number, request: IVentilatorConfigurationChangeRequest): Observable<MonitorApplyRecomendationType> {
    return this.http.post<IMonitorApplyRecomendation>(`${this.resourceUrl}/assignments/${id}/ventilator/change`, request, {
      observe: 'response'
    });
  }

  ackFio2Value(id: number): Observable<{}> {
    return this.http.post<{}>(`${this.resourceUrl}/assignments/${id}/ackFio2Value`, { observe: 'response' });
  }

  refreshListeners(id: number): Observable<{}> {
    return this.http.post<{}>(`${this.resourceUrl}/assignments/${id}/reconnectListeners`, '', { observe: 'response' });
  }

  getVentilatorsLogsAsCsv(id: number, from: string, to: string): Observable<BlobType> {
    const options = createRequestOption({
      from,
      to
    });
    return this.http.get(`${this.resourceUrl}/assignments/${id}/ventilator/csv`, {
      params: options,
      responseType: 'blob',
      observe: 'response'
    });
  }
  getMonitorIds(hwId: string, req?: Pagination): Observable<HttpResponse<string[]>> {
    const options = createRequestOption(req);
    return this.http.get<string[]>(this.resourceUrl + '/monitors/' + hwId, { params: options, observe: 'response' });
  }
}
