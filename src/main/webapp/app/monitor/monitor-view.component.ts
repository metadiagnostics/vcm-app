/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { AfterViewInit, Component, HostListener, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subscription, timer } from 'rxjs';

import { DataBlockComponent } from 'app/shared/components/data-block/data-block.component';
import {
  HardwareDataBlockComponent,
  HardwareDataBlockComponentType
} from 'app/shared/components/hardware-data-block/hardware-data-block.component';
import {
  VentilatorRecommenderInputDataBlockComponent,
  VRIDBType
} from 'app/shared/components/ventilator-recommender-input-data-block/ventilator-recommender-input-data-block.component';
import { Column, TabularDataBlockComponent } from 'app/shared/components/tabular-data-block/tabular-data-block.component';
import { BadgeStatus } from 'app/shared/components/status-badge/status-badge.component';
import { IHardwareBloodGasData, IHardwareBloodGasDatas } from 'app/shared/model/ext/hardware-blood-gas-data.model';
import {
  IEMREvent,
  IHardwareDemographicsEvent,
  IHardwareDemographicsEvents,
  IHardwareVentilatorEvent,
  IHardwareVentilatorEvents,
  IHardwareVitalsEvent,
  IHardwareVitalsEvents
} from 'app/shared/model/ext/hardware-data.model';
import { IHardwareStatus } from 'app/shared/model/ext/hardware-status.model';
import { HardwareStatusType } from 'app/shared/model/enumerations/ext/hardware-status-type.model';
import {
  IMonitorApplyRecomendation,
  IMonitorAssignment,
  IMonitorDevice,
  MonitorAssignmentUtils
} from 'app/shared/model/ext/monitor-assignment.models';
import { VentilatorRecommenderType } from 'app/shared/model/enumerations/ventilator-recommender-type.model';
import { VentilatorRecommenderControlType } from 'app/shared/model/enumerations/ventilator-recommender-control.model';
import {
  IVentilatorRecommendationRequest,
  IVentilatorRecommendationResponse,
  VentilatorRecommendationRequest
} from 'app/shared/model/ext/ventilator-recommender.models';
import { MonitorService } from './monitor.service';
import {
  MonitorHardwareConnection,
  MonitorHardwareConnectionError,
  MonitorHardwareService
} from '../shared/services/monitor/monitor-hardware.service';
import { VentilatorParameterDataBlockComponent } from './ventilator-parameter-data-block/ventilator-parameter-data-block.component';
import { VentilatorRecommenderService } from './ventilator-recommender.service';
import { HardwareMessageSeverity, IHardwareMessage } from 'app/shared/model/ext/hardware-message.model';
import { JhiAlert, JhiAlertService } from 'ng-jhipster';
import {
  IVentilatorConfigurationChangeRequest,
  VentilatorConfigurationChangeRequest
} from 'app/shared/model/ext/ventilator-configuration.models';
import { IScalarValue, ScalarValue } from 'app/shared/model/ext/scalar-value.model';
import { DrawerService } from 'app/drawer/drawer.service';
import { DeviceCapabilityType } from 'app/shared/model/enumerations/device-capability-type.model';
import {
  ColumnsTemplate,
  FlowsheetDataBlockComponent,
  Row
} from 'app/shared/components/flowsheet-data-block/flowsheet-data-block.component';
import * as moment from 'moment';
import { now } from 'moment';
import { VentilatorModeType } from 'app/shared/model/enumerations/ventilator-mode-type.model';
import { MonitorDataConnection, MonitorDataService } from 'app/shared/services/monitor/monitor-data.service';
import { DeviceType } from 'app/shared/model/enumerations/device-type.model';
import { HttpResponse } from '@angular/common/http';
import { MenuService } from 'app/core/menu/menu.service';
import { faPhone } from '@fortawesome/free-solid-svg-icons/faPhone';
import { faVenus } from '@fortawesome/free-solid-svg-icons/faVenus';
import { faMars } from '@fortawesome/free-solid-svg-icons/faMars';
import { faNeuter } from '@fortawesome/free-solid-svg-icons/faNeuter';

import { IMemberStatus, ITeamMember } from 'app/monitor/team-member.model';
import { CareTeamService } from 'app/core/careteam/careteam.service';
import { HeaderDataBlockComponent } from 'app/shared/components/header-data-block/header-data-block.component';
import { EmrService } from 'app/core/emr/emr.service';
import { CdspSimulationsService } from 'app/shared/services/cdsp/cdsp-simulations.service';
import { AlarmModel, IAlarmModel, IAlarmWarningModel } from './alarm.model';
import { AlarmService } from 'app/core/alarm/alarm.service';
import { IPumpModel, PumpModel } from 'app/shared/model/pump.model';
import { PumpService } from 'app/core/pump/pump.service';
import { InfusionPumpComponent } from 'app/shared/components/infusion-pump/infusion-pump.component';
import { ILabelModel } from 'app/monitor/label.model';
import { faPlug } from '@fortawesome/free-solid-svg-icons/faPlug';
import { faBatteryFull } from '@fortawesome/free-solid-svg-icons/faBatteryFull';
import { faBatteryThreeQuarters } from '@fortawesome/free-solid-svg-icons/faBatteryThreeQuarters';
import { faBatteryHalf } from '@fortawesome/free-solid-svg-icons/faBatteryHalf';
import { faBatteryQuarter } from '@fortawesome/free-solid-svg-icons/faBatteryQuarter';
import { faBatteryEmpty } from '@fortawesome/free-solid-svg-icons/faBatteryEmpty';
import { faBolt } from '@fortawesome/free-solid-svg-icons/faBolt';
import { PhysiologicService } from 'app/core/physiologic/physiologic.service';
import { VentilatorService } from 'app/core/ventilator/ventilator.service';
import { AlarmDataBlockComponent } from 'app/shared/components/alarm-data-block/alarm-data-block.component';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons/faCheckCircle';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons/faTimesCircle';
import { faVideo } from '@fortawesome/free-solid-svg-icons/faVideo';
import { faUsers } from '@fortawesome/free-solid-svg-icons/faUsers';
import { IHWModel, IHWModels } from 'app/monitor/hardware.model';
import { IEnumValues, IVentModel, IVentModels } from 'app/monitor/ventilator.model';
import { IVentilatorMode, VentilatorMode } from 'app/monitor/ventilator.mode';
import { IIdMap, IMetricMap, IUnitMap } from 'app/monitor/metric.model';
import {
  BatteryInfo,
  IBattery,
  IDeviceCtx,
  IDeviceFrame,
  IDeviceInfo,
  IDeviceMap, IFrame,
  IMeasureValue,
  IMeasureValueMap,
  IMeasureWaveForm,
  IMeasureWaveFormMap,
  IValue,
  IValueMap
} from 'app/monitor/frame.model';
import widget = jqwidgets.widget;
import { IMRModel } from 'app/monitor/emr.model';
import { Dialog } from 'primeng';
import { DomSanitizer, SafeHtml, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

abstract class BlockDefinition {
  constructor(
    public order: number,
    public name: string,
    public alarmMax: number,
    public alarmMin: number,
    public alarmMinCodeId: string,
    public alarmMaxCodeId: string,
    public hwId: string,
    public selectors: string[],
    public dataKeys: string[],
    public ddScope: string,
    public expanded: boolean = false,
    public physiologicMonitors: boolean = false, // Physiologic section where it's located
    public ventFlow: boolean = false,
    public ventSettings: boolean = false, // control block for ventilator
    public ventMonitors: boolean = false, // Ventilator section
    public reduced: boolean = false, // Minimal view of same Block.
    // data can be an array of old events
    public data: any,
    public separator = '',
    public chartMaxDataLength = 15,
    public yaxisMin=-999,
    public yaxisMax=-999,
  ) {}

  abstract getTemplateName(): string;

  public getDataKey(): string {
    if (this.dataKeys.length > 0) {
      return this.dataKeys[0];
    }
    return '';
  }
}

class HeaderDefinition {
  constructor(public order: number, public name: string, public type: string, public selectors: string[]) {}
}

class AlarmDefinition {
  constructor(public id: string, public type: string, public selectors: string[]) {}
}

class HardwareBlockDefinition extends BlockDefinition {
  constructor(
    public order: number,
    public name: string,
    public alarmMax: number,
    public alarmMin: number,
    public alarmMinCodeId: string,
    public alarmMaxCodeId: string,
    public hwId: string,
    public dataKeys: string[],
    public selectors: string[],
    public ddScope: string,
    public chartMaxDataLength: number = 15,
    public data: any = [],
    public expanded: boolean = false,
    public physiologicMonitors: boolean = false,
    public ventFlow: boolean = false,
    public ventSettings: boolean = false,
    public ventMonitors: boolean = false, // Ventilator section
    public reduced: boolean = false, // Minimal view of same Block.
    public type: HardwareDataBlockComponentType = 'Point',
    public xlabel = '',
    public ylabel = '',
    public separator = '',
    public yaxisMin: number = -999,
    public yaxisMax: number = -999,
  ) {
    super(
      order,
      name,
      alarmMax,
      alarmMin,
      alarmMaxCodeId,
      alarmMinCodeId,
      hwId,
      selectors,
      dataKeys,
      ddScope,
      expanded,
      physiologicMonitors,
      ventFlow,
      ventSettings,
      ventMonitors,
      reduced,
      data,
      separator,
      chartMaxDataLength,
      yaxisMin,
      yaxisMax
    );
  }

  getTemplateName(): string {
    return 'jhiHwDataBlockTemplate';
  }
}
// eslint-disable-next-line @typescript-eslint/no-unused-vars
class TabularBlockDefinition extends BlockDefinition {
  constructor(
    public order: number,
    public name: string,
    public selectors: string[],
    public ddScope: string,
    public columns: Column[],
    public expanded: boolean = false
  ) {
    super(order, name, 0, 0, '', '', '', selectors, [], ddScope, expanded, false, false, false, false, false, null);
  }

  getTemplateName(): string {
    return 'jhiTabluarDataBlockTemplate';
  }
}

class FlowsheetBlockDefinition extends BlockDefinition {
  constructor(
    public order: number,
    public name: string,
    public header: string = '',
    public selectors: string[],
    public ddScope: string,
    public columnsTemplate: ColumnsTemplate,
    public rows: Row[],
    public expanded: boolean = false,
    public interval: number
  ) {
    super(order, name, 0, 0, '', '', '', selectors, [], ddScope, expanded, false, false, false, false, false, null);
  }

  getTemplateName(): string {
    return 'jhiFlowsheetDataBlockTemplate';
  }
}

class VentilatorRecommenderBlockDefinition {
  constructor(
    public name: string,
    public type: VRIDBType,
    public dataKey: string,
    public ventilatorRequestDataKey: string,
    public selectors: string[],
    public lowerBound: number,
    public upperBound: number
  ) {}
}

class HardwareBlockDragInfo {
  constructor(public block: BlockDefinition, public source: BlockDefinition[]) {}
}

class HardwareStatusInfo {
  constructor(
    public hardwareId: string,
    public label: string,
    public order: number,
    public badgeStatus: BadgeStatus = BadgeStatus.WARNING,
    public hardwareStatus: HardwareStatusType = HardwareStatusType.UNKNOWN,
    public message: string = ''
  ) {}
}

class VentilatorInputDefinition {
  constructor(
    public order: number,
    public dataKey: string,
    public name: string,
    public ventilatorResponseDataKey: string,
    public ventilatorChangeRequestDataKey: string,
    public value: IScalarValue,
    public rvalue: IScalarValue,
    public step: number,
    public lowerBound: number,
    public upperBound: number,
    public snapshotScalarValue = new ScalarValue(-1, ''),
    public selectors: string[] = [],
    public codeID: string,
    public alarmMinCodeID: string,
    public alarmMaxCodeID: string,
    public hwId: string,
    public enumValues?: IEnumValues,
    public min: number = -1,
    public max: number = 999999,
    public ventSettings: boolean = false,
    public minAlarm: number = 0,
    public maxAlarm: number = 0,
    public type: VRIDBType = VRIDBType.I_SCALAR_VALUE
  ) {}
}

@Component({
  selector: 'jhi-monitor',
  templateUrl: './monitor-view.component.html',
  styleUrls: ['./monitor-view.component.scss']
})
export class MonitorViewComponent implements AfterViewInit, OnInit, OnDestroy {
  fio2Name = 'FIO<sub>2</sub> (%)';
  assignment: IMonitorAssignment = { assignmentId: -1 };
  private NEUROWAVE = 'Neurowave';
  private MAX_PHYSIOLOGIC_DEVICE_DATA_BLOCKS = 3;
  private MAX_VENTILATOR_DEVICE_DATA_BLOCKS = 1; // is 1 due this ticket https://saperisys.atlassian.net/browse/TM-640
  private wsSubscriptions: Subscription[] = [];
  private simSubscription: Subscription;
  private ventChangeSubscription: Subscription[] = []; // From VentilatorRecommenderInputDataBlockComponent

  hardwareStatuses: Map<string, HardwareStatusInfo> = new Map();

  devicesByCapability: Map<DeviceCapabilityType, IMonitorDevice> = new Map();

  @ViewChildren(FlowsheetDataBlockComponent)
  flowsheetDataBlocks!: QueryList<FlowsheetDataBlockComponent>;

  @ViewChildren(HardwareDataBlockComponent)
  hardwareDataBlocks!: QueryList<HardwareDataBlockComponent>;
  // Enable this on TM-574
  // deviceDataFlowsheet: FlowsheetBlockDefinition;

  @ViewChildren(VentilatorRecommenderInputDataBlockComponent)
  ventilatorRecommendationInputDataBlocks!: QueryList<VentilatorRecommenderInputDataBlockComponent>;

  @ViewChildren(TabularDataBlockComponent)
  tabularDataBlocks!: QueryList<TabularDataBlockComponent>;

  @ViewChildren(VentilatorParameterDataBlockComponent)
  ventilatorParameterDataBlocks!: QueryList<VentilatorParameterDataBlockComponent>;

  @ViewChildren(HeaderDataBlockComponent)
  headerBlock!: QueryList<HeaderDataBlockComponent>;

  @ViewChildren(AlarmDataBlockComponent)
  alarmBlock!: QueryList<AlarmDataBlockComponent>;

  @ViewChildren(InfusionPumpComponent)
  infusionBlocks!: QueryList<InfusionPumpComponent>;

  private monitorDataConnection?: MonitorDataConnection;
  private hwConnection?: MonitorHardwareConnection;

  ssHwDemographicsDataSelector = 'hwDemographicsDataSelector';
  ssHwVitalsDataSelector = 'hwVitalsDataSelector';
  ssHwVentilatorDataSelector = 'hwVentilatorDataSelector';
  ssHwBloodGasDataSelector = 'hwBloodGasDataSelector';
  ssRecommenderInputSelector = 'recommenderInputSelector';
  ssVentilatorInputDataSelector = 'ventilatorInputDataSelector';
  ssHwEMRDataSelector = 'hwEMRDataSelector';
  ssAlarmSelector = 'alarmSelector';

  //Configuration definitios (physiologic)
  monitorDefinitions: IHWModel[] = [];

  // Menu items
  menuItems: string[] = [];
  // icons
  phoneIcon = faPhone;
  femaleIcon = faVenus;
  maleIcon = faMars;
  otherIcon = faNeuter;
  check = faCheckCircle;
  cancel = faTimesCircle;
  video = faVideo;
  videoGroup = faUsers;
  // Team Items
  localCareTeamItems: ITeamMember[] = [];
  remoteCareTeamItems: ITeamMember[] = [];
  // Alarms values
  alarmItems: IAlarmModel[] = [];
  // Selectors that shouldn't be updated
  frozenSelectors: string[] = [];
  // AccuPumps
  accuPumps: IPumpModel[] = [];

  // header Blocks
  headerBlocks: HeaderDefinition[];

  // Alarm Block
  alarmDef: AlarmDefinition;

  // Patient Blocks
  ddPatientDataScope = 'patientData';
  patientBlocks: BlockDefinition[];
  patientChartBlocks: BlockDefinition[];
  patientUnselectedBlocks: BlockDefinition[];

  // Ventilator Blocks
  ddVentilatorDataScope = 'ventilatorData';
  ventilatorBlocks: VentilatorInputDefinition[];
  ventilatorBlocksNotVisible: VentilatorInputDefinition[];
  ventilatorChart1Blocks: HardwareBlockDefinition[];
  ventilatorChart2Blocks: HardwareBlockDefinition[];
  ventilatorChartBlocks: BlockDefinition[];
  ventilatorUnselectedBlocks: BlockDefinition[];
  ventilatorModes: VentilatorMode[];
  showVentModes = false;
  showLocalTeamVideo = false;
  showClinicalSummary = false;
  showDevices = true; // By default are the components we should present
  showJitsiTeamConf = false;
  showBedsideCollaborator = false;
  localTeamId = '';
  localTeamVideoURL: SafeResourceUrl;
  clinicalSummaryURL: SafeResourceUrl;
  bedsideURL: SafeResourceUrl;
  jitsiURL: SafeHtml;
  jitsiHtml = '';
  ventMode = '';
  newVentMode = '';

  // Recommender Input Blocks
  recommenderInputCol1Blocks: VentilatorRecommenderBlockDefinition[];

  // Labs Blocks
  ddLabsDataScope = 'labsData';
  labsBlocks: FlowsheetBlockDefinition[];

  // Current Block being dragged
  draggedBlockInfo?: HardwareBlockDragInfo;

  // Current Block being dragged over
  draggedOverBlockInfo?: BlockDefinition;

  ventilatorInputDefinition: VentilatorInputDefinition[];

  ventilatorRecommenderTimeoutId?: number;

  acknowledgeFio2 = false;
  recommendationReason?: string = '';
  lastUpdateDate?: string = '';
  lastUpdateTime?: string = '';
  pressureOrVolumeDef: VentilatorInputDefinition;
  pipFromResponse = new VentilatorParameterDataBlockComponent();
  oldPipValue = 25;
  oldTvValue = 1;
  blockData: any = [];
  datasetMappings: IMetricMap = {};
  unitMappings: IUnitMap = {};
  idMappingSettings: IIdMap = {};
  idMappingMeasures: IIdMap = {};
  reverseIdMappingSettings: IIdMap = {};
  reverseIdMappingMeasures: IIdMap = {};
  mappingsLoaded = 0; // amount of maps loaded in the system, used to know when to load certain widgets
  idPumpHwID: IIdMap = {}; // pumpId -> HwId

  public batteryVentPercentage = 100;
  public ventConnectStatus = faPlug;
  public ventConnectStatusBolt = faBolt;
  public ventConnectStatusBoltVisible = false;
  public ventCds = 'No CDS';
  public ventLocusOfControl = 'Remote';

  public batteryCardioPercentage = 100;
  public cardioConnectStatus = faPlug;
  public cardioConnectStatusBolt = faBolt;
  public cardioConnectStatusBoltVisible = false;

  public cardioCds = 'CDS';
  public cardioLocusOfControl = 'Local';
  private resizeTimer: ReturnType<typeof setTimeout>;

  constructor(
    protected monitorService: MonitorService,
    protected monitorDataService: MonitorDataService,
    protected monitorHardwareService: MonitorHardwareService,
    protected drawerService: DrawerService,
    protected activatedRoute: ActivatedRoute,
    protected alertService: JhiAlertService,
    protected translateService: TranslateService,
    protected menuService: MenuService,
    protected careTeamService: CareTeamService,
    protected alarmService: AlarmService,
    protected emrService: EmrService,
    protected pumpService: PumpService,
    protected physiologicService: PhysiologicService,
    protected ventilatorService: VentilatorService,
    protected simService: CdspSimulationsService,
    protected sanitizer: DomSanitizer
  ) {
    this.localTeamVideoURL = this.sanitizer.bypassSecurityTrustResourceUrl('');
    this.clinicalSummaryURL = this.sanitizer.bypassSecurityTrustResourceUrl('');
    this.bedsideURL = this.sanitizer.bypassSecurityTrustResourceUrl('');
    this.jitsiURL = this.sanitizer.bypassSecurityTrustHtml('');
    this.resizeTimer = setTimeout(() => {}, 1);
    this.headerBlocks = [
      new HeaderDefinition(0, 'demographics', 'demographics', [this.ssHwEMRDataSelector]),
      new HeaderDefinition(1, 'primary-conditions', 'primary-conditions', [this.ssHwEMRDataSelector]),
      new HeaderDefinition(2, 'mechanism-of-injury', 'mechanism-of-injury', [this.ssHwEMRDataSelector]),
      new HeaderDefinition(3, 'location', 'location', [this.ssHwEMRDataSelector]),
      new HeaderDefinition(4, 'allergies', 'allergies', [this.ssHwEMRDataSelector]),
      new HeaderDefinition(5, 'locus-of-control', 'locus-of-control', [this.ssHwEMRDataSelector])
    ];
    this.alarmDef = new AlarmDefinition('1', 'alarmWarning', [this.ssAlarmSelector]);
    // device data section
    // Initializing arrays on constructor
    this.patientBlocks = [];
    this.patientChartBlocks = [];
    this.patientUnselectedBlocks = [];
    this.ventilatorChartBlocks = [];
    this.ventilatorUnselectedBlocks = [];
    this.ventilatorModes = [];

    // Enable this on TM-574
    /* this.deviceDataFlowsheet = new FlowsheetBlockDefinition(
      5,
      'Flowsheet table',
      ' ',
      [this.ssHwVitalsDataSelector, this.ssHwVentilatorDataSelector],
      this.ddPatientDataScope, // this.ddVentilatorDataScope etc02
      // Defining function to present a Timestamp with format HH:mm:ss depending on the value source.
      // The source can be relative or absolute
      new ColumnsTemplate(value => {
        // Initial value to pre-fill the grid
        if (value.dataTimestamp === -1) {
          return '';
        }
        if (value.dataTimestamp < 1000000000) {
          // we assume that the timestamp is relative (i.e. simulation time starting from 0)
          // and not a date.
          const formattedDate = moment()
            .startOf('day')
            .add(moment.duration(value.dataTimestamp, 'milliseconds'))
            .format('HH:mm:ss');
          // eslint-disable-next-line no-console
          // console.log(value.dataTimestamp + ' ' + formattedDate);
          return formattedDate;
        } else {
          // we assume the timestamp to be a date
          return moment(value.dataTimestamp).format('HH:mm:ss');
        }
      }),
      [
        new Row('BP (mmHg)', ['sbp', 'dbp'], '/'),
        new Row('HR (bpm)', ['hr']),
        new Row('O<sub>2</sub>Sat (%)', ['sat']),
        new Row('EtCO<sub>2</sub> (mmHg)', ['etco2']),
        new Row('PIP (cmH2O)', ['pip']),
        new Row('PEEP (cmH2O)', ['peep']),
        new Row('Rate (bpm)', ['rate']),
        new Row('FIO<sub>2</sub> (%)', ['fio2']),
        new Row('TV (CC/Kg)', ['tv', 'weight'], '-', value => {
          try {
            const values = value.split('-');
            const tv = values[0];
            const weight = values[1];
            if (weight === undefined || weight === 0) return '-';
            return ((parseFloat(tv) * 1000.0) / (weight / 2.2)).toFixed(2);
          } catch {
            return '-';
          }
        })
      ],
      true,
      5000, // the interval should be 5 seconds as recommended
      this.flowSheetEnablerComp
    ); */
    // Vent settings
    this.ventilatorBlocks = [];
    this.ventilatorBlocksNotVisible = [];
    this.ventilatorChart1Blocks = [
      new HardwareBlockDefinition(
        8,
        'Pressure Volume Loop',
        0,
        0,
        this.reverseIdMappingSettings['alarmMinPvLoopSetting'],
        this.reverseIdMappingSettings['alarmMaxPvLoopSetting'],
        this.devicesByCapability.get(DeviceCapabilityType.VENTILATOR_CONTROL)?.hardwareId as string,
        ['pvLoop'],
        [this.ssHwVentilatorDataSelector],
        this.ddVentilatorDataScope,
        15,
        null,
        true,
        false,
        true, // this makes the vent flows be rendered on HW component
        false,
        true,
        false,
        'Plot',
        'Pressure (cmH\u2082O)',
        'Volume (liters)'
      )
    ];
    this.ventilatorChart2Blocks = [
      new HardwareBlockDefinition(
        9,
        'Flow Volume Loop',
        0,
        0,
        this.reverseIdMappingSettings['alarmMinFvLoopSetting'],
        this.reverseIdMappingSettings['alarmMaxFvLoopSetting'],
        this.devicesByCapability.get(DeviceCapabilityType.VENTILATOR_CONTROL)?.hardwareId as string,
        ['fvLoop'],
        [this.ssHwVentilatorDataSelector],
        this.ddVentilatorDataScope,
        15,
        null,
        true,
        false,
        true, // this makes the vent flows be rendered on HW component
        false,
        false,
        false,
        'Plot',
        'Volume (liters)',
        'Flow (liters/min)'
      )
    ];
    this.recommenderInputCol1Blocks = [
      new VentilatorRecommenderBlockDefinition(
        'PIP (cmH<sub>2</sub>O)',
        VRIDBType.I_SCALAR_VALUE,
        'pip',
        'pip',
        [this.ssRecommenderInputSelector, this.ssHwVentilatorDataSelector],
        30,
        95
      ),
      new VentilatorRecommenderBlockDefinition(
        'PEEP (cmH<sub>2</sub>O)',
        VRIDBType.I_SCALAR_VALUE,
        'peep',
        'peep',
        [this.ssRecommenderInputSelector, this.ssHwVentilatorDataSelector],
        30,
        95
      ),
      new VentilatorRecommenderBlockDefinition(
        'Rate (bpm)',
        VRIDBType.I_SCALAR_VALUE,
        'rate',
        'rate',
        [this.ssRecommenderInputSelector, this.ssHwVentilatorDataSelector],
        30,
        95
      ),
      new VentilatorRecommenderBlockDefinition(
        'FIO<sub>2</sub> (%)',
        VRIDBType.I_SCALAR_VALUE,
        'fio2',
        'fio2',
        [this.ssRecommenderInputSelector, this.ssHwVentilatorDataSelector],
        30,
        95
      ),
      new VentilatorRecommenderBlockDefinition(
        'Delivered TV (L)',
        VRIDBType.I_SCALAR_VALUE,
        'tv',
        'tidalVolume',
        [this.ssRecommenderInputSelector, this.ssHwVitalsDataSelector],
        30,
        95
      ),
      new VentilatorRecommenderBlockDefinition(
        'EtCO<sub>2</sub> (mmHg)',
        VRIDBType.I_SCALAR_VALUE,
        'etco2',
        'etco2',
        [this.ssRecommenderInputSelector, this.ssHwVitalsDataSelector],
        30,
        95
      ),
      new VentilatorRecommenderBlockDefinition(
        'O<sub>2</sub>Sat (%)',
        VRIDBType.I_SCALAR_VALUE,
        'sat',
        'o2sat',
        [this.ssRecommenderInputSelector, this.ssHwVitalsDataSelector],
        30,
        95
      ),
      new VentilatorRecommenderBlockDefinition(
        'Gender',
        VRIDBType.STRING,
        'gender',
        'gender',
        [this.ssRecommenderInputSelector, this.ssHwDemographicsDataSelector],
        30,
        95
      ),
      new VentilatorRecommenderBlockDefinition(
        'Height (in)',
        VRIDBType.I_SCALAR_VALUE,
        'height',
        'height',
        [this.ssRecommenderInputSelector, this.ssHwDemographicsDataSelector],
        30,
        95
      ),
      new VentilatorRecommenderBlockDefinition(
        'pH (blood gas)',
        VRIDBType.I_SCALAR_VALUE,
        'ph',
        'ph',
        [this.ssRecommenderInputSelector, this.ssHwBloodGasDataSelector],
        30,
        95
      )
    ];

    this.pressureOrVolumeDef = new VentilatorInputDefinition(
      0,
      'pipsetting',
      'PIP (cmH<sub>2</sub>O)',
      'pipsetting',
      'pipsetting',
      new ScalarValue(25, 'cmH2O'),
      new ScalarValue(-1, 'cmH2O'),
      1,
      29,
      40,
      new ScalarValue(-1, 'cmH2O'),
      [this.ssVentilatorInputDataSelector],
      this.reverseIdMappingSettings['pipsetting'],
      this.reverseIdMappingSettings['alarmMinPipSetting'],
      this.reverseIdMappingSettings['alarmMaxPipSetting'],
      this.devicesByCapability.get(DeviceCapabilityType.VENTILATOR_CONTROL)?.hardwareId as string,
      undefined,
      0,
      60
    );
    this.ventilatorInputDefinition = [
      this.pressureOrVolumeDef,
      new VentilatorInputDefinition(
        1,
        'peepsetting',
        'PEEP (cmH<sub>2</sub>O)',
        'peepsetting',
        'peepsetting',
        new ScalarValue(5, 'cmH2O'),
        new ScalarValue(-1, 'cmH2O'),
        1,
        9,
        15,
        new ScalarValue(-1, 'cmH2O'),
        [this.ssVentilatorInputDataSelector],
        this.reverseIdMappingSettings['peepsetting'],
        this.reverseIdMappingSettings['alarmMinPeepSetting'],
        this.reverseIdMappingSettings['alarmMaxPeepSetting'],
        this.devicesByCapability.get(DeviceCapabilityType.VENTILATOR_CONTROL)?.hardwareId as string,
        undefined,
        0,
        30
      ),
      new VentilatorInputDefinition(
        2,
        'ratesetting',
        'Rate (bpm)',
        'ratesetting',
        'ratesetting',
        new ScalarValue(12, '1/min'),
        new ScalarValue(-1, '1/min'),
        1,
        25,
        35,
        new ScalarValue(-1, 'cmH2O'),
        [this.ssVentilatorInputDataSelector],
        this.reverseIdMappingSettings['ratesetting'],
        this.reverseIdMappingSettings['alarmMinRateSetting'],
        this.reverseIdMappingSettings['alarmMaxRateSetting'],
        this.devicesByCapability.get(DeviceCapabilityType.VENTILATOR_CONTROL)?.hardwareId as string,
        undefined,
        0,
        60
      ),
      new VentilatorInputDefinition(
        3,
        'fio2setting',
        this.fio2Name,
        'fio2setting',
        'fio2setting',
        new ScalarValue(30, '%'),
        new ScalarValue(-1, '%'),
        1,
        25,
        35,
        new ScalarValue(-1, 'cmH2O'),
        [this.ssVentilatorInputDataSelector],
        this.reverseIdMappingSettings['fio2setting'],
        this.reverseIdMappingSettings['alarmMinFio2Setting'],
        this.reverseIdMappingSettings['alarmMaxFio2Setting'],
        this.devicesByCapability.get(DeviceCapabilityType.VENTILATOR_CONTROL)?.hardwareId as string,
        undefined,
        21,
        100
      )
    ];
    // default value to set the PIP, this value will be overwritten when the recommender is called
    this.pipFromResponse.ventilatorResponseDataKey = 'pip';
    this.pipFromResponse.ventilatorChangeRequestDataKey = 'pip';
    this.pipFromResponse.rvalue = new ScalarValue(32, 'cmH2O');
    this.pipFromResponse.value = new ScalarValue(32, 'cmH2O');
    this.labsBlocks = [];
    this.simSubscription = this.simService.onSimChange().subscribe(simChange => {
      if (simChange === this.simService.SIM_STARTED) {
        this.subscribeToEMRItems();
      }
    });
  }

  cloneVentDef(o: VentilatorInputDefinition): VentilatorInputDefinition {
    return Object.assign(o);
  }

  cloneHWDef(o: HardwareBlockDefinition): HardwareBlockDefinition {
    // Disabling easy easy way to do this but to avoid lint complain about it, I change it to the basic implementation
    // tslint:disable-next-line:typedef
    // function F() {}
    // F.prototype = o;
    return new HardwareBlockDefinition(
      o.order,
      o.name,
      o.alarmMax,
      o.alarmMin,
      o.alarmMinCodeId,
      o.alarmMaxCodeId,
      o.hwId,
      o.dataKeys,
      o.selectors,
      o.ddScope,
      o.chartMaxDataLength,
      o.data,
      o.expanded,
      o.physiologicMonitors,
      o.ventFlow,
      o.ventSettings,
      o.ventMonitors,
      o.reduced,
      o.type,
      o.xlabel,
      o.ylabel,
      o.separator,
      o.yaxisMin,
      o.yaxisMax
    );
  }

  createVentInputDef(o: IVentModel): VentilatorInputDefinition {
    const sv = new ScalarValue(NaN, o.unit);
    return new VentilatorInputDefinition(
      o.order,
      o.dataKey,
      o.name,
      o.responseDataKey,
      o.requestDataKey,
      sv,
      sv,
      o.step,
      o.alarmMin,
      o.alarmMax,
      sv,
      o.selectors,
      o.codeId,
      o.alarmMinCodeID,
      o.alarmMaxCodeID,
      o.hwId,
      o.enumValues,
      -1,
      999999,
      true,
      o.alarmMin,
      o.alarmMax,
      o.type === 'string' ? VRIDBType.STRING : VRIDBType.I_SCALAR_VALUE
    );
  }

  createHWModelFromHWBlockDef(hbd: BlockDefinition): IHWModel {
    return (hbd as unknown) as IHWModel;
  }

  getBlockData(type: string): any {
    return this.blockData[type];
  }

  getDataKey(datakeys: string[]): string {
    if (datakeys.length > 0) {
      return datakeys[0];
    }
    return '';
  }

  setBlockData(keyType: string, data: any): any {
    if (keyType === '') return;
    if (data !== undefined) {
      if (data.length === 0) {
        this.blockData[keyType] = [];
      } else {
        this.blockData[keyType].push(data);
      }
    }
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ assignment }) => {
      this.assignment = assignment;

      this.devicesByCapability = MonitorAssignmentUtils.getDevicesByCapability(this.assignment);

      this.connectToMonitorHardwareService();

      // load the drawer
      this.drawerService.navigateToPath('monitor/' + this.assignment.assignmentId + '/drawer');
    });

    this.monitorService.ventilatorRecommenderType.subscribe((recommenderType: VentilatorRecommenderType) => {
      this.assignment.ventilatorRecommenderType = recommenderType;
    });

    this.monitorService.ventilatorRecommenderControl.subscribe((recommenderControl: VentilatorRecommenderControlType) => {
      this.assignment.ventilatorControlType = recommenderControl;
      this.changeVentilatorControlInputs();
    });
    // Change this to TV if this is a Volume mode Recommender
    if (VentilatorRecommenderControlType.VOLUME === this.assignment.ventilatorControlType) {
      this.pressureOrVolumeDef = this.changePipToTV(this.pressureOrVolumeDef, 1);
    }
    this.menuService.get().subscribe((res: HttpResponse<string[]>) => (this.menuItems = res.body || []));
    this.careTeamService.getLocalTeamItems().subscribe((res: HttpResponse<ITeamMember[]>) => (this.localCareTeamItems = res.body || []));
    this.careTeamService.getRemoteTeamItems().subscribe((res: HttpResponse<ITeamMember[]>) => (this.remoteCareTeamItems = res.body || []));
    this.subscribeToEMRItems();
    this.initializePhysiologicLabels();
    this.initializeVentilatorLabels();
    this.initializeVentilatorModes();
    this.initializePumps();
  }
  // Obtain initial alarm values
  setVentilatorBattery(
    comp: MonitorViewComponent,
    percentageAttr: string,
    onMainsPower: boolean,
    battery: IBattery,
    connectStatus: string
  ): void {
    // Battery state bits:
    // 0 - Present
    // 1 - Charging
    // 2 - Unknown Battery Life
    const conAttr = connectStatus as 'cardioConnectStatus' | 'ventConnectStatus';
    const perAttr = percentageAttr as 'batteryCardioPercentage' | 'batteryVentPercentage';
    const percentage = battery.powerLevel;
    comp[perAttr] = percentage;
    if (onMainsPower === false) {
      // Not Connected
      comp[conAttr + 'BoltVisible'] = false;
      if (comp[perAttr] > 75) {
        comp[conAttr] = faBatteryFull;
      } else if (comp[perAttr] > 51) {
        comp[conAttr] = faBatteryThreeQuarters;
      } else if (comp[perAttr] > 26) {
        comp[conAttr] = faBatteryHalf;
      } else if (comp[perAttr] > 5) {
        comp[conAttr] = faBatteryQuarter;
      } else {
        comp[connectStatus] = faBatteryEmpty;
        comp[conAttr] = faBatteryEmpty;
      }
    } else {
      if (battery.present) {
        //Charging the battery
        comp[conAttr] = faBatteryEmpty;
        comp[conAttr + 'BoltVisible'] = true;
      } else {
        // just connected to the power.
        comp[conAttr] = faPlug;
      }
    }
  }


  setLabels(
    labelModel: ILabelModel,
    comp: MonitorViewComponent,
    cds: string,
    locusOfControl: string,
    batteryPercentage: string,
    connectStatus: string
  ): void {
    comp[cds] = labelModel.cds;
    comp[locusOfControl] = labelModel.locusOfControl;
    // this.setVentilatorBattery(comp, batteryPercentage, labelModel.batteryPercentage, labelModel.powerSupplyConnected, connectStatus);
  }

  setVentModes(ventModes: IVentilatorMode[] | null): void {
    ventModes?.forEach(vm => {
      this.ventilatorModes.push(vm);
    });
    if (this.ventilatorModes.length > 0) {
      this.ventMode = this.ventilatorModes[0].shortLabel;
      this.newVentMode = this.ventilatorModes[0].shortLabel;
    }
  }

  // Obtain initial Physiologic Labels and Power status
  initializePhysiologicLabels(): void {
    this.physiologicService.getLabels().subscribe((res: HttpResponse<ILabelModel>) => {
      const labelModel = res.body || ({} as ILabelModel);
      this.setLabels(labelModel, this, 'cardioCds', 'cardioLocusOfControl', 'batteryCardioPercentage', 'cardioConnectStatus');
    });
  }

  // Obtain initial Ventilators Labels and Power status
  initializeVentilatorLabels(): void {
    this.ventilatorService.getLabels().subscribe((res: HttpResponse<ILabelModel>) => {
      const labelModel = res.body || ({} as ILabelModel);
      this.setLabels(labelModel, this, 'ventCds', 'ventLocusOfControl', 'batteryVentPercentage', 'ventConnectStatus');
    });
  }

  initializeVentilatorModes(): void {
    const hwId = this.devicesByCapability.get(DeviceCapabilityType.VITALS)?.hardwareId;
    if (hwId !== undefined) {
      this.ventilatorService.getModes(hwId).subscribe((res: HttpResponse<IVentilatorMode[]>) => {
        this.setVentModes(res.body);
      });
    }
  }

  setAlarms(alarms: IAlarmModel[]): void {
    this.patientBlocks.forEach(b => this.setAlarm(alarms, b));
    this.patientUnselectedBlocks.forEach(b => this.setAlarm(alarms, b));
    this.patientChartBlocks.forEach(b => this.setAlarm(alarms, b));
    this.ventilatorBlocks.forEach(b => {
      const alarm = this.getAlarmByName(alarms, b.ventilatorResponseDataKey);
      if ( alarm !==undefined ) {
        b.minAlarm = +alarm.min;
        b.maxAlarm = +alarm.max;
      }
    });
  }

  setAlarm(alarms: IAlarmModel[], block: BlockDefinition): void {
    const alarm = this.getAlarmByName(alarms, block.dataKeys[0]);
    if ( alarm !==undefined ) {
      block.alarmMin = +alarm.min;
      block.alarmMax = +alarm.max;
    }
  }

  subscribeToEMRItems(): void {
    this.emrService.getEmrItems().subscribe((res: HttpResponse<IMRModel>) => {
      // adds current patient being monitored
      Object.values(res.body || {}).forEach(emr => {
        emr.currentPatientId = this.assignment.assignmentId;
      });
      this.updateBlocksValues(res.body || {}, this.ssHwEMRDataSelector);
    });
  }

  subscribeToPhysiologicMonitorItems(hardwareId: string): void {
    this.physiologicService.getMonitorDefinitions(hardwareId).subscribe((res: HttpResponse<IHWModel[]>) => {
      // We should execute this ONLY if WS did not receive settings data,
      this.updatePhysiologicMonitorBoard(res.body || []);
    });
  }

  /** This methods obtain a map from device metric to ui widget from configuration file
   *
   * @param hardwareId
   */
  subscribeToMonitorIds(hardwareId: string): void {
    this.monitorService.getMonitorIds(hardwareId).subscribe((res: HttpResponse<string[]>) => {
      this.updateMonitorIds(res.body || []);
    });
  }

  subscribeToVentilatorRoItems(hardwareId: string): void {
    this.ventilatorService.getVentilatorRoDefinitions(hardwareId).subscribe((res: HttpResponse<IHWModel[]>) => {
      // We should execute this ONLY if WS did not receive settings data,
      this.updateVentilatorRoBoard(res.body || []);
    });
  }

  initializePumps(): void {
    this.pumpService.getPumps().subscribe((res: HttpResponse<IPumpModel>) => {
      this.updatePumps(res.body || {});
    });
  }

  private updatePhysiologicMonitorBoard(hwModels: IHWModel[]): void {
    // check if the WS already set the widgets
    if (this.patientBlocks.length > 0) return;
    this.monitorDefinitions = hwModels;
    let selectedCount = 0;
    hwModels.forEach(hw => {
      const hwBlock = this.cloneHWDef((hw as unknown) as HardwareBlockDefinition);
      hwBlock.data = this.getBlockData(hwBlock.dataKeys[0]);

      if (hw.selected === true && selectedCount < 3) {
        this.patientBlocks.push(hwBlock);
        const hwBlockSelected = this.cloneHWDef(hwBlock);
        hwBlockSelected.physiologicMonitors = true;
        hwBlockSelected.reduced = false;
        this.pushDefIfNotExists(this.patientChartBlocks, hwBlockSelected);
        selectedCount++;
      } else {
        this.pushDefIfNotExists(this.patientUnselectedBlocks, hwBlock);
      }
    });
    this.patientBlocks.sort((a, b) => a.order - b.order);
    this.patientChartBlocks.sort((a, b) => a.order - b.order);
    this.patientUnselectedBlocks.sort((a, b) => a.order - b.order);
  }

  // check for duplicates before push checking by def name
  private pushDefIfNotExists(array: BlockDefinition[], def: BlockDefinition): void {
    if ((array.length > 0 && array.some(b => b.name !== def.name)) || array.length === 0) {
      array.push(def);
    }
  }

  private updateVentilatorRoBoard(hwModels: IHWModel[]): void {
    // check if the WS already set the widgets
    if (this.ventilatorChartBlocks.length > 0) return;
    let selectedCount = 0;
    hwModels.forEach(hw => {
      const hwBlock = this.cloneHWDef((hw as unknown) as HardwareBlockDefinition);
      if (hw.selected === true && selectedCount < 2) { //change this if you want more than 1
        this.ventilatorChartBlocks.push(hwBlock);
        selectedCount++;
      } else {
        this.ventilatorUnselectedBlocks.push(hwBlock);
      }
    });
    this.ventilatorChartBlocks.sort((a, b) => a.order - b.order);
    this.ventilatorUnselectedBlocks.sort((a, b) => a.order - b.order);
  }

  private updateMonitorIds(monitorIds: string[]): void {}

  private updateVentilatorRwBoard(ventModels: IVentModel[]): void {
    // check if the WS already set the widgets
    if (this.ventilatorBlocks.length > 0) return;
    let pos = 0;
    ventModels.forEach(ventSetting => {
      const ventBlock = this.createVentInputDef(ventSetting); // this.cloneHWDef((hw as unknown) as HardwareBlockDefinition));
      if (pos < 4) {
        this.ventilatorBlocks.push(ventBlock);
      } else {
        // Here we add all not visible widgets, normally used when the ventilator mode change (also adding ventMode)
        this.ventilatorBlocksNotVisible.push(ventBlock);
      }
      pos++;
    });
    this.hwConnection?.ventSettingReceived();
  }

  private getAlarmByName(alarms: IAlarmModel[], name: string): IAlarmModel | undefined {
    const alarm = alarms.find(a => {
      return a.name.toLowerCase() === name.toLowerCase();
    });
    return alarm !== undefined ? alarm : undefined;
  }

  ngAfterViewInit(): void {
    const ventSettings = this.ventilatorRecommendationInputDataBlocks.filter(b => b.ventSettings);
    const req = new VentilatorConfigurationChangeRequest();
    const assigId = this.assignment.assignmentId;
    const monitorServ = this.monitorService;
    const subs = ventSettings.map(function(e): Subscription {
      return e.ventChange().subscribe(vc => {
        ventSettings.forEach(b => {
          b.configureRequest(req);
        });
        // apply the changes
        monitorServ.applyVentilatorChanges(assigId, req).subscribe((res: HttpResponse<IMonitorApplyRecomendation>) => {
          // eslint-disable-next-line no-console
          console.log(res.body!.timestamp);
        });
      });
    });
    this.ventChangeSubscription.push(...subs);
  }

  ngOnDestroy(): void {
    // Remove the ventilator loop function if it is set.
    if (this.ventilatorRecommenderTimeoutId) {
      // Cancel any timeout that may exist.
      window.clearTimeout(this.ventilatorRecommenderTimeoutId);
    }

    // reset the drawer
    this.drawerService.resetToDefault();

    // unsubscribe from websockets
    if (this.wsSubscriptions) {
      this.wsSubscriptions.forEach(s => {
        s.unsubscribe();
      });
    }

    if (this.assignment && this.monitorDataConnection) {
      this.monitorDataConnection.disconnectAndDeactivate(this.assignment.assignmentId);
    }

    if (this.hwConnection) {
      this.hwConnection.disconnect();
    }
    this.simSubscription.unsubscribe();
    // unsubscribe from websockets
    if (this.ventChangeSubscription) {
      this.ventChangeSubscription.forEach(s => {
        s.unsubscribe();
      });
    }
    //If there was some alarm sounding it should be muted the patient context ha disappeared
    const audio = this.alarmService.getAudio();
    if (audio !== undefined) {
      audio.pause();
    }
    this.assignment.assignmentId = -1;
  }

  connectToMonitorDataService(): void {
    this.monitorDataService.connect().subscribe({
      next: dataConnection => {
        this.monitorDataConnection = dataConnection;

        this.monitorDataConnection.subscribe(this.assignment.assignmentId);

        this.monitorDataConnection.activateAssignment(this.assignment.assignmentId);

        // Subscription to Ventilator Recommendations.
        this.wsSubscriptions.push(
          this.monitorDataConnection.receiveVentilatorRecommendationResponse().subscribe((data: IVentilatorRecommendationResponse) => {
            this.onVentilatorRecommendationResponse(data);
          })
        );
      }
    });
  }

  connectToMonitorHardwareService(): void {
    this.monitorHardwareService.connect().subscribe({
      next: hwConnection => {
        this.hwConnection = hwConnection;

        const devices: IMonitorDevice[] = MonitorAssignmentUtils.getUniqueDevices(this.assignment);
        if (devices.length > 0) {
          this.hardwareStatuses = this.createDefaultHardwareStatusInfo(devices);
          this.groupDevicesByHarwdwareId(devices).forEach((d, hwId) => {
            this.hwConnection!.subscribeToAll(hwId);
          });
        }

        // Subscription to Demographics Data
        this.wsSubscriptions.push(
          this.hwConnection.receiveDemographicsData().subscribe((data: IHardwareDemographicsEvent) => {
            if (this.devicesByCapability.get(DeviceCapabilityType.DEMOGRAPHICS)?.hardwareId === data.hardwareId) {
              this.updateBlocksValues(data, this.ssHwDemographicsDataSelector);
            }
          })
        );
        // Subscription to Demographics Data Elapsed
        this.wsSubscriptions.push(
          this.hwConnection.receiveDemographicsDataElapsed().subscribe((data: IHardwareDemographicsEvents) => {
            if (this.devicesByCapability.get(DeviceCapabilityType.DEMOGRAPHICS)?.hardwareId === data[0].hardwareId) {
              this.updateBlocksValues(data, this.ssHwDemographicsDataSelector);
            }
          })
        );

        // Subscription to Vitals Data
        this.wsSubscriptions.push(
          this.hwConnection.receiveVitalsData().subscribe((data: IHardwareVitalsEvent) => {
            if (this.devicesByCapability.get(DeviceCapabilityType.VITALS)?.hardwareId === data.hardwareId) {
              this.updateBlocksValues(data, this.ssHwVitalsDataSelector);
            }
          })
        );
        // Subscription to Vitals Data Elapsed
        this.wsSubscriptions.push(
          this.hwConnection.receiveVitalsDataElapsed().subscribe((data: IHardwareVitalsEvents) => {
            if (this.devicesByCapability.get(DeviceCapabilityType.VITALS)?.hardwareId === data[0].hardwareId) {
              this.updateBlocksValues(data, this.ssHwVitalsDataSelector);
            }
          })
        );
        // Subscription to Settings Vitals Data
        this.wsSubscriptions.push(
          this.hwConnection.receiveVitalsDataSettings().subscribe((data: IHWModels) => {
            this.updatePhysiologicMonitorBoard(data || []);
          })
        );

        // Subscription to Ventilator Data
        this.wsSubscriptions.push(
          this.hwConnection.receiveVentilatorData().subscribe((data: IHardwareVentilatorEvent) => {
            if (this.devicesByCapability.get(DeviceCapabilityType.VENTILATOR)?.hardwareId === data.hardwareId) {
              // Only for simulator we are receiving this values here, the others are arriving on frames
              data['pipsetting']= data['pip'];
              data['ratesetting']= data['rate'];
              data['peepsetting']= data['peep'];
              data['fio2setting']= data['fio2'];
              data['tvsetting']= data['tv'];
              // remove the 5 lines above when we create a Mock Device frame based.
              this.updateBlocksValues(data, this.ssHwVentilatorDataSelector, false);
              this.updateAttrBlocksValues('currentValue', data, this.ssVentilatorInputDataSelector);
            }
          })
        );
        // Subscription to Ventilator Data Elapsed
        this.wsSubscriptions.push(
          this.hwConnection.receiveVentilatorDataElapsed().subscribe((data: IHardwareVentilatorEvents) => {
            if (this.devicesByCapability.get(DeviceCapabilityType.VENTILATOR)?.hardwareId === data[0].hardwareId) {
              this.updateBlocksValues(data, this.ssHwVentilatorDataSelector, false);
              this.updateAttrBlocksValues('currentValue', data, this.ssVentilatorInputDataSelector);
            }
          })
        );
        // Subscription to Ventilator Read Only Settings
        this.wsSubscriptions.push(
          this.hwConnection.receiveVentilatorRoDataSettings().subscribe((data: IHWModels) => {
            // eslint-disable-next-line no-console
            // console.log(data);
            this.updateVentilatorRoBoard(data || []);
          })
        );

        // Subscription to Ventilator Read Write Settings
        this.wsSubscriptions.push(
          this.hwConnection.receiveVentilatorRwDataSettings().subscribe((data: IVentModels) => {
            this.updateVentilatorRwBoard(data || []);
          })
        );

        // Subscription to Blood Gas Analysis Data
        this.wsSubscriptions.push(
          this.hwConnection.receiveBloodGasData().subscribe((data: IHardwareBloodGasData) => {
            if (this.devicesByCapability.get(DeviceCapabilityType.BLOOD_GAS_ANALYZER)?.hardwareId === data.hardwareId) {
              this.updateBlocksValues(data, this.ssHwBloodGasDataSelector);
            }
          })
        );
        // Subscription to Blood Gas Analysis Data Elapsed
        this.wsSubscriptions.push(
          this.hwConnection.receiveBloodGasDataElapsed().subscribe((data: IHardwareBloodGasDatas) => {
            if (data[0]!== undefined && this.devicesByCapability.get(DeviceCapabilityType.BLOOD_GAS_ANALYZER)?.hardwareId === data[0].hardwareId) {
              this.updateBlocksValues(data, this.ssHwBloodGasDataSelector);
            }
          })
        );
        this.wsSubscriptions.push(
          this.hwConnection.receiveDatasetMappings().subscribe((data: IMetricMap) => {
            this.loadDatasetMappings(data);
          })
        );
        this.wsSubscriptions.push(
          this.hwConnection.receiveDatasetUnits().subscribe((data: IUnitMap) => {
            this.loadUnitMappings(data);
          })
        );
        this.wsSubscriptions.push(
          this.hwConnection.receiveDeviceFrameData().subscribe((data: IDeviceFrame) => {
            this.processNewFrame(data);
            /* const devices: IMonitorDevice[] = MonitorAssignmentUtils.getUniqueDevices(this.assignment);
            if (devices.length > 0) {
              this.groupDevicesByHarwdwareId(devices).forEach((d, hwId) => {
                if (data.hardwareId === hwId) {
                  this.processNewFrame(data);
                }
              });
            }*/
          })
        );
        this.wsSubscriptions.push(
          this.hwConnection.receiveSettingsIds().subscribe((data: IIdMap) => {
            this.loadIdMappingSettings(data);
            // Dynamic Widgets creation
            this.createDynamicWidgets();
          })
        );
        this.wsSubscriptions.push(
          this.hwConnection.receiveMeasuresIds().subscribe((data: IIdMap) => {
            this.loadIdMappingMeasures(data);
            // Dynamic Widgets creation
            this.createDynamicWidgets();
          })
        );

        // Subscription to Hardware Status Data
        this.wsSubscriptions.push(
          this.hwConnection.receiveStatus().subscribe((status: IHardwareStatus) => {
            this.updateHardwareStatus(status);
          })
        );

        // Subscription to Hardware Messages
        this.wsSubscriptions.push(
          this.hwConnection.receiveMessage().subscribe((message: IHardwareMessage) => {
            this.processHardwareMessage(message);
          })
        );

        // Subscription to Hardware Connection Errors
        this.wsSubscriptions.push(
          this.hwConnection
            .receiveConnectionErrorMessage()
            .subscribe((error: MonitorHardwareConnectionError) => this.processMonitorHardwareConnectionError(error))
        );

        // Subscription to Hardware Messages
        this.wsSubscriptions.push(
          this.hwConnection.receivePumpData().subscribe((message: IPumpModel) => {
            this.processPumpData(message);
          })
        );

        // Subscription to Hardware Messages
        this.wsSubscriptions.push(
          this.hwConnection.receiveAlarmWarningsData().subscribe((alarmWarnings: IAlarmWarningModel[]) => {
            this.processAlarmWarnings(alarmWarnings);
          })
        );
        // Subscription to Hardware Messages
        this.wsSubscriptions.push(
          this.hwConnection.receiveAlarmsData().subscribe((alarms: IAlarmModel[]) => {
            // Set max and min of each Alarm
            this.setAlarms(alarms);
          })
        );

        // Connect to Monitor Service
        this.connectToMonitorDataService();
      },
      error: error => this.processMonitorHardwareConnectionError(error)
    });
  }

  /** Obtains from the BE Dataset Mappings.
   * This is used to map waveform Ids received on frames
   * to the corresponding UI widget.
   **/
  loadDatasetMappings(data: IMetricMap): void {
    this.datasetMappings = data;
  }

  /** Obtains from the BE Unit Mappings.
   * This is used to map unit Ids received on frames
   * to the corresponding string description
   **/
  loadUnitMappings(units: IUnitMap): void {
    this.unitMappings = units;
  }

  /**
   * Obtain from middleware the id to resolve the correct setting widget id
   * @param ids
   */
  loadIdMappingSettings(ids: IIdMap): void {
    this.idMappingSettings = ids;
    for (const [key, value] of Object.entries(ids)) {
      this.reverseIdMappingSettings[value] = key;
    }
    this.mappingsLoaded += 1;
  }

  /**
   * Obtain from middleware the id to resolve the correct measure widget id
   * @param ids
   */
  loadIdMappingMeasures(ids: IIdMap): void {
    this.idMappingMeasures = ids;
    for (const [key, value] of Object.entries(ids)) {
      this.reverseIdMappingMeasures[value] = key;
    }
    this.mappingsLoaded += 1;
  }

  createDynamicWidgets(): void {
    if (this.mappingsLoaded <= 1) return;
    //Makes the physiologic monitors section as dynamic as possible
    const hwId = this.devicesByCapability.get(DeviceCapabilityType.VITALS)?.hardwareId;
    if (hwId !== undefined) {
      this.subscribeToPhysiologicMonitorItems(hwId);
      this.subscribeToVentilatorRoItems(hwId);
    }
  }

  updateBlocksValues(data: any, selector: string, force = false): void {
    // if ventilatorRecommenderTypes == RB we should set ph in 7.4 (only the RB recommender input uses that)
    // https://saperisys.atlassian.net/browse/SASA-810
    if (
      data !== undefined &&
      'ph' in data &&
      data.ph !== null &&
      this.devicesByCapability.get(DeviceCapabilityType.BLOOD_GAS_ANALYZER)?.type === DeviceType.MOCK
    ) {
      if (this.assignment.ventilatorRecommenderType === VentilatorRecommenderType.RULE_BASED) {
        data.ph.value = Math.round(7.4 * 100) / 100;
      } else {
        delete data.ph;
      }
    }
    // On Block update (header information) we should need from where the information is coming
    if (Array.isArray(data) && data.length > 0) {
      data[0].type = selector;
    } else {
      data.type = selector;
    }
    this.getAllBlocks().forEach(b => {
      if (b.hasSelector(selector) && (force || !this.isAtLeastOneSelectorFrozen(b.selectors))) {
        if (this.blockData[b.dataKey] === undefined && b.dataKey !== '') {
          this.setBlockData(b.dataKey, []);
        }

        b.setBlockData(this.blockData[b.dataKey]);
        b.updateBulkValue(data);
        b.updateValue(data);
      }
    });
  }

  updateAttrBlocksValues(attr: string, data: any, selector: string, force = false): void {
    this.getAllBlocks().forEach(b => {
      if (b.hasSelector(selector) && (force || !this.isAtLeastOneSelectorFrozen(b.selectors))) {
        b.updateAttr(attr, data);
      }
    });
  }

  updatePumps(data: any): void {
    const pumpData = data as PumpModel[];
    pumpData.forEach(p => {
      this.accuPumps.push(p);
    });
  }

  // Process Device Frame Data
  private processNewFrame(deviceFrame: IDeviceFrame): void {
    // eslint-disable-next-line no-console
    // console.log('New Frame received');
    if (deviceFrame === undefined) {
      // eslint-disable-next-line no-console
      console.log('device Frame is empty');
      return;
    }

    let currentFrame: IFrame;
    // Setting the currentFrame to grab waveforms from it
    if (deviceFrame.frame === undefined) {
      currentFrame = deviceFrame.frame;
    }
    let frame: IFrame = deviceFrame.aggregateFrame;
    //Try to get the frame from aggregate first then from regular frame.
    if (deviceFrame.aggregateFrame === undefined) {
      // eslint-disable-next-line no-console
      console.log('Aggregate Frame inside device frame is empty');
      if (deviceFrame.frame === undefined) {
        // eslint-disable-next-line no-console
        console.log('Frame inside device frame is empty');
        return;
      }
      frame = deviceFrame.frame;
    }
    const deviceFromAggregate: IDeviceCtx | undefined = this.getMasterDevice(Object.values(frame.devices));
    if (frame.devices === undefined) {
      // eslint-disable-next-line no-console
      console.log('Devices inside are empty');
      return;
    }
    //Since the name can change from device to device we just grab what exist as first element there
    const device: IDeviceCtx | undefined = this.getMasterDevice(Object.values(frame.devices));
    if (device === undefined) return;
    this.setupPumpHwId(device);

    if (device.deviceInfo !== undefined && device.deviceInfo !== null) {
      this.presentBatteriesStatus(device.deviceInfo);
      this.presentVentilatorMode(undefined, device.settingValues);
    }

    // eslint-disable-next-line no-console
    // console.log(now());
    // eslint-disable-next-line no-console
    // console.log(deviceFrame);
    const measureValues: IValueMap = device.measureValues;
    Object.keys(measureValues).forEach(metricId => {
      // eslint-disable-next-line no-console
      // console.log('Receiving measure for metric (' + metricId + '): ' + this.datasetMappings[Number(metricId)] + ' value: ' + measureValues[metricId].value + ' ' + measureValues[metricId].unit_code);
      // send this values to all the components to see if they are interested in the data
      /** if (this.devicesByCapability.get(DeviceCapabilityType.VITALS)?.hardwareId === data[0].hardwareId) {
        this.updateBlocksValues(frameToEvent(measureValues[metricId]), this.ssHwVitalsDataSelector);
      }**/

      const eventAndKey = this.frameToEvent(measureValues, metricId, false);
      if (eventAndKey === undefined) return;
      const k = Object.values(eventAndKey)[1];
      // create a map here with this information
      const selector = this.getSelectorFromphysiologicDefinition('' + k);


      // if this device
      const isNK =
        this.assignment.devices?.some(d => {
          return d.device.type === DeviceType.NIHONKOHDEN;
        }) || false;
      if (selector === this.ssHwVitalsDataSelector && isNK ===  true) {
        // eslint-disable-next-line no-console
        // console.log('NK devices should not update Physiologic section');
      } else {
        this.updateBlocksValues(Object.values(eventAndKey)[0], selector);
      }
    });

    const settingValues: IValueMap = device.settingValues;
    if (settingValues !== undefined) {
      // TODO: define if this will be standard for all devices.
      this.presentVentilatorMode(undefined, settingValues);
    }
    Object.keys(settingValues).forEach(metricId => {
      const eventAndKey = this.frameToEvent(settingValues, metricId, true);
      if (eventAndKey === undefined) return;
      const k = Object.values(eventAndKey)[1];
      //Augment event And Key to receive also the manufacturer
      const temp = Object.values(eventAndKey)[0] as Object;
      if (temp !== undefined) {
        temp['manufacturer'] = deviceFromAggregate?.deviceInfo.manufacturer;
      }
      //eventAndKey.manufacturer = deviceFromAggregate?.deviceInfo.manufacturer;
      // create a map here with this information
      const selector = this.getSelectorFromVentSettingsDefinitions('' + k);
      this.updateBlocksValues(Object.values(eventAndKey)[0], selector);
      // eslint-disable-next-line no-console
      // console.log(measureValues[metricId]);
    });

    //get waveform values
    const waveformValues: IMeasureWaveFormMap = (deviceFromAggregate!==undefined)?deviceFromAggregate.measureWaveFormValues:device.measureWaveFormValues;
    // eslint-disable-next-line no-console
    // console.log(waveformValues);
    // HashMap<String, DeviceCtx> devices = new HashMap();
    // private ArrayList<AggregatorEvent> events = new ArrayList();
    Object.keys(waveformValues).forEach(metricId => {
      // eslint-disable-next-line no-console
      // console.log('Receiving waveform for metric (' + metricId + '): ' + this.datasetMappings[Number(metricId)] );
      const eventsAndKeys = this.waveframeToEvent(waveformValues, metricId);
      if (eventsAndKeys === undefined) return;
      // We can just iterate throught the correct blocks to avoid sent this to all the blocks
      Object.values(eventsAndKeys).forEach(eventAndKey => {
        if (eventAndKey === undefined) return;
        const k = Object.values(eventAndKey)[1];
        const selector = this.getSelectorFromWaveforms('' + k);
        if (k !== undefined && selector !== undefined && selector !== '') {
          this.updateVentCharts(Object.values(eventAndKey)[0], selector);
        }
      });
      /*
      const k = Object.values(eventAndKey)[1];
      // create a map here with this information
      const selector = this.getSelectorFromDefinition('' + k); */
    });
  }

  async sleep(ms: number): Promise<any> {
    await new Promise(resolve => setTimeout(resolve, ms));
  }

  updateVentCharts(eventAndKey: any, selector: string): void {
    this.updateBlocksValues(eventAndKey, selector);
  }

  presentBatteriesStatus(deviceInfo: IDeviceInfo): void {
    // eslint-disable-next-line no-console
    // console.log(batteriesInfo);
    const batteriesInfo = deviceInfo.batteryInfo;
    if (batteriesInfo === undefined) return;
    if (deviceInfo.manufacturer === this.NEUROWAVE) return; //this is being handled in the Pump component
    const battery1: IBattery = batteriesInfo[0];
    const battery2: IBattery = batteriesInfo[1];
    if (battery1 !== undefined) {
      this.setVentilatorBattery(this, 'batteryCardioPercentage', deviceInfo.onMainsPower, battery1, 'cardioConnectStatus');
    }
    if (battery2 !== undefined) {
      this.setVentilatorBattery(this, 'batteryVentPercentage', deviceInfo.onMainsPower, battery2, 'ventConnectStatus');
    }
  }

  presentVentilatorMode(vmode: string | undefined, settingValues: IValueMap): void {
    const ventControlMetricId = this.reverseIdMappingSettings['ventcontrolsetting']; // If it's pressure or volume
    const ventControlSetting = settingValues[ventControlMetricId];
    if (vmode === undefined) {
      // Try to retrieve it from settings, this will be commented since the setting will not have it anymore
      if (this.ventilatorBlocks !== undefined && this.ventilatorBlocks.length > 0) {
        const ventModeDefinition = this.findVentilatorDef('ventmodesetting'); // contains all modes
        const ventModeMetricId = this.reverseIdMappingSettings['ventmodesetting'];
        const ventModeSetting = settingValues[ventModeMetricId];
        if (ventModeSetting !== undefined) {
          const enumValues = ventModeDefinition?.enumValues;
          const ventModePosition = +ventModeSetting.stringValue;
          if (enumValues !== undefined && enumValues.length > ventModePosition) {
            this.ventMode = enumValues[ventModePosition].shortLabel;
            this.newVentMode = this.ventMode;
          }
        }
      }
    } else {
      this.newVentMode = vmode;
    }
    if (ventControlSetting === undefined) return;
    if (this.ventilatorBlocks[0] !== undefined) {
      const pipDef = this.findVentilatorDef('pipsetting'); // Find on ventilator blocks
      const tvDef = this.findVentilatorDef('tvsetting'); // Find on ventilator blocks
      // (0 - Volume, 1 - Pressure)
      if (
        ventControlSetting.stringValue === '0' &&
        pipDef !== undefined &&
        tvDef !== undefined &&
        this.ventilatorBlocks[0].dataKey === 'pipsetting'
      ) {
        // change to volume, and put pip on ventilatorBlocksNotVisible (if the list of not visible grows change this impl)
        this.ventilatorBlocks[0] = tvDef;
        this.ventilatorBlocksNotVisible[0] = pipDef;
      } else if (
        ventControlSetting.stringValue === '1' &&
        pipDef !== undefined &&
        tvDef !== undefined &&
        this.ventilatorBlocks[0].dataKey === 'tvsetting'
      ) {
        // change to pressure, and put tv on ventilatorBlocksNotVisible (if the list of not visible grows change this impl)
        this.ventilatorBlocks[0] = pipDef;
        this.ventilatorBlocksNotVisible[0] = tvDef;
      }
    }
  }

  findVentilatorDef(dataKey: string): VentilatorInputDefinition | undefined {
    const ventDefs: VentilatorInputDefinition[] = this.ventilatorBlocks.concat(this.ventilatorBlocksNotVisible);
    for (let i = 0; i < ventDefs.length; i++) {
      if (ventDefs[i] !== undefined && ventDefs[i].dataKey === dataKey) {
        return this.cloneVentDef(ventDefs[i]); // do a clone
      }
    }
    return undefined;
  }

  getSelectorFromphysiologicDefinition(name: string): string {
    let def: IHWModel;
    // need to add fixed definitions to present on other sections like ventilators blocks not selected
    let physiologicAndManualAdded: IHWModel[] = this.monitorDefinitions;
    for (let i = 0; i < this.ventilatorUnselectedBlocks.length; i++) {
      physiologicAndManualAdded = physiologicAndManualAdded.concat(this.createHWModelFromHWBlockDef(this.ventilatorUnselectedBlocks[i]));
    }

    for (let i = 0; i < physiologicAndManualAdded.length; i++) {
      def = physiologicAndManualAdded[i];
      if (name === def.dataKeys[0]) {
        return def.selectors[0];
      }
    }
    return '';
  }

  getSelectorFromVentSettingsDefinitions(name: string): string {
    let def: VentilatorInputDefinition;
    for (let i = 0; i < this.ventilatorBlocks.length; i++) {
      def = this.ventilatorBlocks[i];
      if (name === def.ventilatorChangeRequestDataKey) {
        return def.selectors[1]; // we use hwVentilatorDataSelector
      }
    }
    return '';
  }

  getSelectorFromWaveforms(name: string): string {
    let def: BlockDefinition;
    for (let i = 0; i < this.ventilatorChartBlocks.length; i++) {
      def = this.ventilatorChartBlocks[i];
      if (def.dataKeys !== undefined && name === def.dataKeys[0]) {
        return def.selectors[0]; // we use hwVentilatorDataSelector
      }
    }
    return '';
  }

  /**
   * Extract from the measure a "valid" data to be used on widgets
   * @param measure IScalarValue
   */
  frameToEvent(measures: IValueMap, metricId: string, isSetting: boolean): any {
    const measure: IValue = measures[metricId];
    const key = metricId; // this.datasetMappings[Number(metricId)];
    // Value is coming in stringValue whe this is a setting
    let value = isSetting ? this.getValueFromSetting(metricId, measure) : Number(measure.stringValue);
    const temp = {};
    const widgetKey = isSetting ? this.datasetMappingToWidgetSettingKey(key) : this.datasetMappingToWidgetMeasureKey(key);
    if (typeof value === 'string' || value instanceof String) {
      temp[widgetKey] = value;
    } else if (typeof value === 'number') {
      if (value % 1 !== 0) {
        value = Math.trunc(value * 100) / 100;
      }
      temp[widgetKey] = new ScalarValue(value, '' + this.unitMappings[measure.unit_code]);
      // If it's a sbp
      if (widgetKey === 'sbp') {
        const diastolicKey = 18962; // how to avoid this harcoded key ??
        temp['dbp'] = new ScalarValue(Number(measures[diastolicKey].stringValue), '' + this.unitMappings[measures[diastolicKey].unit_code]);
      }
    }
    return { temp, widgetKey };
  }

  getValueFromSetting(metricId: string, measure: IValue): any {
    const widgetKey = this.datasetMappingToWidgetSettingKey(metricId);
    for (let j = 0; j < this.ventilatorBlocks.length; j++) {
      const block = this.ventilatorBlocks[j];
      if (block.dataKey === widgetKey && block.enumValues !== null && block.enumValues !== undefined) {
        // The value will be some of the predefined values
        for (let i = 0; i < block.enumValues.length; i++) {
          if (block.enumValues[i].constrainedValue === false && '' + block.enumValues[i].position === measure.stringValue) {
            return block.enumValues[i].humanLabel;
          } else if (block.enumValues[i].constrainedValue === true && block.enumValues[i].internalValue === measure.stringValue) {
            return block.enumValues[i].humanLabel;
          }
        }
      }
    }
    return Number(measure.stringValue);
  }

  /**
   * Extract from the measure a "valid" data array to be used on widgets
   * @param measure IScalarValue
   */
  waveframeToEvent(measures: IMeasureWaveFormMap, metricId: string): any[] {
    const measure: IMeasureWaveForm = measures[metricId];
    const key = metricId;
    const values = measure.array;
    const varray: any[] = [];
    values.forEach(value => {
      if (value % 1 !== 0) {
        value = Math.trunc(value * 100) / 100;
      }
      const temp = {};
      let widgetKey = this.datasetMappingToWidgetMeasureKey(key);
      // Try to find the key in settings or in measures
      if (widgetKey === undefined) {
        widgetKey = this.datasetMappingToWidgetSettingKey(key);
      }
      // eslint-disable-next-line no-console
      // console.log(widgetKey);

      temp[widgetKey] = new ScalarValue(value, '');

      // eslint-disable-next-line no-console
      // console.log(widgetKey);
      // const unit = this.unitMappings[value.unit_code]; // This is not present on waveforms, we should consider add it on frames
      const unit = ' ';
      temp[widgetKey] = new ScalarValue(value, '' + unit);

      varray.push({ temp, widgetKey });
    });
    return varray;
  }

  datasetMappingToWidgetSettingKey(mapping: string): string {
    return this.idMappingSettings[mapping];
  }

  datasetMappingToWidgetMeasureKey(mapping: string): string {
    return this.idMappingMeasures[mapping];
  }

  getMetricName(metricId: number): string {
    switch (metricId) {
      case 127:
        return '';
      default:
        return '';
    }
  }

  updateHardwareStatus(status: IHardwareStatus): void {
    const currentStatus = this.hardwareStatuses.get(status.hardwareId);

    if (!currentStatus) {
      // We received a status for a Device we don't know? This should never happen.
      return;
    }

    const badgeStatus =
      status.status === HardwareStatusType.CONNECTED
        ? BadgeStatus.SUCCESS
        : status.status === HardwareStatusType.UNKNOWN
        ? BadgeStatus.WARNING
        : status.status === HardwareStatusType.WARNING
        ? BadgeStatus.WARNING
        : BadgeStatus.ERROR;

    let message = 'Status: ' + status.status + '.';
    if (status.message && status.message.trim().length > 0) {
      message += ' (' + status.message + ')';
    }

    currentStatus.badgeStatus = badgeStatus;
    currentStatus.hardwareStatus = status.status;
    currentStatus.message = message;
  }

  getHardwareStatuses(): HardwareStatusInfo[] {
    return [...this.hardwareStatuses.values()].sort((a, b) => {
      return a.order - b.order;
    });
  }

  processHardwareMessage(message: IHardwareMessage): void {
    const alertDef: JhiAlert = {
      type: 'success',
      msg: message.messageKey,
      params: message.parameters,
      toast: true,
      position: 'top'
    };

    if (message.severity === HardwareMessageSeverity.INFO) {
      alertDef.type = 'info';
      alertDef.timeout = 2000;
    } else if (message.severity === HardwareMessageSeverity.WARNING) {
      alertDef.type = 'warning';
      alertDef.timeout = 2000;
    } else {
      alertDef.type = 'danger';
      alertDef.timeout = 2000;
    }
    this.alertService.addAlert(alertDef, []);
  }

  /**
   * A MonitorHardwareConnectionError usually means an irreversible state. We will
   * display these errors as a 'danger' alert that will not automatically disapear
   * fom the UI.
   * @param error the error
   */
  processMonitorHardwareConnectionError(error: MonitorHardwareConnectionError): void {
    this.alertService.addAlert(
      {
        type: 'danger',
        msg: error.msgKey,
        params: error.params,
        timeout: 0,
        toast: true,
        position: 'top'
      },
      []
    );
  }

  processPumpData(pumpData: IPumpModel): void {
    // eslint-disable-next-line no-console
    // console.log('Pump data for ' + pumpData.name + ' ' + pumpData.infusionRate1);
    const pump = this.infusionBlocks.find(p => p.pumpDef.hwId === pumpData.hwId);
    pump!.reconfigure(pumpData);
  }

  /**
   * If alarms on the Frame the service will inject them each X seconds
   * @param alarmData
   */
  processAlarmWarnings(alarmData: IAlarmWarningModel[]): void {
    this.updateBlocksValues(alarmData || {}, this.ssAlarmSelector);
  }

  getAllBlocks(): DataBlockComponent[] {
    return [
      ...this.hardwareDataBlocks.toArray(),
      ...this.ventilatorRecommendationInputDataBlocks.toArray(),
      ...this.tabularDataBlocks.toArray(),
      ...this.flowsheetDataBlocks.toArray(),
      ...this.ventilatorParameterDataBlocks.toArray(),
      ...this.headerBlock.toArray(),
      ...this.alarmBlock.toArray()
    ];
  }

  blockDragStart(def: BlockDefinition, srcList: BlockDefinition[]): void {
    this.draggedOverBlockInfo = undefined;
    this.draggedBlockInfo = new HardwareBlockDragInfo(def, srcList);
  }

  blockDragOver(blockDef: BlockDefinition): void {
    this.draggedOverBlockInfo = blockDef;
  }

  blockDrop1(targetList: BlockDefinition[], expand: boolean): void {
    if (this.draggedBlockInfo) {
      // eslint-disable-next-line no-console
      // console.log('HERE');
    }
  }

  blockDrop(targetList: BlockDefinition[], expand: boolean): void {
    if (this.draggedBlockInfo) {
      if (
        this.draggedBlockInfo.block !== undefined &&
        this.draggedBlockInfo.block.physiologicMonitors === true &&
        this.draggedBlockInfo.block.reduced !== true
      ) {
        // We can't move a drop from physiologic-list
        return;
      }
      if (this.draggedBlockInfo.source === targetList) {
        // We can't move a drop from one list to the same list.
        return;
      }
      let validTarget = false;
      // check if the target is the device-data section and that the limit is not overflowed
      if (targetList === this.patientBlocks) {
        validTarget = true;
        if (this.draggedBlockInfo.block.physiologicMonitors === false) {
          return;
        }
        if (targetList.length === this.MAX_PHYSIOLOGIC_DEVICE_DATA_BLOCKS) {
          // Swap blocks
          const blockReplaced = this.draggedOverBlockInfo === undefined ? (targetList.pop() as BlockDefinition) : this.draggedOverBlockInfo;
          const replacedOrder = blockReplaced.order;
          blockReplaced.expanded = false;
          blockReplaced.reduced = true;
          blockReplaced.order = this.draggedBlockInfo.block.order;
          this.moveBlock(targetList, this.patientUnselectedBlocks, blockReplaced);
          this.patientUnselectedBlocks.sort((a, b) => a.order - b.order);
          this.draggedBlockInfo.block.order = replacedOrder;
          // Removes from patientCharts the removed block (using name) and adds the dropped  one
          const index = this.getBlockIndex(this.patientChartBlocks, blockReplaced);
          if (index > -1) {
            const newBlockExpandedAndSelected = this.cloneHWDef(this.draggedBlockInfo.block as HardwareBlockDefinition);
            newBlockExpandedAndSelected.expanded = true;
            newBlockExpandedAndSelected.physiologicMonitors = true;
            newBlockExpandedAndSelected.reduced = false;
            this.patientChartBlocks[index] = newBlockExpandedAndSelected;
          }
        }
      }
      if (targetList === this.ventilatorChartBlocks) {
        validTarget = true;
        if (this.draggedBlockInfo.block.ventMonitors === false) {
          return;
        }
        if (targetList.length === this.MAX_VENTILATOR_DEVICE_DATA_BLOCKS) {
          // Swap blocks
          const blockReplaced = this.draggedOverBlockInfo === undefined ? (targetList.pop() as BlockDefinition) : this.draggedOverBlockInfo;
          const replacedOrder = blockReplaced.order;
          blockReplaced.expanded = false;
          blockReplaced.reduced = true;
          blockReplaced.order = this.draggedBlockInfo.block.order;
          // Removes from the targetList the component "dragged into" adds into second param list the block draggedOver
          this.moveBlock(targetList, this.ventilatorUnselectedBlocks, blockReplaced);
          this.ventilatorUnselectedBlocks.sort((a, b) => a.order - b.order);
          // the block inserted on the graph are should not be reduced
          this.draggedBlockInfo.block.reduced = false;
        }
      }
      if (validTarget) {
        this.draggedBlockInfo.block.expanded = expand;
        this.moveBlock(this.draggedBlockInfo.source, targetList, this.draggedBlockInfo.block);
        targetList.sort((a, b) => a.order - b.order);
      }
    }
  }

  /**
   * Switch Ventilator Mode (there are three suported values but as per requeriments is should only toggle from Manual
   * to Close=Autopilot and vice-versa)
   */
  switchVentilatorMode(): void {
    if (this.assignment.ventilatorMode === VentilatorModeType.MANUAL) {
      this.assignment.ventilatorMode = VentilatorModeType.CLOSED;
    } else {
      this.assignment.ventilatorMode = VentilatorModeType.MANUAL;
    }
  }

  /**
   * Tells whether there is an "active" ventilator recommendation. An "active" recommendation
   * is a recommendation that was requested but it hasn't been appllied or rejected.
   */
  isActiveVentilatorRecommendation(): boolean {
    return this.isSelectorFrozen(this.ssRecommenderInputSelector);
  }

  /**
   * When the backend creates a recommendation (either because we triggered it from the UI)
   * or because the ventilator was in OPEN or CLOSED Loop, we need to show the data used
   * for the request, and the recommendation itself.
   */
  private onVentilatorRecommendationResponse(rec: IVentilatorRecommendationResponse): void {
    // In order to show the request data, we need to freeze the blocks.
    this.freezeSelector(this.ssRecommenderInputSelector);

    // Create a Snapshot of the current Ventilator Parameters
    this.ventilatorParameterDataBlocks.forEach(b => {
      b.createSnapshot();
    });

    // TODO: this only works because the name of the attributes in rec
    // are the same as in the hardware events.
    this.updateBlocksValues(rec.request!, this.ssRecommenderInputSelector, true);

    // Show the Recommendation
    this.showVentilatorRecommendation(rec);
    this.recommendationReason = rec.reason;
    if (this.assignment.ventilatorMode !== VentilatorModeType.MANUAL) {
      this.updateLastUpdateTime(rec.timestamp);
    }

    // Shows an alert that call the backend to save
    // Check if the fio2 current value
    const fio2vp = this.ventilatorRecommendationInputDataBlocks.find(vp => vp.name === this.fio2Name);
    if (fio2vp?.value?.value !== rec.fio2?.value) {
      this.acknowledgeFio2 = true; // Makes the alert visible
    }
  }

  private showVentilatorRecommendation(rec: IVentilatorRecommendationResponse): void {
    this.ventilatorParameterDataBlocks.forEach(b => {
      b.configureFromResponse(rec);
    });
    // save PIP in case that TV is being displayed in first place
    this.pipFromResponse.configureFromResponse(rec);
    // Save values for PIP
    this.oldPipValue = rec.pip?.value as number;
    if ('tv' in rec && rec.tv !== null && rec.tv!.value !== undefined) {
      this.oldTvValue = rec.tv?.value as number;
    }
  }

  private createVentilatorRecommendationRequest(): IVentilatorRecommendationRequest {
    this.recommendationReason = '';
    const req = new VentilatorRecommendationRequest();
    this.ventilatorRecommendationInputDataBlocks.forEach(b => {
      b.configureRequest(req);
    });
    return req;
  }

  // Ventilator Changes

  updateLastUpdateTime(ts: number): any {
    // Format desired (16 SEP 2020) 09:16:3
    const dateAndTime = moment(ts).format('(DD MMM YYYY)--HH:mm:ss');
    const [date, time] = dateAndTime.split('--');
    this.lastUpdateDate = date;
    this.lastUpdateTime = time;
  }

  discardVentilatorChanges(): void {
    this.recommendationReason = '';
    // Unfreeze the recommender input blocks
    this.unfreezeSelector(this.ssRecommenderInputSelector);

    // Restore the snapshot of the Ventilator Parameters
    this.ventilatorParameterDataBlocks.forEach(b => {
      // b.restoreSnapshot();
      b.resetRecommendation(); // no recommended value is indicated
    });
  }

  // Private methods

  private createVentilatorChangeRequest(): IVentilatorConfigurationChangeRequest {
    const req = new VentilatorConfigurationChangeRequest();
    this.ventilatorParameterDataBlocks.forEach(b => {
      b.configureRequest(req);
    });
    if (VentilatorRecommenderControlType.VOLUME === this.assignment.ventilatorControlType) {
      this.pipFromResponse.configureRequest(req);
    }
    return req;
  }

  private isAtLeastOneSelectorFrozen(selectors: string[]): boolean {
    for (let i = 0; i < selectors.length; i++) {
      if (this.isSelectorFrozen(selectors[i])) {
        return true;
      }
    }
    return false;
  }

  private isSelectorFrozen(selector: string): boolean {
    return this.frozenSelectors.includes(selector);
  }

  private freezeSelector(selector: string): void {
    if (!this.isSelectorFrozen(selector)) {
      this.frozenSelectors.push(selector);
    }
  }

  private unfreezeSelector(selector: string): void {
    this.frozenSelectors = this.frozenSelectors.filter(ss => ss !== selector);
  }

  private moveBlock(srcList: BlockDefinition[], targetList: BlockDefinition[], block: BlockDefinition): void {
    const index = this.getBlockIndex(srcList, block);
    if (index > -1) {
      srcList.splice(index, 1);
    }
    targetList.push(block);
  }

  private getBlockIndex(srcList: BlockDefinition[], block: BlockDefinition): number {
    return srcList.findIndex(b => {
      return b.name === block?.name;
    });
  }

  private groupDevicesByHarwdwareId(devices: IMonitorDevice[]): Map<string, IMonitorDevice[]> {
    const result = new Map();
    devices.forEach(d => {
      let elements = result.get(d.hardwareId);
      if (!elements) {
        elements = [];
        result.set(d.hardwareId, elements);
      }
      elements.push(d);
    });
    return result;
  }

  private createDefaultHardwareStatusInfo(devices: IMonitorDevice[]): Map<string, HardwareStatusInfo> {
    const result = new Map();
    devices.forEach((d, i) => {
      result.set(
        d.hardwareId,
        new HardwareStatusInfo(d.hardwareId, this.getDeviceLabel(d), i, BadgeStatus.WARNING, HardwareStatusType.UNKNOWN, 'Status: UNKNOWN.')
      );
    });
    return result;
  }

  private getDeviceLabel(device: IMonitorDevice): string {
    return device.hardwareId + ' (' + device.type + ': ' + device.model + ')';
  }

  /* Change PIP by TV or vice versa in the Ventilator Controls section.
  - When the control mode is Volume we should present TV
  - When the control mode is PRessure we should present PIP
  */
  private changeVentilatorControlInputs(): void {
    // Get the pressureOrVolumeDef
    if (VentilatorRecommenderControlType.PRESSURE === this.assignment.ventilatorControlType) {
      this.pressureOrVolumeDef.name = 'PIP (cmH<sub>2</sub>O)';
      this.pressureOrVolumeDef.ventilatorResponseDataKey = 'pip';
      this.pressureOrVolumeDef.ventilatorChangeRequestDataKey = 'pip';
      this.pressureOrVolumeDef.lowerBound = 29;
      this.pressureOrVolumeDef.upperBound = 40;
      this.pressureOrVolumeDef.min = 0;
      this.pressureOrVolumeDef.max = 60;
      this.pressureOrVolumeDef.step = 1;
      this.pressureOrVolumeDef.value = new ScalarValue(this.oldPipValue, 'cmH2O');
    } else {
      this.pressureOrVolumeDef = this.changePipToTV(this.pressureOrVolumeDef, this.oldTvValue);
    }
  }

  private changePipToTV(ventInputDef: VentilatorInputDefinition, value: number): VentilatorInputDefinition {
    ventInputDef.dataKey = 'tv';
    ventInputDef.name = 'TV (L)';
    ventInputDef.ventilatorResponseDataKey = 'tv';
    ventInputDef.ventilatorChangeRequestDataKey = 'tv';
    ventInputDef.lowerBound = 0;
    ventInputDef.upperBound = 2;
    ventInputDef.min = 0;
    ventInputDef.max = 15;
    ventInputDef.step = 0.1;
    ventInputDef.value = new ScalarValue(value, 'L');
    return ventInputDef;
  }

  getStyleName(status: IMemberStatus): string {
    return status.toString().toLowerCase();
  }

  getStatusName(status: IMemberStatus): string {
    const st = this.getStyleName(status);
    if (st.length > 1) {
      return st.charAt(0).toUpperCase() + st.slice(1);
    }
    return st;
  }

  /**
   * This function should receive the team member id as parameter, it will be used to generate the url
   * @param name
   */
  localTeamVideo(event: MouseEvent, name: string): void {
    event.stopPropagation();
    this.localTeamId = name;
    this.localTeamVideoURL = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.saperi.io/services?' + this.localTeamId);
    this.showLocalTeamVideo = true;
  }

  menuItemClick(event: MouseEvent): void {
    this.showClinicalSummary = false;
    this.showDevices = false;
    // Find the item that is selected

    const selectedElement = document.querySelector('#menuItems .selected');

    if (selectedElement !== null) {
      selectedElement.className = '';
    }
    (event.target as HTMLElement).className = 'selected';
    if ('Devices' === (event.target as HTMLElement).innerHTML) {
      this.showDevices = true;
    }
    if ('Clinical Summary' === (event.target as HTMLElement).innerHTML) {
      this.showClinicalSummary = true;
      this.presentClinicalSummary();
    }
  }

  /**
   * This function should set the clinical summary URL to be presented in an iframe.
   * The patient id that is an integer should have some 1 to 1 mapping to retrieve the id on some EMR.
   * This can be an extra field when a patient is registered.
   */
  presentClinicalSummary(): void {
    this.clinicalSummaryURL = this.sanitizer.bypassSecurityTrustResourceUrl(
      'https://stronghold-clinical-summary-zpm254mtxa-uw.a.run.app/clinicalsummary/' + this.assignment.assignmentId
    );

    this.showClinicalSummary = true;
  }

  jitsiTeamConf(): void {
    this.jitsiURL = this.sanitizer.bypassSecurityTrustHtml(this.getJitsiHtmlString());
    this.showJitsiTeamConf = true;
  }

  closeJitsiTeamConf(): void {
    this.showJitsiTeamConf = false;
  }

  bedsideCollaborator(): void {
    this.bedsideURL = this.sanitizer.bypassSecurityTrustResourceUrl(
      'https://vcmfieldhospitalcollaboration-7p6dedpmrq-uw.a.run.app/login/' + this.assignment.assignmentId
    );
    this.showBedsideCollaborator = true;
  }

  closeBedsideCollaborator(): void {
    this.showBedsideCollaborator = false;
  }

  getJitsiHtmlString(): string {
    this.jitsiHtml =
      '<!DOCTYPE html>\n' +
      '<html itemscope itemtype="http://schema.org/Product" prefix="og: http://ogp.me/ns#" xmlns="http://www.w3.org/1999/html">\n' +
      '    <head>\n' +
      '        <meta charset="utf-8">\n' +
      '        <meta http-equiv="content-type" content="text/html;charset=utf-8">\n' +
      '    </head>\n' +
      '    <body>\n' +
      '        <script src="https://jitsi.saperi.ai/external_api.js"></script>\n' +
      '        <script>\n' +
      '            var domain = "jitsi.saperi.ai";\n' +
      '            var options = {\n' +
      '                roomName: "vcm-care-team-video-' +
      this.assignment.assignmentId +
      '",\n' +
      '                width: 850,\n' +
      '                height: 600,\n' +
      '                parentNode: undefined,\n' +
      '                configOverwrite: {},\n' +
      '                interfaceConfigOverwrite: {}\n' +
      '            }\n' +
      '            var api = new JitsiMeetExternalAPI(domain, options);\n' +
      '        </script>\n' +
      '    </body>\n' +
      '</html>';
    return this.jitsiHtml;
  }

  careTeamConf(): void {
    window.open('https://openvidu.saperi.ai/#/vcm-care-team-conf-patient-' + this.assignment.assignmentId, '_blank');
  }

  closeLocalTeamVideo(): void {
    this.localTeamId = '';
    this.showLocalTeamVideo = false;
  }

  getVisibilityDevicesCss(): string {
    if (this.showDevices) {
      return '';
    }
    return 'visibility: hidden; display:none';
  }

  getPositionLCTDevicesCss(): string {
    if (this.showDevices) {
      return 'position:inherit';
    }
    return 'position:absolute; top:541px; width:128px;';
  }

  getPositionRCTDevicesCss(): string {
    if (this.showDevices) {
      return 'position:inherit';
    }
    return 'position:absolute; top:888px; width:128px;';
  }

  getPositionHWStatusesDevicesCss(): string {
    if (this.showDevices) {
      return 'position:inherit';
    }
    return 'position:relative; margin-top:8px;';
  }

  getIframeCss(): string {
    if (this.showClinicalSummary) {
      return '';
    }
    return 'visibility: hidden; display:none';
  }

  /**
   * See if this is needed
   */
  closeClinicalSummary(): void {
    this.showClinicalSummary = false;
  }

  /**
   * The p-dialog in the html should have an id like this: #idElem
   * That will be used in this way: (onShow)="showDialogMaximized(idElem)"
   * @param dialog
   */
  showDialogMaximized(dialog: Dialog): void {
    dialog.maximize();
  }

  iconClass(percentage: number): string {
    if (percentage < 25) {
      return 'red-percentage-color blinking';
    }
    return 'regular-percentage-color';
  }

  cdsColorClass(cds1: string, locusOfControl: string): string {
    let classes = 'cds-color';
    if (cds1.startsWith('No')) {
      classes = 'no-cds-color';
    }
    if (locusOfControl === 'Local') {
      classes += ' label-separator';
    }
    return classes;
  }

  locusClass(locusOFControl: string): string {
    if (locusOFControl.startsWith('Local')) {
      return 'local-color';
    }
    return 'remote-color';
  }

  ventModes(ventMode: string): void {
    // Disabling change vent modes modal until new requirements
    //return;
    this.showVentModes = true;
  }
  closeVentModeDialog(): void {
    // this.ventMode = this.newVentMode;
    this.showVentModes = false;
  }
  closeModeDialog(): void {
    this.showVentModes = false;
  }
  applyVentMode(): void {
    // this.newVentMode = this.ventMode;
    this.closeVentModeDialog();
  }

  /**
   * This method do not work on modern day browsers that can prevent you in the settings to not be able
   * to resize the window. There is no way around that. BTW next code should redimension a window if the
   * browser allows that.
   *
   */
  ensureMinimumWindowSize(width: number, height: number): void {
    const tooThin = width > window.innerWidth;
    const tooShort = height > window.innerHeight;

    if (tooThin || tooShort) {
      window.resizeTo(width, height);
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any): void {
    const screenWidth = event.target.innerWidth;
    const screenHeight = event.target.innerHeight;
    clearTimeout(this.resizeTimer);
    this.resizeTimer = setTimeout(() => {
      this.ensureMinimumWindowSize(1280, 1024);
    }, 250);
  }

  getMasterDevice(devicesMap: IDeviceCtx[]): IDeviceCtx | undefined {
    if (devicesMap !== undefined && devicesMap.length === 3) {
      if (devicesMap[0].deviceInfo.manufacturer === this.NEUROWAVE && devicesMap[0].deviceInfo.parent_device_id === undefined) {
        return devicesMap[0];
      } else if (devicesMap[1].deviceInfo.manufacturer === this.NEUROWAVE && devicesMap[1].deviceInfo.parent_device_id === undefined) {
        return devicesMap[1];
      }
      return devicesMap[2];
    }
    if (devicesMap.length > 0) return devicesMap[0];
    return undefined;
  }

  setupPumpHwId(deviceCtx: IDeviceCtx): void {
    if (deviceCtx.deviceInfo.manufacturer === this.NEUROWAVE) {
      this.idPumpHwID['pump1'] = deviceCtx.deviceInfo.id;
      this.idPumpHwID['pump2'] = deviceCtx.deviceInfo.id;
    }
  }
}
