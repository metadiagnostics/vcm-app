/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Injectable, NgModule } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, Router, RouterModule, Routes } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { MonitorAssignmentsComponent } from './monitor-assignments.component';
import { MonitorViewComponent } from './monitor-view.component';
import { MonitorDrawerComponent } from './monitor-drawer.component';
import { MonitorService } from './monitor.service';
import { IMonitorAssignment } from 'app/shared/model/ext/monitor-assignment.models';

import { DRAWER_OUTLET } from 'app/drawer/drawer.route';

@Injectable({ providedIn: 'root' })
export class AssignmentResolve implements Resolve<IMonitorAssignment> {
  constructor(private service: MonitorService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMonitorAssignment> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.findAssignment(id).pipe(
        flatMap((assignment: HttpResponse<IMonitorAssignment>) => {
          if (assignment.body) {
            return of(assignment.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    this.router.navigate(['404']);
    return EMPTY;
  }
}

const monitorRoute: Routes = [
  {
    path: 'monitor',
    component: MonitorAssignmentsComponent,
    data: {
      authorities: [],
      pageTitle: 'vcmApp.monitor.home.title'
    }
  },
  {
    path: 'monitor/:id/view',
    component: MonitorViewComponent,
    resolve: {
      assignment: AssignmentResolve
    },
    data: {
      authorities: [],
      pageTitle: 'vcmApp.monitor.home.title'
    }
  },
  {
    path: 'monitor/:id/drawer',
    component: MonitorDrawerComponent,
    outlet: DRAWER_OUTLET,
    resolve: {
      assignment: AssignmentResolve
    }
  }
];

const ROUTE = [...monitorRoute];
@NgModule({
    imports: [RouterModule.forChild(ROUTE)],
    exports: [RouterModule]
})
export class MonitorRoute {
}
