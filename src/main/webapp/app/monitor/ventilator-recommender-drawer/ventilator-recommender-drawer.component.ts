/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { saveAs } from 'file-saver';
import { parse as parseContentDisposition } from 'content-disposition';

import { JhiAlertService } from 'ng-jhipster';
import { IMonitorAssignment } from 'app/shared/model/ext/monitor-assignment.models';
import { VentilatorRecommenderType } from 'app/shared/model/enumerations/ventilator-recommender-type.model';
import { MonitorService } from '../monitor.service';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'jhi-ventilator-recommender-drawer',
  templateUrl: './ventilator-recommender-drawer.component.html',
  styleUrls: ['./ventilator-recommender-drawer.component.scss']
})
export class VentilatorRecommenderDrawerComponent implements OnInit {
  @Input()
  assignment: IMonitorAssignment = { assignmentId: -1 };

  /**
   * If there is an error, the UI doesn't show any control.
   */
  error?: string;

  ventilatorRecommenderTypes = [VentilatorRecommenderType.AI, VentilatorRecommenderType.RULE_BASED];

  private dateTimeFormat = 'yyyy-MM-ddTHH:mm';

  logsFromDateTime = '';
  logsToDateTime = '';

  constructor(
    private datePipe: DatePipe,
    protected monitorService: MonitorService,
    protected alertService: JhiAlertService,
    protected translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.logsFromDateTime = this.previousDay();
    this.logsToDateTime = this.tomorrow();
  }

  onVentilatorRecommenderTypeChange(): void {
    this.monitorService
      .changeVentilatorRecommenderType(this.assignment.assignmentId, this.assignment.ventilatorRecommenderType!)
      .subscribe();
  }

  onVentilatorControlModeChange(): void {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    this.monitorService.changeVentilatorRecommenderMode(this.assignment.assignmentId, this.assignment.ventilatorControlType!).subscribe();
  }

  getVentilatorsLogsAsCsv(): void {
    const fromMoment = moment(this.logsFromDateTime).toDate();
    const toMoment = moment(this.logsToDateTime).toDate();

    this.monitorService
      .getVentilatorsLogsAsCsv(this.assignment.assignmentId, fromMoment.toISOString(), toMoment.toISOString())
      .subscribe(csv => {
        if (csv.body) {
          const filename = this.getFilename(csv.headers, 'assignment-' + this.assignment.assignmentId + '-ventilator-logs.csv');
          saveAs(csv.body, filename);
        } else {
          this.error = 'Error retrieving CSV Logs';
        }
      });
  }

  private previousDay(): string {
    const date = new Date();
    date.setDate(date.getDate() - 1);
    return this.datePipe.transform(date, this.dateTimeFormat)!;
  }

  private tomorrow(): string {
    const date = new Date();
    date.setDate(date.getDate() + 1);
    return this.datePipe.transform(date, this.dateTimeFormat)!;
  }

  private getFilename(headers: HttpHeaders, defaultFilename: string): string {
    const contentDisposition = headers.get('content-disposition');
    if (!contentDisposition) {
      return defaultFilename;
    }

    try {
      const cd = parseContentDisposition(contentDisposition);
      return cd.parameters.filename || defaultFilename;
    } catch (e) {
      return defaultFilename;
    }
  }
}
