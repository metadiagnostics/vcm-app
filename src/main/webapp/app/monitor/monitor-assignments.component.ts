/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';

import { IMonitorAssignment, MonitorAssignmentUtils } from 'app/shared/model/ext/monitor-assignment.models';
import { MonitorService } from './monitor.service';
import { PulseSimulationsService } from 'app/shared/services/pulse/pulse-simulations.service';
import { Subscription } from 'rxjs';
import { PulseStatesService } from 'app/shared/services/pulse/pulse-states.service';
import { faSyncAlt } from '@fortawesome/free-solid-svg-icons/faSyncAlt';
@Component({
  selector: 'jhi-monitor',
  templateUrl: './monitor-assignments.component.html',
  styleUrls: ['./monitor-assignments.component.scss']
})
export class MonitorAssignmentsComponent implements OnInit, OnDestroy {
  assignments?: IMonitorAssignment[];
  simsSubscription?: Subscription;
  hwId_assignId = {};
  assignId_hwId = {};
  refresh = faSyncAlt;

  private audio = new Audio();

  constructor(
    protected monitorService: MonitorService,
    private pulseService: PulseSimulationsService,
    private pulseStateService: PulseStatesService
  ) {}

  ngOnInit(): void {
    this.loadAll();
    this.audio.src = '/content/silence.mp3';
  }

  ngOnDestroy(): void {
    if (this.simsSubscription !== undefined) {
      this.simsSubscription.unsubscribe();
    }
  }

  loadAll(): void {
    this.monitorService.queryAssignments().subscribe((res: HttpResponse<IMonitorAssignment[]>) => {
      this.assignments = res.body || [];
      // assignmentId -> hardwareId
      for (const assignment of this.assignments) {
        let deviceName = '';
        const devices = MonitorAssignmentUtils.getUniqueDevices(assignment);
        for (const device of devices) {
          if (device.model === 'Pulse Simulator') {
            deviceName = device.hardwareId;
            break;
          }
        }

        this.hwId_assignId[deviceName] = assignment.assignmentId;
        this.assignId_hwId[assignment.assignmentId] = deviceName;
      }
      this.pulseStateService.enabled().subscribe(r => {
        if (r.body === true) {
          this.simsSubscription = this.pulseService.querySims().subscribe(sims => {
            // console.log(sims.body || []); // eslint-disable-line no-console
            this.getPatientInfo(sims.body || []);
          });
        }
      });
    });
  }

  trackId(index: number, item: IMonitorAssignment): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.assignmentId!;
  }

  deviceNames(assignment: IMonitorAssignment): string {
    return MonitorAssignmentUtils.getUniqueDevices(assignment)
      .sort((d1, d2) => {
        return d1.hardwareId.localeCompare(d2.hardwareId);
      })
      .map(d => d.hardwareId + ' (' + d.model + ')')
      .join(' / ');
  }

  // This is only implemented for pulse devices
  patientName(assignment: IMonitorAssignment): string {
    return assignment.patientId === undefined?'':assignment.patientId;
  }

  private getPatientInfo(sims: string[]): void {
    for (const sim of sims) {
      this.pulseService.setExistingPatientInfo(sim);
    }
  }

  public initSoundAndRefreshListeners(assignmentId: number): void {
    this.audio.play();
    this.monitorService.refreshListeners(assignmentId).subscribe(); //subscribe to void ... just call it
  }

  public refreshListeners(assignmentId: number): void {
    this.monitorService.refreshListeners(assignmentId).subscribe(); //subscribe to void ... just call it
  }
}
