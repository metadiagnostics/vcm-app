/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

export interface IVentModel {
  name: string;
  unit: string;
  alarmMax: number;
  alarmMin: number;
  dataKey: string;
  requestDataKey: string;
  responseDataKey: string;
  selectors: string[];
  step: number;
  hwId: string;
  codeId: string;
  alarmMinCodeID: string;
  alarmMaxCodeID: string;
  order: number;
  enumValues: IEnumValues;
  displayString: string;
  type: string;
}

export class IEnumValues extends Array<IEnumValue> {}

export interface IEnumValue {
  group: string;
  enumunerationName: string; // TODO: wrong name on the BE object refactor it
  position: number;
  internalValue: string;
  humanLabel: string;
  shortLabel: string;
  longLabel: string;
  constrainedValue: boolean;
  setValue: string;
}

export class IVentModels extends Array<IVentModel> {}
