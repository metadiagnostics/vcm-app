/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

export interface IAlarmModel {
  id: string
  name: string;
  min: string;
  max: string;
  minSetting: string;
  maxSetting: string;
}

export interface IAlarmWarningModel {
  id: string;
  title: string;
  body: string;
  ack: boolean;
  muted: boolean;
  severity: string;
}

export class AlarmModel implements IAlarmModel {
  constructor(public name: string,
              public id: string,
              public min: string,
              public max: string,
              public minSetting: string,
              public maxSetting: string) {}
}

export class AlarmWarningModel implements IAlarmWarningModel {
  constructor(
    public id: string,
    public title: string,
    public body: string,
    public ack: boolean,
    public muted: boolean,
    public severity: string
  ) {}
}
