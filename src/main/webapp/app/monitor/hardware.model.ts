/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { HardwareDataBlockComponentType } from 'app/shared/components/hardware-data-block/hardware-data-block.component';

export interface IHWModel {
  order: number;
  name: string;
  unit: string;
  alarmMax: number;
  alarmMin: number;
  dataKeys: string[];
  selectors: string[];
  ddScope: string;
  data: any;
  expanded: boolean;
  physiologicMonitors: boolean;
  ventFlow: boolean;
  ventSettings: boolean;
  ventMonitors: boolean;
  reduced: boolean;
  selected: boolean;
  type: HardwareDataBlockComponentType;
  xlabel: string;
  ylabel: string;
  separator: string;
  chartMaxDataLength: number;
  yaxisMin: number;
  yaxisMax: number;
}

export class IHWModels extends Array<IHWModel> {}
