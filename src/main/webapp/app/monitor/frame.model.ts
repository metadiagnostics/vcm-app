/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

export interface IDeviceFrame {
  id: string;
  hardwareId: string;
  frame: IFrame;
  aggregateFrame: IFrame;
}

export interface IDeviceMap {
  [key: string]: IDeviceCtx;
}

export interface IMeasureValueMap {
  [key: string]: IMeasureValue;
}

export interface ISettingMap {
  [key: string]: ISetting;
}

// Less restrictive interface to be used on common methods/functions
export interface IValueMap {
  [key: string]: IValue;
}

export interface IMeasureWaveFormMap {
  [key: string]: IMeasureWaveForm;
}

export interface IMeasureWaveForm {
  source_timestamp: Date;
  device_relative_timestamp: Date;
  metric_id: string;
  array: IListDoubles;
  state: number;
}

export interface IListDoubles extends Array<number> {}

export interface IValue {
  source_timestamp: Date;
  metric_id: string;
  unit_code: number;
  state: number;
  valueAsString: string;
  stringValue: string;
}

export interface IMeasureValue extends IValue {
  device_relative_timestamp: Date;
}

export interface ISetting extends IValue {}

export interface IDeviceCtx {
  measureValues: IMeasureValueMap;
  settingValues: ISettingMap;
  measureWaveFormValues: IMeasureWaveFormMap;
  deviceInfo: IDeviceInfo;
}

export const enum DeviceStatus {
  DISCONNECTED = 'Disconnected',
  UNASSOCIATED = 'Unassociated',
  ASSOCIATING = 'associating',
  ASSOCIATED = 'associated',
  CONFIGURING = 'configuring',
  CONFIGURED = 'configured',
  OPERATING = 'operating',
  RE_INITIALIZING = 're_initializing',
  TERMINATING = 'terminating',
  DISASSOCIATING = 'disassociating',
  DISASSOCIATED = 'disassociated ',
  RE_CONFIGURING = 're_configuring',
  REMOTE_ACCESS_DISCONNECTED = 'Remote access disconnected'
}

export interface IDeviceInfo {
  id: string;
  manager_id: string;
  operating_mode: number;
  operating_mode_code: string;
  status: DeviceStatus;
  system_type: string;
  system_model_number: string;
  manufacturer: string;
  line_frequency: number;
  altitude_meters: number;
  batteryInfo: BatteryInfo;
  parent_device_id: string;
  onMainsPower: boolean;
}

export interface BatteryInfo extends Array<IBattery> {}

export interface IBattery {
  powerLevel: number;
  present: boolean;
  status: number;
  unitId: number;
}

export interface IFrame {
  devices: IDeviceMap;
  events: AggregatorEvents;
}

export interface AggregatorEvents extends Array<AggregatorEvent> {}

export interface AggregatorEvent {
  topic: string;
  user: string;
}
