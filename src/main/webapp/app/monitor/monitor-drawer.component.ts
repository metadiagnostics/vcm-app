/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { AfterViewInit, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMonitorAssignment } from 'app/shared/model/ext/monitor-assignment.models';
import { DeviceType } from 'app/shared/model/enumerations/device-type.model';
import { AlarmService } from 'app/core/alarm/alarm.service';
import { IAlarmWarningModel } from 'app/monitor/alarm.model';
import { Subject } from 'rxjs';

@Component({
  templateUrl: './monitor-drawer.component.html',
  styleUrls: ['./monitor-drawer.component.scss']
})
export class MonitorDrawerComponent implements OnInit, OnDestroy {
  assignment: IMonitorAssignment = { assignmentId: -1 };

  hasPulseDevices = false;

  @Input()
  hasAlarms = false;

  private alarmSubscription: Subject<IAlarmWarningModel[]>;

  @Input()
  public alarms: IAlarmWarningModel[] = [];

  @Input()
  volumeVal = 5;

  constructor(protected activatedRoute: ActivatedRoute, protected alarmService: AlarmService) {
    this.alarmSubscription = this.alarmService.alarmsSubscription();
    this.alarmService.setAudioVolume(this.volumeVal / 100);
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ assignment }) => {
      this.assignment = assignment;
      this.hasPulseDevices =
        this.assignment.devices?.some(d => {
          return d.device.type === DeviceType.PULSE;
        }) || false;
    });

    this.alarmSubscription.subscribe(alarms => {
      this.hasAlarms = alarms.length > 0;
      this.alarms = alarms;
    });
  }

  ngOnDestroy(): void {
    this.alarmService.getAudio().pause();
  }

  volumeChange(e: any): void {
    this.alarmService.setAudioVolume(e.value / 100);
  }

}
