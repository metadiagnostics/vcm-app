/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, Input, OnInit, AfterViewInit } from '@angular/core';

import { IScalarValue, ScalarValue } from 'app/shared/model/ext/scalar-value.model';
import { IVentilatorRecommendationResponse } from 'app/shared/model/ext/ventilator-recommender.models';
import { IVentilatorConfigurationChangeRequest } from 'app/shared/model/ext/ventilator-configuration.models';
import { DataBlockComponent } from 'app/shared/components/data-block/data-block.component';

@Component({
  selector: 'jhi-vp-data-block',
  templateUrl: './ventilator-parameter-data-block.component.html',
  styleUrls: ['./ventilator-parameter-data-block.component.scss']
})
export class VentilatorParameterDataBlockComponent extends DataBlockComponent implements AfterViewInit, OnInit {
  @Input()
  public name = 'Name';

  @Input()
  public ventilatorResponseDataKey = '';

  @Input()
  public ventilatorChangeRequestDataKey = '';

  @Input()
  public value?: IScalarValue;

  @Input()
  public rvalue?: IScalarValue;

  @Input()
  public step = 1;

  @Input()
  public lowerBound: number | undefined;

  @Input()
  public lowerBoundClass = 'lower';

  @Input()
  public upperBound: number | undefined;

  @Input()
  public upperBoundClass = 'upper';

  @Input()
  public normalBoundClass = 'normal';

  @Input()
  public differentBoundClass = 'different';

  @Input()
  public disabled = false;

  valueClass = 'normal';

  @Input()
  snapshotScalarValue?: IScalarValue;

  @Input()
  public max: number | undefined;

  @Input()
  public min: number | undefined;

  INITIAL_RECOMMENDER_VALUE: IScalarValue = new ScalarValue(-1, '');

  oldValue?: IScalarValue;

  constructor() {
    super();
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {}

  public createSnapshot(): void {
    this.snapshotScalarValue = this.value;
  }

  public restoreSnapshot(): void {
    if (this.snapshotScalarValue) {
      this.value = this.snapshotScalarValue;
    }
  }

  public configureFromResponse(response: IVentilatorRecommendationResponse): void {
    // callback from recommender
    // Both values will have same response value since we should not preserve the user input
    this.rvalue = response[this.ventilatorResponseDataKey] as IScalarValue;
    this.value = response[this.ventilatorResponseDataKey] as IScalarValue;
    if (this.snapshotScalarValue?.value !== undefined && this.value?.value !== this.snapshotScalarValue.value) {
      this.valueClass = this.differentBoundClass;
    } else {
      this.valueClass = this.normalBoundClass;
    }
  }

  public configureRequest(request: IVentilatorConfigurationChangeRequest): void {
    if (this.value) {
      request[this.ventilatorChangeRequestDataKey] = this.value;
    }
  }

  public resetRecommendation(): void {
    this.rvalue = this.INITIAL_RECOMMENDER_VALUE;
  }

  public applyRecommendedValue(): void {
    if (this.value !== undefined && this.rvalue !== undefined) {
      this.createSnapshot();
      this.value = this.rvalue;
    }
  }

  public updateAttr(attr: string, data: any): void {
    if (attr === 'currentValue') {
      const updatedValue = data[this.ventilatorResponseDataKey] as IScalarValue;
      // The value was not present
      if (updatedValue === null) {
        return;
      }
      if (updatedValue !== undefined && updatedValue.value !== this.snapshotScalarValue?.value) {
        // This updates also the current vent value on Numeric data block (see )
        this.snapshotScalarValue = updatedValue;
      }
      if (this.snapshotScalarValue?.value !== undefined && this.value?.value !== this.snapshotScalarValue.value) {
        this.valueClass = this.differentBoundClass;
      } else {
        this.valueClass = this.normalBoundClass;
      }
    }
  }
}
