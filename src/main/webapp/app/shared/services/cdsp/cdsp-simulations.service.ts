/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Injectable } from '@angular/core';
import { HttpResponse, HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';

type StringArrayResponseType = HttpResponse<string[]>;

@Injectable({ providedIn: 'root' })
export class CdspSimulationsService {
  public resourceUrl = SERVER_API_URL + 'api/ext/cdsp/sim';
  public SIM_STARTED = 'simulation-started';
  public SIM_STOPPED = 'simulation-stopped';
  private simSubject = new Subject<String>();

  constructor(protected http: HttpClient) {}

  queryScenarios(): Observable<StringArrayResponseType> {
    const options = createRequestOption();
    return this.http.get<string[]>(`${this.resourceUrl}/scenarios`, { params: options, observe: 'response' });
  }

  start(name: string, initialStateName: string, scenario: string, steps: number, increment: number): Observable<{}> {
    const options = createRequestOption({
      initialStateName,
      scenarioName: scenario,
      steps,
      increment
    });
    this.simSubject.next(this.SIM_STARTED);
    return this.http.post<{}>(`${this.resourceUrl}/${name}/start`, '', { params: options, observe: 'response' });
  }

  stop(name: string): Observable<{}> {
    const options = createRequestOption();
    this.simSubject.next(this.SIM_STOPPED);
    return this.http.post<{}>(`${this.resourceUrl}/${name}/stop`, '', { params: options, observe: 'response' });
  }

  onSimChange(): Observable<String> {
    return this.simSubject.asObservable();
  }

  run(name: string, steps: number, increment: number): Observable<{}> {
    const options = createRequestOption({
      steps,
      increment
    });
    return this.http.post<{}>(`${this.resourceUrl}/${name}/run`, '', { params: options, observe: 'response' });
  }

  terminate(name: string): Observable<{}> {
    const options = createRequestOption();
    return this.http.post<{}>(`${this.resourceUrl}/${name}/terminate`, '', { params: options, observe: 'response' });
  }
}
