/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { EventEmitter, Injectable, Output } from '@angular/core';
import { HttpResponse, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ISimulationInfo } from 'app/shared/model/ext/pulse.models';

type ISimulationInfoResponseType = HttpResponse<ISimulationInfo>;
type StringArrayResponseType = HttpResponse<string[]>;

@Injectable({ providedIn: 'root' })
export class PulseSimulationsService {
  public resourceUrl = SERVER_API_URL + 'api/ext/pulse/sim';

  @Output() simulationInfo: EventEmitter<ISimulationInfo> = new EventEmitter();

  simName_patientName = {};

  constructor(protected http: HttpClient) {}

  queryInfo(name: string): Observable<ISimulationInfoResponseType> {
    const options = createRequestOption();
    return this.http.get<ISimulationInfo>(`${this.resourceUrl}/${name}/info`, { params: options, observe: 'response' });
  }

  querySims(): Observable<StringArrayResponseType> {
    const options = createRequestOption();
    return this.http.get<string[]>(`${this.resourceUrl}/`, { params: options, observe: 'response' });
  }

  // Send the Simulation Information to all simulationInfo event subscribers
  public notify(info?: ISimulationInfo): void {
    this.simulationInfo.emit(info);
    // this service will store a simple hash simName -> patientName
    this.extractNameAndId(info);
  }

  start(name: string, initialStateName: string, disease: string, steps: number, increment: number): Observable<{}> {
    const options = createRequestOption({
      initialStateName,
      diseaseName: disease,
      steps,
      increment
    });

    return this.http.post<{}>(`${this.resourceUrl}/${name}/start`, '', { params: options, observe: 'response' });
  }

  stop(name: string): Observable<{}> {
    const options = createRequestOption();
    return this.http.post<{}>(`${this.resourceUrl}/${name}/stop`, '', { params: options, observe: 'response' });
  }

  run(name: string, steps: number, increment: number): Observable<{}> {
    const options = createRequestOption({
      steps,
      increment
    });
    return this.http.post<{}>(`${this.resourceUrl}/${name}/run`, '', { params: options, observe: 'response' });
  }

  terminate(name: string): Observable<{}> {
    const options = createRequestOption();
    return this.http.post<{}>(`${this.resourceUrl}/${name}/terminate`, '', { params: options, observe: 'response' });
  }

  clearPatient(name: string): void {
    delete this.simName_patientName[name];
  }

  extractNameAndId(simInfo?: ISimulationInfo): void {
    if (simInfo === undefined) return;
    let patientInfo = '';
    // Example:  Doe, Jane  (ID#9092203421)
    if (simInfo.initialStateInfo != null) {
      // See if the Information has tags
      if (simInfo.initialStateInfo.patient !== undefined && simInfo.initialStateInfo.patient.name) {
        const patient = simInfo.initialStateInfo.patient.name;
        patientInfo = patient;
      }
      // Create an Id from the simName only if the scenario is running
      if (simInfo.name !== undefined && simInfo.name.length > 0) {
        patientInfo += ' (ID#' + this.convertLetterToNumber(simInfo.name) + ')';
        this.simName_patientName[simInfo.name] = patientInfo;
      }
    }
  }

  private convertLetterToNumber(str: string): string {
    let out = 0;
    for (let i = 0; i < str.length; i++) {
      const char = str.charCodeAt(i);
      out = (out << 5) - out + char; // eslint-disable-line no-bitwise
      // Convert to 32bit integer
      out = out & out; // eslint-disable-line no-bitwise
    }
    let sout = '' + out;
    // console.log(str + ' ' + out + '  ' + sout); // eslint-disable-line no-console
    const maxDigits = 10;
    if (sout.length > maxDigits) {
      sout = sout.substr(sout.length - maxDigits, sout.length - 1);
    }
    return sout;
  }

  public getPatientName(simInfo?: ISimulationInfo): string {
    if (simInfo !== undefined && simInfo.name !== undefined && simInfo.name.length > 0) {
      return this.simName_patientName[simInfo.name];
    }
    return '';
  }

  public setExistingPatientInfo(simName: string): void {
    // call get Info here
    this.queryInfo(simName).subscribe(info => {
      this.extractNameAndId(info.body || undefined);
    });
  }

  public getPatientNameFromSimName(simName: string): string {
    if (simName !== undefined && simName.length > 0) {
      return this.simName_patientName[simName];
    }
    return '';
  }
}
