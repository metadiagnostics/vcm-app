/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { Subject, Observable } from 'rxjs';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'webstomp-client';

import { IHardwareBloodGasData, IHardwareBloodGasDatas } from 'app/shared/model/ext/hardware-blood-gas-data.model';
import {
  IHardwareDemographicsEvent,
  IHardwareVitalsEvent,
  IHardwareVentilatorEvent,
  IHardwareVentilatorEvents,
  IHardwareDemographicsEvents,
  IHardwareVitalsEvents
} from 'app/shared/model/ext/hardware-data.model';
import { IHardwareStatus } from 'app/shared/model/ext/hardware-status.model';
import { IHardwareMessage } from 'app/shared/model/ext/hardware-message.model';
import { IPumpModel } from 'app/shared/model/pump.model';
import { IHWModel, IHWModels } from 'app/monitor/hardware.model';
import { IVentModels } from 'app/monitor/ventilator.model';
import { IIdMap, IMetricMap, IUnitMap } from 'app/monitor/metric.model';
import { IDeviceFrame } from 'app/monitor/frame.model';
import {IAlarmModel, IAlarmWarningModel} from "app/monitor/alarm.model";

export class MonitorHardwareConnectionError {
  constructor(public msgKey: string, public params?: any) {}
}

export class MonitorHardwareConnection {
  private stompSubscriptions: Stomp.Subscription[] = [];

  private demographicsDataSubject: Subject<IHardwareDemographicsEvent> = new Subject();
  private demographicsDataSubjectElapsed: Subject<IHardwareDemographicsEvents> = new Subject();
  private vitalsDataSubject: Subject<IHardwareVitalsEvent> = new Subject();
  private vitalsDataSubjectElapsed: Subject<IHardwareVitalsEvents> = new Subject();
  private vitalsDataSettingsSubject: Subject<IHWModels> = new Subject();
  private ventilatorDataSubject: Subject<IHardwareVentilatorEvent> = new Subject();
  private ventilatorDataSubjectElapsed: Subject<IHardwareVentilatorEvents> = new Subject();
  private ventilatorDataRoSettingsSubject: Subject<IHWModels> = new Subject();
  private ventilatorDataRwSettingsSubject: Subject<IVentModels> = new Subject();
  private bloodGasDataSubject: Subject<IHardwareBloodGasData> = new Subject();
  private bloodGasDataSubjectElapsed: Subject<IHardwareBloodGasDatas> = new Subject();
  private statusSubject: Subject<IHardwareStatus> = new Subject();
  private messagesSubject: Subject<IHardwareMessage> = new Subject();
  private connectionErrorSubject: Subject<MonitorHardwareConnectionError> = new Subject();
  private pumpSubject: Subject<IPumpModel> = new Subject();
  private alarmWarningsSubject: Subject<IAlarmWarningModel[]> = new Subject();
  private alarmsSubject: Subject<IAlarmModel[]> = new Subject();
  private deviceFrameSubject: Subject<IDeviceFrame> = new Subject();
  private datasetSubject: Subject<IMetricMap> = new Subject();
  private unitSubject: Subject<IUnitMap> = new Subject();
  private idSettingsSubject: Subject<IIdMap> = new Subject();
  private idMeasuresSubject: Subject<IIdMap> = new Subject();

  constructor(private client: Stomp.Client) {
    const originalOnCloseFunction = client.ws.onclose;
    client.ws.onclose = () => {
      this.connectionErrorSubject.next(new MonitorHardwareConnectionError('global.messages.error.connectionLost'));
      originalOnCloseFunction();
    };
  }

  subscribeToAll(hardwareId: string): void {
    // eslint-disable-next-line no-console
    // console.log("[INVOKED] subscribeToAll()");
    this.subscribe(hardwareId, true, true, true);
  }

  subscribe(hardwareId: string, subscribeToData: boolean, subscribeToStatus: boolean, subscribeToMessages: boolean): void {
    // eslint-disable-next-line no-console
    // console.log("[INVOKED] subscribe()");

    // eslint-disable-next-line no-console
    // console.log("[EXECUTED] Creating Subscriptions");
    if (subscribeToData) {
      this.stompSubscriptions?.push(
        this.client.subscribe('/topic/hardware/' + hardwareId + '/demographics-data', (data: Stomp.Message) => {
          this.demographicsDataSubject.next(JSON.parse(data.body));
        })
      );
      this.stompSubscriptions?.push(
        this.client.subscribe('/topic/hardware/' + hardwareId + '/demographics-data-elapsed', (data: Stomp.Message) => {
          this.demographicsDataSubjectElapsed.next(JSON.parse(data.body));
        })
      );
      this.stompSubscriptions?.push(
        this.client.subscribe('/topic/hardware/' + hardwareId + '/vitals-data', (data: Stomp.Message) => {
          this.vitalsDataSubject.next(JSON.parse(data.body));
        })
      );
      this.stompSubscriptions?.push(
        this.client.subscribe('/topic/hardware/' + hardwareId + '/vitals-data-elapsed', (data: Stomp.Message) => {
          this.vitalsDataSubjectElapsed.next(JSON.parse(data.body));
        })
      );
      this.stompSubscriptions?.push(
        this.client.subscribe('/topic/hardware/' + hardwareId + '/vitals-data/settings', (data: Stomp.Message) => {
          this.vitalsDataSettingsSubject.next(JSON.parse(data.body));
        })
      );
      this.stompSubscriptions?.push(
        this.client.subscribe('/topic/hardware/' + hardwareId + '/ventilator-data', (data: Stomp.Message) => {
          this.ventilatorDataSubject.next(JSON.parse(data.body));
        })
      );
      this.stompSubscriptions?.push(
        this.client.subscribe('/topic/hardware/' + hardwareId + '/ventilator-data-elapsed', (data: Stomp.Message) => {
          this.ventilatorDataSubjectElapsed.next(JSON.parse(data.body));
        })
      );
      this.stompSubscriptions?.push(
        this.client.subscribe('/topic/hardware/' + hardwareId + '/ventilator-data/settings-ro', (data: Stomp.Message) => {
          this.ventilatorDataRoSettingsSubject.next(JSON.parse(data.body));
        })
      );
      this.stompSubscriptions?.push(
        this.client.subscribe('/topic/hardware/' + hardwareId + '/ventilator-data/settings-rw', (data: Stomp.Message) => {
          this.ventilatorDataRwSettingsSubject.next(JSON.parse(data.body));
        })
      );
      this.stompSubscriptions?.push(
        this.client.subscribe('/topic/hardware/' + hardwareId + '/blood-gas-data', (data: Stomp.Message) => {
          this.bloodGasDataSubject.next(JSON.parse(data.body));
        })
      );
      this.stompSubscriptions?.push(
        this.client.subscribe('/topic/hardware/' + hardwareId + '/blood-gas-data-elapsed', (data: Stomp.Message) => {
          this.bloodGasDataSubjectElapsed.next(JSON.parse(data.body));
        })
      );
      this.stompSubscriptions?.push(
        this.client.subscribe('/topic/hardware/' + hardwareId + '/pump-data', (data: Stomp.Message) => {
          this.pumpSubject.next(JSON.parse(data.body));
        })
      );
      this.stompSubscriptions?.push(
        this.client.subscribe('/topic/hardware/' + hardwareId + '/device-frame', (data: Stomp.Message) => {
          this.deviceFrameSubject.next(JSON.parse(data.body));
        })
      );
      this.stompSubscriptions?.push(
        this.client.subscribe('/topic/dataset-mapping', (data: Stomp.Message) => {
          this.datasetSubject.next(JSON.parse(data.body));
        })
      );
      this.stompSubscriptions?.push(
        this.client.subscribe('/topic/unit-mapping', (data: Stomp.Message) => {
          this.unitSubject.next(JSON.parse(data.body));
        })
      );
      this.stompSubscriptions?.push(
        this.client.subscribe('/topic/id-settings-mapping', (data: Stomp.Message) => {
          this.idSettingsSubject.next(JSON.parse(data.body));
        })
      );
      this.stompSubscriptions?.push(
        this.client.subscribe('/topic/id-measures-mapping', (data: Stomp.Message) => {
          this.idMeasuresSubject.next(JSON.parse(data.body));
        })
      );
      this.stompSubscriptions?.push(
        this.client.subscribe('/topic/hardware/' + hardwareId + '/alarmwarnings', (data: Stomp.Message) => {
          this.alarmWarningsSubject.next(JSON.parse(data.body));
        })
      );
      this.stompSubscriptions?.push(
        this.client.subscribe('/topic/hardware/' + hardwareId + '/alarms', (data: Stomp.Message) => {
          this.alarmsSubject.next(JSON.parse(data.body));
        })
      );
    }
    if (subscribeToStatus) {
      this.stompSubscriptions?.push(
        this.client.subscribe('/topic/hardware/' + hardwareId + '/status', (data: Stomp.Message) => {
          this.statusSubject.next(JSON.parse(data.body));
        })
      );
    }
    if (subscribeToMessages) {
      this.stompSubscriptions?.push(
        this.client.subscribe('/topic/hardware/' + hardwareId + '/messages', (data: Stomp.Message) => {
          this.messagesSubject.next(JSON.parse(data.body));
        })
      );
    }
  }

  receiveBloodGasData(): Subject<IHardwareBloodGasData> {
    return this.bloodGasDataSubject;
  }

  receiveBloodGasDataElapsed(): Subject<IHardwareBloodGasDatas> {
    return this.bloodGasDataSubjectElapsed;
  }

  receiveDemographicsData(): Subject<IHardwareDemographicsEvent> {
    return this.demographicsDataSubject;
  }

  receiveDemographicsDataElapsed(): Subject<IHardwareDemographicsEvents> {
    return this.demographicsDataSubjectElapsed;
  }

  receiveVitalsData(): Subject<IHardwareVitalsEvent> {
    return this.vitalsDataSubject;
  }

  receiveVitalsDataElapsed(): Subject<IHardwareVitalsEvents> {
    return this.vitalsDataSubjectElapsed;
  }

  receiveVitalsDataSettings(): Subject<IHWModels> {
    return this.vitalsDataSettingsSubject;
  }

  receiveVentilatorData(): Subject<IHardwareVentilatorEvent> {
    return this.ventilatorDataSubject;
  }

  receiveVentilatorDataElapsed(): Subject<IHardwareVentilatorEvents> {
    return this.ventilatorDataSubjectElapsed;
  }

  receiveVentilatorRoDataSettings(): Subject<IHWModels> {
    return this.ventilatorDataRoSettingsSubject;
  }

  receiveVentilatorRwDataSettings(): Subject<IVentModels> {
    return this.ventilatorDataRwSettingsSubject;
  }

  receiveStatus(): Subject<IHardwareStatus> {
    return this.statusSubject;
  }

  receiveMessage(): Subject<IHardwareMessage> {
    return this.messagesSubject;
  }

  receiveConnectionErrorMessage(): Subject<MonitorHardwareConnectionError> {
    return this.connectionErrorSubject;
  }

  receivePumpData(): Subject<IPumpModel> {
    return this.pumpSubject;
  }

  receiveAlarmWarningsData(): Subject<IAlarmWarningModel[]> {
    return this.alarmWarningsSubject;
  }

  receiveAlarmsData(): Subject<IAlarmModel[]> {
    return this.alarmsSubject;
  }

  receiveDeviceFrameData(): Subject<IDeviceFrame> {
    return this.deviceFrameSubject;
  }

  receiveDatasetMappings(): Subject<IMetricMap> {
    return this.datasetSubject;
  }

  receiveDatasetUnits(): Subject<IUnitMap> {
    return this.unitSubject;
  }

  receiveSettingsIds(): Subject<IIdMap> {
    return this.idSettingsSubject;
  }

  receiveMeasuresIds(): Subject<IIdMap> {
    return this.idMeasuresSubject;
  }

  unsubscribe(): void {
    if (this.stompSubscriptions) {
      this.stompSubscriptions.forEach(s => {
        s.unsubscribe();
      });
      this.stompSubscriptions = [];
    }
  }

  disconnect(): void {
    this.unsubscribe();
    if (this.client) {
      if (this.client.connected) {
        this.client.disconnect();
      }
    }
  }

  sendNewVentilatorValue(hwId: string, metricId: string, data: string): void {
    if (this.client && this.client.connected) {
      this.client.send(
        '/topic/ventilator/' + hwId + '/change/' + metricId, // destination
        data, // body
        {} // header
      );
    }
  }

  sendNewPumpSetting(masterDeviceId: string, deviceId: string, metricId: string, data: string): void {
    if (this.client && this.client.connected) {
      this.client.send(
        '/topic/pump/' + masterDeviceId + '/device/'+ deviceId + '/change/' + metricId, // destination
        data, // body
        {} // header
      );
    }
  }

  sendNewSyncPumpSetting(masterDeviceId: string, deviceId: string, metricId1: string, data1: string, metricId2: string, data2: string): void {
    if (this.client && this.client.connected) {
      this.client.send(
        '/topic/pump/' + masterDeviceId + '/device/'+ deviceId + '/change1/' + metricId1 +  '/change2/' + metricId2, // destination
        data1+'::'+data2, // body
        {} // header
      );
    }
  }

  pauseInfusionPump(hwId: string, pump: string): void {
    if (this.client && this.client.connected) {
      this.client.send(
        '/topic/pump/' + hwId + '/pause/' + pump,
        '', // body
        {} // header
      );
    }
  }

  resumeInfusionPump(hwId: string, pump: string): void {
    if (this.client && this.client.connected) {
      this.client.send(
        '/topic/pump/' + hwId + '/resume/' + pump,
        '', // body
        {} // header
      );
    }
  }

  ventSettingReceived(): void {
    if (this.client && this.client.connected) {
      const url = this.client.ws._transport.url;
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const sessionId = new RegExp('/([^/]+)/websocket/hardware/([^/]+)/([^/]+)')!.exec(url)![3];
      this.client.send(
        '/topic/ventilator/settingreceived/' + sessionId,
        'true', // body
        {} // header
      );
    }
  }
}

@Injectable({ providedIn: 'root' })
export class MonitorHardwareService {
  constructor(private location: Location) {}

  /**
   * TODO: This function creates a WS Client for each invocation.
   * In theory, it should be possible to keep 1 client and reuse it
   * for the returned MonitorHardwareConnections.
   */
  connect(): Observable<MonitorHardwareConnection> {
    // eslint-disable-next-line no-console
    // console.log("[INVOKED] connect()");
    return new Observable(observer => {
      // building absolute path so that websocket doesn't fail when deploying with a context path
      let url = '/websocket/hardware';
      url = this.location.prepareExternalUrl(url);
      const socket: WebSocket = new SockJS(url);
      const stompClient = Stomp.over(socket);
      stompClient.debug = () => {};
      stompClient.connect(
        {},
        () => {
          // eslint-disable-next-line no-console
          // console.log("[EXECUTED] WS Connected");
          observer.next(new MonitorHardwareConnection(stompClient));
          observer.complete();
        },
        (e: any) => {
          // eslint-disable-next-line no-console
          // console.log("Error connecting to WS: ", e);
          if (!e || (e.type && e.type === 'close')) {
            observer.error(new MonitorHardwareConnectionError('global.messages.error.connectionLost'));
          } else {
            observer.error(
              new MonitorHardwareConnectionError('global.messages.error.unexpectedConnectionError', { msg: e.reason || 'Unknown Error' })
            );
          }
          observer.complete();
        }
      );
    });
  }
}
