/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { Subject, Observable } from 'rxjs';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'webstomp-client';

import { IVentilatorRecommendationResponse } from 'app/shared/model/ext/ventilator-recommender.models';
import {HttpXsrfTokenExtractor} from "@angular/common/http";

export class MonitorDataConnectionError {
  constructor(public msgKey: string, public params?: any) {}
}

export class MonitorDataConnection {
  private stompSubscriptions: Stomp.Subscription[] = [];

  private ventilatorRecommendationsResponseSubject: Subject<IVentilatorRecommendationResponse> = new Subject();
  private connectionErrorSubject: Subject<MonitorDataConnectionError> = new Subject();

  constructor(private client: Stomp.Client) {
    const originalOnCloseFunction = client.ws.onclose;
    client.ws.onclose = () => {
      this.connectionErrorSubject.next(new MonitorDataConnectionError('global.messages.error.connectionLost'));
      originalOnCloseFunction();
    };
  }

  subscribe(assigmentId: number): void {
    this.stompSubscriptions?.push(
      this.client.subscribe('/topic/monitor/' + assigmentId + '/ventilator-recommendation', (data: Stomp.Message) => {
        this.ventilatorRecommendationsResponseSubject.next(JSON.parse(data.body)['source']);
      })
    );
  }

  receiveVentilatorRecommendationResponse(): Subject<IVentilatorRecommendationResponse> {
    return this.ventilatorRecommendationsResponseSubject;
  }

  receiveConnectionErrorMessage(): Subject<MonitorDataConnectionError> {
    return this.connectionErrorSubject;
  }

  unsubscribe(): void {
    if (this.stompSubscriptions) {
      this.stompSubscriptions.forEach(s => {
        s.unsubscribe();
      });
      this.stompSubscriptions = [];
    }
  }

  public activateAssignment(assignmentId: number): void {
    if (this.client && this.client.connected) {
      this.client.send(
        '/topic/monitor/' + assignmentId + '/activate', // destination
        '', // body
        {} // header
      );
    }
  }

  public deactivateAssignment(assignmentId: number): void {
    if (this.client && this.client.connected) {
      this.client.send(
        '/topic/monitor/' + assignmentId + '/deactivate', // destination
        '', // body
        {} // header
      );
    }
  }

  disconnectAndDeactivate(assignmentId: number): void {
    this.deactivateAssignment(assignmentId);
    this.unsubscribe();

    if (this.client) {
      if (this.client.connected) {
        this.client.disconnect();
      }
    }
  }
}

@Injectable({ providedIn: 'root' })
export class MonitorDataService {
  constructor(private location: Location,
              private tokenExtractor: HttpXsrfTokenExtractor) {}

  /**
   * TODO: This function creates a WS Client for each invocation.
   * In theory, it should be possible to keep 1 client and reuse it
   * for the returned MonitorHardwareConnections.
   */
  connect(): Observable<MonitorDataConnection> {
    return new Observable(observer => {
      // building absolute path so that websocket doesn't fail when deploying with a context path
      let url = '/websocket/monitor';
      url = this.location.prepareExternalUrl(url);
      const socket: WebSocket = new SockJS(url);
      const stompClient = Stomp.over(socket);
      stompClient.debug = () => {};
      // const headerName = 'XSRF-TOKEN';
      const token = this.tokenExtractor.getToken() as string;
      let newHeaders = {};
      if (token !== null) {
        newHeaders = {"X-XSRF-TOKEN": token};
      }
      stompClient.connect(
        newHeaders,
        () => {
          // eslint-disable-next-line no-console
          // console.log("[EXECUTED] WS Connected");
          observer.next(new MonitorDataConnection(stompClient));
          observer.complete();
        },
        (e: any) => {
          // eslint-disable-next-line no-console
          // console.log("Error connecting to WS: ", e);
          if (!e || (e.type && e.type === 'close')) {
            observer.error(new MonitorDataConnectionError('global.messages.error.connectionLost'));
          } else {
            observer.error(
              new MonitorDataConnectionError('global.messages.error.unexpectedConnectionError', { msg: e.reason || 'Unknown Error' })
            );
          }
          observer.complete();
        }
      );
    });
  }
}
