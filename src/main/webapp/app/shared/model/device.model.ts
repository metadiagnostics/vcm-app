/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { IDeviceModel } from 'app/shared/model/device-model.model';
import { IAssignedDevice } from 'app/shared/model/assigned-device.model';

export interface IDevice {
  id?: number;
  hardwareId?: string;
  model?: IDeviceModel;
  assignments?: IAssignedDevice[];
}

export class Device implements IDevice {
  constructor(public id?: number, public hardwareId?: string, public model?: IDeviceModel, public assignments?: IAssignedDevice[]) {}
}
