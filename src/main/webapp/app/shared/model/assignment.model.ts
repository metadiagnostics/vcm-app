/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { IAssignedDevice } from 'app/shared/model/assigned-device.model';
import { VentilatorModeType } from 'app/shared/model/enumerations/ventilator-mode-type.model';
import { VentilatorRecommenderType } from 'app/shared/model/enumerations/ventilator-recommender-type.model';
import { VentilatorRecommenderControlType } from 'app/shared/model/enumerations/ventilator-recommender-control.model';

export interface IAssignment {
  id?: number;
  patientId?: string;
  mrn?: string;
  active?: boolean;
  ventilatorMode?: VentilatorModeType;
  ventilatorRecommenderType?: VentilatorRecommenderType;
  ventilatorControlType?: VentilatorRecommenderControlType;
  devices?: IAssignedDevice[];
}

export class Assignment implements IAssignment {
  constructor(
    public id?: number,
    public patientId?: string,
    public mrn?: string,
    public active?: boolean,
    public ventilatorMode?: VentilatorModeType,
    public ventilatorRecommenderType?: VentilatorRecommenderType,
    public ventilatorControlType?: VentilatorRecommenderControlType,
    public devices?: IAssignedDevice[]
  ) {
    this.active = this.active || false;
  }
}
