/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { IDeviceModel } from 'app/shared/model/device-model.model';
import { DeviceCapabilityType } from 'app/shared/model/enumerations/device-capability-type.model';

export interface IDeviceCapability {
  id?: number;
  type?: DeviceCapabilityType;
  deviceModels?: IDeviceModel[];
}

export class DeviceCapability implements IDeviceCapability {
  constructor(public id?: number, public type?: DeviceCapabilityType, public deviceModels?: IDeviceModel[]) {}
}
