/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { IAssignment } from 'app/shared/model/assignment.model';
import { IDevice } from 'app/shared/model/device.model';
import { IDeviceCapability } from 'app/shared/model/device-capability.model';

export interface IAssignedDevice {
  id?: number;
  assignment?: IAssignment;
  device?: IDevice;
  capability?: IDeviceCapability;
}

export class AssignedDevice implements IAssignedDevice {
  constructor(public id?: number, public assignment?: IAssignment, public device?: IDevice, public capability?: IDeviceCapability) {}
}
