/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { HardwareStatusType } from "app/shared/model/enumerations/ext/hardware-status-type.model"
import { IHardwareEvent } from "./hardware-event.model"


export interface IHardwareStatus extends IHardwareEvent{
  status: HardwareStatusType;
  message: string;
}

export class HardwareData implements IHardwareStatus {
  constructor(
    public hardwareId: string,
    public timestamp: number,
    public status: HardwareStatusType,
    public message: string
  ) { }
}
