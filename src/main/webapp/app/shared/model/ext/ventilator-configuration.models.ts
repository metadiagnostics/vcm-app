/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { IScalarValue } from './scalar-value.model';

export interface IVentilatorConfigurationChangeRequest {
  peep?: IScalarValue;
  pip?: IScalarValue;
  rate?: IScalarValue;
  fio2?: IScalarValue;
  tv?: IScalarValue;
  inspiratoryExpiratoryRatio?: IScalarValue;
}

export class VentilatorConfigurationChangeRequest implements IVentilatorConfigurationChangeRequest {
  constructor(
    public peep?: IScalarValue,
    public pip?: IScalarValue,
    public rate?: IScalarValue,
    public fio2?: IScalarValue,
    public tv?: IScalarValue,
    public inspiratoryExpiratoryRatio?: IScalarValue
  ) {}
}
