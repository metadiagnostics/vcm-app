/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { IScalarValue } from './scalar-value.model';

export interface IVentilatorRecommendationRequest {
  peep?: IScalarValue;
  pip?: IScalarValue;
  rate?: IScalarValue;
  fio2?: IScalarValue;
  ph?: IScalarValue;
  etco2?: IScalarValue;
  o2sat?: IScalarValue;
  tidalVolume?: IScalarValue;
  height?: IScalarValue;
  gender?: string;
}

export class VentilatorRecommendationRequest implements IVentilatorRecommendationRequest {
  constructor(
    public peep?: IScalarValue,
    public pip?: IScalarValue,
    public rate?: IScalarValue,
    public fio2?: IScalarValue,
    public ph?: IScalarValue,
    public etco2?: IScalarValue,
    public o2sat?: IScalarValue,
    public tidalVolume?: IScalarValue,
    public height?: IScalarValue,
    public gender?: string
  ) {}
}

export interface IVentilatorRecommendationResponse {
  timestamp: number;
  assignmentId: number;
  peep?: IScalarValue;
  pip?: IScalarValue;
  rate?: IScalarValue;
  fio2?: IScalarValue;
  tv?: IScalarValue;
  reason?: string;
  request?: IVentilatorRecommendationRequest;
}

export class VentilatorRecommendationResponse implements IVentilatorRecommendationResponse {
  constructor(
    public timestamp: number,
    public assignmentId: number,
    public peep?: IScalarValue,
    public pip?: IScalarValue,
    public rate?: IScalarValue,
    public fio2?: IScalarValue,
    public reason?: string,
    public request?: IVentilatorRecommendationRequest
  ) {}
}
