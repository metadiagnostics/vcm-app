/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { DeviceType } from 'app/shared/model/enumerations/device-type.model';
import { DeviceCapabilityType } from '../enumerations/device-capability-type.model';
import { VentilatorModeType } from '../enumerations/ventilator-mode-type.model';
import { VentilatorRecommenderType } from '../enumerations/ventilator-recommender-type.model';
import { VentilatorRecommenderControlType } from '../enumerations/ventilator-recommender-control.model';

export interface IMonitorDevice {
  deviceId: number;
  hardwareId: string;
  model: string;
  type: DeviceType;
}

export interface IMonitorAssignedDevice {
  device: IMonitorDevice;
  capability: DeviceCapabilityType;
}

export interface IMonitorAssignment {
  assignmentId: number;
  patientId?: string;
  ventilatorMode?: VentilatorModeType;
  ventilatorControlType?: VentilatorRecommenderControlType;
  ventilatorRecommenderType?: VentilatorRecommenderType;
  devices?: IMonitorAssignedDevice[];
}

export interface IMonitorApplyRecomendation {
  timestamp: number;
}

export class MonitorApplyRecomendation implements IMonitorApplyRecomendation {
  constructor(public timestamp: number) {}
}

export class MonitorAssignment implements IMonitorAssignment {
  constructor(
    public assignmentId: number,
    public patientId?: string,
    public patientName?: string,
    public ventilatorMode?: VentilatorModeType,
    public ventilatorRecommenderType?: VentilatorRecommenderType,
    public devices?: IMonitorAssignedDevice[]
  ) {}
}

export class MonitorDevice implements IMonitorDevice {
  constructor(public deviceId: number, public hardwareId: string, public model: string, public type: DeviceType) {}
}

/**
 * Utility methods for the MonitorAssignment class.
 */
export class MonitorAssignmentUtils {
  /**
   * Returns an array of all the uniqe devices associated to an Assignment.
   * @param assignment the assignment.
   */
  public static getUniqueDevices(assignment: IMonitorAssignment): IMonitorDevice[] {
    if (!assignment || !assignment.devices) {
      return [];
    }

    const devices = new Map();
    assignment.devices
      .flatMap(ad => ad.device)
      .forEach(d => {
        devices.set(d?.hardwareId, d);
      });
    return Array.from(devices.values());
  }

  /**
   * Returns a Map for the devices that are associated to an Assignment
   * having as a key the Capability that each specific Device provides
   * to the Assignment.
   * @param assignment the assignment.
   */
  public static getDevicesByCapability(assignment: IMonitorAssignment): Map<DeviceCapabilityType, IMonitorDevice> {
    const devices = new Map();
    if (!assignment || !assignment.devices) {
      return devices;
    }

    assignment.devices.forEach(ad => {
      devices.set(ad.capability, ad.device);
    });
    return devices;
  }
}
