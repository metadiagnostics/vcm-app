/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { IScalarValue } from 'app/shared/model/ext/scalar-value.model';

export const enum PulseSimulationState {
  NON_EXISTING = 'NON_EXISTING',
  INITIALIZED = 'INITIALIZED',
  RUNNING = 'RUNNING',
  STOPPED = 'STOPPED',
  TERMINATED = 'TERMINATED',
  PATIENT_IRREVERSIBLE_STATE = 'PATIENT_IRREVERSIBLE_STATE'
}

export interface IStateInfo {
  stateName: string;
  description: string;
  patient: IPatient;
  tags?: Map<string, string>;
}

export interface IPatient {
  name: string;
  sex: string;
  age: IScalarValue;
  weight: IScalarValue;
  height: IScalarValue;
  bodyDensity: IScalarValue;
  bodyFatFraction: IScalarValue;
  leanBodyMass: IScalarValue;
  idealBodyWeight: IScalarValue;
  alveoliSurfaceArea: IScalarValue;
  rightLungRatio: IScalarValue;
  skinSurfaceArea: IScalarValue;
  basalMetabolicRate: IScalarValue;
  bloodVolumeBaseline: IScalarValue;
  diastolicArterialPressureBaseline: IScalarValue;
  heartRateBaseline: IScalarValue;
  meanArterialPressureBaseline: IScalarValue;
  respirationRateBaseline: IScalarValue;
  systolicArterialPressureBaseline: IScalarValue;
  tidalVolumeBaseline: IScalarValue;
  heartRateMaximum: IScalarValue;
  heartRateMinimum: IScalarValue;
  expiratoryReserveVolume: IScalarValue;
  functionalResidualCapacity: IScalarValue;
  inspiratoryCapacity: IScalarValue;
  inspiratoryReserveVolume: IScalarValue;
  residualVolume: IScalarValue;
  totalLungCapacity: IScalarValue;
  vitalCapacity: IScalarValue;
}

export interface ISimulationInitData {
  sateName: string;
  tags?: Map<string, string>;
}

export interface ISimulationInfo {
  name: string;
  state: PulseSimulationState;
  initialStateInfo?: IStateInfo;
  initData?: ISimulationInitData;
  simulationTimestamp: number;
}

/**
 * Utility methods for the ISimulationInfo class.
 */
export class ISimulationInfoUtils {
  /**
   * Returns the value of a specific tag in a ISimulationInfo
   * instance or a default value.
   */
  public static getTag(info: ISimulationInfo, tagName: string, defaultValue = ''): string {
    if (!info || !info.initData || !info.initData.tags) {
      return defaultValue;
    }

    return info.initData.tags[tagName] || defaultValue;
  }
}
