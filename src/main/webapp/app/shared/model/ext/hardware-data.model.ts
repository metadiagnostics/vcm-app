/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { IHardwareEvent } from './hardware-event.model';
import { IScalarValue } from './scalar-value.model';
import { IScalarPointValue } from './scalar-point-value.model';

export interface IHardwareDemographicsEvent extends IHardwareEvent {
  dataTimestamp: number;
  height: IScalarValue;
  gender: string;
  pbw: IScalarValue;
}

export class IHardwareDemographicsEvents extends Array<IHardwareDemographicsEvent> {}

export class HardwareDemographicsEvent implements IHardwareDemographicsEvent {
  constructor(
    public hardwareId: string,
    public timestamp: number,
    public dataTimestamp: number,
    public height: IScalarValue,
    public gender: string,
    public pbw: IScalarValue
  ) {}
}

// Needed ?
export interface IEMREvent {
  dataTimestamp: number;
  kind: string;
  conditions: string[];
  mechanismOfInjury: string[];
  location: string[];
  allergies: string[];
  locusOfControl: string;
}

export class EMREvent implements IEMREvent {
  constructor(
    public dataTimestamp: number,
    public kind: string,
    public conditions: string[],
    public mechanismOfInjury: string[],
    public location: string[],
    public allergies: string[],
    public locusOfControl: string
  ) {}
}

export interface IHardwareVitalsEvent extends IHardwareEvent {
  dataTimestamp: number;
  rate: IScalarValue;
  sat: IScalarValue;
  hr: IScalarValue;
  dbp: IScalarValue;
  sbp: IScalarValue;
  cvp: IScalarValue;
  tv: IScalarValue;
  weight: IScalarValue;
  temp: IScalarValue;
}

export class IHardwareVitalsEvents extends Array<IHardwareVitalsEvent> {}

export class HardwareVitalsEvent implements IHardwareVitalsEvent {
  constructor(
    public hardwareId: string,
    public timestamp: number,
    public dataTimestamp: number,
    public rate: IScalarValue,
    public sat: IScalarValue,
    public hr: IScalarValue,
    public dbp: IScalarValue,
    public sbp: IScalarValue,
    public cvp: IScalarValue,
    public tv: IScalarValue,
    public weight: IScalarValue,
    public temp: IScalarValue
  ) {}
}

export interface IHardwareVentilatorEvent extends IHardwareEvent {
  dataTimestamp: number;
  pip: IScalarValue;
  peep: IScalarValue;
  rate: IScalarValue;
  it: IScalarValue;
  fio2: IScalarValue;
  etco2: IScalarValue;
  flow: IScalarValue;
  pvLoop: IScalarPointValue[];
  fvLoop: IScalarPointValue[];
  ip: IScalarValue;
  ef: IScalarValue;
  ventMode: IScalarValue;
}

export interface IHardwareVentilatorEvents extends Array<IHardwareVentilatorEvent> {}

export class HardwareVentilatorEvent implements IHardwareVentilatorEvent {
  constructor(
    public hardwareId: string,
    public timestamp: number,
    public dataTimestamp: number,
    public pip: IScalarValue,
    public peep: IScalarValue,
    public rate: IScalarValue,
    public it: IScalarValue,
    public fio2: IScalarValue,
    public etco2: IScalarValue,
    public flow: IScalarValue,
    public pvLoop: IScalarPointValue[],
    public fvLoop: IScalarPointValue[],
    public ip: IScalarValue,
    public ef: IScalarValue,
    public ventMode: IScalarValue
  ) {}
}
