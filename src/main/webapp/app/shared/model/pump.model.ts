/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

export interface IPumpModel {
  id: string;
  hwId: string;
  name: string;
  infusionRate1: number;
  infusionRate2: number;
  currentRate1: number;
  currentRate2: number;
  vi1: number;
  vtbi1: number;
  vi2: number;
  vtbi2: number;
  timeRemaining1: string;
  timeRemaining2: string;
  medicine1: string;
  medicine2: string;
  medicineBgClass1: string;
  medicineFntClass1: string;
  medicineBgClass2: string;
  medicineFntClass2: string;
  cds1: string;
  cds2: string;
  locusOfControl1: string;
  locusOfControl2: string;
  plugged: boolean;
  batteryPercentage: number;
  onMainsPower: boolean;
  batteryPresent: boolean;
  bolusProg1: number;
  bolusProg2: number;
  bolusProgRate1: number;
  bolusProgRate2: number;
  devProgMode1: string;
  devProgMode2: string;
}

export class PumpModel implements IPumpModel {
  constructor(
    public id: string,
    public hwId: string,
    public name: string,
    public infusionRate1: number,
    public infusionRate2: number,
    public currentRate1: number,
    public currentRate2: number,
    public vi1: number,
    public vtbi1: number,
    public vi2: number,
    public vtbi2: number,
    public timeRemaining1: string,
    public timeRemaining2: string,
    public medicine1: string,
    public medicine2: string,
    public medicineBgClass1: string,
    public medicineFntClass1: string,
    public medicineBgClass2: string,
    public medicineFntClass2: string,
    public cds1: string,
    public cds2: string,
    public locusOfControl1: string,
    public locusOfControl2: string,
    public plugged: boolean,
    public batteryPercentage = 15,
    public onMainsPower: boolean,
    public batteryPresent: boolean,
    public bolusProg1: number,
    public bolusProg2: number,
    public bolusProgRate1: number,
    public bolusProgRate2: number,
    public devProgMode1: string,
    public devProgMode2: string
  ) {}
}
