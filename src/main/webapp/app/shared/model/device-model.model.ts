/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { IDeviceCapability } from 'app/shared/model/device-capability.model';
import { DeviceType } from 'app/shared/model/enumerations/device-type.model';

export interface IDeviceModel {
  id?: number;
  name?: string;
  type?: DeviceType;
  capabilities?: IDeviceCapability[];
}

export class DeviceModel implements IDeviceModel {
  constructor(public id?: number, public name?: string, public type?: DeviceType, public capabilities?: IDeviceCapability[]) {}
}
