/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { IScalarValue } from 'app/shared/model/ext/scalar-value.model';

@Component({
  selector: 'jhi-num-data-block',
  templateUrl: './numeric-data-block.component.html',
  styleUrls: ['./numeric-data-block.component.scss']
})
export class NumericDataBlockComponent implements OnInit {
  @Output()
  public valueChange = new EventEmitter<IScalarValue>();

  @Input()
  public step = 1;

  @Input()
  public lowerBound: number | undefined;

  @Input()
  public lowerBoundClass = 'lower';

  @Input()
  public upperBound: number | undefined;

  @Input()
  public upperBoundClass = 'upper';

  @Input()
  public normalBoundClass = 'normal';

  @Input()
  public differentBoundClass = 'different';

  @Input()
  public disabled = false;

  @Input()
  valueClass = 'normal';

  @Input()
  public min: number | undefined;

  @Input()
  public max: number | undefined;

  plainValue = 0;

  private _value?: IScalarValue;

  // Recommneded value
  private _rvalue?: IScalarValue;

  // Previous value
  private _currentVentValue?: IScalarValue;

  private _name = 'Name';

  constructor() {}

  ngOnInit(): void {}

  @Input()
  set value(value: IScalarValue) {
    if (value) {
      this._value = value;
      this.plainValue = this._value.value;
    }
  }

  get value(): IScalarValue {
    return this._value!;
  }

  /**
   * When a current setting changes this method is invoked and we store the current value to handle styles when the
   * value changes on onValueChange method.
   */
  @Input()
  set snapshotScalarValue(value: IScalarValue) {
    if (value) {
      this._currentVentValue = value;
    }
  }

  get snapshotScalarValue(): IScalarValue {
    return this._currentVentValue!;
  }

  get rvalue(): IScalarValue {
    return this._rvalue!;
  }

  @Input()
  set rvalue(value: IScalarValue) {
    if (value) {
      this._rvalue = value;
      this.name = this._name;
    }
  }

  get isInitialValue(): boolean {
    return this._rvalue!.value === -1;
  }

  @Input()
  set name(name: string) {
    if (name) {
      this._name = name;
    }
  }

  get name(): string {
    return this._name;
  }

  onValueChange(): void {
    if (this.value) {
      this.value.value = this.plainValue;
      this.valueChange.emit(this.value);
    }
    if (this._currentVentValue?.value === this.value?.value) {
      this.valueClass = this.normalBoundClass;
    } else {
      this.valueClass = this.differentBoundClass;
    }
  }
}
