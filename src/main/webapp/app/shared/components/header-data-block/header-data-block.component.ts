/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { DataBlockComponent } from 'app/shared/components/data-block/data-block.component';
import { EMREvent, HardwareDemographicsEvent } from 'app/shared/model/ext/hardware-data.model';
import { IPatientModel } from 'app/monitor/emr.model';

@Component({
  selector: 'jhi-header-data-block',
  templateUrl: './header-data-block.component.html',
  styleUrls: ['./header-data-block.component.scss']
})
export class HeaderDataBlockComponent extends DataBlockComponent implements OnInit {
  @Input()
  public name = 'Name';

  @Input()
  public value?: string;

  @Input()
  public type?: string;

  @Input()
  public style?: string;

  ssHwDemographicsDataSelector = 'hwDemographicsDataSelector';
  ssHwEMRDataSelector = 'hwEMRDataSelector';

  constructor(private elRef: ElementRef) {
    super();
  }

  ngOnInit(): void {}

  public updateValue(data: any): void {
    this.value = '';
    const p: IPatientModel[] = [];
    let type = '';
    const patientIdPatientPos = {};
    let pos = 0;
    Object.values(data).forEach(d => {
      if (typeof d === 'string') {
        type = d;
      } else {
        const patient = d as IPatientModel;
        p.push(patient);
        patientIdPatientPos[patient.id] = pos++;
      }
    });
    if (p.length === 0) return;
    // grab the currentPatient from the first one, this information is present on all the records.
    const patient = p[patientIdPatientPos[p[0].currentPatientId]];
    if ( patient === null || patient === undefined) return;
    if (type === this.ssHwEMRDataSelector) {
      if (this.type === 'demographics') {
        patient.demographics.forEach(dem => {
          this.value += ' ' + dem;
        });
      }
      if (this.type === 'primary-conditions') {
        if (patient.conditions.length > 0) {
          this.value = patient.conditions.join(', ');
        }
      } else if (this.type === 'mechanism-of-injury') {
        if (patient.mechanism_of_injury.length > 0) {
          this.value = patient.mechanism_of_injury.join(', ');
        }
      } else if (this.type === 'location') {
        if (patient.locations.length > 0) {
          this.value = patient.locations.join(', ');
        }
      } else if (this.type === 'allergies') {
        if (patient.allergies.length > 0) {
          this.value = patient.allergies.join(', ');
          this.style = 'with-allergies';
          // eslint-disable-next-line no-console
          // console.log( this.elRef.nativeElement);
          if (
            this.elRef.nativeElement !== undefined &&
            this.elRef.nativeElement.parentElement !== undefined &&
            this.elRef.nativeElement.parentElement.parentElement !== undefined
          ) {
            this.elRef.nativeElement.parentElement.parentElement.classList.remove('header-no-allergies');
            this.elRef.nativeElement.parentElement.parentElement.classList.add('header-with-allergies');
          }
        } else {
          this.value = 'No Known Allergies';
          this.style = 'no-allergies';
          if (
            this.elRef.nativeElement !== undefined &&
            this.elRef.nativeElement.parentElement !== undefined &&
            this.elRef.nativeElement.parentElement.parentElement !== undefined
          ) {
            this.elRef.nativeElement.parentElement.parentElement.classList.remove('header-with-allergies');
            this.elRef.nativeElement.parentElement.parentElement.classList.add('header-no-allergies');
          }
        }
      } else if (this.type === 'locus-of-control') {
        this.value = patient.locus;
        this.style = 'header-text-centered';
      }
    }

    /* if (this.type === 'primary-conditions') {
      if (emrData.conditions.length > 0) {
        this.value = emrData.conditions.join(', ');
      }
    }*/

    /* let d = data;
    if (Array.isArray(data) && data.length > 0) {
      d = data[0];
    }
    if (d) {
      if (Object.hasOwnProperty.call(d, 'type')) {
        if (d.type === this.ssHwDemographicsDataSelector) {
          const demographicData = d as HardwareDemographicsEvent;
          this.value =
            'Height: ' +
            demographicData.height.value +
            ' ' +
            demographicData.height.unit +
            ', Gender: ' +
            demographicData.gender +
            ', pwb: ' +
            demographicData.pbw.value +
            ' ' +
            demographicData.pbw.unit;
        } else if (d.type === this.ssHwEMRDataSelector) {
          const emrData = d as EMREvent;
          if (this.type === 'primary-conditions') {
            if (emrData.conditions.length > 0) {
              this.value = emrData.conditions.join(', ');
            }
          } else if (this.type === 'mechanism-of-injury') {
            if (emrData.mechanismOfInjury.length > 0) {
              this.value = emrData.mechanismOfInjury.join(', ');
            }

        }
      }
    } */
  }
}
