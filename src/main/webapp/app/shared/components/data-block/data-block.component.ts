/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IHardwareVentilatorEvents } from 'app/shared/model/ext/hardware-data.model';

export class DragEvent {
  constructor(public srcEvent: any, public component: DataBlockComponent) {}
}

@Component({
  template: ''
})
export class DataBlockComponent implements OnInit {
  @Input()
  public ddScope = '';

  @Input()
  public selectors: string[] = [];

  @Input()
  public expanded = false;

  @Input()
  public physiologicMonitors = false;

  @Input()
  public ventMonitors = false;

  @Input()
  public reduced = false;

  @Input()
  public ventFlow = false;

  @Input()
  public ventSettings = false;

  @Output()
  public dragStart: EventEmitter<DragEvent> = new EventEmitter();

  @Output()
  public dragEnd: EventEmitter<DragEvent> = new EventEmitter();

  @Input()
  public visible = true;

  @Input()
  public data: any = [];

  @Input()
  public dataKey = '';

  displayModal = false;

  constructor() {}

  ngOnInit(): void {}

  public hasSelector(selector: string): boolean {
    return this.selectors && this.selectors.includes(selector);
  }

  // eslint-disable-next-line
  public updateValue(data: any): void {}

  // eslint-disable-next-line
  public updateBulkValue(data: IHardwareVentilatorEvents): void {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public updateAttr(attr: string, data: any): void {}

  public onDragStart(event: any): void {
    // return; // disabling this as per new requirements
    this.dragStart.emit(new DragEvent(event, this));
  }

  public onDragEnd(event: any): void {
    // return; // disabling this as per new requirements
    this.dragEnd.emit(new DragEvent(event, this));
  }

  getFormattedUnitFor(dataKey: string, unit: string): string {
    if (unit === undefined) return '';
    if (unit === 'cmH2O') {
      return 'cmH<sub>2</sub>O';
    }
    if (dataKey === 'rate') {
      return 'bpm';
    }
    if (dataKey === 'hr') {
      return 'bpm';
    }
    return unit;
  }
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public setBlockData(blockData: any): void {}
}
