/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, Input, OnInit } from '@angular/core';

export const enum BadgeStatus {
  SUCCESS = 'SUCCESS',
  WARNING = 'WARNING',
  ERROR = 'ERROR'
}

@Component({
  selector: 'jhi-status-badge',
  templateUrl: './status-badge.component.html',
  styleUrls: ['./status-badge.component.scss']
})
export class StatusBadgeComponent implements OnInit {

  @Input()
  public label = 'Label';

  @Input()
  public status = BadgeStatus.SUCCESS;

  @Input()
  public tooltipPosition = 'top';

  @Input()
  public tooltipText = '';

  constructor() {
  }

  ngOnInit(): void {
  }

  getBadgeClass():string {
    if (this.status === BadgeStatus.SUCCESS){
      return 'badge-success';
    }
    if (this.status === BadgeStatus.WARNING){
      return 'badge-warning';
    }
    if (this.status === BadgeStatus.ERROR){
      return 'badge-danger';
    } else {
      return 'primary';
    }
  }
}
