/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, Input, OnInit } from '@angular/core';

import { DataBlockComponent } from 'app/shared/components/data-block/data-block.component';

export class Column {
  constructor(public header: string, public valueTemplate: string) {}

  public getValue(data: any): string {
    if (!data) {
      return '-';
    }
    try {
      return new Function('return `' + this.valueTemplate + '`;').call({ value: data });
    } catch (e) {
      return '-';
    }
  }
}

@Component({
  selector: 'jhi-tabular-data-block',
  templateUrl: './tabular-data-block.component.html',
  styleUrls: ['./tabular-data-block.component.scss']
})
export class TabularDataBlockComponent extends DataBlockComponent implements OnInit {
  @Input()
  public name = 'Name';

  @Input()
  public columns: Column[] = [];

  @Input()
  public values: any[] = [];

  @Input()
  public tableColsLength = 20;

  constructor() {
    super();
  }

  ngOnInit(): void {}

  public updateValue(data: any): void {
    if (!data) {
      return;
    }
    this.values.unshift(data);
    // Trim data if it exceeds the maximum
    if (this.values.length > this.tableColsLength) {
      this.values.shift();
    }
  }
}
