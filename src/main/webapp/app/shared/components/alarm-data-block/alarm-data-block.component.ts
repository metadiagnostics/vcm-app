/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, ElementRef, Input, OnDestroy, OnInit } from '@angular/core';
import { DataBlockComponent } from 'app/shared/components/data-block/data-block.component';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons/faCheckCircle';
import { faVolumeMute } from '@fortawesome/free-solid-svg-icons/faVolumeMute';
import { faVolumeUp } from '@fortawesome/free-solid-svg-icons/faVolumeUp';
import { AlarmService } from 'app/core/alarm/alarm.service';
import { DrawerService } from 'app/drawer/drawer.service';
import { IAlarmWarningModel } from 'app/monitor/alarm.model';
import { Observable, Subject, Subscription } from 'rxjs';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'jhi-alarm-data-block',
  templateUrl: './alarm-data-block.component.html',
  styleUrls: ['./alarm-data-block.component.scss']
})
export class AlarmDataBlockComponent extends DataBlockComponent implements OnInit, OnDestroy {
  private MULTIPLE_ALARMS = 'Multiple Alarms';

  @Input()
  public id = '';

  @Input()
  public title?: string;

  @Input()
  public body?: string;

  @Input()
  public muted?: boolean;

  @Input()
  public type?: string;

  @Input()
  public patientId: string;

  @Input()
  public style = 'alarm-data-block-no-bg';

  @Input()
  public onDrawer = false;

  @Input()
  public severity = 'low';

  @Input()
  public noAlarms = true;

  public check = faCheckCircle;
  public mute = faVolumeMute;

  public unmute = faVolumeUp;

  private silenceTimeMs = 60000; // 60 seconds 60000
  private reEntryAlarmTimeMs = 300000; // 5 minutes 300000

  private alarms: IAlarmWarningModel[];

  private audio: HTMLAudioElement;

  private alarmSubscription: Subject<IAlarmWarningModel[]>;
  private alarmSubs: Subscription;

  private updateSubs: Subscription;

  private destroyed = false;

  constructor(private elRef: ElementRef, protected alarmService: AlarmService, protected drawerService: DrawerService) {
    super();
    this.alarms = [];
    this.audio = this.alarmService.getAudio();
    this.alarmSubscription = this.alarmService.alarmsSubscription();
    this.title = 'No alarms detected.';
    this.body = 'No alarms detected.';
    this.noAlarms = true;
    this.patientId = '-1';
    this.updateSubs = new Subscription();
    this.alarmSubs = new Subscription();
  }

  ngOnInit(): void {
    this.alarmSubs = this.alarmSubscription.subscribe(alarms => {
      // if someone deletes all the alarms from the monitor drawer we should clear the component
      if (alarms.length === 0) {
        this.audio.pause();
        this.title = this.body = 'No alarms detected.';
        this.noAlarms = true;
        // change the style to white
        this.style = 'alarm-data-block-no-bg';
      } else if (alarms.length === 1) {
        this.title = alarms[0].title;
        this.body = alarms[0].body;
        this.noAlarms = false;
        this.style = 'alarm-data-block-' + alarms[0].severity;
      }
      if (!this.onDrawer && alarms.length > 1) {
        this.style = 'alarm-data-block-high';
        this.title = this.MULTIPLE_ALARMS;
        this.noAlarms = false;
      }
      if (!this.onDrawer) {
        if (this.alarmService.areAllAlarmsMuted() || this.alarmService.forcedMute()) {
          this.audio.pause();
          this.muted = true;
        } else {
          this.muted = false;
          this.playAudio();
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.updateSubs.unsubscribe();
    this.alarmSubs.unsubscribe();
    this.destroyed = true;
  }

  private playAudio(): void {
    if (!this.audio.onplay) {
      try {
        this.audio.play();
      } catch {
        // eslint-disable-next-line no-console
        console.log('Was not possible to play audio on this browser');
      }
    }
  }

  public getStyle(severity: string): string {
    if (this.onDrawer) {
      this.style = 'alarm-data-block-' + severity;
    } else {
      // ignores severity the color will be provided on the updateValue method
    }
    return this.style;
  }

  public updateValue(data: any): void {
    this.style = 'alarm-data-block-no-bg';
    if (!Array.isArray(data)) {
      return;
    }
    this.alarms = data as IAlarmWarningModel[];
    this.noAlarms = true;
    if (this.alarms.length === 1) {
      // presents the data on same component
      const d = this.alarms[0];
      this.title = d.title;
      this.body = d.body;
      this.noAlarms = false;
      try {
        // this.playAudio(); // DISABLED for demo
      } catch {
        // eslint-disable-next-line no-console
        console.log('');
      }
      // When no severity is provided the length is less than 3 that is the length of the word 'low'
      if (d.severity.length<3) {
        d.severity = 'low'; // forcing a low color since is unknown for us
      }
      this.style = 'alarm-data-block-' + d.severity;
      this.alarmService.notify(this.alarms);
    } else if (this.alarms.length > 1) {
      // we should set the first value in the component, the buttons behavior now change
      // 1) ack, is a general ack for all,
      // 2) mute, also mutes all not only the first alarm.
      // 3) Opens automatically the right panel (side door and present all the alarms)
      // this.playAudio(); // DISABLED for demo
      this.style = 'alarm-data-block-high';
      this.title = this.MULTIPLE_ALARMS;
      this.noAlarms = false;
      this.alarmService.notify(this.alarms);
      this.expandDrawerIfHighAlarmWarning();
    }
  }

  private expandDrawerIfHighAlarmWarning(): void {
    // only expand if there is some alarm with high severity
    if (this.alarms !== undefined && this.alarms.filter(a => a.severity === 'high').length > 0) {
      // this.drawerService.expandsDrawer(); //DISABLED only for Emory's demo purposes
    }
  }

  public muteSound(onDrawer: boolean, id: string, event: MouseEvent): void {
    event.stopPropagation();
    const alarms = this.alarmService.getAlarms() || [];
    if (!onDrawer) { // Not expanded, without details
      if (alarms.length > 0) {
        if (!this.muted) {
          this.audio.pause();
          this.alarmService.forceMuteSound(); //General Mute
        } else {
          this.playAudio();
          this.alarmService.forceUnmuteSound(); //General Un-mute
        }
        const mut = this.muted! || this.alarmService.forcedMute();
        this.muteAlarms(alarms, !mut); // this also notify
        // re-enable audio after silenceTimeMs only if user mute it
        /*
        // DISABLED for demo
        if (!mut) {
          setTimeout(() => {
            if (this.alarmService.getAlarms().length > 0) {
              this.muteAlarms(alarms, mut);
              if (mut) {
                this.audio.pause();
              } else if (!this.destroyed) {
                this.playAudio();
                this.expandDrawerIfHighAlarmWarning();
              }
            }
          }, this.silenceTimeMs);
        }
        */
      }
      return;
    }
    // mute only one individual alarm
    const i = alarms.findIndex(a => a.id === id);
    if (i === -1) return;
    alarms[i].muted = !alarms[i].muted;
    if (!alarms[i].muted && !this.audio.onplay) {
      this.alarmService.forceUnmuteSound(); //General Un-mute
      this.playAudio();
    }
    this.alarmService.notify(alarms);
    /*
    // DISABLED for demo
    if (alarms[i].muted) {
       setTimeout(() => {
           const al = this.alarmService.getAlarms() || [];
           const j = al.findIndex(a => a.id === id);
           if (j === -1) return;
           al[j].muted = false;
           this.alarmService.notify(al);
           if (!this.audio.onplay) {
             this.playAudio();
             if (al[j].severity === 'high') {
               this.drawerService.expandsDrawer();
             }
           }
         }, this.silenceTimeMs);
       }
       */
    if (this.alarmService.areAllAlarmsMuted()) {
      // if all muted stop the sound
      this.audio.pause();
      /*
      // DISABLED for demo
      // reenable audio only for this item after silenceTimeMs
      setTimeout(() => {
        if (this.alarmService.getAlarms().length > 0 && !this.audio.onplay) {
          this.playAudio();
          this.expandDrawerIfHighAlarmWarning();
        }
      }, this.silenceTimeMs);
      */
   }
 }

 /**
  * Mute/unmute all alarms
  * **/
  private muteAlarms(alarms: IAlarmWarningModel[], muted: boolean): void {
    alarms.forEach(a => {
      a.muted = muted;
    });
    this.alarmService.notify(alarms);
  }

  public ackAlert(onDrawer: boolean, id: string, event: MouseEvent): void {
    event.stopPropagation();
    const alarms = this.alarmService.getAlarms() || [];
    let idAck = '';
    if (alarms.length === 0) {
      return;
    }

    if (onDrawer) {
      const i = alarms.findIndex(a => a.id === id);
      if (i === -1) return;
      alarms.splice(i, 1);
      idAck = id;
      this.alarmService.notify(alarms);
      /** Not need to ack to server them
      this.alarmService.ackAlarmWarning(idAck, this.patientId).subscribe(() => {
        // Re-entrant alarm, after reEntryAlarmTimeMs we will check with a REST service if the alarm
        // still exists calling a service, if it's still there re re-agregate the alarm to alarms and
        // reorder it by severity
        setTimeout(() => {
          // here we call the rest service
          this.updateSubs = this.alarmService.getAlarmWarning(idAck, this.patientId).subscribe((res: HttpResponse<IAlarmWarningModel>) => {
            const alarm = res.body || ({} as IAlarmWarningModel);
            if (alarm.id === idAck) {
              alarms.push(alarm);
              alarms.sort(this.compAlarmfn);
              this.alarmService.notify(alarms);
              // only expand if there is some alarm with high severity
              if (alarm.severity === 'high') {
                this.drawerService.expandsDrawer();
              }
            }
          });
        }, this.reEntryAlarmTimeMs);
      });*/
    } else {
      this.alarmService.notify([]);
      this.muted = true;
      /**
      this.updateSubs = this.alarmService.ackAllAlarmWarnings(this.patientId).subscribe(rp => {
        const newAlarms = rp.body || ({} as IAlarmWarningModel[]);
        if (newAlarms.length > 0) {
          newAlarms.sort(this.compAlarmfn);
          setTimeout(() => {
            newAlarms.forEach(a => (a.muted = false));
            this.muted = false;
            if (!this.audio.onplay && !this.destroyed) {
              this.playAudio();
              this.noAlarms = true;
            }
            this.alarmService.notify(newAlarms);
            // only expand if there is some alarm with high severity
            if (newAlarms.filter(a => a.severity === 'high').length > 0) {
              this.drawerService.expandsDrawer();
            }
          }, this.reEntryAlarmTimeMs);
        } else {
          this.alarmService.notify([]);
        }
      });*/
    }
  }

  private compAlarmfn(a: IAlarmWarningModel, b: IAlarmWarningModel): number {
    if (a.severity === 'high' && b.severity !== 'high') {
      return -1;
    } else if (a.severity === 'medium' && b.severity === 'low') {
      return -1;
    } else if (a.severity === 'low' && b.severity !== 'low') {
      return 1;
    }
    return 0;
  }

  public openDrawer(): void {
    const alarms = this.alarmService.getAlarms() || [];
    if (alarms.length >= 1) {
      this.drawerService.expandsDrawer();
    }
  }
}
