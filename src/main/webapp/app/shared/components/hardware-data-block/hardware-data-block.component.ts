/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, Input, OnInit } from '@angular/core';

import { IScalarValue } from 'app/shared/model/ext/scalar-value.model';

import { DataBlockComponent } from 'app/shared/components/data-block/data-block.component';
import { IScalarPointValue } from 'app/shared/model/ext/scalar-point-value.model';
import { IHardwareVentilatorEvents } from 'app/shared/model/ext/hardware-data.model';
import { Chart } from 'chart.js';
import * as ChartAnnotation from 'chartjs-plugin-annotation';
import {MonitorHardwareConnection, MonitorHardwareService} from "app/shared/services/monitor/monitor-hardware.service";
import {faCheckCircle} from "@fortawesome/free-solid-svg-icons/faCheckCircle";
import {faTimesCircle} from "@fortawesome/free-solid-svg-icons/faTimesCircle";
export type HardwareDataBlockComponentType = 'Point' | 'Plot';

@Component({
  selector: 'jhi-hw-data-block',
  templateUrl: './hardware-data-block.component.html',
  styleUrls: ['./hardware-data-block.component.scss']
})
export class HardwareDataBlockComponent extends DataBlockComponent implements OnInit {
  @Input()
  public name = 'Name';

  @Input()
  public alarmMax = 80;

  @Input()
  public newAlarmMax = 0;

  @Input()
  public alarmMin = 15;

  @Input()
  public newAlarmMin = 0;

  @Input()
  public dataKeys: string[] = [];

  @Input()
  public type: HardwareDataBlockComponentType = 'Point';

  @Input()
  public value?: string;

  @Input()
  public unit = '';

  @Input()
  public lowerBound: number | undefined;

  @Input()
  public lowerBoundClass = 'lower';

  @Input()
  public upperBound: number | undefined;

  @Input()
  public upperBoundClass = 'upper';

  @Input()
  public normalBoundClass = 'normal';

  @Input()
  public chartMaxDataLength = 15;

  @Input()
  public xlabel = '';

  @Input()
  public ylabel = '';

  @Input()
  public separator = '';

  @Input()
  public hwId = '';

  @Input()
  public alarmMinCodeId = '';

  @Input()
  public alarmMaxCodeId = '';

  @Input()
  public yaxisMin = -999; // not defined by default

  @Input()
  public yaxisMax = -999; // not defined by default

  inputFocus= 'alarmMin';

  newValueString: any;

  public check = faCheckCircle;

  public cancel = faTimesCircle;

  valueClass = '';

  chartData: any;
  chartOptions: any;

  chartDefaultDataSetOptions?: any;

  private MAX = 500;

  private xMin = 0;
  private xMax = 0;
  // To preserve y axis static
  private yMin = Number.MAX_VALUE;
  private yMax = Number.MIN_VALUE;
  private pos = 0;
  private posLoop = this.chartMaxDataLength;
  private labels: string[] = [];
  private arrayData: number[] = [];

  private hwConnection?: MonitorHardwareConnection;

  constructor(protected monitorHardwareService: MonitorHardwareService) {
    super();
    Chart.pluginService.register(ChartAnnotation);
    this.chartOptions = {
      pointRadius: 0,
      title: {
        display: false
      },
      legend: {
        display: false
      },
      animation: false,
      parsing: false,
      normalized: true,
      maintainAspectRatio: false,
      tooltips: {
        enabled: false
      },
      elements: {
        line: {
          tension: 0 // disables bezier curves
        }
      },
      scales: {
        xAxes: [
          {
            gridLines: {
              display: true
            },
            ticks: {
              display: true
            },
            scaleLabel: {
              display: true,
              labelString: this.xlabel
            }
          }
        ],
        yAxes: [
          {
            ticks: {
              beginAtZero: false,
            },
            scaleLabel: {
              display: true,
              labelString: this.ylabel
            }
          }
        ]
      },
      annotation: {
        annotations: []
      }
    };
  }

  getChartOptions(): any {
    this.chartOptions.scales.xAxes[0].scaleLabel.labelString = this.xlabel;
    this.chartOptions.scales.yAxes[0].scaleLabel.labelString = this.ylabel;
    this.chartOptions.scales.xAxes[0].id = 'x-axis-0';
    this.chartOptions.scales.xAxes[0].ticks.display = false;
    this.chartOptions.scales.yAxes[0].id = 'y-axis-0';

    this.chartOptions.fill = false;
    if (this.chartMaxDataLength < 169999) {
      // This is disabling the box that indicates where is the last data
      this.chartOptions.annotation.annotations = [
        {
          // Indicates the type of annotation
          type: 'box',
          xScaleID: 'x-axis-0',
          yScaleID: 'y-axis-0',
          xMin: this.xMin,
          xMax: this.xMax,
          yMin: this.alarmMin,
          yMax: this.alarmMax,
          backgroundColor: '#FFFFFF',
          adjustScaleRange: true,
          borderColor: '#000000'
        }
      ];
    }
    return this.chartOptions;
  }

  ngOnInit(): void {
    this.posLoop = this.chartMaxDataLength;
    if (this.type === 'Point') {
      this.chartDefaultDataSetOptions = {
        fill: false,
        borderColor: '#c8c8c8',
        pointRadius: 0,
        annotation: {
          annotations: []
        }
      };
    } else {
      this.chartDefaultDataSetOptions = {
        title: {
          display: false
        },
        legend: {
          display: false
        },
        animation: false,
        parsing: false,
        normalized: true,
        maintainAspectRatio: false,
        showLine: true,
        pointRadius: 0,
        tooltips: {
          enabled: true
        },
        elements: {
          line: {
            tension: 0 // disables bezier curves
          }
        },
        scales: {
          xAxes: [
            {
              gridLines: {
                display: true
              },
              ticks: {
                display: true
              },
              scaleLabel: {
                display: true,
                labelString: this.xlabel
              }
            }
          ],
          yAxes: [
            {
              ticks: {
                beginAtZero: false
              },
              scaleLabel: {
                display: true,
                labelString: this.ylabel
              }
            }
          ]
        }
      };
    }
    let d = this.data === undefined ? [] : this.data;
    let l: Array<string> = [];
    d.forEach(() => {
      l.push('');
    });
    // just for performance we will "drop" values if we have more than MAX
    if (d.length > this.MAX) {
      d = d.slice(-this.MAX);
      l = l.slice(-this.MAX);
    }
    this.chartData = {
      labels: l,
      datasets: [
        {
          ...this.chartDefaultDataSetOptions,
          label: '',
          data: d
        }
      ]
    };
    // Connect to the device
    this.monitorHardwareService.connect().subscribe(hwConnection => {
      this.hwConnection = hwConnection;
    });
  }

  public updateValue(data: any): void {
    if (!data || this.dataKeys.length === 0) {
      this.value = undefined;
      this.unit = '';
    } else {
      // check if it's a composed value or a simple one
      const d = data[this.dataKeys[0]] as IScalarValue;
      if (!d) {
        return;
      }
      if (this.dataKeys.length === 1) {
        if (d == null) {
          this.value = undefined;
          this.unit = '';
          this.valueClass = this.normalBoundClass;
          return;
        }
        if (d.value !== undefined && !Number.isInteger(d.value)) {
          d.value = Math.round(d.value * 100) / 100;
        }
        this.value = '' + d.value;
        this.unit = this.getFormattedUnitFor(this.dataKeys[0], d.unit);
        if (this.lowerBound && d.value <= this.lowerBound) {
          this.valueClass = this.lowerBoundClass;
        } else if (this.upperBound && d.value >= this.upperBound) {
          this.valueClass = this.upperBoundClass;
        } else {
          this.valueClass = this.normalBoundClass;
        }
        // update the chart data
        this.updateChartData(d);
      } else {
        // this is a composed Hardware value
        let hwData = '';
        for (const attr of this.dataKeys) {
          hwData += data[attr].value + this.separator;
        }
        // removes last separator if exist multiple attributes
        this.value = hwData.slice(0, -1);
        this.valueClass = this.normalBoundClass;
        this.unit = this.getFormattedUnitFor(this.dataKeys[0], d.unit);
      }
    }
  }

  public updateBulkValue(data: IHardwareVentilatorEvents): void {
    if (!Array.isArray(data)) return;
    const allData = data;
    allData.forEach(d => {
      this.updateValue(d);
    });
  }
  getChatType(): string {
    return this.type === 'Point' ? 'line' : 'scatter';
  }

  valueAndUnit(): string {
    let valueAndUnit = '';
    if (this.type === 'Plot') {
      return '-';
    } else if (this.value === undefined) {
      valueAndUnit = '- -';
    } else {
      valueAndUnit += this.value;
    }
    return valueAndUnit + ' ' + this.unit;
  }

  nameAndUnit(): string{
    if (this.unit === undefined || this.unit==='') return this.name;
    return this.name + ' (' + this.unit + ')';
  }

  onlyValue(): string{
    let value = '';
    if (this.type === 'Plot') {
      return '-';
    } else if (this.value === undefined) {
      value = '- -';
    } else {
      value += this.value;
    }
    return value;
  }

  public setBlockData(blockData: any): void {
    if (blockData !== undefined) {
      this.data = blockData;
    }
  }

  private updateChartData(data: IScalarValue | IScalarPointValue[]): void {
    // Right now, when a block is dragged to an "expanded" section,
    // the block is recreated by Angular (a new instance is created).
    // This is why it makes no sense for now to keep updating the chart
    // if the component is not expanded.
    if (!this.expanded) {
      // Just concatenate data and exit
      if (this.type === 'Point') {
        this.data.push((data as IScalarValue).value);
      } else {
        const plotData = data as IScalarPointValue[];
        if (!plotData || plotData.length === 0) {
          // do nothing
          return;
        }
        const mappedData = plotData.map(d => {
          return {
            x: d.x.value,
            y: d.y.value
          };
        });
        this.data.push(mappedData);
      }
      return;
    }

    if (this.type === 'Point') {
      if (this.arrayData.length < this.posLoop) {
        this.arrayData.push((data as IScalarValue).value);
        this.labels.push('' + this.pos++);
      } else {
        this.pos = this.pos + 1;
        this.pos = this.pos % this.posLoop;
        const currentValue = (data as IScalarValue).value;
        this.arrayData[this.pos] = currentValue;
        if (this.yMax < currentValue) {
          this.yMax = currentValue;
        }
        if (this.yMin > currentValue) {
          this.yMin = currentValue;
        }
      }
      this.xMin = this.pos; // this.pos % (this.posLoop - 1);
      this.xMax = this.pos + 1; // (this.pos + 1) % (this.posLoop - 1);
      if (this.xMax < this.xMin) {
        this.xMax = this.xMin + 1;
      }

      this.chartDefaultDataSetOptions = this.getChartOptions();
      if (this.chartDefaultDataSetOptions.annotation.annotations.length > 0) {
        this.chartDefaultDataSetOptions.annotation.annotations[0].yMin = -1000;
        this.chartDefaultDataSetOptions.annotation.annotations[0].yMax = 1000;
      }
      //If scales on graph were specified
      if (this.yaxisMin !== -999) {
        this.chartDefaultDataSetOptions.scales.yAxes[0].ticks.min = this.yaxisMin;
      } else {
        // this.chartDefaultDataSetOptions.scales.yAxes[0].ticks.min = Math.min(Math.round(this.yMin) - 1, this.alarmMin); // make this dynamic
      }
      if (this.yaxisMax !== -999){
        this.chartDefaultDataSetOptions.scales.yAxes[0].ticks.max = this.yaxisMax;
      } else {
        // this.chartDefaultDataSetOptions.scales.yAxes[0].ticks.max = Math.max(Math.round(this.yMax) + 1, this.alarmMax); // make this dynamic
      }
      const newChartData = {
        labels: this.labels,
        options: this.chartDefaultDataSetOptions,
        datasets: [
          {
            ...this.chartDefaultDataSetOptions,
            borderColor: "#0da233",
            borderWidth: 1,
            data: this.arrayData
          }
        ]
      };
      // Trim data if it exceeds the maximum
      if (newChartData.labels.length > this.chartMaxDataLength) {
        newChartData.labels.shift();
        newChartData.datasets[0].data.shift();
      }
      this.chartData = newChartData;
      // eslint-disable-next-line no-console
      // console.log('pos ' + this.pos)
      // eslint-disable-next-line no-console
      // console.log(this.chartData)
    } else {
      // type === Plot
      const plotData = data as IScalarPointValue[];
      if (!plotData || plotData.length === 0) {
        // do nothing
        return;
      }

      const mappedData = plotData.map(d => {
        return {
          x: d.x.value,
          y: d.y.value
        };
      });
      const newChartData = {
        labels: [...this.chartData.labels, ''],
        datasets: [
          {
            ...this.chartDefaultDataSetOptions,
            data: mappedData
          }
        ]
      };

      this.chartData = newChartData;
    }
  }

  public adjustAlarmValues(): void {
    // open a dialog
    this.displayModal = true;
  }
  public closeDialog(): void {
    this.displayModal = false;
  }
  public numpadClick(num: number): void {
    this.newValueString += '' + num;
  }
  public setInput(inputName: string): void {
    this.inputFocus = inputName;
  }

  public numpadLimitClick(num: number): void {
    if (this.inputFocus.includes('Min')) {
      if (this.alarmMinCodeId===null || this.alarmMinCodeId===undefined) {
        return;
      }
    }
    if (this.inputFocus.includes('Max')) {
      if (this.alarmMaxCodeId===null || this.alarmMaxCodeId===undefined) {
        return;
      }
    }
    this[this.inputFocus] += '' + num;
  }

  public numpadLimit(ev: Event, limit: string): void {
    if (ev.target !== null) {
      this[limit] = (ev.target as HTMLInputElement).value;
    }
  }


  public applyLimitChanges(): void {
    this.doLimitChanges('newAlarmMin', this.alarmMinCodeId);
    this.doLimitChanges('newAlarmMax', this.alarmMaxCodeId);
    this.displayModal = false;
  }

  private doLimitChanges(attr:string, codeId:string): void {
    if (codeId === null) return;
    let newNumber;
    if (this[attr] !== undefined && (typeof this[attr] === 'number' || typeof this[attr] === 'string')) {
      newNumber = Number(this[attr]);
    } else if (this[attr] !== undefined && typeof this[attr].position === 'number') {
      // Workaround to fix the non working prime ng dropdown
      newNumber = this[attr].setValue;
    }
    // We are not validating here if the range is correct
    if (this[attr].length === 0 || isNaN(newNumber)) {
      return;
    }
    // this.value = newNumber; // not apply directly just wait next frames (5 seconds or more) to set it from device
    this.hwConnection?.sendNewVentilatorValue(this.hwId, codeId, '' + newNumber);
  }
}
