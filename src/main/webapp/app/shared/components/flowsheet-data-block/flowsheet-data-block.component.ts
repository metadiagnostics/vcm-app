/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, Input, OnInit, AfterViewInit, ViewChild } from '@angular/core';

import { DataBlockComponent } from 'app/shared/components/data-block/data-block.component';
import { Table } from 'primeng/table';
import { IHardwareVentilatorEvents } from 'app/shared/model/ext/hardware-data.model';
export type HeaderTitleCallbackFunction = (value: any) => string;
export type ValueCoverterCallbackFunction = (value: any) => string;

export class ColumnsTemplate {
  constructor(public headerTemplateFc: HeaderTitleCallbackFunction) {}

  public getValue(data: any): string {
    if (!data) {
      return '-';
    }
    try {
      return this.headerTemplateFc.call(this, data);
    } catch (e) {
      return '-';
    }
  }
}

export class Row {
  constructor(
    public header: string,
    public dataAttr: string[],
    public separator: string = '',
    public converter: ValueCoverterCallbackFunction = value => {
      return value;
    }
  ) {}
  // this function obtain the value from an IScalarValue,
  public getValue(data: any): string {
    if (!data) {
      return '-';
    }
    try {
      let rowData = '';
      for (const attr of this.dataAttr) {
        const d = Math.round(data[attr].value * 100) / 100;
        rowData += d + this.separator;
      }
      // removes last separator if exist multiple attributes
      if (this.dataAttr.length > 1) {
        return this.converter.call(this, rowData.slice(0, -1));
      }
      return this.converter.call(this, rowData);
    } catch (e) {
      return '-';
    }
  }
}

@Component({
  selector: 'jhi-flowsheet-data-block',
  templateUrl: './flowsheet-data-block.component.html',
  styleUrls: ['./flowsheet-data-block.component.scss']
})
export class FlowsheetDataBlockComponent extends DataBlockComponent implements OnInit, AfterViewInit {
  @Input()
  public name = 'Name';

  @Input()
  public headerName = 'headerName';

  @Input()
  public columnsTemplate = new ColumnsTemplate(() => 'Col');

  @Input()
  public rows: Row[] = [];

  @Input()
  public tableColsLength = 5;

  @Input()
  public autoScroll = true;

  scrollModes: any[];

  @ViewChild(Table)
  table?: Table;

  columnValues: string[] = [];

  rowValues: Map<string, string[]> = new Map();

  rowValuesToDisplay: any[] = [];

  private _lastUpdate?: number;
  private _lastDataTimestamp?: number;
  private marginLeft?: any = 0;

  @Input()
  private interval = 14500; // 14 seconds and a half in milliseconds, default interval

  constructor() {
    super();
    this.scrollModes = [
      { label: 'On', value: true },
      { label: 'Off', value: false }
    ];
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    if (this.table) {
      const p = this.table.containerViewChild.nativeElement.getElementsByClassName('ui-table-scrollable-body')[1];
      const node = p.getElementsByClassName('ui-table-tbody')[0];

      const observer = new MutationObserver(mutations => {
        mutations.forEach(() => this.forceScrollToMaxLeft());
      });

      observer.observe(node, {
        attributes: false,
        childList: true,
        characterData: false
      });

      const nodeScroll = this.table.containerViewChild.nativeElement.getElementsByClassName('ui-table-scrollable-footer-box')[1];
      const observer2 = new MutationObserver(mutations => {
        mutations.forEach(m => this.checkScrollPosition(m));
      });

      observer2.observe(nodeScroll, {
        attributes: true,
        childList: false,
        characterData: false
      });

      // The interval should hava at least half a second less to give a margin on the received data, if the value is
      // strictly defined and the value arrives a couple of milliseconds after we could skip this information,
      // see method updateValue where this value is being used.
      if (this.interval >= 1000) {
        this.interval -= 500;
      }
      // Try to init the table with blanks
      this.initTableWithBlanks(13);
    }
  }

  initTableWithBlanks(cols: number): void {
    for (let i = 0; i < cols; i++) {
      this.insertColumn({ dataTimestamp: -1 });
    }
  }

  insertColumn(data: any): void {
    this.columnValues.push(this.columnsTemplate.getValue(data));
    this.rows.forEach(r => {
      let values = this.rowValues.get(r.header);
      if (!values) {
        values = [];
        this.rowValues.set(r.header, values);
      }
      let value = r.getValue(data);
      if (values.length > 0 && data['dataTimestamp'] !== -1 && value === '-') {
        value = values[values.length - 1] !== '' ? values[values.length - 1] : '-';
      }
      values.push(data['dataTimestamp'] !== -1 ? value : '');
    });
    this.recalculateRowValuesToDisplay();
  }

  public updateBulkValue(data: IHardwareVentilatorEvents): void {
    if (!Array.isArray(data)) return;
    const allData = data;

    // eslint-disable-next-line no-console
    // console.log("Last Update: "+this._lastUpdate+", Event: "+data['dataTimestamp']);
    // eslint-disable-next-line no-console
    // console.log(data);

    if (!data || !this.expanded) {
      return;
    }
    let t = 0; // allData[0].dataTimestamp;

    this._lastUpdate = -1;
    allData.forEach(d => {
      if (d.dataTimestamp - t > this.interval) {
        const lastTime = this.columnValues[this.columnValues.length - 1];
        const ms = this.msFromString(lastTime);
        const lt = ms === 0 ? this.interval : ms + this.interval;
        if (d.dataTimestamp < lt) {
          let pos = 0;
          // Check if the column already exists
          this.columnValues.forEach(col => {
            const colMilis = this.msFromString(col);
            if (colMilis + this.interval > d.dataTimestamp) {
              this.rows.forEach(r => {
                const values = this.rowValues.get(r.header);
                if (values && values[pos] === '-') {
                  values[pos] = r.getValue(d);
                  this.rowValues.set(r.header, values);
                  return;
                }
              });
            }
            pos++;
          });
        } else {
          if (this.columnValues.length >= this.tableColsLength) {
            this.columnValues.shift();
            this.rowValues.forEach(rv => {
              rv.shift();
            });
          }
          this.insertColumn(d);
          t = d.dataTimestamp;
          this._lastUpdate = t;
        }
        // eslint-disable-next-line no-console
        // console.log('update bulk value ' + d['dataTimestamp']);
      }
    });
  }

  private msFromString(str: string): number {
    if (str !== '' && str !== undefined) {
      const time = str.split(':');
      return parseInt(time[0], 0) * 60 * 60000 + parseInt(time[1], 0) * 60000 + parseInt(time[2], 0) * 1000;
    }
    return 0;
  }

  public updateValue(data: any): void {
    const now = new Date().getTime();

    // eslint-disable-next-line no-console
    // console.log("Last Update: "+this._lastUpdate+", Event: "+data['dataTimestamp']);
    // eslint-disable-next-line no-console
    // console.log(data);

    if (!data || !this.expanded) {
      return;
    }

    if ((this._lastUpdate === undefined || now - this.interval > this._lastUpdate) && this._lastDataTimestamp !== data['dataTimestamp']) {
      this._lastUpdate = now;
      this._lastDataTimestamp = data['dataTimestamp'];
    } else if (this._lastDataTimestamp === data['dataTimestamp']) {
      // If the data has the same dataTimestamp that the last data, it means that this is an update
      // of it (or maybe another event adding more information).
      // update the last column
      this.rows.forEach(r => {
        const val = r.getValue(data);
        if (!val || val === '-') {
          return;
        }

        const values = this.rowValues.get(r.header);
        values![values!.length - 1] = val;
      });
      return;
    } else {
      return;
    }

    if (this.columnValues.length >= this.tableColsLength) {
      this.columnValues.shift();
      this.rowValues.forEach(rv => {
        rv.shift();
      });
    }
    this.insertColumn(data);
  }

  public recalculateRowValuesToDisplay(): void {
    const data: any[] = [];
    this.rows.forEach(r => {
      data.push({
        header: r.header,
        values: this.rowValues.get(r.header)
      });
    });
    this.rowValuesToDisplay = data;
  }

  private forceScrollToMaxLeft(): void {
    if (this.autoScroll) {
      const t = this.table!.containerViewChild.nativeElement.getElementsByClassName('ui-table-scrollable-body')[1];
      t.scrollLeft = t.scrollWidth + 200;
      this.marginLeft = t.scrollLeft;
    }
  }

  private checkScrollPosition(record: MutationRecord): void {
    // Creating an element to extract the styles
    const element = document.createElement('div');
    const targetElement = record.target as any;
    element.setAttribute('style', targetElement.attributes.style.value);
    let mLeft = element.style['margin-left'];
    mLeft = -parseInt(mLeft, 10);
    this.autoScroll = true;
    if (parseInt(this.marginLeft, 10) > mLeft) {
      this.autoScroll = false;
    }
  }

  public changeAutoScroll(autoScroll: boolean): void {
    this.autoScroll = !autoScroll;
    this.forceScrollToMaxLeft();
  }
}
