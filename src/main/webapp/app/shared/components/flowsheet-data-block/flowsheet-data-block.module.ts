/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';

import { DragDropModule } from 'primeng/dragdrop';
import { TableModule } from 'primeng/table';
import { FieldsetModule } from 'primeng/fieldset';
import { DataBlockModule } from '../data-block/data-block.module';
import { FlowsheetDataBlockComponent } from './flowsheet-data-block.component';
import { SelectButtonModule } from 'primeng/selectbutton';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    DragDropModule,
    TableModule,
    DataBlockModule,
    FieldsetModule,
    SelectButtonModule,
    FormsModule,
    ButtonModule
  ],
  declarations: [FlowsheetDataBlockComponent],
  exports: [FlowsheetDataBlockComponent]
})
export class FlowsheetDataBlockModule {}
