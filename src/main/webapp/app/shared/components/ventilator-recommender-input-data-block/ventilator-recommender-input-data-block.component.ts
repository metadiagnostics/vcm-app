/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, Input, OnInit } from '@angular/core';

import { IScalarValue } from 'app/shared/model/ext/scalar-value.model';
import { IVentilatorRecommendationRequest } from 'app/shared/model/ext/ventilator-recommender.models';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons/faCheckCircle';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons/faTimesCircle';

import { DataBlockComponent } from 'app/shared/components/data-block/data-block.component';
import { Subject } from 'rxjs';
import { MonitorHardwareConnection, MonitorHardwareService } from 'app/shared/services/monitor/monitor-hardware.service';
import { IHardwareStatus } from 'app/shared/model/ext/hardware-status.model';
import { IEnumValues } from 'app/monitor/ventilator.model';

export enum VRIDBType {
  STRING,
  I_SCALAR_VALUE
}

@Component({
  selector: 'jhi-vri-data-block',
  templateUrl: './ventilator-recommender-input-data-block.component.html',
  styleUrls: ['./ventilator-recommender-input-data-block.component.scss']
})
export class VentilatorRecommenderInputDataBlockComponent extends DataBlockComponent implements OnInit {
  @Input()
  public name = 'Name';

  @Input()
  public type = VRIDBType.I_SCALAR_VALUE;

  @Input()
  public dataKey = '';

  @Input()
  public ventilatorRequestDataKey = '';

  @Input()
  value?: any;

  @Input()
  public lowerBound: number | undefined;

  @Input()
  public lowerBoundClass = 'lower';

  @Input()
  public upperBound: number | undefined;

  @Input()
  public upperBoundClass = 'upper';

  @Input()
  public normalBoundClass = 'normal';

  @Input()
  public chartMaxDataLength = 200;

  @Input()
  public alarmMax = 80;

  @Input()
  public newAlarmMax = 80;

  @Input()
  public alarmMin = 15;

  @Input()
  public newAlarmMin = 15;

  @Input()
  public unit = '';

  @Input()
  public hwId = '';

  @Input()
  public codeId = '';

  @Input()
  public alarmMinCodeId = '';

  @Input()
  public alarmMaxCodeId = '';

  @Input()
  public order = 0;

  @Input()
  public enumValues!: IEnumValues;

  private ventSettingSubject = new Subject<string>();

  public check = faCheckCircle;
  public cancel = faTimesCircle;
  private manufacturer='';

  valueClass = '';

  // This is necessary so we can access the enum from the html template
  VRIDBType = VRIDBType;

  displayModal = false;

  newValueString: any;

  inputFocus = 'alarmMin';

  private hwConnection?: MonitorHardwareConnection;

  constructor(protected monitorHardwareService: MonitorHardwareService) {
    super();
  }

  ngOnInit(): void {
    // Connect to the device
    this.monitorHardwareService.connect().subscribe(hwConnection => {
      this.hwConnection = hwConnection;
    });
    this.newValueString = '';
  }

  public updateValue(data: any): void {
    if (this.dataKey === undefined || data[this.dataKey] === undefined) return;
    this.manufacturer = data['manufacturer'];
    if (this.type === VRIDBType.I_SCALAR_VALUE) {
      this.value = data[this.dataKey] as IScalarValue;
      if (this.value === null) {
        return;
      }
      if (this.value === undefined) {
        // When we freeze values we use the request data keys to update values
        this.value = data[this.ventilatorRequestDataKey] as IScalarValue;
      }
      if (this.value !== undefined && !Number.isInteger(this.value.value)) {
        this.value.value = Math.round(this.value.value * 100) / 100;
      }
      if (this.lowerBound && this.value <= this.lowerBound) {
        this.valueClass = this.lowerBoundClass;
      } else if (this.upperBound && this.value >= this.upperBound) {
        this.valueClass = this.upperBoundClass;
      } else {
        this.valueClass = this.normalBoundClass;
      }
    } else if (this.type === VRIDBType.STRING) {
      this.value = data[this.dataKey] as string;
    } else {
      throw new Error('Unssuported Type ' + this.type);
    }
  }

  public configureRequest(req: IVentilatorRecommendationRequest): void {
    if (this.value) {
      req[this.ventilatorRequestDataKey] = this.value; // on vent settings this have change Request Data key
    }
  }

  valueAndUnit(): string {
    let valueAndUnit = '';
    if (this.value === undefined || this.value === null || (isNaN(this.value) && (this.enumValues === null || this.enumValues === undefined))) {
      valueAndUnit = '- -';
    } else {
      if (this.enumValues !== null && this.enumValues !== undefined) {
        return this.value; // no need to display units
      } else {
        valueAndUnit += this.value.value;
      }
    }
    return valueAndUnit + ' ' + this.unit;
  }

  nameAndUnit(): string{
    if (this.unit === undefined || this.unit==='') return this.name;
    return this.name ===undefined?'':this.name+ ' (' + this.unit + ')';
  }
  onlyUnit(): string {
    let unit = '('
    if (this.unit === undefined || this.unit===null) unit += '';
    else unit += this.unit + ')';
    return unit;
  }
  onlyValue(): string{
    let value = '';
    if (this.value === undefined ||
      this.value === null ||
      (isNaN(this.value) && (this.type !== VRIDBType.I_SCALAR_VALUE) && (this.enumValues === null || this.enumValues === undefined))
    ) {
      value = '- -';
    } else {
      if (this.enumValues !== null && this.enumValues !== undefined) {
        if (typeof this.value === 'string'){
          return this.value; // this is the case for Fio2 that contains "air" or "maximum"
        }
        if (isNaN(this.value)) {
          return ''; // until this not get initialized is NaN
        }
        return this.value; // no need to display units
      } else {
        value += this.value.value;
      }
    }
    return value;
  }

  public adjustAlarmValues(): void {
    // open a dialog
    this.displayModal = true;
  }

  public getValue(): number {
    if (this.value === undefined || this.value === null) {
      return 0;
    } else {
      if (this.enumValues !== null && this.enumValues !== undefined) {
        if (isNaN(this.value)) {
          return 0; // until this not get initialized is NaN
        }
        return this.value; // is not an object is just a string
      } else {
        return this.value.value;
      }
    }
  }

  public numpadClick(num: number): void {
    this.newValueString += '' + num;
  }
  public setInput(inputName: string): void {
    this.inputFocus = inputName;
  }

  public numpadLimitClick(num: number): void {
    if (this.inputFocus.includes('Min')) {
      if (this.alarmMinCodeId===null || this.alarmMinCodeId===undefined) {
        return;
      }
    }
    if (this.inputFocus.includes('Max')) {
      if (this.alarmMaxCodeId===null || this.alarmMaxCodeId===undefined) {
        return;
      }
    }
    this[this.inputFocus] += '' + num;
  }

  public numpadNewValue(ev: Event): void {
    if (ev.target !== null) {
      this.newValueString = (ev.target as HTMLInputElement).value;
    }
  }
  public numpadLimit(ev: Event, limit: string): void {
    if (ev.target !== null) {
      this[limit] = (ev.target as HTMLInputElement).value;
    }
  }

  public ventChange(): Subject<string> {
    return this.ventSettingSubject;
  }

  public closeDialog(): void {
    this.displayModal = false;
  }

  public applyVentChanges(): void {
    let newNumber;
    if (this.newValueString !== undefined && (typeof this.newValueString === 'number' || typeof this.newValueString === 'string')) {
      newNumber = Number(this.newValueString);
    } else if (this.newValueString !== undefined && typeof this.newValueString.position === 'number') {
      // Workaround to fix the non working prime ng dropdown
      newNumber = this.newValueString.setValue;
    }
    // We are not validating here if the range is correct
    if (this.value === undefined || this.newValueString.length === 0 || isNaN(newNumber)) {
      return;
    }
    // this.value = newNumber; // not apply directly just wait next frames (5 seconds or more) to set it from device
    this.hwConnection?.sendNewVentilatorValue(this.hwId, this.codeId, '' + newNumber);
    this.displayModal = false;
  }

  public applyLimitChanges(): void {
    this.doLimitChanges('newAlarmMin', this.alarmMinCodeId);
    this.doLimitChanges('newAlarmMax', this.alarmMaxCodeId);
    this.displayModal = false;
  }

  public isThornhill(): boolean{
     return this.manufacturer === 'Thornhill Medical';
  }

  private doLimitChanges(attr:string, codeId:string): void {
    let newNumber;
    if (this[attr] !== undefined && (typeof this[attr] === 'number' || typeof this[attr] === 'string')) {
      newNumber = Number(this[attr]);
    } else if (this[attr] !== undefined && typeof this[attr].position === 'number') {
      // Workaround to fix the non working prime ng dropdown
      newNumber = this[attr].setValue;
    }
    // We are not validating here if the range is correct
    if (this[attr].length === 0 || isNaN(newNumber)) {
      return;
    }
    // this.value = newNumber; // not apply directly just wait next frames (5 seconds or more) to set it from device
    this.hwConnection?.sendNewVentilatorValue(this.hwId, codeId, '' + newNumber);
  }

}
