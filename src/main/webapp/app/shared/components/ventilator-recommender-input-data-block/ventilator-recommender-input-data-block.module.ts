/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';
import { ChartModule } from 'primeng/chart';
import { DragDropModule } from 'primeng/dragdrop';
import { FieldsetModule } from 'primeng/fieldset';
import { DataBlockModule } from '../data-block/data-block.module';
import { VentilatorRecommenderInputDataBlockComponent } from './ventilator-recommender-input-data-block.component';
import { DialogModule } from 'primeng/dialog';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TabViewModule } from 'primeng/tabview';
import { DropdownModule } from 'primeng/dropdown';
@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ChartModule,
    DragDropModule,
    FieldsetModule,
    DataBlockModule,
    DialogModule,
    FontAwesomeModule,
    TabViewModule,
    DropdownModule
  ],
  declarations: [VentilatorRecommenderInputDataBlockComponent],
  exports: [VentilatorRecommenderInputDataBlockComponent]
})
export class VentilatorRecommenderInputDataBlockModule {}
