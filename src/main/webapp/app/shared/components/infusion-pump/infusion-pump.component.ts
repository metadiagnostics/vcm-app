/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, ElementRef, HostListener, Input, OnInit, ViewChild } from '@angular/core';
import { jqxKnobComponent } from 'jqwidgets-ng/jqxknob';
import { faPlay } from '@fortawesome/free-solid-svg-icons/faPlay';
import { faPause } from '@fortawesome/free-solid-svg-icons/faPause';
import { faForward } from '@fortawesome/free-solid-svg-icons/faForward';
import { IPumpModel } from 'app/shared/model/pump.model';
import { DrawerService } from 'app/drawer/drawer.service';
import { MonitorHardwareConnection, MonitorHardwareService } from "app/shared/services/monitor/monitor-hardware.service";
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons/faCheckCircle';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons/faTimesCircle';
import { faPlug } from '@fortawesome/free-solid-svg-icons/faPlug';
import { faBatteryFull } from '@fortawesome/free-solid-svg-icons/faBatteryFull';
import { faBatteryThreeQuarters } from '@fortawesome/free-solid-svg-icons/faBatteryThreeQuarters';
import { faBatteryHalf } from '@fortawesome/free-solid-svg-icons/faBatteryHalf';
import { faBatteryQuarter } from '@fortawesome/free-solid-svg-icons/faBatteryQuarter';
import { faBatteryEmpty } from '@fortawesome/free-solid-svg-icons/faBatteryEmpty';
import { faBolt } from '@fortawesome/free-solid-svg-icons/faBolt';
@Component({
  selector: 'jhi-infusion-pump',
  templateUrl: './infusion-pump.component.html',
  styleUrls: ['./infusion-pump.component.scss']
})
export class InfusionPumpComponent implements OnInit {
  @ViewChild('myKnob1') myKnob1?: jqxKnobComponent;
  @ViewChild('myKnob1') myKnob1Element = {};
  @ViewChild('myKnob2') myKnob2?: jqxKnobComponent;
  @ViewChild('infChannel1') infChannel1?: ElementRef;

  @Input()
  public pumpDef!: IPumpModel;

  @Input()
  public hwId = '';

  public play = faPlay;
  public forward = faForward;
  public pause = faPause;

  // pump initial state (this will change when data is received from service)
  public pumpStatus1 = this.pause;
  public pumpStatus2 = this.pause;

  public connectStatus = faPlug;
  // Rate values
  public rateValue1 = 0;
  public rateValue2 = 0;
  public currentRate1 = 0;
  public currentRate2 = 0;

  public pumpMedicine1 = '';
  public pumpMedicineFntClass1 = '';
  public pumpMedicineBgClass1 = '';
  public pumpMedicine2 = '';
  public pumpMedicineFntClass2 = '';
  public pumpMedicineBgClass2 = '';
  public cds1 = '';
  public cds2 = '';
  public locusOfControl1 = '';
  public locusOfControl2 = '';

  public pumpVi1 = 0;
  public pumpVi2 = 0;
  public pumpVtbi1 = 0;
  public pumpVtbi2 = 0;

  public timeRemaining1 = '';
  public timeRemaining2 = '';

  public bolus1 = 0;
  public bolusRate1 = 0;
  public bolus2 = 0;
  public bolusRate2 = 0;
  public devProgMode1 = '';
  public devProgMode2 = '';
  public devProgModeSent1 = '';
  public devProgModeSent2 = '';

  public check = faCheckCircle;
  public cancel = faTimesCircle;

  @Input()
  public pumpWidth!: number;

  @Input()
  public redraw = true;

  @Input()
  public vtbiCodeId = '';

  @Input()
  public rateCodeId = '';

  @Input()
  public bolusCodeId = '';

  @Input()
  public bolusRateCodeId = '';

  private oldPumpWidth = this.pumpWidth;

  private redrawId: ReturnType<typeof setTimeout>;
  private hwConnection?: MonitorHardwareConnection;
  private inputFocus = '';

  // Pumps batteries
  public batteryPercentage = 100;
  public batteryConnectStatus = faPlug;
  public batteryConnectStatusBolt = faBolt;
  public batteryConnectStatusBoltVisible = false;

  displayModal = false;
  newVtbi= '';
  newRate= '';
  vtbi = 0;
  rate = 0;
  private deviceId='';
  private masterDeviceId='';
  private PAUSE_STRING = 'PAUSED_CONTINUOUS';
  private INFUSING_STRING = 'INFUSING_CONTINUOUS';
  public actionchannel1disabled = false;
  public actionchannel2disabled = false;

  bolus = 0;
  bolusRate = 0;
  newBolus = '';
  newBolusRate = '';


  style: any = {
    stroke: '#dfe3e9',
    strokeWidth: 3,
    fill: {
      color: '#fefefe',
      gradientType: 'linear',
      gradientStops: [
        [0, 1],
        [50, 0.9],
        [100, 1]
      ]
    }
  };
  marks: any = {
    colorRemaining: { color: 'grey', border: 'grey' },
    colorProgress: { color: '#00a4e1', border: '#00a4e1' },
    type: 'line',
    offset: '76%',
    thickness: 2,
    size: '0%', // visible attribute do not work disabling setting this to 0
    majorSize: '0%', // visible attribute do not work disabling setting this to 0
    majorInterval: 10,
    minorInterval: 2,
    visible: false
  };
  labels: any = {
    offset: '98%',
    step: 10,
    visible: false
  };
  progressBar: any = {
    style: { fill: '#0da233', stroke: 'grey' },
    size: '15%',
    offset: '60%',
    background: { fill: 'rgba(34,228,66,0.39)', stroke: 'rgba(34,228,66,0.39)' }
  };
  pointer: any = {
    type: 'arrow',
    style: { fill: '#0da233', stroke: 'grey' },
    size: '59%',
    offset: '49%',
    thickness: 20,
    visible: true
  };

  constructor(protected drawerService: DrawerService,
              protected monitorHardwareService: MonitorHardwareService) {
    this.redrawId = setTimeout(() => {}, 1);
  }

  getPumpSize(): number {
    if (this.oldPumpWidth !== this.pumpWidth && this.redraw === true) {
      this.oldPumpWidth = this.pumpWidth;
    }
    return this.pumpWidth;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any): void {
    const screenWidth = event.target.innerWidth;
    this.oldPumpWidth = this.pumpWidth;
    this.pumpWidth = this.getProportionalWidth(screenWidth);
    this.redraw = false;
    /** Workround to redraw the jqxKnobComponent since the redraw method is not implemented
     * the library supports change height and weight but the library never re-render it.
     * This can be removed if a new library is used and if it provides the redraw() method.
     * **/
    clearTimeout(this.redrawId);
    this.redrawId = global.setTimeout(() => {
      this.redraw = true;
    }, 100);
  }

  getProportionalWidth(screenWidth: number): number {
    return ((screenWidth * 0.91 * 0.6) / 2) * 0.5;
  }

  onChange(event: any, pump: string): void {
    if (event.args.changeSource === 'propertyChange' || event.args.changeSource === 'val') {
      return;
    }
    const pumpRate = event.args.value;
    if ('pump1' === pump) {
      this.rateValue1 = pumpRate;
    } else if ('pump2' === pump) {
      this.rateValue2 = pumpRate;
    }
  }

  onMouseDown(event: any): void {
    event.stopPropagation();
  }

  playOrPause(pump: string): void {
    let pst = this.pumpStatus1;
    if ('pump2' === pump) {
      pst = this.pumpStatus2;
    }
    if (pst === this.play) {
      if ('pump1' === pump) {
        // Send play to BE
        this.hwConnection?.resumeInfusionPump(this.hwId, 'PUMP1');
        // Right after this command is executed we need to disable the button that will be enabled when new pump date is coming with different value
        // save that should be infusing
        this.devProgModeSent1 = this.INFUSING_STRING;
        // Disable the button
        this.actionchannel1disabled = true;
      } else {
        this.hwConnection?.resumeInfusionPump(this.hwId, 'PUMP2');
        // Right after this command is executed we need to disable the button that will be enabled when new pump date is coming with different value
        // save that should be paused
        this.devProgModeSent2 = this.INFUSING_STRING;
        // Disable the button
        this.actionchannel2disabled = true;
      }
    } else { // is a pause
      if ('pump1' === pump) {
        this.hwConnection?.pauseInfusionPump(this.hwId, 'PUMP1');
        // Right after this command is executed we need to disable the button that will be enabled when new pump date is coming with different value
        // save that should be infusing
        this.devProgModeSent1 = this.PAUSE_STRING;
        // Disable the button
        this.actionchannel1disabled = true;
      } else {
        this.hwConnection?.pauseInfusionPump(this.hwId, 'PUMP2');
        // Right after this command is executed we need to disable the button that will be enabled when new pump date is coming with different value
        // save that should be infusing
        this.devProgModeSent2 = this.PAUSE_STRING;
        // Disable the button
        this.actionchannel2disabled = true;
      }
    }
  }

  forwardAction(pump: string): void {

  }

  medicineColorClass(bgColor: string, fontColor: string): string {
    return bgColor + ' ' + fontColor;
  }

  cdsColorClass(cds1: string, locusOfControl: string): string {
    let classes = 'cds-color';
    if (cds1.startsWith('No')) {
      classes = 'no-cds-color';
    }
    if (locusOfControl === 'Remote') {
      classes += ' label-separator';
    }
    return classes;
  }

  locusClass(locusOFControl: string): string {
    if (locusOFControl.startsWith('Local')) {
      return 'local-color';
    }
    return 'remote-color';
  }

  iconClass(percentage: number): string {
    if (percentage < 25) {
      return 'red-percentage-color blinking';
    }
    return 'regular-percentage-color';
  }

  ngOnInit(): void {
    this.reconfigure(this.pumpDef); // this is called by a rest callback ans ensures integrity ! Do not remove this.
    const screenWidth = window.innerWidth;
    this.pumpWidth = this.getProportionalWidth(screenWidth);
    // Connect to the device
    this.monitorHardwareService.connect().subscribe(hwConnection => {
    this.hwConnection = hwConnection;
    });
  }

  reconfigure(pumpDef: IPumpModel): void {
    this.pumpDef.name = pumpDef.name;


    this.rateValue1 = !pumpDef.infusionRate1 ? this.rateValue1 : pumpDef.infusionRate1;
    this.pumpMedicineBgClass1 = !pumpDef.medicineBgClass1 ? this.pumpMedicineBgClass1 : pumpDef.medicineBgClass1;
    this.pumpMedicineFntClass1 = !pumpDef.medicineFntClass1 ? this.pumpMedicineFntClass1 : pumpDef.medicineFntClass1;
    this.pumpMedicine1 = !pumpDef.medicine1 ? this.pumpMedicine1 : pumpDef.medicine1;
    this.pumpVi1 = !pumpDef.vi1 ? this.pumpVi1 : pumpDef.vi1;
    this.pumpVtbi1 = !pumpDef.vtbi1 ? this.pumpVtbi1 : pumpDef.vtbi1;
    this.currentRate1 = !pumpDef.currentRate1 ? this.currentRate1 : pumpDef.currentRate1;
    this.timeRemaining1 = !pumpDef.timeRemaining1 ? this.timeRemaining1 : pumpDef.timeRemaining1;
    this.locusOfControl1 = !pumpDef.locusOfControl1 ? this.locusOfControl1 : pumpDef.locusOfControl1;
    this.cds1 = !pumpDef.cds1 ? this.cds1 : pumpDef.cds1;
    this.myKnob1?.value(this.rateValue1);
    this.bolus1 = !pumpDef.bolusProg1 ? this.bolus1 : pumpDef.bolusProg1;
    this.bolusRate1 = !pumpDef.bolusProgRate1 ? this.bolusRate1 : pumpDef.bolusProgRate1;
    this.devProgMode1 = pumpDef.devProgMode1;
    // change the pause resume icon
    if (this.devProgMode1 === this.PAUSE_STRING) {
      this.pumpStatus1 = this.play; // show a play because it's paused
    } else {
      this.pumpStatus1 = this.pause;
    }
    this.actionchannel1disabled = (this.devProgModeSent1 !== '') && (this.devProgMode1!=='') && this.devProgMode1 !== this.devProgModeSent1;

    this.rateValue2 = !pumpDef.infusionRate2 ? this.rateValue2 : pumpDef.infusionRate2;
    this.pumpMedicineBgClass2 = !pumpDef.medicineBgClass2 ? this.pumpMedicineBgClass2 : pumpDef.medicineBgClass2;
    this.pumpMedicineFntClass2 = !pumpDef.medicineFntClass2 ? this.pumpMedicineFntClass2 : pumpDef.medicineFntClass2;
    this.pumpMedicine2 = !pumpDef.medicine2 ? this.pumpMedicine2 : pumpDef.medicine2;
    this.pumpVi2 = !pumpDef.vi2 ? this.pumpVi2 : pumpDef.vi2;
    this.pumpVtbi2 = !pumpDef.vtbi2 ? this.pumpVtbi2 : pumpDef.vtbi2;
    this.currentRate2 = !pumpDef.currentRate2 ? this.currentRate2 : pumpDef.currentRate2;
    this.timeRemaining2 = !pumpDef.timeRemaining2 ? this.timeRemaining2 : pumpDef.timeRemaining2;
    this.bolus2 = !pumpDef.bolusProg2? this.bolus2 : pumpDef.bolusProg2;
    this.bolusRate2 = !pumpDef.bolusProgRate2 ? this.bolusRate2 : pumpDef.bolusProgRate2;
    this.cds2 = !pumpDef.cds2 ? this.cds2 : pumpDef.cds2;
    this.locusOfControl2 = !pumpDef.locusOfControl2 ? this.locusOfControl2 : pumpDef.locusOfControl2;
    this.myKnob2?.value(this.rateValue2);
    this.devProgMode2 = pumpDef.devProgMode2;
    // change the pause resume icon
    if (this.devProgMode2 === 'PAUSED_CONTINUOUS') {
      this.pumpStatus2 =  this.play; // show a play because it's paused
    } else {
      this.pumpStatus2 = this.pause;
    }
    this.actionchannel2disabled =  (this.devProgModeSent2 !== '') && (this.devProgMode2!=='') && this.devProgMode2 !== this.devProgModeSent2;

    this.batteryPercentage = pumpDef.batteryPercentage;
    if (!pumpDef.onMainsPower) { // present
      this.batteryConnectStatusBoltVisible = false;
      if (this.batteryPercentage > 75) {
        this.batteryConnectStatus = faBatteryFull;
      } else if (this.batteryPercentage > 51) {
        this.batteryConnectStatus = faBatteryThreeQuarters;
      } else if (this.batteryPercentage > 26) {
        this.batteryConnectStatus = faBatteryHalf;
      } else if (this.batteryPercentage > 5) {
        this.batteryConnectStatus = faBatteryQuarter;
      } else {
        this.batteryConnectStatus = faBatteryEmpty;
      }
    } else {
      if (pumpDef.batteryPresent) {
        //Charging the battery
        this.batteryConnectStatus = faBatteryEmpty;
        this.batteryConnectStatusBoltVisible = true;
      } else {
        this.batteryConnectStatus = faPlug;
      }
    }
  }

  public openDialog(vtbi: number,
                    rate: number,
                    bolus: number,
                    bolusRate: number,
                    deviceId :string,
                    suffix :string): void {
    this.vtbi = vtbi;
    this.rate = rate;
    this.bolus = bolus;
    this.bolusRate = bolusRate;
    this.masterDeviceId = deviceId;
    this.deviceId = deviceId+suffix;
    this.displayModal = true;
  }

  public closeDialog(): void {
    this.displayModal = false;
  }

  public setInput(inputName: string): void {
    this.inputFocus = inputName;
  }

  public numpadClick(num: number): void {
    if (this.inputFocus === '') return;
    this[this.inputFocus] += '' + num;
  }

  public numpadNewVtbi(ev: Event): void {
    if (ev.target !== null) {
      this.newVtbi = (ev.target as HTMLInputElement).value;
    }
  }

  public numpadNewRate(ev: Event): void {
    if (ev.target !== null) {
      this.newRate = (ev.target as HTMLInputElement).value;
    }
  }

  public numpadNewBolus(ev: Event): void {
    if (ev.target !== null) {
      this.newBolus = (ev.target as HTMLInputElement).value;
    }
  }

  public numpadNewBolusRate(ev: Event): void {
    if (ev.target !== null) {
      this.newBolusRate = (ev.target as HTMLInputElement).value;
    }
  }

  public getBolusHints(bolus: number, bolusRate: number): string {
    return 'Bolus Volume: ' + bolus + ' / Bolus Rate ' + bolusRate;
  }

  public applyChannelSettings(): void {
    this.doSettingChange('newVtbi', this.vtbiCodeId);
    this.doSettingChange('newRate', this.rateCodeId);
    this.displayModal = false;
    this.newVtbi = ''; // clearing the value that the user provides
    this.newRate = ''; // clearing the value that the user provides
  }

  public applyBolusSettings(): void {
    this.doSyncSettingChange('newBolus', this.bolusCodeId, 'newBolusRate', this.bolusRateCodeId);
    this.displayModal = false;
    this.newBolus = ''; // clearing the value that the user provides
    this.newBolusRate = ''; // clearing the value that the user provides
  }

  public getStyleChannel1(): string {
    return this.actionchannel1disabled?'color: #999999':'color: #000000';
  }

  public getStyleChannel2(): string {
    return this.actionchannel2disabled?'color: #999999':'color: #000000';
  }


  private doSyncSettingChange(attr1:string, codeId1:string,
                              attr2:string, codeId2:string): void {
    //Checking that the values can be set
    // Check 1
    let newNumber1;
    if (this[attr1] !== undefined && (typeof this[attr1] === 'number' || typeof this[attr1] === 'string')) {
      newNumber1 = Number(this[attr1]);
    } else if (this[attr1] !== undefined && typeof this[attr1].position === 'number') {
      // Workaround to fix the non working prime ng dropdown
      newNumber1 = this[attr1].setValue;
    }
    // We are not validating here if the range is correct
    if (this[attr1].length === 0 || isNaN(newNumber1)) {
      return;
    }
    let newNumber2;
    if (this[attr2] !== undefined && (typeof this[attr2] === 'number' || typeof this[attr2] === 'string')) {
      newNumber2 = Number(this[attr2]);
    } else if (this[attr2] !== undefined && typeof this[attr2].position === 'number') {
      // Workaround to fix the non working prime ng dropdown
      newNumber2 = this[attr2].setValue;
    }
    // We are not validating here if the range is correct
    if (this[attr2].length === 0 || isNaN(newNumber2)) {
      return;
    }
    if (newNumber2 >= newNumber1) {
      this.hwConnection?.sendNewSyncPumpSetting(this.masterDeviceId, this.deviceId, codeId1, '' + newNumber1, codeId2, '' + newNumber2);
    }
    else {
      // eslint-disable-next-line no-console
      console.log("We can not send a Bolus Volume greater than a Bolus Rate");
    }
  }

  private doSettingChange(attr:string, codeId:string): void {
    let newNumber;
    if (this[attr] !== undefined && (typeof this[attr] === 'number' || typeof this[attr] === 'string')) {
      newNumber = Number(this[attr]);
    } else if (this[attr] !== undefined && typeof this[attr].position === 'number') {
      // Workaround to fix the non working prime ng dropdown
      newNumber = this[attr].setValue;
    }
    // We are not validating here if the range is correct
    if (this[attr].length === 0 || isNaN(newNumber)) {
      return;
    }
    // this.value = newNumber; // not apply directly just wait next frames (5 seconds or more) to set it from device
    this.hwConnection?.sendNewPumpSetting(this.masterDeviceId, this.deviceId, codeId, '' + newNumber);
  }
}
