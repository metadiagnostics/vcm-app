/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import './vendor';
import { VcmSharedModule } from 'app/shared/shared.module';
import { VcmCoreModule } from 'app/core/core.module';
import { VcmAppRoutingModule } from './app-routing.module';
import { VcmHomeModule } from './home/home.module';
import { VcmEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
// custom modules
import { MonitorModule } from './monitor/monitor.module';
import { DrawerModule } from './drawer/drawer.module';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    DrawerModule,
    VcmSharedModule,
    VcmCoreModule,
    VcmHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    VcmEntityModule,
    MonitorModule,
    VcmAppRoutingModule
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent]
})
export class VcmAppModule {}
