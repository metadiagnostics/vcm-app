/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { AfterViewInit, Component, OnInit, OnDestroy, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { MonitorHardwareService, MonitorHardwareConnection } from '../shared/services/monitor/monitor-hardware.service';
import { JhiAlertService } from 'ng-jhipster';
import { IMonitorAssignment, IMonitorDevice, MonitorAssignmentUtils } from 'app/shared/model/ext/monitor-assignment.models';
import { IHardwareStatus } from 'app/shared/model/ext/hardware-status.model';
import { Subscription } from 'rxjs';
import { CdspSimulationsService } from 'app/shared/services/cdsp/cdsp-simulations.service';
import { DeviceType } from 'app/shared/model/enumerations/device-type.model';
import { PulseStatesService } from 'app/shared/services/pulse/pulse-states.service';
import { PulseSimulationsService } from 'app/shared/services/pulse/pulse-simulations.service';
import { ISimulationInfo, PulseSimulationState, ISimulationInfoUtils } from 'app/shared/model/ext/pulse.models';
import { MonitorDataConnection, MonitorDataService } from 'app/shared/services/monitor/monitor-data.service';

class ScenarioModel {
  public constructor(public id: string, public name: string) {}
}

class UIState {
  public constructor(
    public disableInitialStateDropdown: boolean = false,
    public disableScenarioDropdown: boolean = false,
    public disableExecutionStepsDropdown: boolean = false,
    public showStartButton: boolean = false,
    public showRunButton: boolean = false,
    public showPauseButton: boolean = false,
    public showTerminateButton: boolean = false,
    public selectedInitialState: string = '',
    public selectedScenario: ScenarioModel = { id: '', name: '' },
    public selectedExecutionSteps: number = 30
  ) {}

  public reset(): void {
    this.disableInitialStateDropdown = false;
    this.disableScenarioDropdown = false;
    this.disableExecutionStepsDropdown = false;
    this.showStartButton = false;
    this.showRunButton = false;
    this.showPauseButton = false;
    this.showTerminateButton = false;
    this.selectedInitialState = '';
    this.selectedScenario = { id: '', name: '' };
    this.selectedExecutionSteps = 30;
  }
}

@Component({
  selector: 'jhi-pulse-controller',
  templateUrl: './pulse-controller.component.html',
  styleUrls: ['./pulse-controller.component.scss']
})
export class PulseControllerComponent implements AfterViewInit, OnInit, OnDestroy {
  public static SCENARIO_TAG_NAME = '_VCM_SCENARIO_';
  public static SIM_STEPS_TAG_NAME = '_VCM_SIM_STEPS_TAG_NAME_';

  public static SIM_STEP_INCREMENT = 1000;

  /**
   * For the time being, a Simulation is always tied to an Assignment.
   * This is why this component needs an Assignment and not just a HardwareId (Simulation Name).
   * The only way we have right now to start listening events from Pulse is by activating
   * an Assingment. We may want to change this in the future.
   */
  @Input()
  assignment: IMonitorAssignment = { assignmentId: -1 };

  /**
   * If there is an error, the UI doesn't show any control.
   */
  error?: string;

  /**
   * A Status message.
   */
  status?: string;

  /**
   * The state of the various UI Components.
   */
  uiState = new UIState();

  /**
   * The list of possible Initial States.
   */
  states?: string[];

  /**
   * The list of possible Scenarios
   */
  scenarios?: ScenarioModel[];

  private monitorDataConnection?: MonitorDataConnection;
  private hwConnection?: MonitorHardwareConnection;

  private hardwareSubscriptions: Subscription[] = [];

  private pulseDevice?: IMonitorDevice;

  constructor(
    protected pulseStatesService: PulseStatesService,
    protected pulseSimulationsService: PulseSimulationsService,
    protected cdspSimulationsService: CdspSimulationsService,
    protected monitorDataService: MonitorDataService,
    protected monitorHardwareService: MonitorHardwareService,
    protected alertService: JhiAlertService,
    protected translateService: TranslateService
  ) {}

  ngOnInit(): void {
    this.pulseStatesService.enabled().subscribe(r => {
      if (r.body === true) {
        this.pulseStatesService.queryStates().subscribe(states => (this.states = states.body || []));
      }
    });
    // Get Scenarios from backend
    this.cdspSimulationsService.queryScenarios().subscribe(list => {
      this.scenarios = list.body?.map(s => new ScenarioModel(s, s));
    });

    const pulseDevices = MonitorAssignmentUtils.getUniqueDevices(this.assignment).filter(d => {
      return d.type === DeviceType.PULSE;
    });

    if (!pulseDevices || pulseDevices?.length === 0) {
      this.error = 'vcmApp.pulseController.error.noPulseDevice';
      return;
    } else if (pulseDevices.length !== 1) {
      this.error = 'vcmApp.pulseController.error.moreThanOnePulseDevice';
      return;
    } else {
      this.pulseDevice = pulseDevices[0];
    }

    this.fetchAndUpdateSimulationInfo(this.pulseDevice.hardwareId);

    // Connect to the monitor
    this.monitorDataService.connect().subscribe(dataConnection => {
      this.monitorDataConnection = dataConnection;

      // Activate the assignment so we can start receiving events
      this.monitorDataConnection.activateAssignment(this.assignment.assignmentId);
    });

    // Connect to the device
    this.monitorHardwareService.connect().subscribe(hwConnection => {
      this.hwConnection = hwConnection;

      // Subscription to Status Events
      this.hardwareSubscriptions.push(
        this.hwConnection.receiveStatus().subscribe((status: IHardwareStatus) => {
          this.onSimulationStatusUpdate(status);
        })
      );

      // Subscribe only to Status events
      this.hwConnection.subscribe(this.pulseDevice!.hardwareId, false, true, false);
    });
  }

  ngAfterViewInit(): void {}

  ngOnDestroy(): void {
    // unsubscribe from hadware
    if (this.hardwareSubscriptions) {
      this.hardwareSubscriptions.forEach(s => {
        s.unsubscribe();
      });
    }

    // Disconnect from the device
    if (this.hwConnection) {
      this.hwConnection.disconnect();
    }

    // Disconnect from the monitor
    if (this.monitorDataConnection) {
      this.monitorDataConnection.disconnectAndDeactivate(this.assignment.assignmentId);
    }
  }

  startSimulation(): void {
    this.cdspSimulationsService
      .start(
        this.pulseDevice!.hardwareId,
        this.uiState.selectedInitialState,
        this.uiState.selectedScenario ? this.uiState.selectedScenario.id : '',
        this.uiState.selectedExecutionSteps,
        PulseControllerComponent.SIM_STEP_INCREMENT
      )
      .subscribe();
  }

  stopSimulation(): void {
    this.pulseSimulationsService.clearPatient(this.pulseDevice!.hardwareId);
    this.cdspSimulationsService.stop(this.pulseDevice!.hardwareId).subscribe();
  }

  runSimulation(): void {
    this.cdspSimulationsService
      .run(this.pulseDevice!.hardwareId, this.uiState.selectedExecutionSteps, PulseControllerComponent.SIM_STEP_INCREMENT)
      .subscribe();
  }

  terminateSimulation(): void {
    this.pulseSimulationsService.clearPatient(this.pulseDevice!.hardwareId);
    this.cdspSimulationsService.terminate(this.pulseDevice!.hardwareId).subscribe();
  }

  onSimulationStatusUpdate(status: IHardwareStatus): void {
    if (status.hardwareId !== this.pulseDevice!.hardwareId) {
      // We should never reach this point, but we add a control in any case
      return;
    }

    // eslint-disable-next-line no-console
    //  console.log("[SIM STATUS EVENT] "+JSON.stringify(status));

    // We should have almost everything we need to update the UI without
    // having to go to the server one more time. But in any case we go
    // to the server to prevent incorrect information.
    this.fetchAndUpdateSimulationInfo(this.pulseDevice!.hardwareId);
  }

  private fetchAndUpdateSimulationInfo(simulationName: string): void {
    this.pulseSimulationsService.queryInfo(simulationName).subscribe(info => {
      this.updateUIState(info.body || undefined);
      this.pulseSimulationsService.notify(info.body || undefined);
    });
  }

  private updateUIState(info?: ISimulationInfo): void {
    if (!info) {
      this.error = 'vcmApp.pulseController.error.unableToRetrieveSimulationInfo';
      return;
    }
    this.status = info.state;

    this.uiState.reset();
    this.uiState.selectedInitialState = info.initialStateInfo?.stateName || '';

    const scenarioTag = ISimulationInfoUtils.getTag(info, PulseControllerComponent.SCENARIO_TAG_NAME, '1 Min');
    this.scenarios?.some(d => {
      if (d.id === scenarioTag) {
        this.uiState.selectedScenario = d;
        return true;
      }
      return false;
    });

    const stepsTag = ISimulationInfoUtils.getTag(info, PulseControllerComponent.SIM_STEPS_TAG_NAME, '30');
    this.uiState.selectedExecutionSteps = +stepsTag;

    if (info.state === PulseSimulationState.NON_EXISTING || info.state === PulseSimulationState.TERMINATED) {
      this.uiState.disableScenarioDropdown = false;
      this.uiState.disableInitialStateDropdown = false;
      this.uiState.disableExecutionStepsDropdown = false;
      this.uiState.showStartButton = true;
    } else if (info.state === PulseSimulationState.INITIALIZED || info.state === PulseSimulationState.STOPPED) {
      this.uiState.disableScenarioDropdown = true;
      this.uiState.disableInitialStateDropdown = true;
      this.uiState.disableExecutionStepsDropdown = false;
      // For now, hide the run button;
      // this.uiState.showRunButton = true;
      this.uiState.showRunButton = false;
      this.uiState.showTerminateButton = true;
    } else if (info.state === PulseSimulationState.RUNNING) {
      this.uiState.disableScenarioDropdown = true;
      this.uiState.disableInitialStateDropdown = true;
      this.uiState.disableExecutionStepsDropdown = true;
      // For now, hide the pause button;
      // this.uiState.showPauseButton = true;
      this.uiState.showPauseButton = false;
      this.uiState.showTerminateButton = true;
    } else if (info.state === PulseSimulationState.PATIENT_IRREVERSIBLE_STATE) {
      this.uiState.disableScenarioDropdown = true;
      this.uiState.disableInitialStateDropdown = true;
      this.uiState.disableExecutionStepsDropdown = true;
      this.uiState.showTerminateButton = true;
    }
  }
}
