/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { NgModule } from '@angular/core';

import { ButtonModule } from 'primeng/button';
import { PanelModule } from 'primeng/panel'

import { VcmSharedModule } from 'app/shared/shared.module';
import { PulseControllerComponent } from './pulse-controller.component'

@NgModule({
  imports:[
    VcmSharedModule,
    ButtonModule,
    PanelModule
  ],
  declarations: [
    PulseControllerComponent
  ],
  exports: [
    PulseControllerComponent
  ]
})
export class PulseControllerModule { }
