/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, OnInit } from '@angular/core';
import { DrawerService } from './drawer.service';

@Component({
  templateUrl: './drawer-default-view.component.html',
  styleUrls: ['./drawer-default-view.component.scss']
})
export class DrawerDefaultViewComponent implements OnInit {

  constructor(
    protected drawerService: DrawerService
  ){}

  ngOnInit(): void {
    // Hide the drawer by default. Future enhancements on this component
    // may want to present a default drawer with some content, but for now,
    // the default it to hide the drawer.
    this.drawerService.hideDrawer();
  }
}
