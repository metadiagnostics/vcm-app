/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Component, OnInit, ChangeDetectorRef } from '@angular/core';

import { DrawerService } from './drawer.service';

@Component({
  selector: 'vcm-drawer',
  templateUrl: './drawer.component.html',
  styleUrls: ['./drawer.component.scss']
})
export class DrawerComponent implements OnInit {
  visible = true;
  expanded = false;

  constructor(protected drawerService: DrawerService, private cdRef: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.drawerService.drawerVisible.subscribe(visible => {
      this.visible = visible;
      this.cdRef.detectChanges();
    });
    this.drawerService.drawerExpanded.subscribe(expanded => {
      this.expanded = expanded;
      this.cdRef.detectChanges();
    });
  }
}
