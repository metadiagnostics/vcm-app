/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { BehaviorSubject, Observable } from 'rxjs';

import { DRAWER_OUTLET } from './drawer.route';

@Injectable({ providedIn: 'root' })
export class DrawerService {
  private drawerVisibleSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  private drawerExpandedSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public readonly drawerVisible: Observable<boolean> = this.drawerVisibleSubject.asObservable();
  public readonly drawerExpanded: Observable<boolean> = this.drawerExpandedSubject.asObservable();

  constructor(private router: Router) {}

  /**
   * Displays the default view of the Drawer.
   */
  public navigateToDefault(): void {
    this.navigateToPath('drawer-default');
  }

  /**
   * Used when navigateToDefault() can't be used because of a timing issue.
   * For example, when called from a ngOnDestroy() method.
   */
  public resetToDefault(): void {
    setTimeout(() => {
      this.navigateToDefault();
    }, 0);
  }

  /**
   * Displays a specific route in the drawer. The oute must be configured
   * with "outlet: drawer-outlet".
   * This method makes the drawer visible.
   *
   * @param path the route
   */
  public navigateToPath(path: string): void {
    this.showDrawer();
    const route = {};
    route[DRAWER_OUTLET] = path;
    this.router.navigate([{ outlets: route }]);
  }

  /**
   * Hides the drawer. Note that this is not "collapse" but "hide".
   * After this call, the drawer will be hidden from the UI.
   */
  public hideDrawer(): void {
    this.drawerVisibleSubject.next(false);
  }

  /**
   * Shows the drawer. Note that this is not "expand" but "show".
   */
  public showDrawer(): void {
    this.drawerVisibleSubject.next(true);
  }

  /**
   * Expands the drawer.
   */
  public expandsDrawer(): void {
    this.drawerExpandedSubject.next(true);
  }

  /**
   * Collapse the drawer.
   */
  public collapseDrawer(): void {
    this.drawerExpandedSubject.next(false);
  }
}
