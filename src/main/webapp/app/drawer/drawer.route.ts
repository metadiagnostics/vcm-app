/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DrawerDefaultViewComponent } from './drawer-default-view.component';

export const DRAWER_OUTLET = "drawer-outlet";

const DRAWER_ROUTES: Routes = [
  {
    path: 'drawer-default',
    component: DrawerDefaultViewComponent,
    outlet: DRAWER_OUTLET
  }
];

const ROUTE = [...DRAWER_ROUTES];
@NgModule({
    imports: [RouterModule.forChild(ROUTE)],
    exports: [RouterModule]
})
export class DrawerRoutes {
}
