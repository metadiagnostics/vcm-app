/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */


package io.saperi.vcm.constant;

import io.saperi.vcm.service.ext.errors.BackendException;

/**
 * Constants used to send messages that can be translated in the client side.
 * The keys in this class usually correspond to keys in the i18n files of the
 * client that is consuming the services in this application.
 * <p>
 * This constants are usually used in conjunction with a
 * {@link BackendException}.
 * </p>
 *
 * @author esteban
 */
public final class ExtendedErrorConstants {

    public static final String ERR_ASSIGNMENT_DEVICE_ALREADY_ASSIGNED_FAILURE = "assignment.invalid.device";
    public static final String ERR_ASSIGNMENT_NOT_FOUND = "assignment.no.found";

    //Ventilator
    public static final String ERR_VENTILATOR_CHANGES_ERROR = "ext.monitor.ventilator.changes.error";

    //Ventilator Recommender
    public static final String ERR_VENTILATOR_RECOMMENDER_MISSING_PARAMETER_ERROR = "ext.monitor.ventilator.recommender.missing.parameter";
    public static final String ERR_VENTILATOR_RECOMMENDER_UNSUPPORTED_TYPE = "ext.monitor.ventilator.recommender.unsupported.type";

    private ExtendedErrorConstants() {
    }
}
