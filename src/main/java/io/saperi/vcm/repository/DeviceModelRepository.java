/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.repository;

import io.saperi.vcm.domain.DeviceModel;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the DeviceModel entity.
 */
@Repository
public interface DeviceModelRepository extends JpaRepository<DeviceModel, Long> {

    @Query(value = "select distinct deviceModel from DeviceModel deviceModel left join fetch deviceModel.capabilities",
        countQuery = "select count(distinct deviceModel) from DeviceModel deviceModel")
    Page<DeviceModel> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct deviceModel from DeviceModel deviceModel left join fetch deviceModel.capabilities")
    List<DeviceModel> findAllWithEagerRelationships();

    @Query("select deviceModel from DeviceModel deviceModel left join fetch deviceModel.capabilities where deviceModel.id =:id")
    Optional<DeviceModel> findOneWithEagerRelationships(@Param("id") Long id);

    DeviceModel findFirstByName(@Param("name") String name);
}
