/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.repository.ext;

import io.saperi.vcm.domain.PersistentAuditEvent;
import java.time.Instant;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 */
public interface ExtendedPersistenceAuditEventRepository extends JpaRepository<PersistentAuditEvent, Long> {

    @Query(value = "SELECT e FROM PersistentAuditEvent e JOIN e.data d WHERE e.auditEventType IN :types AND (KEY(d) = :dataKey and d = :dataValue)")
    List<PersistentAuditEvent> findByDataKeyAndDataValue(
        @Param("types") String[] types,
        @Param("dataKey") String dataKey,
        @Param("dataValue") String dataValue
    );

    @Query(value = "SELECT e FROM PersistentAuditEvent e JOIN e.data d WHERE e.auditEventDate >= :from AND e.auditEventDate < :to AND e.auditEventType IN :types AND (KEY(d) = :dataKey and d = :dataValue)")
    List<PersistentAuditEvent> findByDataKeyAndDataValueWithDateRange(
        @Param("types") String[] types,
        @Param("from") Instant from,
        @Param("to") Instant to,
        @Param("dataKey") String dataKey,
        @Param("dataValue") String dataValue
    );

}
