/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.rest;

import io.saperi.vcm.service.AlarmWarningService;
import io.saperi.vcm.service.dto.Alarm;
import io.saperi.vcm.service.dto.AlarmWarning;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

/**
 * REST controller to retrieve the Local Team Items presented on VCM application.
 */
@RestController
@RequestMapping("/api")
public class AlarmWarningResource {

    private static class AlarmWarningException extends RuntimeException {

        private static final long serialVersionUID = 1L;

        private AlarmWarningException(String message, Throwable e) {
            super(message, e);
        }
    }

    private final Logger log = LoggerFactory.getLogger(AlarmWarningResource.class);

    private AlarmWarningService alarmWarningService;

    @Autowired
    public AlarmWarningResource(AlarmWarningService alarmWarningService) {
        this.alarmWarningService = alarmWarningService;
    }

    @GetMapping("/alarm/warning/patient/{patientId}")
    @SuppressWarnings("unchecked")
    public List<AlarmWarning> getAlarmWarnings(@PathVariable String patientId) {
        try {
            log.debug("REST request to get all Alarm Warnings for patient {}", patientId);
            return alarmWarningService.getAlarmWarnings(patientId);
        } catch (ResponseStatusException e){
            // trying to avoid an error on the UI so sending an empty List
            return new ArrayList<>();
        } catch (Exception e) {
            throw new AlarmWarningException("Alarm warnings could not be found", e);
        }
    }

    @GetMapping("/alarm/warning/{id}/patient/{patientId}")
    @SuppressWarnings("unchecked")
    public AlarmWarning getAlarmWarning(@PathVariable String id, @PathVariable String patientId) {
        try {
            log.debug("REST request to get one Alarm Warnings for patient {}", patientId);
            return alarmWarningService.getAlarmWarning(id, patientId);
        } catch (ResponseStatusException e){
            // trying to avoid an error on the UI so sending an empty AlarmWarning
            return new AlarmWarning("-1");
        }
        catch (Exception e) {
            throw new AlarmWarningException("Alarm warning could not be found", e);
        }
    }

    @PutMapping("/alarm/warning/patient/{patientId}")
    @SuppressWarnings("unchecked")
    public List<AlarmWarning> ackAllAlarmWarnings(@PathVariable String patientId) {
        try {
            log.debug("REST request to ack ALL the Alarm warnings for patient {}", patientId);
            return alarmWarningService.ackAlarmWarning(patientId);
        } catch (ResponseStatusException e){
            // trying to avoid an error on the UI so sending an empty AlarmWarning list
            return new ArrayList<>();
        }catch (Exception e) {
            throw new AlarmWarningException("Alarm warning ACK could not be executed", e);
        }
    }

    @PutMapping("/alarm/warning/{id}/patient/{patientId}")
    @SuppressWarnings("unchecked")
    public void ackAlarmWarning(@PathVariable String id, @PathVariable String patientId) {
        try {
            log.debug("REST request to ack Alarm warning for patient {}", patientId);
            alarmWarningService.ackAlarmWarning(id, patientId);
        } catch (Exception e) {
            throw new AlarmWarningException("Alarm warning ACK could not be executed", e);
        }
    }

    @PostMapping("/alarm/warning/reset")
    @SuppressWarnings("unchecked")
    public void getAlarmWarning() {
        try {
            log.debug("REST request to reset Alarm Warning occurrences");
            alarmWarningService.resetAllOccurences();
        } catch (Exception e) {
            throw new AlarmWarningException("Alarm warning reset could not be executed", e);
        }
    }

}
