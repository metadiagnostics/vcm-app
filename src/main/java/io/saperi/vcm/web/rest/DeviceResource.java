/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.saperi.vcm.domain.Device;
import io.saperi.vcm.event.*;
import io.saperi.vcm.service.DeviceService;
import io.saperi.vcm.service.ext.impl.DevicesDataCache;
import io.saperi.vcm.service.ext.impl.gtx.GtxAccsLiveDataEvent;
import io.saperi.vcm.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import liquibase.pro.packaged.S;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link io.saperi.vcm.domain.Device}.
 */
@RestController
@RequestMapping("/api")
public class DeviceResource {

    private final Logger log = LoggerFactory.getLogger(DeviceResource.class);

    private static final String ENTITY_NAME = "device";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DeviceService deviceService;

    private final ApplicationEventPublisher applicationEventPublisher;
    private final DevicesDataCache devicesCache;

    public DeviceResource(DeviceService deviceService,
                          ApplicationEventPublisher applicationEventPublisher,
                          DevicesDataCache devicesCache) {
        this.deviceService = deviceService;
        this.applicationEventPublisher = applicationEventPublisher;
        this.devicesCache = devicesCache;
    }

    /**
     * {@code POST  /devices} : Create a new device.
     *
     * @param device the device to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new device, or with status {@code 400 (Bad Request)} if the device has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/devices")
    public ResponseEntity<Device> createDevice(@Valid @RequestBody Device device) throws URISyntaxException {
        log.debug("REST request to save Device : {}", device);
        if (device.getId() != null) {
            throw new BadRequestAlertException("A new device cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Device result = deviceService.save(device);
        return ResponseEntity.created(new URI("/api/devices/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /devices} : Updates an existing device.
     *
     * @param device the device to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated device,
     * or with status {@code 400 (Bad Request)} if the device is not valid,
     * or with status {@code 500 (Internal Server Error)} if the device couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/devices")
    public ResponseEntity<Device> updateDevice(@Valid @RequestBody Device device) throws URISyntaxException {
        log.debug("REST request to update Device : {}", device);
        if (device.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Device result = deviceService.save(device);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, device.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /devices} : get all the devices.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of devices in body.
     */
    @GetMapping("/devices")
    public List<Device> getAllDevices() {
        log.debug("REST request to get all Devices");
        return deviceService.findAll();
    }

    /**
     * {@code GET  /devices/:id} : get the "id" device.
     *
     * @param id the id of the device to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the device, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/devices/{id}")
    public ResponseEntity<Device> getDevice(@PathVariable Long id) {
        log.debug("REST request to get Device : {}", id);
        Optional<Device> device = deviceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(device);
    }

    /**
     * {@code DELETE  /devices/:id} : delete the "id" device.
     *
     * @param id the id of the device to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/devices/{id}")
    public ResponseEntity<Void> deleteDevice(@PathVariable Long id) {
        log.debug("REST request to delete Device : {}", id);
        deviceService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/device/{hardwareId}/alldata")
    public ResponseEntity<Void> consumeData(@PathVariable String hardwareId, @RequestBody GtxAccsLiveAllDataDTO device) {
        hardwareId = transformHWId(hardwareId);
        // Call here a service to do all this stuff
        DevicesDataCache.DeviceData deviceData = devicesCache.getData(hardwareId);
        GtxAccsLiveAllDataDTO accsLiveAllData = new GtxAccsLiveAllDataDTO();
        //device.getHardwareVentilatorEvent().setHardwareId(hardwareId);
        //device.getHardwareVitalsEvent().setHardwareId(hardwareId);
        if ( deviceData != null ) {
            //Here we should put the melded object
            accsLiveAllData = meldGtxAccsLiveAllData(hardwareId, deviceData, device);
        } else {
            HardwareVitalsEvent vitalsEvent = new HardwareVitalsEvent(hardwareId, device.getHardwareVitalsEvent());
            HardwareVentilatorEvent ventilatorEvent = new HardwareVentilatorEvent(hardwareId, device.getHardwareVentilatorEvent());
            accsLiveAllData.setHardwareVentilatorEvent(ventilatorEvent);
            accsLiveAllData.setHardwareVitalsEvent(vitalsEvent);
        }
        applicationEventPublisher.publishEvent(accsLiveAllData);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/device/{hardwareId}/ventilatordata")
    public ResponseEntity<Void> consumeData(@PathVariable String hardwareId, @RequestBody GtxAccsLiveVentilatorDataDTO ventilatorData) {
        hardwareId = transformHWId(hardwareId);
        // Call here a service to do all this stuff
        DevicesDataCache.DeviceData deviceData = devicesCache.getData(hardwareId);
        GtxAccsLiveVentilatorDataDTO accsVentilatorData = new GtxAccsLiveVentilatorDataDTO();
        if ( deviceData != null ) {
            //Here we should put the melded object
            accsVentilatorData = meldGtxAccsLiveVentilatorData(hardwareId, deviceData, ventilatorData);
        } else {
            HardwareVentilatorEvent ventilatorEvent = new HardwareVentilatorEvent(hardwareId, ventilatorData.getHardwareVentilatorEvent());
            accsVentilatorData.setHardwareVentilatorEvent(ventilatorEvent);

        }
        applicationEventPublisher.publishEvent(accsVentilatorData);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/device/{hardwareId}/vitalsdata")
    public ResponseEntity<Void> consumeData(@PathVariable String hardwareId, @RequestBody GtxAccsLiveVitalsDataDTO vitalsData) {
        hardwareId = transformHWId(hardwareId);
        // Call here a service to do all this stuff
        DevicesDataCache.DeviceData deviceData = devicesCache.getData(hardwareId);
        GtxAccsLiveVitalsDataDTO accsVitalsData = new GtxAccsLiveVitalsDataDTO();
        if ( deviceData != null ) {
            //Here we should put the melded object
            accsVitalsData = meldGtxAccsLiveVitalsData(hardwareId, deviceData, vitalsData);
        } else {
            HardwareVitalsEvent vitalsEvent = new HardwareVitalsEvent(hardwareId, vitalsData.getHardwareVitalsEvent());
            accsVitalsData.setHardwareVitalsEvent(vitalsEvent);

        }
        applicationEventPublisher.publishEvent(accsVitalsData);
        return ResponseEntity.ok().build();
    }

    //TODO: This should do a real merge not just create the object
    private GtxAccsLiveVentilatorDataDTO meldGtxAccsLiveVentilatorData(String hardwareId, DevicesDataCache.DeviceData deviceData, GtxAccsLiveVentilatorDataDTO ventilatorData) {
        GtxAccsLiveVentilatorDataDTO accsLiveVentilatorData = new GtxAccsLiveVentilatorDataDTO();
        HardwareVentilatorEvent ventilatorEvent = new HardwareVentilatorEvent(hardwareId, ventilatorData.getHardwareVentilatorEvent());
        accsLiveVentilatorData.setHardwareVentilatorEvent(ventilatorEvent);
        // just returning what I receive with source and hw id set
        return accsLiveVentilatorData;
    }

    //TODO: This should do a real merge not just create the object
    private GtxAccsLiveVitalsDataDTO meldGtxAccsLiveVitalsData(String hardwareId, DevicesDataCache.DeviceData deviceData, GtxAccsLiveVitalsDataDTO vitalsData) {
        GtxAccsLiveVitalsDataDTO accsLiveVitalsData = new GtxAccsLiveVitalsDataDTO();
        HardwareVitalsEvent vitalsEvent = new HardwareVitalsEvent(hardwareId, vitalsData.getHardwareVitalsEvent());
        accsLiveVitalsData.setHardwareVitalsEvent(vitalsEvent);
        // just returning what I receive with source and hw id set
        return accsLiveVitalsData;
    }

    //TODO: This should do a real merge not just create the object
    private GtxAccsLiveAllDataDTO meldGtxAccsLiveAllData(String hardwareId, DevicesDataCache.DeviceData deviceData, GtxAccsLiveAllDataDTO device) {
        GtxAccsLiveAllDataDTO accsLiveData = new GtxAccsLiveAllDataDTO();
        HardwareVitalsEvent vitalsEvent = new HardwareVitalsEvent(hardwareId, device.getHardwareVitalsEvent());
        HardwareVentilatorEvent ventilatorEvent = new HardwareVentilatorEvent(hardwareId, device.getHardwareVentilatorEvent());
        accsLiveData.setHardwareVentilatorEvent(ventilatorEvent);
        accsLiveData.setHardwareVitalsEvent(vitalsEvent);
        // just returning what I receive with source and hw id set
        return accsLiveData;
    }

    private String transformHWId(String hwId){
        if (hwId!=null && hwId.length()==40){
            String [] hwIdParts = hwId.split("0x");
            for (int i = 0; i< hwIdParts.length-1;  i++) {
                hwIdParts[i] = hwIdParts[i+1];
            }
            hwIdParts = Arrays.copyOf(hwIdParts, hwIdParts.length-1);
            StringBuilder sb = new StringBuilder();
            for (String p: hwIdParts) {
                sb.append("0x");
                sb.append(p);
                sb.append(" ");
            }
            String id = sb.toString();
            return id.substring(0, id.length()-1);
        }
        return hwId;
    }

    public static void main(final String[] args) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        //mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        GtxAccsLiveAllDataDTO dto = new GtxAccsLiveAllDataDTO();
        dto.setHardwareVitalsEvent(new HardwareVitalsEvent("1", "1"));
        HardwareVentilatorEvent hw = new HardwareVentilatorEvent("1", "1");
        hw.setFio2(new ScalarValue(1.1, "ml", 2));
        dto.setHardwareVentilatorEvent(hw);
        String sobj = mapper.writeValueAsString(dto);
        System.out.println(sobj);

        GtxAccsLiveAllDataDTO newdto = mapper.readValue(sobj, GtxAccsLiveAllDataDTO.class);
        System.out.println(sobj);
        String sobj2 = mapper.writeValueAsString(newdto);
        System.out.println(sobj2);

        /*String ll = transformHWId("0x714D30510x000000000x000000000x00000000");
        System.out.println(ll);*/


    }

}
