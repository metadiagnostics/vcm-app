/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.rest.ext;

import static io.saperi.vcm.constant.ExtendedErrorConstants.ERR_ASSIGNMENT_NOT_FOUND;
import static io.saperi.vcm.constant.ExtendedErrorConstants.ERR_VENTILATOR_RECOMMENDER_UNSUPPORTED_TYPE;

import io.saperi.vcm.domain.Assignment;
import io.saperi.vcm.domain.enumeration.VentilatorRecommenderType;
import io.saperi.vcm.domain.ext.VentilatorRecommendationRequest;
import io.saperi.vcm.domain.ext.VentilatorRecommendationResponse;
import io.saperi.vcm.service.AssignmentService;
import io.saperi.vcm.service.ext.errors.BackendException;
import io.saperi.vcm.service.ext.recommender.VentilatorRecommenderService;
import io.saperi.vcm.service.ext.recommender.impl.CloudAiVentilatorRecommenderService;
import io.saperi.vcm.service.ext.recommender.impl.CloudRuleBasedVentilatorRecommenderService;
import java.security.Principal;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

/**
 * REST controller for getting ventilator recommendations. The current
 * implementation of this controller uses a single
 * {@link VentilatorRecommenderService} instance. In the future, the concrete
 * implementation may vary depending on each {@link Assignment}
 *
 */
@RestController
@RequestMapping("/api/ext/ventilator-recommender")
public class VentilatorRecommenderResource {

    private final Logger log = LoggerFactory.getLogger(VentilatorRecommenderResource.class);

    private final AssignmentService assignmentService;

    private final CloudRuleBasedVentilatorRecommenderService ruleBasedrecommenderService;
    private final CloudAiVentilatorRecommenderService aIrecommenderService;

    public VentilatorRecommenderResource(AssignmentService assignmentService, CloudRuleBasedVentilatorRecommenderService ruleBasedrecommenderService, CloudAiVentilatorRecommenderService aIrecommenderService) {
        this.assignmentService = assignmentService;
        this.ruleBasedrecommenderService = ruleBasedrecommenderService;
        this.aIrecommenderService = aIrecommenderService;
    }

    @PostMapping("/")
    public VentilatorRecommendationResponse getRecommendation(
        @RequestParam(required = false) Long assignmentId,
        @RequestBody(required = true) VentilatorRecommendationRequest request, Principal principal) {
        log.debug("REST request to get a Ventilator Recommendation for Assignment {}", assignmentId);

        Optional<Assignment> assignment = assignmentService.findOne(assignmentId);
        if (assignment.isEmpty()) {
            throw new BackendException(String.format("An Assignment with id '%s' could not be found", assignmentId), ERR_ASSIGNMENT_NOT_FOUND, new BackendException.Param("id", assignmentId + ""));
        }

        VentilatorRecommenderType recommenderType = assignment.get().getVentilatorRecommenderType();
        VentilatorRecommenderService service;
        switch (recommenderType) {
            case AI:
                service = aIrecommenderService;
                break;
            case RULE_BASED:
                service = ruleBasedrecommenderService;
                break;
            default:
                throw new BackendException(
                    String.format("Unsupported Ventilator Recommender '%s'", recommenderType),
                    ERR_VENTILATOR_RECOMMENDER_UNSUPPORTED_TYPE,
                    new BackendException.Param("param", recommenderType == null ? "null" : recommenderType.name())
                );
        }

        request.setRequester(principal.getName());
        return service.createRecommendation(assignment.get(), request);
    }

}
