/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.rest;

import io.saperi.vcm.service.PumpService;
import io.saperi.vcm.service.dto.Pump;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * REST controller to retrieve the Initial Pumps presented on VCM application with their initial values.
 */
@RestController
@RequestMapping("/api/pump")
public class PumpResource {

    private static class PumpResourceException extends RuntimeException {

        private static final long serialVersionUID = 1L;

        private PumpResourceException(String message, Throwable e) {
            super(message, e);
        }
    }

    private final Logger log = LoggerFactory.getLogger(PumpResource.class);

    private PumpService pumpService;

    @Autowired
    public PumpResource(PumpService pumpService) {
        this.pumpService = pumpService;
    }

    /**
     * {@code GET } : get the AccuPumps available.
     *
     * @return an array of pumps.
     * @throws PumpResourceException if the pump couldn't be returned.
     */
    @GetMapping
    @SuppressWarnings("unchecked")
    public List<Pump> getPumps() {
        try {
            log.debug("REST request to get all Pumps");
            return pumpService.gePumps();
        } catch (Exception e) {
            throw new PumpResourceException("Pumps could not be found", e);
        }
    }

}
