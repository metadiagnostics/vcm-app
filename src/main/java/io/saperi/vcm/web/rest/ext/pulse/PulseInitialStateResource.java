/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.rest.ext.pulse;

import io.saperi.pulse.sim.api.service.StateService;
import io.saperi.vcm.security.oauth2.JWTExpiredException;
import java.util.List;

import io.saperi.vcm.service.ext.impl.pulse.PulseEventListenerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

/**
 *
 */
@RestController
@RequestMapping("/api/ext/pulse")
public class PulseInitialStateResource {

    public static final String VCM_TAG_KEY = "type";
    public static final String VCM_TAG_VALUE = "VCM";

    private final Logger log = LoggerFactory.getLogger(PulseInitialStateResource.class);

    private final StateService client;
    private final PulseEventListenerService pulseEventListenerService;

    public PulseInitialStateResource(StateService client,
                                     PulseEventListenerService pulseEventListenerService) {
        this.client = client;
        this.pulseEventListenerService = pulseEventListenerService;
    }

    @GetMapping("/enabled")
    public boolean isPulseEnabled() throws JWTExpiredException {
        return pulseEventListenerService.isPulseEnabled();
    }

    @GetMapping("/states")
    public List<String> getStateNames() throws JWTExpiredException {
        log.debug("REST request to get all Pulse Initial States");
        return client.getStateNames(VCM_TAG_KEY, VCM_TAG_VALUE);
    }

}
