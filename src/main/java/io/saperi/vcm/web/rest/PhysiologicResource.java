/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.rest;

import io.saperi.vcm.service.PhysiologicService;
import io.saperi.vcm.service.dto.Label;
import io.saperi.vcm.service.dto.MonitorDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller to retrieve the Initial physiologic labels.
 */
@RestController
@RequestMapping("/api/physiologic")
public class PhysiologicResource {

    private static class PhysiologicResourceException extends RuntimeException {

        private static final long serialVersionUID = 1L;

        private PhysiologicResourceException(String message, Throwable e) {
            super(message, e);
        }
    }

    private final Logger log = LoggerFactory.getLogger(PhysiologicResource.class);

    private PhysiologicService physiologicService;

    @Autowired
    public PhysiologicResource(PhysiologicService physiologicService) {
        this.physiologicService = physiologicService;
    }

    /**
     * {@code GET} : get the PhysiologicService Labels.
     *
     * @return A {@link Label}.
     * @throws PhysiologicResourceException if the labels couldn't be returned.
     */
    @GetMapping
    @SuppressWarnings("unchecked")
    public Label getLabels() {
        try {
            log.debug("REST request to get all Physiologic Labels");
            return physiologicService.getLabels();
        } catch (Exception e) {
            throw new PhysiologicResourceException("Physiologic labels could not be found", e);
        }
    }

    @GetMapping("/monitors/{hardwareId}")
    @SuppressWarnings("unchecked")
    public MonitorDefinition[] getMonitorDefinitions(@PathVariable String hardwareId) {
        try {
            log.debug("REST request to get all Physiologic Monitor Definitions");
            return physiologicService.getMonitorDefinitions(hardwareId);
        } catch (Exception e) {
            throw new PhysiologicResourceException("Physiologic Monitor Definitions could not be found", e);
        }
    }
}
