/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.rest;

import io.saperi.vcm.service.MonitorDefinitionConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller to retrieve the menu items presented on VCM application.
 */
@RestController
@RequestMapping("/api")
public class MenuResource {

    private static class MenuResourceException extends RuntimeException {

        private static final long serialVersionUID = 1L;

        private MenuResourceException(String message, Throwable e) {
            super(message, e);
        }
    }

    private final Logger log = LoggerFactory.getLogger(MenuResource.class);

    // @Value("${vcm.menu.items}")
    // private String[] items;

    @Autowired
    MonitorDefinitionConfig monitorDefinitionConfig;

    public MenuResource() {}

    /**
     * {@code GET  /menu} : get the menu items.
     *
     * @return an array of menu items.
     * @throws MenuResourceException if the menu items couldn't be returned.
     */
    @GetMapping("/menu")
    @SuppressWarnings("unchecked")
    public String[] getMenuItems() {
        try {
            log.debug("REST request to get all Menu Items");
            return monitorDefinitionConfig.getMenu().get("items");
        } catch (Exception e) {
            throw new MenuResourceException("Menu items could not be found", e);
        }
    }
}
