/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.rest;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing global OIDC logout.
 */
@RestController
public class LogoutResource {
    private final Environment env;
    private final ClientRegistration registration;

    @Value("${spring.security.oauth2.client.provider.oidc.logout-uri:#{null}}")
    private String logoutUrl;

    public LogoutResource(Environment env, ClientRegistrationRepository registrations) {
        this.env = env;
        this.registration = registrations.findByRegistrationId("oidc");
    }

    /**
     * {@code POST  /api/logout} : logout the current user.
     *
     * @param request the {@link HttpServletRequest}.
     * @param idToken the ID token.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and a body with a global logout URL and ID token.
     */
    @PostMapping("/api/logout")
    public ResponseEntity<?> logout(HttpServletRequest request,
                                    @AuthenticationPrincipal(expression = "idToken") OidcIdToken idToken) {
        Map<String, String> logoutDetails = new HashMap<>();
        String finalLogoutUrl = logoutUrl != null ? logoutUrl : this.registration.getProviderDetails()
            .getConfigurationMetadata().get("end_session_endpoint").toString();

        if (env.acceptsProfiles(Profiles.of("cognito"))) {
            logoutDetails.put("logoutUrl", finalLogoutUrl);
            logoutDetails.put("id", registration.getClientId());
            logoutDetails.put("implementation", "cognito");
        } else {
            logoutDetails.put("logoutUrl", finalLogoutUrl);
            logoutDetails.put("id", idToken.getTokenValue());
        }
        request.getSession().invalidate();
        return ResponseEntity.ok().body(logoutDetails);
    }
}
