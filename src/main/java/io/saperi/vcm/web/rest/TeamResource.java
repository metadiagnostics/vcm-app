/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.rest;

import io.saperi.vcm.service.TeamService;
import io.saperi.vcm.service.dto.TeamMember;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

/**
 * REST controller to retrieve the Local Team Items presented on VCM application.
 */
@RestController
@RequestMapping("/api")
public class TeamResource {

    private static class TeamResourceException extends RuntimeException {

        private static final long serialVersionUID = 1L;

        private TeamResourceException(String message, Throwable e) {
            super(message, e);
        }
    }

    private final Logger log = LoggerFactory.getLogger(TeamResource.class);

    private TeamService teamService;

    @Autowired
    public TeamResource(TeamService teamService) {
        this.teamService = teamService;
    }

    /**
     * {@code GET  /localteam} : get the local team items.
     *
     * @return an array of local team items.
     * @throws TeamResourceException if the local team items couldn't be returned.
     */
    @GetMapping("/localteam")
    @SuppressWarnings("unchecked")
    public List<TeamMember> getLocalTeamItems() {
        try {
            log.debug("REST request to get all Local Team Items");
            return teamService.getLocalTeamMembers();
        } catch (Exception e) {
            throw new TeamResourceException("Local Team items could not be found", e);
        }
    }

    /**
     * {@code GET  /remoteteam} : get the remote team items.
     *
     * @return an array of remote team items.
     * @throws TeamResourceException if the remote team items couldn't be returned.
     */
    @GetMapping("/remoteteam")
    @SuppressWarnings("unchecked")
    public List<TeamMember> getRemoteTeamItems() {
        try {
            log.debug("REST request to get all Remote Team Items");
            return teamService.getRemoteTeamMembers();
        } catch (Exception e) {
            throw new TeamResourceException("Remote Team items could not be found", e);
        }
    }
}
