/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.rest;

import io.saperi.vcm.domain.DeviceCapability;
import io.saperi.vcm.service.DeviceCapabilityService;
import io.saperi.vcm.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link io.saperi.vcm.domain.DeviceCapability}.
 */
@RestController
@RequestMapping("/api")
public class DeviceCapabilityResource {

    private final Logger log = LoggerFactory.getLogger(DeviceCapabilityResource.class);

    private final DeviceCapabilityService deviceCapabilityService;

    public DeviceCapabilityResource(DeviceCapabilityService deviceCapabilityService) {
        this.deviceCapabilityService = deviceCapabilityService;
    }

    /**
     * {@code GET  /device-capabilities} : get all the deviceCapabilities.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of deviceCapabilities in body.
     */
    @GetMapping("/device-capabilities")
    public List<DeviceCapability> getAllDeviceCapabilities() {
        log.debug("REST request to get all DeviceCapabilities");
        return deviceCapabilityService.findAll();
    }

    /**
     * {@code GET  /device-capabilities/:id} : get the "id" deviceCapability.
     *
     * @param id the id of the deviceCapability to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the deviceCapability, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/device-capabilities/{id}")
    public ResponseEntity<DeviceCapability> getDeviceCapability(@PathVariable Long id) {
        log.debug("REST request to get DeviceCapability : {}", id);
        Optional<DeviceCapability> deviceCapability = deviceCapabilityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(deviceCapability);
    }
}
