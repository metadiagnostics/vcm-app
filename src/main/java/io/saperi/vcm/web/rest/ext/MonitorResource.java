/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.rest.ext;

import static io.saperi.vcm.constant.ExtendedErrorConstants.ERR_ASSIGNMENT_NOT_FOUND;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import io.saperi.vcm.domain.Assignment;
import io.saperi.vcm.domain.Device;
import io.saperi.vcm.domain.enumeration.*;
import io.saperi.vcm.domain.ext.VentilatorConfigurationChangeRequest;
import io.saperi.vcm.event.Fio2AckRequestEvent;
import io.saperi.vcm.event.RecommenderControlTypeChangeEvent;
import io.saperi.vcm.event.VentilatorConfigurationChangeRequestEvent;
import io.saperi.vcm.service.AssignmentService;
import io.saperi.vcm.service.dto.ext.MonitorAssignmentDTO;
import io.saperi.vcm.service.dto.ext.MonitorTimestampDTO;
import io.saperi.vcm.service.ext.ExtendedAssignmentService;
import io.saperi.vcm.service.ext.errors.BackendException;
import io.saperi.vcm.service.ext.impl.VentilatorAuditEventServiceImpl;
import io.saperi.vcm.service.ext.recommender.impl.CloudAiVentilatorRecommenderService;
import io.saperi.vcm.service.ext.recommender.impl.CloudRuleBasedVentilatorRecommenderService;
import io.saperi.vcm.service.impl.AssignmentServiceImpl;
import io.saperi.vcm.service.mapper.ext.MonitorAssignmentMapper;
import java.io.IOException;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import liquibase.pro.packaged.io;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

/**
 * REST controller for managing {@link io.saperi.vcm.domain.Assignment}.
 */
@RestController
@RequestMapping("/api/ext/monitor")
public class MonitorResource {

    private final Logger log = LoggerFactory.getLogger(MonitorResource.class);

    private final AssignmentService assignmentService;

    private final ExtendedAssignmentService extendedAssignmentService;

    private final CloudRuleBasedVentilatorRecommenderService ruleBasedrecommenderService;

    private final CloudAiVentilatorRecommenderService aIrecommenderService;

    private final ApplicationEventPublisher applicationEventPublisher;

    private final MonitorAssignmentMapper monitorAssignmentMapper;

    private final VentilatorAuditEventServiceImpl ventilatorAuditEventService;

    private final AssignmentServiceImpl assignmentServiceImpl;

    public MonitorResource(
        AssignmentService assignmentService,
        ExtendedAssignmentService extendedAssignmentService,
        CloudRuleBasedVentilatorRecommenderService ruleBasedrecommenderService,
        CloudAiVentilatorRecommenderService aIrecommenderService,
        ApplicationEventPublisher applicationEventPublisher,
        MonitorAssignmentMapper monitorAssignmentMapper,
        VentilatorAuditEventServiceImpl ventilatorAuditEventService,
        AssignmentServiceImpl assignmentServiceImpl) {
        this.assignmentService = assignmentService;
        this.extendedAssignmentService = extendedAssignmentService;
        this.ruleBasedrecommenderService = ruleBasedrecommenderService;
        this.aIrecommenderService = aIrecommenderService;
        this.applicationEventPublisher = applicationEventPublisher;
        this.monitorAssignmentMapper = monitorAssignmentMapper;
        this.ventilatorAuditEventService = ventilatorAuditEventService;
        this.assignmentServiceImpl = assignmentServiceImpl;
    }

    @GetMapping("/assignments")
    public List<MonitorAssignmentDTO> getAllAssignments(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all Monitor's Assignments");
        List<Assignment> assignments = extendedAssignmentService.findAllActiveEager();
        return assignments.stream().map(a -> monitorAssignmentMapper.assignmentToMonitorAssignmentDTO(a, "Test Name")).collect(toList());
    }

    @GetMapping("/assignments/{id}")
    public MonitorAssignmentDTO getAssignment(@PathVariable Long id) {
        log.debug("REST request to get Monitor Assignment : {}", id);
        Optional<Assignment> assignment = extendedAssignmentService.findOneWithEagerRelationships(id);
        if (assignment.isEmpty()) {
            throw new BackendException(String.format("An Assignment with id '%s' could not be found", id), ERR_ASSIGNMENT_NOT_FOUND, new BackendException.Param("id", id + ""));
        }
        if (!assignment.get().isActive()) {
            throw new BackendException(String.format("The Assignment with id '%s' is not active", id), ERR_ASSIGNMENT_NOT_FOUND, new BackendException.Param("id", id + ""));
        }
        return monitorAssignmentMapper.assignmentToMonitorAssignmentDTO(assignment.get(), "Test Name");
    }

    @PutMapping("/assignments/{id}/ventilator-recommendation/{mode}")
    public ResponseEntity<Void> changeVentilatorRecommendationMode(@PathVariable Long id, @PathVariable VentilatorModeType mode, Principal principal) {
        log.debug("REST request to change the recommendation mode of Ventilator for Assignment {} to {}", id, mode);
        Optional<Assignment> assignment = extendedAssignmentService.findOneWithEagerRelationships(id);
        if (assignment.isEmpty()) {
            throw new BackendException(String.format("An Assignment with id '%s' could not be found", id), ERR_ASSIGNMENT_NOT_FOUND, new BackendException.Param("id", id + ""));
        }

        if (assignment.get().getVentilatorMode() == mode) {
            //do nothing
            return ResponseEntity.noContent().build();
        }

        assignment.get().setVentilatorMode(mode);
        assignmentService.save(assignment.get(), principal.getName());

        //invoke the recommender if the mode is OPEN or CLOSE
        if (assignment.get().getVentilatorMode() == VentilatorModeType.OPEN || assignment.get().getVentilatorMode() == VentilatorModeType.CLOSED) {
            switch (assignment.get().getVentilatorRecommenderType()) {
                case AI:
                    aIrecommenderService.createRecommendation(assignment.get(), principal.getName());
                    break;
                case RULE_BASED:
                    ruleBasedrecommenderService.createRecommendation(assignment.get(), principal.getName());
                    break;
            }
        }

        return ResponseEntity.noContent().build();
    }

    @PutMapping("/assignments/{id}/ventilator-recommender-type/{type}")
    public ResponseEntity<Void> changeVentilatorRecommenderType(@PathVariable Long id, @PathVariable VentilatorRecommenderType type) {
        log.debug("REST request to change the recommender type of Ventilator for Assignment {} to {}", id, type);
        Optional<Assignment> assignment = extendedAssignmentService.findOneWithEagerRelationships(id);
        if (assignment.isEmpty()) {
            throw new BackendException(String.format("An Assignment with id '%s' could not be found", id), ERR_ASSIGNMENT_NOT_FOUND, new BackendException.Param("id", id + ""));
        }

        if (assignment.get().getVentilatorRecommenderType() == type) {
            //do nothing
            return ResponseEntity.noContent().build();
        }

        assignment.get().setVentilatorRecommenderType(type);
        assignmentService.save(assignment.get());

        return ResponseEntity.noContent().build();
    }

    /**
     * Change the Ventilator mode.
     *
     * The allowed modes are: PRESSURE, CONTROL
     * @param id The assignment id that will be modified.
     * @param control the {@link VentilatorControlType} to set
     * @return A {@link ResponseEntity} with {@link Void} entity.
     */
    @PutMapping("/assignments/{id}/ventilator-recommender-control/{control}")
    public ResponseEntity<Void> changeVentilatorRecommenderMode(@PathVariable Long id, @PathVariable VentilatorControlType control, Principal principal) {
        log.debug("REST request to change the recommender control mode of Ventilator for Assignment {} to {}", id, control);
        Optional<Assignment> assignment = extendedAssignmentService.findOneWithEagerRelationships(id);
        if (assignment.isEmpty()) {
            throw new BackendException(String.format("An Assignment with id '%s' could not be found", id), ERR_ASSIGNMENT_NOT_FOUND, new BackendException.Param("id", id + ""));
        }

        if (assignment.get().getVentilatorControlType() == control) {
            //do nothing
            return ResponseEntity.noContent().build();
        }

        assignment.get().setVentilatorControlType(control);
        assignmentService.save(assignment.get());
        applicationEventPublisher.publishEvent(new RecommenderControlTypeChangeEvent(control, id, principal.getName()));
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/assignments/{id}/ventilator/change")
    public ResponseEntity<MonitorTimestampDTO> changeVentilatorConfiguration(@PathVariable Long id, @RequestBody VentilatorConfigurationChangeRequest request, Principal principal) {
        log.debug("REST request to change the configuration of Ventilator for Assignment '{}' from user '{}'", id, principal.getName());
        Optional<Assignment> assignment = extendedAssignmentService.findOneWithEagerRelationships(id);

        Set<Device> ventilators = getVentilatorsControl(assignment, id);
        if (ventilators == null) {
            return ResponseEntity.noContent().build();
        }

        //Convert the request into an event and publish it as an internal event
        //so the concrete class in charge of the model can send the change to
        //the hardware
        for (Device ventilator : ventilators) {
            applicationEventPublisher.publishEvent(new VentilatorConfigurationChangeRequestEvent(ventilator, request, id, principal.getName()));
        }

        return ResponseEntity.ok(new MonitorTimestampDTO(System.currentTimeMillis()));
    }

    @GetMapping(path = "/assignments/{id}/ventilator/csv", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<byte[]> getVentilatorsLogsAsCsv(
        @PathVariable Long id,
        @RequestParam("from") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX") Date fromDate,
        @RequestParam("to") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX") Date toDate
    ) throws IOException {

        String csv = ventilatorAuditEventService.getVentilatorAuditEventsAsCSV(id, fromDate, toDate);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_PLAIN);
        headers.setContentDisposition(ContentDisposition.parse("attachment; filename=assignment-" + id + "-from-" + sdf.format(fromDate) + "-to-" + sdf.format(toDate) + "-ventilators-logs.csv"));
        headers.setContentLength(csv.getBytes().length);
        return new ResponseEntity<>(csv.getBytes(), headers, HttpStatus.CREATED);
    }

    @PostMapping("/assignments/{id}/ackFio2Value")
    public ResponseEntity<Void> ackFio2Value(@PathVariable Long id, Principal principal) {
        log.debug("REST call to ack that Fi02 value was changed for assignemnt '{}'", id);
        Optional<Assignment> assignment = extendedAssignmentService.findOneWithEagerRelationships(id);
        Set<Device> ventilators = getVentilatorsControl(assignment, id);
        for (Device ventilator : ventilators) {
            // We only store that an ack occurs if the device type is GTX, since it is the only device type that do not support fio2 setting
            if (ventilator.getModel()!=null && DeviceType.GTX.equals(ventilator.getModel().getType())){
                applicationEventPublisher.publishEvent(new Fio2AckRequestEvent(ventilator, assignment.get(), id, principal.getName()));
            }
        }
        return ResponseEntity.noContent().build();
    }

    private Set<Device> getVentilatorsControl(final Optional<Assignment> assignment, final Long id) {
        if (!assignment.isPresent()) {
            throw new BackendException(String.format("An Assignment with id '%s' could not be found", id), ERR_ASSIGNMENT_NOT_FOUND, new BackendException.Param("id", id + ""));
        }
        //Find the Ventilator_Control that are associated to the Assignment
        return assignment.get().getDevices().stream()
            .filter(d -> d.getCapability().getType() == DeviceCapabilityType.VENTILATOR_CONTROL)
            .map(ad -> ad.getDevice())
            .collect(toSet());
    }

    @PostMapping("/assignments/{id}/reconnectListeners")
    @Transactional
    public ResponseEntity<Void> reconnectListeners(@PathVariable Long id) {
        log.debug("REST call to refresh assignments id '{}' listeners", id);
        Optional<Assignment> optionalAssignment = assignmentServiceImpl.findOne(id);
        if (optionalAssignment.isPresent()){
            assignmentServiceImpl.publishRefreshAssignmentChange(optionalAssignment.get());
        }
        return ResponseEntity.noContent().build();
    }
}
