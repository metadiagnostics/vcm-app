
/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.rest.ext.pulse;

import com.cognitivemedicine.cdsp.simulator.definition.ScenarioDefinition;
import com.kitware.physiology.cdm.PatientActions;
import com.kitware.physiology.datamodel.patient.actions.SEAcuteRespiratoryDistressSyndromeExacerbation;
import com.kitware.physiology.datamodel.patient.actions.SEIntubation;
import com.kitware.physiology.datamodel.patient.actions.SELobarPneumoniaExacerbation;
import com.kitware.physiology.datamodel.patient.actions.SEPatientAction;
import com.kitware.physiology.datamodel.properties.CommonUnits;
import com.kitware.physiology.datamodel.properties.SEScalarMassPerVolume;
import com.kitware.physiology.datamodel.properties.SEScalarVolume;
import io.saperi.pulse.sim.api.model.SimulationInitData;
import io.saperi.pulse.sim.api.model.TimeUnit;
import io.saperi.pulse.sim.simulator.definition.AdvanceSimulationTimeStepDefinition;
import io.saperi.pulse.sim.simulator.definition.ApplyActionToSimulationStepDefinition;
import io.saperi.pulse.sim.simulator.definition.InitSimulationStepDefinition;
import io.saperi.pulse.sim.simulator.definition.SubstanceBolusStepDefinition;
import io.saperi.vcm.event.HardwareEvent;
import io.saperi.vcm.security.oauth2.JWTExpiredException;
import io.saperi.vcm.service.ext.cdsp.CdspScenarioService;
import io.saperi.vcm.service.ext.impl.DevicesDataCache;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import io.saperi.vcm.util.CacheUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.cache.Cache;

/**
 *
 */
@RestController
@RequestMapping("/api/ext/cdsp/sim")
public class CdspSimulationResource {

    private final Logger log = LoggerFactory.getLogger(CdspSimulationResource.class);

    private final int PULSE_SAMPLE_RATE_VALUE = 1000;
    private final TimeUnit PULSE_SAMPLE_RATE_UNIT = TimeUnit.MSEC;
    private final String ONE_MINUTE = "1 Min";
    private final String FIVE_MINUTES = "5 Mins";
    private final String ARDS_STATIC = "ARDS (0.75,1,1) - STATIC";
    private final String ARDS_RAMP = "ARDS (0.75,1,1) - RAMP";
    private final String PNEUMONIA_STATIC = "PNEUMONIA (0.75,1,1) - STATIC";
    private final String PNEUMONIA_RAMP = "PNEUMONIA (0.75,1,1) - RAMP";

    private final CdspScenarioService service;
    private final DevicesDataCache cache;
    private Cache<String, List<HardwareEvent>> eventsCache;

    public CdspSimulationResource(
        CdspScenarioService cdspScenarioService,
        DevicesDataCache cache
    ) {
        this.service = cdspScenarioService;
        this.cache = cache;

    }

    @PostConstruct
    public void postConstruct() {
        this.eventsCache = CacheUtil.eventsCache();
    }


    @GetMapping("/scenarios")
    public List<String> getSimulations() throws JWTExpiredException {
        //TODO: make this dymanic
        return Arrays.asList(ONE_MINUTE, FIVE_MINUTES, ARDS_STATIC, ARDS_RAMP, PNEUMONIA_STATIC, PNEUMONIA_RAMP);
    }

    @PostMapping("/{name}/start")
    public void start(
        @PathVariable String name,
        @RequestParam String initialStateName,
        @RequestParam(required = true) String scenarioName,
        @RequestParam(required = false, defaultValue = "1000") int increment
    ) throws ExecutionException, JWTExpiredException {
        log.debug("REST request to start CDSP Scenario'{}'", name);

        Map<String, String> tags = new HashMap<>();
        tags.put("_VCM_SCENARIO_", scenarioName);

        ScenarioDefinition scenario = null;

        //TODO: make this dynamic
        switch (scenarioName) {
            case ONE_MINUTE:
                scenario = createNormalScenario(initialStateName, name, 1, tags);
                break;
            case FIVE_MINUTES:
                scenario = createNormalScenario(initialStateName, name, 5, tags);
                break;
            case ARDS_STATIC:
                scenario = createConditionScenario(initialStateName, name, tags, 0.75, 1.0, 1.0, false, false);
                break;
            case ARDS_RAMP:
                scenario = createConditionScenario(initialStateName, name, tags, 0.75, 1.0, 1.0, true, false);
                break;
            case PNEUMONIA_STATIC:
                scenario = createConditionScenario(initialStateName, name, tags, 0.75, 1.0, 1.0, false, true);
                break;
            case PNEUMONIA_RAMP:
                scenario = createConditionScenario(initialStateName, name, tags, 0.75, 1.0, 1.0, true, true);
                break;
            default:
                throw new IllegalArgumentException(String.format("Unknown Scenario '%s'", scenarioName));
        }

        service.start(scenario);

        log.trace("Scenario is running");
    }

    @PostMapping("/{name}/stop")
    public void stop(@PathVariable String name) throws ExecutionException, JWTExpiredException {
        log.debug("REST request to stop Pulse Simulation '{}'", name);

        service.pause(name);
        log.trace("Simulation is stopped");
    }

    @PostMapping("/{name}/terminate")
    public void terminate(@PathVariable String name) throws ExecutionException, JWTExpiredException {
        log.debug("REST request to terminate Pulse Simulation '{}'", name);

        service.stop(name);
        log.trace("Simulation is terminated");

        //clear the cache fo the associated Device
        cache.clearCache(name);
        eventsCache.clear();
    }

    private ScenarioDefinition createNormalScenario(String initialStateName, String simulationName, int minutes, Map<String, String> tags) {
        ScenarioDefinition scenario = initScenario(simulationName, initialStateName, simulationName, tags);
        //Advance time
        for (int j = 0; j < minutes; j++) {
            scenario.addStep(new AdvanceSimulationTimeStepDefinition(
                simulationName,
                PULSE_SAMPLE_RATE_VALUE,
                PULSE_SAMPLE_RATE_UNIT,
                calculateNumberOfSteps(60),
                true,
                AdvanceSimulationTimeStepDefinition.Type.SYNC)
            );
        }

        return scenario;
    }

    private ScenarioDefinition createConditionScenario(String initialStateName, String simulationName, Map<String, String> tags,
                                                  Double complicationSeverityValue, Double leftLungAffected, Double rightLungAffected, boolean gradualSeverity, boolean pneumonia) {
        ScenarioDefinition scenario = initScenario(simulationName, initialStateName, simulationName, tags);

        //Intubate Patient
        SEIntubation intubation = new SEIntubation();
        intubation.setComment("Patient undergoes proper tracheal intubation");
        intubation.setType(PatientActions.IntubationData.eType.Tracheal);
        scenario.addStep(new ApplyActionToSimulationStepDefinition(simulationName, intubation));

        //Anesthesia Machine
        /*SEAnesthesiaMachineConfiguration amConfig = new SEAnesthesiaMachineConfiguration();
        amConfig.getConfiguration().getOxygenFraction().setValue(.21);
        amConfig.getConfiguration().getPositiveEndExpiredPressure().setValue(2.0, CommonUnits.PressureUnit.cmH2O);
        amConfig.getConfiguration().getVentilatorPressure().setValue(14.0, CommonUnits.PressureUnit.cmH2O);
        amConfig.getConfiguration().getRespiratoryRate().setValue(10.0, "1/min");
        amConfig.getConfiguration().setConnection(AnesthesiaMachine.AnesthesiaMachineData.eConnection.Tube);
        ApplyActionToSimulationStepDefinition configureAnesthesiaMachineStep = new ApplyActionToSimulationStepDefinition(simulationName, amConfig);
        scenario.addStep(configureAnesthesiaMachineStep);

        Advance 5 seconds (accelerated)
        AdvanceSimulationTimeStepDefinition timePlus5MinStep = new AdvanceSimulationTimeStepDefinition(
            simulationName,
            1,
            TimeUnit.SEC,
            5,
            false,
            AdvanceSimulationTimeStepDefinition.Type.SYNC
        );
        scenario.addStep(timePlus5MinStep);*/

        //Rocuronium
        SubstanceBolusStepDefinition rocuronium = new SubstanceBolusStepDefinition(
            simulationName,
            "Rocuronium",
            new SEScalarMassPerVolume(3100, CommonUnits.MassPerVolumeUnit.mg_Per_m3),
            new SEScalarVolume(20, CommonUnits.VolumeUnit.mL)
        );
        rocuronium.setAdministrationRoute(PatientActions.SubstanceBolusData.eRoute.Intravenous);
        scenario.addStep(rocuronium);

        //Advance 5 seconds (accelerated)
        AdvanceSimulationTimeStepDefinition timePlus5MinStep = new AdvanceSimulationTimeStepDefinition(
            simulationName,
            1,
            TimeUnit.SEC,
            5,
            false,
            AdvanceSimulationTimeStepDefinition.Type.SYNC
        );
        scenario.addStep(timePlus5MinStep);

        //Disease
        int internal_iterations= 50;
        int outer_iterations= 6; // 300 cycles in total
        int complicationIteration = 0;
        int complicationDuration = 15; // 15 cycles
        double initial_severity= 0.7; // Think if this can be consumed from the condition step.

        double complicationSeverity = complicationSeverityValue;
        int recoveryIteration = (complicationIteration + complicationDuration)<=internal_iterations*outer_iterations?complicationIteration + complicationDuration:internal_iterations*outer_iterations;
        double severityRamp = 1.0 / (recoveryIteration - complicationIteration);
        for (int j = 0; j < outer_iterations; j++) { // 2.5 hours in total
            for (int i = 0; i < internal_iterations; i++) { //25 minutes in total

                Double severity = calculateSeverity(i+(j*internal_iterations), initial_severity,
                    complicationSeverity, complicationIteration, recoveryIteration, severityRamp, gradualSeverity);
                if (severity != null) {
                    SEPatientAction cond = new SEAcuteRespiratoryDistressSyndromeExacerbation();
                    if (pneumonia) {
                        // Pneumonia
                        cond = new SELobarPneumoniaExacerbation();
                        ((SELobarPneumoniaExacerbation)cond).getLeftLungAffected().setValue(leftLungAffected);
                        ((SELobarPneumoniaExacerbation)cond).getRightLungAffected().setValue(rightLungAffected);
                        ((SELobarPneumoniaExacerbation)cond).getSeverity().setValue(severity);
                    } else {
                        // ARDS
                        ((SEAcuteRespiratoryDistressSyndromeExacerbation)cond).getLeftLungAffected().setValue(leftLungAffected);
                        ((SEAcuteRespiratoryDistressSyndromeExacerbation)cond).getRightLungAffected().setValue(rightLungAffected);
                        ((SEAcuteRespiratoryDistressSyndromeExacerbation)cond).getSeverity().setValue(severity);
                    }

                    scenario.addStep(new ApplyActionToSimulationStepDefinition(simulationName, cond));
                }

                scenario.addStep(new AdvanceSimulationTimeStepDefinition(
                    simulationName,
                    PULSE_SAMPLE_RATE_VALUE,
                    PULSE_SAMPLE_RATE_UNIT,
                    calculateNumberOfSteps(30),
                    true,
                    AdvanceSimulationTimeStepDefinition.Type.SYNC)
                );
            }
        }
        return scenario;
    }

    private Double calculateSeverity(int iteration, double initialSeverity, double complicationSeverity, int complicationIteration, int recoveryIteration, double severityRamp, boolean gradualSeverity){
        Double severity = complicationSeverity;
        if (!gradualSeverity){
            return complicationSeverity;
        }
        if (iteration <= complicationIteration){
            severity = initialSeverity;
        } else if (iteration > complicationIteration && iteration < recoveryIteration) {
            int complication_internal_pos = iteration - complicationIteration; //is 0 o greater
            severity = (severityRamp * complication_internal_pos * complicationSeverity) + initialSeverity;
            if (severity > complicationSeverity){
                severity = complicationSeverity;
            }
        }
        return severity;
    }

    private ScenarioDefinition initScenario(String scenarioName, String initialStateName, String simulationName, Map<String, String> tags) {
        ScenarioDefinition scenario = new ScenarioDefinition(scenarioName);

        //Init
        SimulationInitData initData = new SimulationInitData();
        initData.setSateName(initialStateName);
        if (tags != null) {
            initData.setTags(tags);
        }

        InitSimulationStepDefinition initSim = new InitSimulationStepDefinition(simulationName, initData);
        scenario.addStep(initSim);

        return scenario;
    }

    private int calculateNumberOfSteps(int desiredSeconds) {
        switch (PULSE_SAMPLE_RATE_UNIT) {
            case YEAR:
            case DAY:
            case HOUR:
            case MIN:
                throw new IllegalArgumentException("The sampling rate is greater than seconds");
            case SEC:
                if (desiredSeconds < PULSE_SAMPLE_RATE_VALUE) {
                    throw new IllegalArgumentException("The requested time period is smaller than the sampling rate");
                }
                return desiredSeconds / PULSE_SAMPLE_RATE_VALUE; //note that this may be an aproximation
            case MSEC:
                if (desiredSeconds < (PULSE_SAMPLE_RATE_VALUE / 1000)) {
                    throw new IllegalArgumentException("The requested time period is smaller than the sampling rate");
                }
                return desiredSeconds * 1000 / PULSE_SAMPLE_RATE_VALUE; //note that this may be an aproximation
            default:
                throw new IllegalArgumentException("Unsupported Sample Rate Unit " + PULSE_SAMPLE_RATE_UNIT);
        }
    }
}
