/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.rest;

import io.saperi.vcm.domain.AssignedDevice;
import io.saperi.vcm.service.AssignedDeviceService;
import io.saperi.vcm.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link io.saperi.vcm.domain.AssignedDevice}.
 */
@RestController
@RequestMapping("/api")
public class AssignedDeviceResource {

    private final Logger log = LoggerFactory.getLogger(AssignedDeviceResource.class);

    private static final String ENTITY_NAME = "assignedDevice";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AssignedDeviceService assignedDeviceService;

    public AssignedDeviceResource(AssignedDeviceService assignedDeviceService) {
        this.assignedDeviceService = assignedDeviceService;
    }

    /**
     * {@code POST  /assigned-devices} : Create a new assignedDevice.
     *
     * @param assignedDevice the assignedDevice to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new assignedDevice, or with status {@code 400 (Bad Request)} if the assignedDevice has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/assigned-devices")
    public ResponseEntity<AssignedDevice> createAssignedDevice(@Valid @RequestBody AssignedDevice assignedDevice) throws URISyntaxException {
        log.debug("REST request to save AssignedDevice : {}", assignedDevice);
        if (assignedDevice.getId() != null) {
            throw new BadRequestAlertException("A new assignedDevice cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AssignedDevice result = assignedDeviceService.save(assignedDevice);
        return ResponseEntity.created(new URI("/api/assigned-devices/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /assigned-devices} : Updates an existing assignedDevice.
     *
     * @param assignedDevice the assignedDevice to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated assignedDevice,
     * or with status {@code 400 (Bad Request)} if the assignedDevice is not valid,
     * or with status {@code 500 (Internal Server Error)} if the assignedDevice couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/assigned-devices")
    public ResponseEntity<AssignedDevice> updateAssignedDevice(@Valid @RequestBody AssignedDevice assignedDevice) throws URISyntaxException {
        log.debug("REST request to update AssignedDevice : {}", assignedDevice);
        if (assignedDevice.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AssignedDevice result = assignedDeviceService.save(assignedDevice);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, assignedDevice.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /assigned-devices} : get all the assignedDevices.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of assignedDevices in body.
     */
    @GetMapping("/assigned-devices")
    public List<AssignedDevice> getAllAssignedDevices() {
        log.debug("REST request to get all AssignedDevices");
        return assignedDeviceService.findAll();
    }

    /**
     * {@code GET  /assigned-devices/:id} : get the "id" assignedDevice.
     *
     * @param id the id of the assignedDevice to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the assignedDevice, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/assigned-devices/{id}")
    public ResponseEntity<AssignedDevice> getAssignedDevice(@PathVariable Long id) {
        log.debug("REST request to get AssignedDevice : {}", id);
        Optional<AssignedDevice> assignedDevice = assignedDeviceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(assignedDevice);
    }

    /**
     * {@code DELETE  /assigned-devices/:id} : delete the "id" assignedDevice.
     *
     * @param id the id of the assignedDevice to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/assigned-devices/{id}")
    public ResponseEntity<Void> deleteAssignedDevice(@PathVariable Long id) {
        log.debug("REST request to delete AssignedDevice : {}", id);
        assignedDeviceService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
