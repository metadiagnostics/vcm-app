/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.rest;

import io.saperi.vcm.service.EmrService;
import io.saperi.vcm.service.dto.Emr;
import io.saperi.vcm.service.dto.PatientDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

/**
 * REST controller to retrieve the EMR Items presented on VCM application.
 */
@RestController
@RequestMapping("/api/emr")
public class EmrResource {

    private static class EmrResourceException extends RuntimeException {

        private static final long serialVersionUID = 1L;

        private EmrResourceException(String message, Throwable e) {
            super(message, e);
        }
    }

    private final Logger log = LoggerFactory.getLogger(EmrResource.class);

    private EmrService emrService;

    @Autowired
    public EmrResource(EmrService emrService) {
        this.emrService = emrService;
    }

    /**
     * {@code GET  /item} : get the EMR items.
     *
     * @return an array of local emr items.
     * @throws EmrResourceException if the emr items couldn't be returned.
     */
    @GetMapping("/item")
    @SuppressWarnings("unchecked")
    public HashMap<String, PatientDefinition> getEmrItems() {
        try {
            log.debug("REST request to get all EMR Items");
            return emrService.getEmrInformation();
        } catch (Exception e) {
            throw new EmrResourceException("EMR items could not be found", e);
        }
    }

}
