/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.rest.ext.pulse;

import io.saperi.pulse.sim.api.model.PulseSimulation;
import io.saperi.pulse.sim.api.model.SimulationInfo;
import io.saperi.pulse.sim.api.model.SimulationInitData;
import io.saperi.pulse.sim.api.model.TimeUnit;
import io.saperi.pulse.sim.api.service.SimulationService;
import io.saperi.vcm.security.oauth2.JWTExpiredException;
import io.saperi.vcm.service.ext.impl.DevicesDataCache;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 */
@RestController
@RequestMapping("/api/ext/pulse/sim")
public class PulseSimulationResource {

    private final Logger log = LoggerFactory.getLogger(PulseSimulationResource.class);

    private final SimulationService client;
    private final DevicesDataCache cache;


    public PulseSimulationResource(
        SimulationService simulationServiceSpringClient,
        DevicesDataCache cache
    ) {
        this.client = simulationServiceSpringClient;
        this.cache = cache;
    }

    @GetMapping("/")
    public List<String> getSimulations() {
        log.debug("REST request to get information about All existing Pulse Simulations");
        List<String> simulations = new ArrayList<String>(client.getSimulationNames());
        return simulations;
    }

    @GetMapping("/{name}/info")
    public SimulationInfo getInfo(@PathVariable String name) throws JWTExpiredException {
        log.debug("REST request to get information about Pulse Simulation '{}'", name);

        if (!client.getSimulationNames().contains(name)) {
            SimulationInfo nonExistentSimulationInfo = new SimulationInfo();
            nonExistentSimulationInfo.setName(name);
            nonExistentSimulationInfo.setState(PulseSimulation.State.NON_EXISTING);
            return nonExistentSimulationInfo;
        }

        return client.getSimulationInfo(name);
    }

    @PostMapping("/{name}/start")
    public void start(
        @PathVariable String name,
        @RequestParam String initialStateName,
        @RequestParam(required = false) String diseaseName,
        @RequestParam(required = false, defaultValue = "60") int steps,
        @RequestParam(required = false, defaultValue = "1000") int increment
    ) throws ExecutionException, JWTExpiredException {
        log.debug("REST request to start Pulse Simulation '{}'", name);

        SimulationInitData initData = new SimulationInitData();
        initData.setSateName(initialStateName);
        if (!StringUtils.isBlank(diseaseName)) {
            initData.addTag("_VCM_DISEASE_", diseaseName);
        }
        initData.addTag("_VCM_SIM_STEPS_TAG_NAME_", steps + "");

        client.init(name, initData);
        log.trace("Simulation Initialized");
        client.executeFixed(name, increment, TimeUnit.MSEC, steps, true);
        log.trace("Simulation is running");
    }

    @PostMapping("/{name}/stop")
    public void stop(@PathVariable String name) throws ExecutionException, JWTExpiredException {
        log.debug("REST request to stop Pulse Simulation '{}'", name);

        client.stop(name);
        log.trace("Simulation is stopped");
    }

    @PostMapping("/{name}/run")
    public void run(
        @PathVariable String name,
        @RequestParam(required = false, defaultValue = "3600") int steps,
        @RequestParam(required = false, defaultValue = "1000") int increment
    ) throws ExecutionException, JWTExpiredException {
        log.debug("REST request to run Pulse Simulation '{}'", name);

        client.executeFixed(name, increment, TimeUnit.MSEC, steps, true);
        log.trace("Simulation is running");
    }

    @PostMapping("/{name}/terminate")
    public void terminate(@PathVariable String name) throws ExecutionException, JWTExpiredException {
        log.debug("REST request to terminate Pulse Simulation '{}'", name);

        client.terminate(name);
        log.trace("Simulation is terminated");

        //clear the cache fo the associated Device
        cache.clearCache(name);
    }

}
