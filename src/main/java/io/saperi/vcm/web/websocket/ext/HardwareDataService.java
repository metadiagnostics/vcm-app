/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.websocket.ext;

import io.saperi.vcm.event.*;
import io.saperi.vcm.service.ext.ExtendedAssignmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * Controller that transmits internal events about Devices to clients through
 * Websockets.
 *
 * @author esteban
 */
@Controller
public class HardwareDataService {

    private static final Logger log = LoggerFactory.getLogger(HardwareDataService.class);

    private final SimpMessageSendingOperations messagingTemplate;

    public HardwareDataService(
        ExtendedAssignmentService extendedAssignmentService,
        SimpMessageSendingOperations messagingTemplate,
        ApplicationEventPublisher applicationEventPublisher) {
        this.messagingTemplate = messagingTemplate;
    }

    @EventListener
    public void handleHardwareDemographicsEvent(HardwareDemographicsEvent event) {
        log.trace("New Demographics Hardware Data Event from Hardware {}", event.getHardwareId());
        messagingTemplate.convertAndSend("/topic/hardware/" + event.getHardwareId() + "/demographics-data", event);
    }

    @EventListener
    public void handleHardwareDemographicsEvent(HardwareDemographicsEvent[] events) {
        if (events!=null && events.length > 1) {
            log.trace("New Demographics Hardware Data Event from Hardware {}", events[0].getHardwareId());
            messagingTemplate.convertAndSend("/topic/hardware/" + events[0].getHardwareId() + "/demographics-data-elapsed", events);
        }
    }

    @EventListener
    public void handleHardwareVitalsEvent(HardwareVitalsEvent event) {
        log.trace("New Vitals Hardware Data Event from Hardware {}", event.getHardwareId());
        messagingTemplate.convertAndSend("/topic/hardware/" + event.getHardwareId() + "/vitals-data", event);
    }

    @EventListener
    public void handleHardwareVitalsEvent(HardwareVitalsEvent[] events) {
        if (events!=null && events.length > 1){
            log.trace("New Vitals Hardware Data Event from Hardware {}", events[0].getHardwareId());
            messagingTemplate.convertAndSend("/topic/hardware/" + events[0].getHardwareId() + "/vitals-data-elapsed", events);
        }
    }

    @EventListener
    public void handleHardwareVentilatorEvent(HardwareVentilatorEvent event) {
        log.trace("New Ventilator Hardware Data Event from Hardware {}", event.getHardwareId());
        messagingTemplate.convertAndSend("/topic/hardware/" + event.getHardwareId() + "/ventilator-data", event);
    }

    @EventListener
    public void handleHardwareVentilatorEventPassed(HardwareVentilatorEvent[] events) {
        if (events!=null && events.length > 1){
            log.trace("New Ventilator Hardware Data Passed Event from Hardware, count {}", events.length);
            messagingTemplate.convertAndSend("/topic/hardware/" + events[0].getHardwareId() + "/ventilator-data-elapsed", events);
        }
    }

    @EventListener
    public void handleHardwareDataEvent(HardwareBloodGasDataEvent event) {
        log.trace("New Hardware Blood Gas Data Event from Hardware {}", event.getHardwareId());
        messagingTemplate.convertAndSend("/topic/hardware/" + event.getHardwareId() + "/blood-gas-data", event);
    }

    @EventListener
    public void handleHardwareDataEvent(HardwareBloodGasDataEvent[] events) {
        if (events!=null && events.length > 1){
            log.trace("New Hardware Blood Gas Data Event from Hardware {}", events[0].getHardwareId());
            messagingTemplate.convertAndSend("/topic/hardware/" + events[0].getHardwareId() + "/blood-gas-data-elapsed", events);
        }
    }

    @EventListener
    public void handleHardwareStatusEvent(HardwareStatusEvent event) {
        log.trace("New Hardware Status Event from Hardware {}: {}", event.getHardwareId(), event.getStatus());
        messagingTemplate.convertAndSend("/topic/hardware/" + event.getHardwareId() + "/status", event);
    }

    @EventListener
    public void handleHardwareMessageEvent(HardwareMessageEvent event) {
        log.trace("New Message from Hardware {}", event.getSource().getHardwareId());
        messagingTemplate.convertAndSend("/topic/hardware/" + event.getSource().getHardwareId() + "/messages", event);
    }

    @EventListener
    public void handleHardwarePumpEvent(PumpEvent event) {
        log.trace("New Pump Data Event from Hardware {}", event.getHardwareId());
        messagingTemplate.convertAndSend("/topic/hardware/" + event.getHardwareId() + "/pump-data", event);
    }

    @EventListener
    public void handleDeviceFrameEvent(DeviceFrameEvent event) {
        log.trace("New device frame event fromm Hardware {}", event.getHardwareId());
        messagingTemplate.convertAndSend("/topic/hardware/" + event.getHardwareId() + "/device-frame", event);
    }

    @EventListener
    public void handleAlarmWarningEvent(AlarmWarningEvent[] events) {
        if (events==null || events.length==0) return;
        log.trace("New Alarm Warning Data Event from Hardware {}", events[0].getHardwareId());
        messagingTemplate.convertAndSend("/topic/hardware/" + events[0].getHardwareId() + "/alarmwarnings", events);
    }

    @EventListener
    public void handleAlarmEvent(AlarmEvent[] events) {
        if (events==null || events.length==0) return;
        log.trace("New Alarm Data Event from Hardware {}", events[0].getHardwareId());
        messagingTemplate.convertAndSend("/topic/hardware/" + events[0].getHardwareId() + "/alarms", events);
    }

}
