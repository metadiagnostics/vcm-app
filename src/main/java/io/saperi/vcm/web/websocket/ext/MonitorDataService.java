/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.websocket.ext;

import io.saperi.vcm.domain.Assignment;
import io.saperi.vcm.domain.Device;
import io.saperi.vcm.domain.enumeration.DeviceCapabilityType;
import io.saperi.vcm.domain.ext.util.AssignmentUtils;
import io.saperi.vcm.event.*;
import io.saperi.vcm.service.MonitorService;
import io.saperi.vcm.service.PumpService;
import io.saperi.vcm.service.dto.Pump;
import io.saperi.vcm.service.ext.ExtendedAssignmentService;
import io.saperi.vcm.service.mapper.ISO11073;
import io.saperi.vcm.util.CacheUtil;
import io.saperi.vcm.web.rest.errors.BadRequestAlertException;
import java.security.Principal;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import io.saperi.virtualhealth.device.aggregators.common.managers.DataSetManager;
import io.saperi.virtualhealth.device.aggregators.common.managers.UnitManager;
import io.saperi.virtualhealth.device.aggregators.common.mappers.DataSetNameMapper;
import io.saperi.virtualhealth.device.aggregators.common.model.DataSet;
import io.saperi.virtualhealth.device.aggregators.common.model.UnitSet;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.handler.annotation.*;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import javax.annotation.PostConstruct;
import javax.cache.Cache;

/**
 * Controller in charge of sending/receiving data (through a Websocket) related
 * to an Assignment being monitored.
 *
 * @author esteban
 */
@Controller
@Slf4j
public class MonitorDataService implements ApplicationListener<SessionDisconnectEvent> {

    private final ExtendedAssignmentService extendedAssignmentService;
    private final MonitorService monitorService;
    private final SimpMessageSendingOperations messagingTemplate;
    private final ApplicationEventPublisher applicationEventPublisher;

    private final Map<String, List<Device>> hardwareListenersPerSession = new HashMap<>();
    private Cache<String, List<HardwareEvent>> eventsCache;
    private PumpService pumpService;
    private List<Pump> pumpList;

    @Autowired
    public MonitorDataService(
        ExtendedAssignmentService extendedAssignmentService,
        SimpMessageSendingOperations messagingTemplate,
        ApplicationEventPublisher applicationEventPublisher,
        PumpService pumpService,
        MonitorService monitorService) {
        this.messagingTemplate = messagingTemplate;
        this.extendedAssignmentService = extendedAssignmentService;
        this.applicationEventPublisher = applicationEventPublisher;
        this.pumpService = pumpService;
        this.monitorService = monitorService;
    }

    @PostConstruct
    private void setupMockedPumps() {
        this.pumpList = this.pumpService.gePumps();
        this.eventsCache = CacheUtil.eventsCache();
    }

    private Map<Integer, String> getDatasetMappings() {
       DataSet ds = DataSetManager.getDataSetManager().getDataSet("ISO11073");
       return ds.getCodes().stream().filter(id -> StringUtils.isNumeric(id)).collect( Collectors.toMap(id -> Integer.valueOf(id), id-> ds.getName(id),(a,b)->a));
    }

    private Map<Integer, String> getUnitMappings() {
       UnitSet unitSet = UnitManager.getUnitManager().getUnitSet("ISO11073_Prototype");
       return unitSet.getUnitsCodes().stream().filter(id -> StringUtils.isNumeric(id)).collect(Collectors.toMap(id -> Integer.valueOf(id), id-> unitSet.getUnit(id)!=null?unitSet.getUnit(id).getAbbreviation():null,(a,b)->a));
    }

    private Map<String, String> getIdSettingsMappings() {
        return monitorService.getMappingSettingIds();
    }

    private Map<String, String> getIdMeasureMappings() {
        return monitorService.getMappingMeasureIds();
    }

    @MessageMapping("topic/monitor/{assignmentId}/activate")
    public void activateAssignment(@DestinationVariable("assignmentId") long assignmentId, StompHeaderAccessor stompHeaderAccessor, Principal principal) {
        log.debug("Activating Hardware Listeners for Assignment {}", assignmentId);
        Optional<Assignment> assignmentO = extendedAssignmentService.findOneWithEagerRelationships(assignmentId);

        // Obtain dataset-mapping);
        messagingTemplate.convertAndSend("/topic/dataset-mapping", getDatasetMappings());
        // Obtain unit-mapping
        messagingTemplate.convertAndSend("/topic/unit-mapping", getUnitMappings());
        // Obtain settingid-metricid mappings
        messagingTemplate.convertAndSend("/topic/id-settings-mapping", getIdSettingsMappings());
        // Obtain measureid-metricid mappings
        messagingTemplate.convertAndSend("/topic/id-measures-mapping", getIdMeasureMappings());

        if (!assignmentO.isPresent()){
            throw new BadRequestAlertException("Invalid id", "Assignment", "idnull");
        }
        List<Device> devices = hardwareListenersPerSession.computeIfAbsent(stompHeaderAccessor.getSessionId(), k -> new CopyOnWriteArrayList<>());
        for (Map.Entry<Device, Set<DeviceCapabilityType>> entry : AssignmentUtils.getRequiredCapabilitiesByDevice(assignmentO.get()).entrySet()) {
            log.debug("Activating Hardware Listeners for Hardware {}", entry.getKey().getHardwareId());
            this.applicationEventPublisher.publishEvent(new HardwareListenerActivationRequestEvent(
                entry.getKey().getModel().getType(),
                entry.getKey().getHardwareId(),
                entry.getValue(),
                entry.getKey().getHardwareId()
            ));
            devices.add(entry.getKey());
            //send all the events that had elapsed
            if (eventsCache.containsKey(entry.getKey().getHardwareId())){
                List<HardwareEvent> events = eventsCache.get(entry.getKey().getHardwareId());
                log.info("Events on cache: {}", events.size());
                HardwareVitalsEvent[] hvitalse = events.stream().filter(e -> e instanceof HardwareVitalsEvent).map(e -> (HardwareVitalsEvent)e).toArray(HardwareVitalsEvent[]::new);
                HardwareVentilatorEvent[] hve = events.stream().filter(e -> e instanceof HardwareVentilatorEvent).map(e -> (HardwareVentilatorEvent)e).toArray(HardwareVentilatorEvent[]::new);
                HardwareDemographicsEvent[] hde = events.stream().filter(e -> e instanceof HardwareDemographicsEvent).map(e -> (HardwareDemographicsEvent)e).toArray(HardwareDemographicsEvent[]::new);
                HardwareBloodGasDataEvent[] hbge = events.stream().filter(e -> e instanceof HardwareBloodGasDataEvent).map(e -> (HardwareBloodGasDataEvent)e).toArray(HardwareBloodGasDataEvent[]::new);
                int i=0;
                ExecutorService es = Executors.newFixedThreadPool(3);
                List<Callable<Integer>> todo = new ArrayList<Callable<Integer>>(3);
                int length = Math.max(Math.max(hvitalse.length, hve.length),hde.length);
                while (i < length) {
                    if (i < hvitalse.length) {
                        todo.add(new BulkEventThread(Arrays.copyOfRange(hvitalse, i, hvitalse.length>i+50?i+50:hvitalse.length-1), this.applicationEventPublisher));
                    }
                    if (i < hve.length) {
                        todo.add(new BulkEventThread(Arrays.copyOfRange(hve, i, hve.length > i + 50 ? i + 50 : hve.length), this.applicationEventPublisher));
                    }
                    if (i < hde.length) {
                        todo.add(new BulkEventThread(Arrays.copyOfRange(hde, i, hde.length>i+50?i+50:hde.length), this.applicationEventPublisher));
                    }
                    try {
                        es.invokeAll(todo);
                    } catch (InterruptedException e) {
                        log.error("Could not execute all bulk event threads", e);
                    }
                    todo = new ArrayList<Callable<Integer>>(3);
                    i = i + 50;
                }
                this.applicationEventPublisher.publishEvent(hbge);
            }
        }
    }

    @MessageMapping("/topic/monitor/{assignmentId}/deactivate")
    public void deactivateAssignment(@DestinationVariable("assignmentId") long assignmentId, StompHeaderAccessor stompHeaderAccessor, Principal principal) {
        log.debug("Terminating Hardware Listeners for Assignment {}", assignmentId);
        this.terminateListenersForSession(stompHeaderAccessor.getSessionId());
    }

    @Override
    public void onApplicationEvent(SessionDisconnectEvent event) {
        log.debug("Terminating Hardware Listeners for Session {} because of disconnection", event.getSessionId());
        this.terminateListenersForSession(event.getSessionId());
    }

    @EventListener
    public void handleVentilatorRecommendationResponseEvent(VentilatorRecommendationResponseEvent event) {
        log.trace("New Recommendation Response for Assignment {}", event.getSource().getAssignmentId());
        messagingTemplate.convertAndSend("/topic/monitor/" + event.getSource().getAssignmentId() + "/ventilator-recommendation", event);
    }

    private void terminateListenersForSession(String sessionId){
        List<Device> devices = hardwareListenersPerSession.remove(sessionId);
        log.debug("The session {} has {} Hardware Listeners registered. We are going to terminate them.", sessionId, devices == null ? 0 : devices.size());
        if (devices != null){
            for (Device device : devices) {
                this.applicationEventPublisher.publishEvent(new HardwareListenerDeactivationRequestEvent(
                    device.getModel().getType(),
                    device.getHardwareId(),
                    device.getHardwareId()
                ));
            }
        }
    }
}
