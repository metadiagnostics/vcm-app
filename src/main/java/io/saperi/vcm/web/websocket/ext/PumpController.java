/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.websocket.ext;

import io.saperi.vcm.service.PumpService;
import io.saperi.vcm.service.VentilatorService;
import io.saperi.virtualhealth.device.aggregators.common.DeviceAggregatorClient;
import io.saperi.virtualhealth.device.aggregators.common.model.DeviceInfo;
import io.saperi.virtualhealth.device.aggregators.common.model.Frame;
import io.saperi.virtualhealth.device.aggregators.common.model.Setting;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * Controller in charge of receiving data (through a Websocket) related with a Pump.
 *
 * @author sgroh
 */
@Controller
@Slf4j
public class PumpController {

    private PumpService pumpService;
    private VentilatorController ventilatorController;

    @Autowired
    PumpController(PumpService pumpService,
                   VentilatorController ventilatorController){
        this.pumpService = pumpService;
        this.ventilatorController = ventilatorController;
    }



    @MessageMapping("/topic/pump/{hwId}/pause/{pumpId}")
    public void pausePump(@DestinationVariable("hwId") String hwId,
                          @DestinationVariable("pumpId") String pumpId) {
        if ("undefined".equals(hwId) || hwId==null || hwId.length()==0) {
            log.error("not possible to pause, HW is undefined");
            return;
        }
        if ("undefined".equals(pumpId) || pumpId==null || pumpId.length()==0) {
            log.error("not possible to pause, PumpID is undefined");
            return;
        }
        pumpService.pausePump(hwId, pumpId);
    }

    @MessageMapping("/topic/pump/{hwId}/resume/{pumpId}")
    public void resumePump(@DestinationVariable("hwId") String hwId,
                           @DestinationVariable("pumpId") String pumpId) {
        if ("undefined".equals(hwId) || hwId==null || hwId.length()==0) {
            log.error("not possible to resume, HW is undefined");
            return;
        }
        if ("undefined".equals(pumpId) || pumpId==null || pumpId.length()==0) {
            log.error("not possible to resume, PumpID is undefined");
            return;
        }
        pumpService.resumePump(hwId, pumpId);
    }

    @MessageMapping("/topic/pump/{masterDeviceId}/device/{deviceId}/change/{metricId}")
    public void changeVentilatorSetting(@DestinationVariable("masterDeviceId") String masterDeviceId,
                                        @DestinationVariable("deviceId") String deviceId,
                                        @DestinationVariable("metricId") String metricId,
                                        String data) {
        pumpService.changePumpSetting(masterDeviceId, deviceId, metricId, data);
    }

    @MessageMapping("/topic/pump/{masterDeviceId}/device/{deviceId}/change1/{metricId1}/change2/{metricId2}")
    public void changeSyncVentilatorSetting(@DestinationVariable("masterDeviceId") String masterDeviceId,
                                        @DestinationVariable("deviceId") String deviceId,
                                        @DestinationVariable("metricId1") String metricId1,
                                        @DestinationVariable("metricId2") String metricId2,
                                        String data) {
        pumpService.changeSyncPumpSetting(masterDeviceId, deviceId, metricId1, metricId2, data);
    }


}

