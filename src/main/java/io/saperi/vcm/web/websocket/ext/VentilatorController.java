/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.websocket.ext;

import io.saperi.vcm.service.VentilatorService;
import io.saperi.vcm.service.ext.impl.jaimsaggregator.JaimsAggregatorEventListenerService;
import io.saperi.virtualhealth.device.aggregators.common.DeviceAggregatorClient;
import io.saperi.virtualhealth.device.aggregators.common.model.DeviceInfo;
import io.saperi.virtualhealth.device.aggregators.common.model.Frame;
import io.saperi.virtualhealth.device.aggregators.common.model.Setting;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * Controller in charge of receiving data (through a Websocket) related with a Ventilator.
 *
 * @author sgroh
 */
@Controller
@Slf4j
public class VentilatorController {

    JaimsAggregatorEventListenerService jaimsAggregatorEventListenerService;
    private VentilatorService ventilatorService;

    @Autowired
    VentilatorController(JaimsAggregatorEventListenerService jaimsAggregatorEventListenerService,
                         VentilatorService ventilatorService){
        this.jaimsAggregatorEventListenerService = jaimsAggregatorEventListenerService;
        this.ventilatorService = ventilatorService;
    }



    @MessageMapping("/topic/ventilator/{hwId}/change/{metricId}")
    public void changeVentilatorSetting(@DestinationVariable("hwId") String hwId,
                                 @DestinationVariable("metricId") String metricId,
                                 String data) {
        log.debug("Change Ventilator for hw id {}, metricId: {}, data: {}", hwId, metricId, data);
        DeviceAggregatorClient daClient = jaimsAggregatorEventListenerService.getDeviceAggregatorClient(hwId);
        Frame aggregateFrame = daClient.getFrameManager().getAggregateFrame();
        List<DeviceInfo> devices = aggregateFrame.getDevicesWithSetting(metricId);
        if (devices.size()>0) {
            Setting setting = new Setting();
            setting.setMetric_id(metricId);
            try {
                setting.setValue(Float.valueOf(data));
                setting.setStringValue(data);
            } catch (NumberFormatException e) {
                log.warn("Could not set ventilator value: {} on metric id: {}", data, metricId);
            }
            setting.setStringValue(data);
            daClient.postSettingChange(devices.get(0), List.of(setting));
        }
    }
    @MessageMapping("/topic/ventilator/settingreceived/{sessionId}")
    public void ventilatorSettingReceived(@DestinationVariable("sessionId") String sessionId, String data) {
        log.debug("Ventilator Setting received for session {}", sessionId);
        ventilatorService.settingsReceivedForSession(sessionId);
    }


}
