/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.websocket.ext;

import org.springframework.context.ApplicationEventPublisher;

import java.util.concurrent.Callable;

public class BulkEventThread<T> implements Callable<Integer> {
    private T[] events;
    private ApplicationEventPublisher applicationEventPublisher;

    public BulkEventThread(T[] events, ApplicationEventPublisher applicationEventPublisher) {
        this.events = events;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    /**
     * This will publish an event that {@link HardwareDataService} will handle.
     *
     * @throws Exception
     */
    @Override
    public Integer call() throws Exception {
        this.applicationEventPublisher.publishEvent(this.events);
        return this.events.length;
    }
}
