/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.security.oauth2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.oauth2.core.OAuth2TokenValidator;
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.util.Assert;

import java.util.List;

public class AudienceValidator implements OAuth2TokenValidator<Jwt> {
    private final String COGNITO_ISSUER_PREFIX = "https://cognito-idp";
    private final String ISSUER_KEY = "iss";
    private final String CLIENT_ID_KEY = "client_id";
    private final Logger log = LoggerFactory.getLogger(AudienceValidator.class);
    private final OAuth2Error error = new OAuth2Error("invalid_token", "The required audience is missing", null);

    private final List<String> allowedAudience;

    public AudienceValidator(List<String> allowedAudience) {
        Assert.notEmpty(allowedAudience, "Allowed audience should not be null or empty.");
        this.allowedAudience = allowedAudience;
    }

    public OAuth2TokenValidatorResult validate(Jwt jwt) {
        //When the Authentication Provider is Cognito there is no "Aud" property so we shoudl validate it
        // using the client_id property as their documentation said:
        //   "aud or client_id – Must match one of the audience entries that is configured for the authorizer."
        // Reference: https://docs.aws.amazon.com/apigateway/latest/developerguide/http-api-jwt-authorizer.html#http-api-jwt-authorizer.evaluation
        if ( (jwt.getClaims()!=null) &&
            (jwt.getClaims().containsKey(ISSUER_KEY)) &&
            (jwt.getClaims().containsKey(CLIENT_ID_KEY)) &&
            ((String)jwt.getClaims().get(ISSUER_KEY)).startsWith(COGNITO_ISSUER_PREFIX)) {
            if (allowedAudience.stream().anyMatch(a -> a.equals(jwt.getClaims().get(CLIENT_ID_KEY)))){
                return OAuth2TokenValidatorResult.success();
            }
        }
        // For the rest of the authenticator providers the authentication flow will use audience
        List<String> audience = jwt.getAudience();
        if (audience.stream().anyMatch(allowedAudience::contains)) {
            return OAuth2TokenValidatorResult.success();
        } else {
            log.warn("Invalid audience: {}", audience);
            return OAuth2TokenValidatorResult.failure(error);
        }
    }
}
