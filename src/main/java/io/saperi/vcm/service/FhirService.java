/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.context.FhirVersionEnum;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.StringClientParam;
import io.saperi.vcm.service.dto.PatientDemographics;
import io.saperi.vcm.util.FhirServiceUtil;
import io.saperi.vcm.util.FhirUtil;
import io.saperi.vcm.util.PatientUtil;
import lombok.extern.slf4j.Slf4j;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.Fallback;
import net.jodah.failsafe.RetryPolicy;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Observation;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.hl7.fhir.r4.model.Patient;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class FhirService {
    private static final String QUERY = "Observation?_count=50&";
    private static final String FHIR_URL = "http://acs-scanr.saperi.ai:8080/fhir";
    public static final StringClientParam PARAM_ID = new StringClientParam("_id");
    private final IGenericClient client;

    public FhirService(IGenericClient client) {
        this.client = client;
    }

    public FhirService() {
        FhirContext fhirContext = new FhirContext(FhirVersionEnum.R4);
        this.client = fhirContext.newRestfulGenericClient(FHIR_URL);
    }

    public IGenericClient getClient() {
        return this.client;
    }


    public PatientDemographics retrievePatientDemographics(String patientId) {

        log.debug("Querying for Patient's demographics data");
        RetryPolicy<Patient> retryPolicy = new RetryPolicy<Patient>()
            .handleResult(null)
            .withDelay(Duration.ofSeconds(1))
            .withMaxRetries(3);

        Fallback<Patient> fallback = Fallback.<Patient>of(() -> {
            throw new IllegalStateException("Couldn't find a Patient with id " + patientId);
        }).handleResultIf(r -> r == null);

        Patient patient = Failsafe.with(fallback, retryPolicy).get(() -> findPatientById(patientId));


        Assert.notNull(patient, "Couldn't find a Patient with id " + patientId);

        String url = QUERY + "subject=Patient/" + patientId;
        // url += "&" + this.buildDemographicsCodeParameters();
        url += "&_sort=date";

        log.debug("Querying for Patient's birth data");
        Bundle bundle = getClient().search().byUrl(url).returnBundle(Bundle.class).execute();

        List<Observation> observations = FhirServiceUtil.buildAndRetrieveAllResources(getClient(), bundle, Observation.class);
        log.debug("Observations found: " + observations.size());
        if (log.isDebugEnabled()) {
            for (Observation observation : observations) {
                log.debug("\t- {}", observation.getIdElement());
            }
        }
        return this.buildDemographicsFromBundle(patient, observations);
    }

    public Patient findPatientById(String patientId) {
        searchResourceById(patientId, Patient.class);
        String query = "Patient?_id=" + patientId;
        Bundle bundle = getClient().search().byUrl(query).returnBundle(Bundle.class).execute();

        if (bundle != null && bundle.hasEntry() && bundle.getEntryFirstRep().getResource() instanceof Patient) {
            return (Patient) bundle.getEntryFirstRep().getResource();
        }

        return null;
    }

    public <T extends IBaseResource> T searchResourceById(String resourceId, Class<T> clazz) {
        Bundle bundle = (Bundle)this.getClient().search().forResource(clazz).where(PARAM_ID.matches().value(resourceId)).returnBundle(Bundle.class).execute();
        List<T> entries = FhirUtil.getEntries(bundle, clazz);
        return entries != null && !entries.isEmpty() ? entries.get(0) : null;
    }

    private PatientDemographics buildDemographicsFromBundle(Patient patient, List<Observation> observations) {


        Date demographicsTimestamp = patient.getMeta().getLastUpdated();

        PatientDemographics demographics = new PatientDemographics();
        demographics.setPatientId(patient.getIdElement().getIdPart());
        demographics.setName(PatientUtil.getPatientName(patient));
        demographics.setMedicalRecordNumber(PatientUtil.getMedicalRecordNumber(patient));

        String birthDate = PatientUtil.getPatientBirthdate(patient);
        demographics.setBirthDate(birthDate);

        Integer daysofLife = PatientUtil.getDaysOfLife(patient);
        demographics.setDaysOfLife(daysofLife);

        if (patient.hasGender()) {
            demographics.setGender(patient.getGender().getDisplay());
        }

        BigDecimal egaDays = null;
        for (Observation observation : observations) {
            if (observation.hasCode()
                && observation.getCode().hasCoding()
                && observation.getCode().getCodingFirstRep() != null
                && observation.getCode().getCodingFirstRep().hasCode()) {

                try {
                    String code = observation.getCode().getCodingFirstRep().getCode();
                    /*if (code.equals(this.translatedBiW.getCode()) && observation.hasValueQuantity()) {
                        demographics.setBirthWeight(this.getWeightString(observation.getValueQuantity()));
                    } else if (code.equals(this.translatedCaW.getCode()) && observation.hasValueQuantity()) {
                        demographics.setCalculatedWeight(this.getWeightString(observation.getValueQuantity()));
                    } else if (code.equals(this.translatedBoW.getCode()) && observation.hasValueQuantity()) {
                        demographics.setTodayWeight(this.getWeightString(observation.getValueQuantity()));
                    } else if (code.equals(this.translatedEga.getCode())
                        && observation.hasValueQuantity()
                        && observation.getValueQuantity().hasValue() && observation.getValueQuantity().hasUnit()
                        && (observation.getValueQuantity().getCode().equals("d") || observation.getValueQuantity().getCode().equals("wk"))) {
                        if (observation.getValueQuantity().getCode().equals("wk")) {
                            egaDays = observation.getValueQuantity().getValue().multiply(BigDecimal.valueOf(7));
                        } else {
                            egaDays = observation.getValueQuantity().getValue();
                        }
                        demographics.setEstimatedGestationalAge(this.getGestationalAgeString(egaDays));
                    }*/
                } catch (FHIRException e) {
                    // We don't want to skip all resources if one observation doesn't have a value quantity
                }

                if (observation.getMeta().getLastUpdated().compareTo(demographicsTimestamp) > 0) {
                    demographicsTimestamp = observation.getMeta().getLastUpdated();
                }
            }
        }

        BigDecimal cga = this.calculateCga(egaDays, daysofLife);

        if (cga != null) {
            demographics.setCorrectedGestationalAge(this.getGestationalAgeString(cga));
        }

        demographics.setTimestamp(demographicsTimestamp.getTime());
        return demographics;
    }

    private BigDecimal calculateCga(BigDecimal egaDays, Integer daysOfLife) {
        BigDecimal cga = null;

        if (egaDays != null && daysOfLife != null) {
            cga = egaDays.add(BigDecimal.valueOf(daysOfLife));
        }

        return cga;
    }

    private String getGestationalAgeString(BigDecimal value) {

        int weeks = value.divide(BigDecimal.valueOf(7), RoundingMode.DOWN).intValue();
        int days = value.remainder(BigDecimal.valueOf(7)).intValue();

        String output = weeks + "";
        if (days != 0) {
            output += "+" + days;
        }
        output += "wks";

        return output;
    }

}
