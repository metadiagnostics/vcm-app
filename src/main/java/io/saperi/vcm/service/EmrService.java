/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.saperi.vcm.service.dto.Emr;
import io.saperi.vcm.service.dto.PatientDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.HashMap;

/**
 * Service class for managing EMR information.
 */
@Service
@Transactional
public class EmrService {

    private final Logger log = LoggerFactory.getLogger(EmrService.class);
    private final ObjectMapper mapper = new ObjectMapper();
    @Autowired
    MonitorDefinitionConfig monitorDefinitionConfig;
    //@Value("${vcm.emr.conditions}")
    private String primaryConditions;
    private String[] primaryConditionsArray;

    //@Value("${vcm.emr.mechanism-of-injury}")
    private String mechanisOfInjury;
    private String[] mechanisOfInjuryArray;

    //@Value("${vcm.emr.locations}")
    private String locations;
    private String[] locationsArray;

    //@Value("${vcm.emr.allergies}")
    private String allergies;
    private String[] allergiesArray;

    //@Value("${vcm.emr.locus}")
    private String locus;

    private Emr emr;
    private FhirService fhirService;

    @Inject
    public EmrService(FhirService fhirService) {
        this.fhirService = fhirService;
    }


    public HashMap<String, PatientDefinition> getEmrInformation(){
        log.info("Obtaining local team members");
        try {
            fhirService.retrievePatientDemographics("224"); // id from ???
        } catch (Exception e) {
          log.error("The Fhir Service could not extract patient information", e);
        }
        return monitorDefinitionConfig.getEmr();
    }
}
