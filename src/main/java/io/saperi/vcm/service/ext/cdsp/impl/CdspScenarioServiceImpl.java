/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.cdsp.impl;

import com.cognitivemedicine.cdsp.simulator.api.ScenarioInstanceEventListenerAdapter;
import com.cognitivemedicine.cdsp.simulator.context.ScenarioContext;
import com.cognitivemedicine.cdsp.simulator.definition.ScenarioDefinition;
import com.cognitivemedicine.cdsp.simulator.instance.ScenarioInstance;
import com.cognitivemedicine.cdsp.simulator.instance.StepInstance;
import io.saperi.pulse.sim.api.service.SimulationService;
import io.saperi.pulse.sim.api.service.SubstanceService;
import io.saperi.pulse.sim.simulator.config.ContextConfigurator;
import io.saperi.pulse.sim.simulator.runtime.PulseSimulatorContextService;
import io.saperi.pulse.sim.simulator.runtime.PulseSubstanceContextService;
import io.saperi.vcm.service.ext.cdsp.CdspScenarioService;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 * @author esteban
 */
@Component
public class CdspScenarioServiceImpl implements CdspScenarioService {

    private static final Logger log = LoggerFactory.getLogger(CdspScenarioServiceImpl.class);

    private final SubstanceService substanceService;
    private final SimulationService simulationService;

    private final Map<String, ScenarioInstance> instances = new ConcurrentHashMap<>();

    public CdspScenarioServiceImpl(SubstanceService substanceService, SimulationService simulationService) {
        this.substanceService = substanceService;
        this.simulationService = simulationService;
    }

    @Override
    public void start(ScenarioDefinition def) {
        ScenarioInstance instance = instances.get(def.getName());
        if (instance != null) {
            //If there was already an instance, terminate it. We have to terminate
            //both: the Cdsp Scenario and the Pulse Simulation.
            instance.stop();
            if (simulationService.getSimulationNames().contains(def.getName())) {
                simulationService.terminate(def.getName());
            }
        }

        ScenarioContext context = createContext();

        instance = def.createInstance(context);
        instances.put(def.getName(), instance);

        instance.addEventListener(new ScenarioInstanceEventListenerAdapter() {
            @Override
            public void onScenarioAbort(StepInstance step, Exception e) {
                //TODO: send to the client?
                log.error("Exception executing Scenario. ", e);
            }

        });

        //TODO: use an executor
        new Thread(instance).start();

    }

    @Override
    public void pause(String defName) {
        ScenarioInstance instance = instances.remove(defName);
        if (instance == null) {
            log.warn("No Scenario with name '{}' was found", defName);
            return;
        }
        instance.pause();
    }

    @Override
    public void resume(String defName) {
        ScenarioInstance instance = instances.remove(defName);
        if (instance == null) {
            log.warn("No Scenario with name '{}' was found", defName);
            return;
        }
        instance.resume();
    }

    @Override
    public void stop(String defName) {
        ScenarioInstance instance = instances.remove(defName);
        if (instance == null) {
            log.warn("No Scenario with name '{}' was found", defName);
        } else {
            instance.stop();
        }
        //in any case, try to terminate the simulation in Pulse
        if (simulationService.getSimulationNames().contains(defName)) {
            simulationService.terminate(defName);
        }
    }

    private ScenarioContext createContext() {
        ScenarioContext context = new ScenarioContext();
        new ContextConfigurator(new PulseSimulatorContextService(simulationService), new PulseSubstanceContextService(substanceService)).configureContext(context);
        return context;
    }

}
