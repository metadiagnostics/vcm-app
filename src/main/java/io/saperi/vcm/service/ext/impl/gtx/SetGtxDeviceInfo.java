/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.gtx;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class SetGtxDeviceInfo {

    private Set<GtxDeviceInfo> setGtxDeviceInfo = new HashSet<>();

    public SetGtxDeviceInfo(GtxDeviceInfo ...devices) {
        setGtxDeviceInfo.addAll(Arrays.asList(devices));
    }

    public Set<GtxDeviceInfo> getAllDevices(){
        // We should remove from here GTXDeviceInfo with same instance and same HW id
        Set<GtxDeviceInfo> clone = new HashSet<GtxDeviceInfo>();
        clone = (HashSet<GtxDeviceInfo>) ((HashSet<GtxDeviceInfo>) setGtxDeviceInfo).clone();
        return clone.stream().filter(info -> info.getRunId()!=null).filter(distinctByKey(info -> info.getClass())).collect(Collectors.toSet());
    }

    private static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor)
    {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    public GtxDeviceInfo getGtxDevice(Class gtxDeviceInfoClass){
        return setGtxDeviceInfo.stream().filter(gtxDeviceInfoClass::isInstance).findFirst().orElse(null);
    }

    public void addGtxDevice(GtxDeviceInfo device) {
        setGtxDeviceInfo.add(device);
    }
}
