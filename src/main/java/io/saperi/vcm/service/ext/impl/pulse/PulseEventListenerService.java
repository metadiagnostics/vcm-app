/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.pulse;

import io.saperi.pulse.sim.api.model.SimulationInfo;
import io.saperi.pulse.sim.api.service.SimulationService;
import io.saperi.pulse.sim.client.SimulationEventClient;
import io.saperi.pulse.sim.client.jms.JmsSimulationEventClient;
import io.saperi.pulse.sim.client.websocket.WebSocketSimulationEventListener;
import io.saperi.vcm.config.ext.pulse.PulseConfigurationUtils;
import io.saperi.vcm.domain.Device;
import io.saperi.vcm.domain.enumeration.DeviceType;
import io.saperi.vcm.event.*;
import io.saperi.vcm.security.SecurityUtils;
import io.saperi.vcm.service.ext.ExtendedDeviceService;
import io.saperi.vcm.service.ext.HardwareEventListenerServiceAdapter;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.cache.Cache;
import javax.jms.ConnectionFactory;

import io.saperi.vcm.util.CacheUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.task.TaskExecutor;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.stereotype.Service;

/**
 * Class that listens for events coming from a Pulse instance, converts them
 * into a {@link HardwareDemographicsEvent} and broadcasts them to the entire
 * application.
 *
 * @author esteban
 */
@Service
public class PulseEventListenerService extends HardwareEventListenerServiceAdapter {

    private static final Logger log = LoggerFactory.getLogger(PulseEventListenerService.class);

    @Value("${vcm.pulse.enabled:false}")
    private boolean pulseEnabled;

    @Value("${vcm.pulse.listener-implementation:websocket}")
    private String listenerImplementation;

    @Value("${vcm.pulse.jms.topic}")
    private String pulseJmsTopicName;

    @Value("${vcm.pulse.jms.discriminator.key}")
    private String pulseJmsDiscriminator;

    private final PulseConfigurationUtils pulseUtils;

    private final SimulationService pulseSimulationService;

    private final ApplicationEventPublisher applicationEventPublisher;

    private final RetryTemplate retryTemplate;

    private final TaskExecutor taskExecutor;

    private final JwtDecoder jwtDecoder;

    private final ConnectionFactory connectionFactory;

    private SimulationEventClient simulationEventListener;

    private final Map<String, String> subscriptionIdBySimulationId = new ConcurrentHashMap<>();

    public PulseEventListenerService(
        PulseConfigurationUtils pulseUtils,
        SimulationService pulseSimulationService,
        ExtendedDeviceService extendedDeviceService,
        ApplicationEventPublisher applicationEventPublisher,
        RetryTemplate retryTemplate,
        TaskExecutor taskExecutor,
        JwtDecoder jwtDecoder,
        @Qualifier("amq-jms-factoy") ConnectionFactory connectionFactory) {
        super(extendedDeviceService, applicationEventPublisher);
        this.pulseUtils = pulseUtils;
        this.pulseSimulationService = pulseSimulationService;
        this.applicationEventPublisher = applicationEventPublisher;
        this.retryTemplate = retryTemplate;
        this.taskExecutor = taskExecutor;
        this.jwtDecoder = jwtDecoder;
        this.connectionFactory = connectionFactory;
    }

    /**
     * On startup, a Control Listener is setup for Pulse. Also, for each of the
     * PULSE devices that exist in the system, a Simulation Listener is
     * registered.
     */
    @PostConstruct
    public void postConstruct() {
        super.eventsCache = CacheUtil.eventsCache();
        if (!pulseEnabled) {
            log.info("Pulse Listener is disabled by configuration");
            return;
        }

        SimulationEventClient eventListener = getEventListener();

        //Register the Control Listener
        eventListener.registerControlListener(new PulseSimulationControlEventListener(applicationEventPublisher));

        registerExistingDevices();
    }

    @PreDestroy
    public void preDestroy() {
        if (simulationEventListener != null) {
            simulationEventListener.dispose();
        }
    }

    @Override
    protected DeviceType[] getTypes() {
        return new DeviceType[] {DeviceType.MOCK};
    }

    /**
     * When an Assignment is open in the UI, we check the status of the
     * associated simulation in Pulse and send a {@link HardwareStatusEvent}.
     *
     * @param request
     */
    @Override
    protected void internalHardwareListenerActivationRequestEvent(HardwareListenerActivationRequestEvent request) {
        SimulationInfo simulationInfo;
        if (!pulseSimulationService.getSimulationNames().contains(request.getHardwareId())) {
            applicationEventPublisher.publishEvent(new HardwareStatusEvent(
                request.getHardwareId(),
                HardwareStatusEvent.Status.ERROR,
                String.format("No Simulation with name '%s' was found", request.getHardwareId()),
                ""
            ));
        } else {
            simulationInfo = pulseSimulationService.getSimulationInfo(request.getHardwareId());
            applicationEventPublisher.publishEvent(PulseUtils.toHardwareStatusEvent(request.getHardwareId(), simulationInfo.getState()));
        }
    }

    @Override
    protected void internalOnHardwareListenerDeactivationRequestEvent(HardwareListenerDeactivationRequestEvent request) {
    }

    private SimulationEventClient createSimulationEventClient() throws Exception {
        switch (listenerImplementation) {
            case "jms":
                return new JmsSimulationEventClient(connectionFactory, pulseJmsTopicName, pulseJmsDiscriminator);
            case "websocket":
                String jwt = SecurityUtils.getJWTToken(SecurityContextHolder.getContext().getAuthentication(), jwtDecoder);
                return new WebSocketSimulationEventListener(pulseUtils.getPulseWebsocketEndpoint(), jwt);
            default:
                throw new IllegalStateException(String.format("Unsupported Pulse Listener Implementation '%s'", listenerImplementation));
        }
    }

    private synchronized SimulationEventClient getEventListener() {
        log.trace("getEventListener() invoked ");
        SimulationEventClient listener = this.retryTemplate.execute((context) -> {
            if (this.simulationEventListener == null || !this.simulationEventListener.isConnected()) {
                log.debug("Creating Pulse Data listener. Retry #{}", context.getRetryCount());
                AtomicInteger errors = new AtomicInteger();
                try {
                    this.simulationEventListener = createSimulationEventClient();
                    this.simulationEventListener.connect();
                    this.simulationEventListener.registerExceptionListener((Throwable e) -> {
                        log.error("Transport Error in Pulse Data Connection: {}", e.getMessage());

                        if (PulseEventListenerService.this.simulationEventListener == null) {
                            log.error("The error happened when trying to connect to Pulse");
                            //the error happened when trying to connect to Pulse
                            errors.incrementAndGet();
                        } else {
                            log.error("The error happened while pre-existing listener were connected.");
                            //Send an error indicating the current situation to all the listeners
                            for (Device device : getExistingDevices()) {
                                applicationEventPublisher.publishEvent(new HardwareStatusEvent(device.getHardwareId(), HardwareStatusEvent.Status.ERROR, e.getMessage(), ""));
                            }
                            //the error happened when the connection was already stablished.
                            //Let's try to reconnect.
                            //We need to do the reconnection in another Thread,
                            //otherwise Stomp will always return a Connection Error.
                            log.error("Trying to reconnect");
                            taskExecutor.execute(PulseEventListenerService.this::reconnect);
                        }
                    });
                } catch (Exception ex) {
                    errors.incrementAndGet();
                    log.error("Exception creating Pulse Listener", ex);
                }

                if (errors.get() > 0) {
                    try {
                        this.simulationEventListener.dispose();
                    } catch (Exception e) {
                        //do nothing
                    }
                    this.simulationEventListener = null;

                    //make the retry template to fail
                    throw new IllegalStateException("Exceptions were caught while creating the Data Listener");
                }

            }

            return this.simulationEventListener;
        });
        if (listener == null) {
            throw new IllegalStateException("Pulse Data Listener could not be configured");
        }
        log.trace("SimulationEventClient is {}", listener);
        return listener;
    }

    private void reconnect() {
        if (!pulseEnabled) {
            return;
        }

        this.simulationEventListener = null;
        getEventListener();
        for (Device device : getExistingDevices()) {
            this.registerListener(device.getHardwareId(), device.getModel().getType());
            applicationEventPublisher.publishEvent(new HardwareStatusEvent(device.getHardwareId(), HardwareStatusEvent.Status.CONNECTED, ""));
        }
    }

    @Override
    protected void registerListener(String simulationName, DeviceType deviceType) {
        if (!pulseEnabled) {
            return;
        }

        String subscriptionId = getEventListener().registerDataListener(
            simulationName,
            new PulseSimulationDataEventListener(
                simulationName,
                pulseSimulationService,
                applicationEventPublisher,
                eventsCache
            )
        );
        subscriptionIdBySimulationId.put(simulationName, subscriptionId);
    }

    @Override
    protected void unregisterListener(String simulationName) {
        if (!pulseEnabled) {
            return;
        }

        String subscriptionId = subscriptionIdBySimulationId.remove(simulationName);
        if (subscriptionId == null) {
            log.warn("There is no Subscription for Device '{}'", simulationName);
            return;
        }
        getEventListener().unregisterListener(subscriptionId);
        log.trace("Subscription '{}' for Device '{}' was removed.", subscriptionId, simulationName);
    }

    public boolean isPulseEnabled(){
        return pulseEnabled;
    }

}
