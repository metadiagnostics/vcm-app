/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.gtx;

import com.athenagtx.framing.Frame;
import com.athenagtx.framing.FrameException;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import org.apache.activemq.command.ActiveMQBytesMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;

/**
 * Listener for GTX Device data that is coming from JMS. This class keeps a
 * cache with the information about the devices.
 *
 * @author esteban
 */
public class GtxJmsListener extends GtxAbstractListener {

    private static final Logger log = LoggerFactory.getLogger(GtxJmsListener.class);

    private final ConnectionFactory connectionFactory;
    private final String gtxJmsTopic;

    private Connection connection;

    public GtxJmsListener(
        ApplicationEventPublisher applicationEventPublisher,
        GtxDevicesCache cache,
        ConnectionFactory connectionFactory,
        String gtxJmsTopic) {
        super(applicationEventPublisher, cache);
        this.connectionFactory = connectionFactory;
        this.gtxJmsTopic = gtxJmsTopic;
    }

    @Override
    protected GtxDeviceInfo createGtxDeviceInfo() {
        return new GtxJmsDeviceInfo(gtxJmsTopic);
    }

    public void start() {

        log.debug("Starting GTX JMS Listener");
        try {
            this.connection = connectionFactory.createConnection();
            this.connection.setExceptionListener((JMSException exception) -> {
                log.error("Exception in GTX JMS Listener", exception);
            });
            this.connection.start();
            log.debug("JMS Connection with id '{}' CREATED", this.getConnectionId(this.connection));

            Session jmsSession = this.connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageConsumer jmsConsumer = jmsSession
                .createConsumer(jmsSession.createTopic(gtxJmsTopic));
            jmsConsumer.setMessageListener((m) -> {
                if (!(m instanceof ActiveMQBytesMessage)) {
                    log.warn("Received a message in topic '{}' that was not an instance of ActiveMQBytesMessage. It was a '{}'", gtxJmsTopic, m.getClass().getName());
                    return;
                }

                ActiveMQBytesMessage amqMessage = (ActiveMQBytesMessage) m;
                try {
                    byte[] bytes = new byte[(int) amqMessage.getBodyLength()];
                    amqMessage.readBytes(bytes, bytes.length);
                    this.processFrame(Frame.fromByteArray(bytes));
                } catch (JMSException | FrameException ex) {
                    log.error("Exception deserializing JMS Message from GTX.", ex);
                }
            });

        } catch (JMSException ex) {
            throw new IllegalStateException("Exception creating JMS Connection", ex);
        }
    }

    public void stop() {
        if (connection != null) {
            try {
                connection.close();
            } catch (JMSException ex) {
                log.error("Exception closing JMS Connection.", ex);
            }
        }
    }

    private String getConnectionId(Connection c) {
        try {
            return c.getClientID();
        } catch (JMSException ex) {
            throw new IllegalStateException("Exception getting Id from JMS Connection", ex);
        }
    }

}
