/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext;

import io.saperi.vcm.domain.Device;
import io.saperi.vcm.domain.DeviceModel;
import io.saperi.vcm.event.NewHardwareDetectedEvent;
import io.saperi.vcm.repository.DeviceModelRepository;
import io.saperi.vcm.repository.DeviceRepository;
import io.saperi.vcm.service.DeviceConfiguration;
import io.saperi.virtualhealth.device.aggregators.common.managers.DeviceCapabilityManager;
import io.saperi.virtualhealth.device.aggregators.common.model.DeviceCapabilities;
import io.saperi.virtualhealth.device.aggregators.common.model.DeviceInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sgroh@saperi.io
 */
@Service
@Slf4j
public class NewDeviceListenerService implements ApplicationListener<NewHardwareDetectedEvent> {

    private static DeviceCapabilityManager dc = DeviceCapabilityManager.getDeviceCapabilityManager();

    private static Map<String,String> manufacturer_model;
    private static String THORNHILL_AGGREGATOR = "Thornhill Aggregator"; // This should be part of some enum

    private DeviceRepository deviceRepository;
    private DeviceModelRepository deviceModelRepository;
    private DeviceConfiguration deviceConfiguration;

    static {
        manufacturer_model = new HashMap<>();
        for (String deviceID:  dc.getDeviceIdentifiers()){
            manufacturer_model.put(deviceID, THORNHILL_AGGREGATOR);
        }
    }

    @Autowired
    public NewDeviceListenerService(DeviceRepository deviceRepository,
                                    DeviceModelRepository deviceModelRepository, DeviceConfiguration deviceConfiguration){
        this.deviceRepository = deviceRepository;
        this.deviceModelRepository = deviceModelRepository;
        this.deviceConfiguration = deviceConfiguration;
    }

    // @EventListener not using this annotation because annotations are processed by the EventListenerMethodProcessor
    // which will run as soon as all beans are instantiated and ready. We need to handle this events as soon as possible.
    public void onApplicationEvent(NewHardwareDetectedEvent event){
        DeviceInfo deviceInfo = event.getNewDevice();
        if (Strings.isNotEmpty(deviceInfo.getManufacturer()) && Strings.isNotEmpty(deviceInfo.getSystem_model_number())){
            // Checks here device capabilities
            DeviceCapabilities cap = dc.getDeviceCapabilities(deviceInfo.getManufacturer(), deviceInfo.getSystem_model_number());
            Device device = deviceRepository.findByHardwareId(event.getHardwareId());
            if (device == null && cap!=null) {
                // Register here the new device with it's capabilities
                device = new Device();
                String devModelName = manufacturer_model.get(deviceInfo.getManufacturer()+"-"+deviceInfo.getSystem_model_number());
                DeviceModel deviceModel = deviceModelRepository.findFirstByName(StringUtils.isNotEmpty(devModelName)?devModelName:"");
                if (deviceModel!=null) {
                    device.setModel(deviceModel);
                    device.setHardwareId(event.getHardwareId());
                    deviceRepository.save(device);
                    log.info("\033[0;32mNew device added to VCM\033[0m, patient assignment should be added manually.");
                    // Assignments should be made manually by some user.
                } else {
                    log.warn("New device could not be added to VCM, deviceModel was not found.");
                }
            } else if (device != null && cap!=null) {
                // Update the device capabilities and transient data
                String devModelName = manufacturer_model.get(deviceInfo.getManufacturer()+"-"+deviceInfo.getSystem_model_number());
                DeviceModel deviceModel = deviceModelRepository.findFirstByName(StringUtils.isNotEmpty(devModelName)?devModelName:"");
                if (deviceModel!=null && !deviceModel.equals(device.getModel())) {
                    device.setModel(deviceModel);
                }
                device.setManufacturer(deviceInfo.getManufacturer());
                device.setModelName(deviceInfo.getSystem_model_number());
                deviceRepository.save(device);
                Map<String, Device> deviceConfigMap = deviceConfiguration.getHw_device();
                if (deviceConfigMap == null){
                    deviceConfigMap = new HashMap<>();
                }
                deviceConfigMap.put(event.getHardwareId(), device);
                deviceConfiguration.setHw_device(deviceConfigMap);
                log.info("\033[0;32mDevice Updated correctly\033[0m from Manager id: {}. Manufacturer: {}, Model: {}, HWID: {}",
                    deviceInfo.getManager_id(), deviceInfo.getManufacturer(), deviceInfo.getSystem_model_number(), event.getHardwareId());
            } else {
                log.error("Device without enough information from Manager id: {}, Ice id: {}", deviceInfo.getManager_id(), deviceInfo.getId());
            }
        } else {
            log.warn("Invalid device from Manager id: {}", deviceInfo.getManager_id());
        }
    }
}
