/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.gtx;

import com.athenagtx.framing.HardwareId;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 *
 * @author esteban
 */
public class GtxJmsDeviceInfo extends GtxDeviceInfo {

    private final String topic;

    public GtxJmsDeviceInfo(String topic) {
        this.topic = topic;
    }

    public GtxJmsDeviceInfo(String topic, HardwareId hardwareId, String runId) {
        super(hardwareId, runId);
        this.topic = topic;
    }

    public String getTopic() {
        return topic;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            appendSuper(super.hashCode()).
            append(topic).
            toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof GtxJmsDeviceInfo))
            return false;
        if (obj == this)
            return true;

        GtxJmsDeviceInfo gtxDeviceInfo = (GtxJmsDeviceInfo) obj;
        return new EqualsBuilder().
            appendSuper(super.equals(obj)).
            append(topic, gtxDeviceInfo.topic).
            isEquals();
    }

}
