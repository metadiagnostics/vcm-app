/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.jaimsaggregator;

public class MapData {
    //TODO: Add Transformer support - This may need to be device type specific
    public String unit;
    public int precision;
    public String property;
    public MapDataType metricType;
    public MapTargetType targetType;
    //TODO: Add Null criteria check logic - This needs to be device type specific


    MapData(MapDataType metricType, MapTargetType targetType, String unit, int precision, String property) {
        this.unit = unit;
        this.precision = precision;
        this.property = property;
        this.metricType = metricType;
        this.targetType = targetType;
    }
}
