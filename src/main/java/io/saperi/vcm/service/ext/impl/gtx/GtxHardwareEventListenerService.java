/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.gtx;

import athena.admsandroid.Protobufs.Accs;
import io.saperi.gtx.sample.SocketUtils;
import io.saperi.vcm.domain.enumeration.DeviceType;
import io.saperi.vcm.event.*;
import io.saperi.vcm.service.ext.ExtendedDeviceService;
import io.saperi.vcm.service.ext.HardwareEventListenerServiceAdapter;
import java.net.DatagramSocket;
import java.util.List;
import java.util.concurrent.Executor;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.cache.Cache;
import javax.cache.CacheManager;
import javax.cache.Caching;
import javax.cache.spi.CachingProvider;
import javax.jms.ConnectionFactory;

import io.saperi.vcm.util.CacheUtil;
import org.ehcache.config.CacheConfiguration;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.jsr107.Eh107Configuration;
import org.glassfish.hk2.api.TypeLiteral;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

/**
 * Class that handles activations and deactivations of GTX devices. This class
 * uses a {@link GtxUdpListener} to listen to the actual GTX devices. When data
 * from an active device is received by this class, it is transformed into the
 * corresponding {@link HardwareEvent event/s} and published.
 *
 * @author esteban
 */
@Service
public class GtxHardwareEventListenerService extends HardwareEventListenerServiceAdapter {

    private static final Logger log = LoggerFactory.getLogger(GtxHardwareEventListenerService.class);

    @Value("${vcm.gtx.udp.enabled:false}")
    private boolean udpEnabled;

    @Value("${vcm.gtx.udp.local-port}")
    private Integer gtxUdpLocalPort;

    @Value("${vcm.gtx.jms.enabled:false}")
    private boolean jmsEnabled;

    @Value("${vcm.gtx.jms.data-topic}")
    private String gtxJmsDataTopic;

    private final ApplicationEventPublisher applicationEventPublisher;
    private final Executor executor;
    private final GtxDevicesCache devicesCache;
    private final ConnectionFactory connectionFactory;

    private DatagramSocket socket;

    //TODO: having 2 hardcoded listeners is just a workaround
    private GtxUdpListener udpListener;
    private GtxJmsListener jmsListener;

    public GtxHardwareEventListenerService(
        ApplicationEventPublisher applicationEventPublisher,
        ExtendedDeviceService extendedDeviceService,
        @Qualifier("gtxTaskExecutor") Executor executor,
        GtxDevicesCache devicesCache,
        @Qualifier("amq-jms-factoy") ConnectionFactory connectionFactory
    ) {
        super(extendedDeviceService, applicationEventPublisher);
        this.applicationEventPublisher = applicationEventPublisher;
        this.executor = executor;
        this.devicesCache = devicesCache;
        this.connectionFactory = connectionFactory;
    }

    @PostConstruct
    public void postConstruct() {

        //start UDP listener thread
        if (udpEnabled) {
            //Create the UDP socket
            socket = SocketUtils.createSocket(gtxUdpLocalPort);

            udpListener = new GtxUdpListener(applicationEventPublisher, devicesCache, socket);
            executor.execute(udpListener);
        } else {
            log.info("No UDP Port configured for GTX Devices. No UDP Data Listener will be configured.");
        }

        //start JMS Listener
        if (jmsEnabled) {
            jmsListener = new GtxJmsListener(applicationEventPublisher, devicesCache, connectionFactory, gtxJmsDataTopic);
            jmsListener.start();
        } else {
            log.info("No JMS Data Topic configured for GTX Devices. No JMS Data Listener will be configured.");
        }

        registerExistingDevices();
        super.eventsCache = CacheUtil.eventsCache();
    }

    @PreDestroy
    public void preDestroy() {
        if (udpListener != null) {
            udpListener.stop();
            socket.close();
        }

        if (jmsListener != null) {
            jmsListener.stop();
        }
    }


    @EventListener
    public void onGtxAccsLiveDataEvent(GtxAccsLiveDataEvent event) {
        //TODO: Do we want to send the status every time we receive data?
        applicationEventPublisher.publishEvent(new HardwareStatusEvent(event.getHardwareId(), HardwareStatusEvent.Status.CONNECTED, event.getHardwareId()));
        applicationEventPublisher.publishEvent(createHardwareVitalsEvent(event.getHardwareId(), event.getData()));
        applicationEventPublisher.publishEvent(createHardwareVentilatorEvent(event.getHardwareId(), event.getData()));
    }

    @EventListener
    public void onGtxAccsRESTDataEvent(GtxAccsLiveAllDataDTO event) {
        // Updating the status if we receive REST data
        if (event.getHardwareId() != null) {
            applicationEventPublisher.publishEvent(new HardwareStatusEvent(event.getHardwareId(), HardwareStatusEvent.Status.CONNECTED, event.getHardwareId()));
        }
        // Updates the cache with the Hardware Vitals already merged from the controller.
        if (event.getHardwareVitalsEvent() != null) {
            applicationEventPublisher.publishEvent(event.getHardwareVitalsEvent());
        }
        // Updates the cache with the Hardware Ventilator already merged from the controller.
        if (event.getHardwareVentilatorEvent() != null) {
            applicationEventPublisher.publishEvent(event.getHardwareVentilatorEvent());
        }
    }

    @EventListener
    public void onGtxAccsRESTDataEvent(GtxAccsLiveVitalsDataDTO event) {
        // Updating the status if we receive REST data
        if (event.getHardwareId() != null) {
            applicationEventPublisher.publishEvent(new HardwareStatusEvent(event.getHardwareId(), HardwareStatusEvent.Status.CONNECTED, event.getHardwareId()));
        }
        // Updates the cache with the Hardware Vitals already merged from the controller.
        if (event.getHardwareVitalsEvent() != null) {
            applicationEventPublisher.publishEvent(event.getHardwareVitalsEvent());
        }
    }

    @EventListener
    public void onGtxAccsRESTDataEvent(GtxAccsLiveVentilatorDataDTO event) {
        // Updating the status if we receive REST data
        if (event.getHardwareId() != null) {
            applicationEventPublisher.publishEvent(new HardwareStatusEvent(event.getHardwareId(), HardwareStatusEvent.Status.CONNECTED, event.getHardwareId()));
        }
        // Updates the cache with the Hardware Ventilator already merged from the controller.
        if (event.getHardwareVentilatorEvent() != null) {
            applicationEventPublisher.publishEvent(event.getHardwareVentilatorEvent());
        }
    }

    @Override
    protected DeviceType[] getTypes() {
        return new DeviceType[] {DeviceType.GTX};
    }

    @Override
    protected void registerListener(String hardwareId, DeviceType deviceType) {
        devicesCache.addDeviceOfInterest(hardwareId);
    }

    @Override
    protected void unregisterListener(String hardwareId) {
        devicesCache.removeDeviceOfInterest(hardwareId);
    }

    @Override
    protected void internalHardwareListenerActivationRequestEvent(HardwareListenerActivationRequestEvent request) {
    }

    @Override
    protected void internalOnHardwareListenerDeactivationRequestEvent(HardwareListenerDeactivationRequestEvent request) {
    }


    private HardwareVitalsEvent createHardwareVitalsEvent(String hardwareId, Accs.AccsLiveData data) {
        HardwareVitalsEvent event = new HardwareVitalsEvent(hardwareId, hardwareId);
        event.setDataTimestamp(System.currentTimeMillis());
        event.setRate(new ScalarValue(data.getVentControl().getRr(), "1/min", 0));
        event.setSat(new ScalarValue(data.getVitals().getSpo2().getSpo2(), "%", 0));
        event.setHr(new ScalarValue(data.getVitals().getNibp().getHeartRate(), "1/min", 0));
        event.setSbp(new ScalarValue(data.getVitals().getNibp().getSystolicBp(), "mmHg", 0));
        event.setDbp(new ScalarValue(data.getVitals().getNibp().getDiastolicBp(), "mmHg", 0));
        event.setCvp(null);
        event.setTv(new ScalarValue(((double) data.getVentControl().getTv()) / 1000.0, "L", 1));
        event.setEtco2(new ScalarValue(data.getVitals().getEtco2().getEtco2MmHg(), "mmHg", 0));
        event.setWeight(null); // No weight information on GTX device
        event.setTemp(new ScalarValue(data.getVitals().getTemperature().getTemperatureCh1(), "C", 0));
        return event;
    }

    private HardwareVentilatorEvent createHardwareVentilatorEvent(String hardwareId, Accs.AccsLiveData data) {
        HardwareVentilatorEvent event = new HardwareVentilatorEvent(hardwareId, hardwareId);
        event.setDataTimestamp(System.currentTimeMillis());
        event.setPip(new ScalarValue(data.getVentControl().getPip(), "cmH2O", 0));
        event.setPeep(new ScalarValue(data.getVentControl().getPeep(), "cmH2O", 0));
        event.setRate(new ScalarValue(data.getVentControl().getRr(), "1/min", 0));
        event.setIt(null);
        event.setFio2(new ScalarValue(data.getVitals().getEtco2().getFio2Percent(), "%", 0));
        event.setFlow(null);
        event.setPvLoop(null);
        event.setFvLoop(null);
        event.setIp(null); // No IntrapleuralPressure information on GTX device
        event.setEf(null); // No ExpiratoryFlow information on GTX device
        event.setTv(new ScalarValue(((double) data.getVentControl().getTv()) / 1000.0, "L", 2));
        event.putInCache(eventsCache);
        return event;
    }

}
