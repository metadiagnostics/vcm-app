/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.gtx;

import java.time.Duration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.cache.Cache;
import javax.cache.CacheManager;
import javax.cache.configuration.Configuration;
import org.ehcache.config.CacheConfiguration;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.stereotype.Component;

/**
 *
 * @author esteban
 */
@Component
public class GtxDevicesCache {

    public static final String CACHE_NAME = "GTX Devices";

    private CacheManager cacheManager;
    private Cache<String, SetGtxDeviceInfo> devicesCache;
    private final List<String> deviceshardwaresOfInterest = new CopyOnWriteArrayList<>();

    public GtxDevicesCache(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
        this.createCache();
    }

    private void createCache() {
        devicesCache = cacheManager.getCache(CACHE_NAME);
        if (devicesCache != null) {
            if (devicesCache.isClosed()) {
                cacheManager.destroyCache(CACHE_NAME);
            } else {
                return;
            }
        }
        CacheConfiguration<String, SetGtxDeviceInfo> config = CacheConfigurationBuilder.newCacheConfigurationBuilder(
            String.class,
            SetGtxDeviceInfo.class,
            ResourcePoolsBuilder.heap(1000))
            .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofMinutes(10)))
            .build();
        Configuration<String, SetGtxDeviceInfo> jCacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(config);

        devicesCache = cacheManager.createCache(CACHE_NAME, jCacheConfiguration);
    }

    public SetGtxDeviceInfo getInfo(String hardwareId) {
        return devicesCache.get(hardwareId);
    }

    public GtxDeviceInfo getInfo(String hardwareId, Class gtxDeviceInfoClass) {
        SetGtxDeviceInfo setDeviceInfo = devicesCache.get(hardwareId);
        if (setDeviceInfo!=null) {
            return setDeviceInfo.getGtxDevice(gtxDeviceInfoClass);
        }
        return null;
    }

    public void addDevice(String hardwareId, GtxDeviceInfo device) {
        SetGtxDeviceInfo currentSetGtxDeviceInfo = getInfo(hardwareId);
        if ( currentSetGtxDeviceInfo==null ) {
            currentSetGtxDeviceInfo = new SetGtxDeviceInfo(device);
        } else {
            currentSetGtxDeviceInfo.addGtxDevice(device);
        }
        this.devicesCache.put(hardwareId, currentSetGtxDeviceInfo);
    }

    public void addDeviceOfInterest(String hardwareId) {
        deviceshardwaresOfInterest.add(hardwareId);
    }

    public void removeDeviceOfInterest(String hardwareId) {
        deviceshardwaresOfInterest.remove(hardwareId);
    }

    public boolean isDeviceOfInterest(String hardwareId) {
        return deviceshardwaresOfInterest.contains(hardwareId);
    }
}
