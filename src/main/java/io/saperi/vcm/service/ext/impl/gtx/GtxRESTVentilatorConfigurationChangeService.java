/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.gtx;

import athena.admsandroid.Protobufs.Accs;
import com.athenagtx.framing.Frame;
import com.athenagtx.framing.HardwareId;
import io.saperi.gtx.sample.SocketUtils;
import io.saperi.vcm.event.GtxAccsLiveVentilatorDataDTO;
import io.saperi.vcm.event.VentilatorConfigurationChangeRequestEvent;
import io.saperi.vcm.service.ext.ExtendedAssignmentService;
import io.saperi.vcm.service.ext.impl.DevicesDataCache;
import io.saperi.vcm.service.ext.recommender.VentilatorConfigurationChangeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.net.ConnectException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Collections;

/**
 * A DocBox Integration Service to invoke an URL to send change Ventilator Control parameters.
 *
 * @author sgroh@sapery.io
 */
@Service
public class GtxRESTVentilatorConfigurationChangeService extends GtxAbstractVentilatorConfigurationChangeService implements VentilatorConfigurationChangeService {

    private static final Logger log = LoggerFactory.getLogger(GtxRESTVentilatorConfigurationChangeService.class);

    @Value("${vcm.gtx.docbox.enabled:false}")
    private boolean enabled;

    @Value("${vcm.gtx.docbox.url:http://localhost:4444}")
    private String docboxUrl;

    private final RestOperations restOperations;

    public GtxRESTVentilatorConfigurationChangeService(
        ApplicationEventPublisher applicationEventPublisher,
        ExtendedAssignmentService extendedAssignmentService,
        GtxDevicesCache devicesCache,
        DevicesDataCache devicesDataCache,
        RestOperations restOperations) {
        super(applicationEventPublisher, extendedAssignmentService, devicesCache, devicesDataCache, "REST Ventilator Configuration Change Service");
        this.restOperations = restOperations;
    }

    @PostConstruct
    public void postConstruct() {
        if (!enabled) {
            log.debug("REST Is not enabled for GTX Devices. No Ventilator Configuration Change Service from REST will be configured.");
            return;
        }
    }

    @PreDestroy
    public void preDestroy() {
        restOperations.delete(docboxUrl);
    }


    @Override
    public Class preSetupDeviceInfo(VentilatorConfigurationChangeRequestEvent request) {
        if (enabled) {
            GtxRestDeviceInfo deviceInfo = new GtxRestDeviceInfo(
                new HardwareId(getHwIdFromString(request.getDevice().getHardwareId())),
                "restId");
            devicesCache.addDevice(request.getDevice().getHardwareId(), deviceInfo);
        }
        return GtxRestDeviceInfo.class;
    }

    public int getHwIdFromString(String shwId){
        if ( shwId==null ) {
            return 0;
        }
        String[] parts = shwId.split("0x");
        if ( parts.length <2 ) {
            return 0;
        }
        return Integer.parseInt(parts[1].substring(0, parts[1].length()-1),16);
    }



    @Override
    public boolean changeVentilator(GtxDeviceInfo info, Frame frame, VentilatorConfigurationChangeRequestEvent req) throws IOException {
        if (!enabled || req==null) {
            return false;
        }
        if (!(info instanceof GtxRestDeviceInfo)) {
            log.trace("The message was not of type GtxRestDeviceInfo. I'm not interested in it.");
            return false;
        }
        return changeVentilatorFromRestCall(GtxAccsLiveVentilatorDataDTO.from(req));
    }

    private boolean changeVentilatorFromRestCall(final GtxAccsLiveVentilatorDataDTO ventilatorDataDTO) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        //headers.set("x-api-key", apiKey);
        HttpEntity<GtxAccsLiveVentilatorDataDTO> entity = new HttpEntity<>(ventilatorDataDTO, headers);

        log.debug("Sending ventilator control Data {} to {}", ventilatorDataDTO, docboxUrl);
        try {
            ResponseEntity<String> httpResponse = restOperations.exchange(docboxUrl, HttpMethod.POST, entity, String.class);

            if (!httpResponse.getStatusCode().is2xxSuccessful()) {
                log.error("Was not possible to send Ventilator Control values");
                return false;
            }
            log.debug("Ventilator control Data {} sent successfuly to {}, Response: {}", ventilatorDataDTO, docboxUrl, httpResponse.getBody());
            return true;
        } catch (RestClientException e) {
            throw new RuntimeException("Rest endpoint " +docboxUrl+" not reachable", e);
        }
    }

}
