/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl;

import static java.util.stream.Collectors.toList;

import io.micrometer.core.instrument.util.StringUtils;
import io.saperi.vcm.domain.PersistentAuditEvent;
import io.saperi.vcm.repository.ext.ExtendedPersistenceAuditEventRepository;
import io.saperi.vcm.service.ext.VentilatorAuditEventService;
import io.saperi.vcm.service.ext.recommender.VentilatorRecommendationToAuditEventConverter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author esteban
 */
@Service
@Transactional
public class VentilatorAuditEventServiceImpl implements VentilatorAuditEventService {

    private final Logger log = LoggerFactory.getLogger(VentilatorAuditEventServiceImpl.class);

    public final String[] VENTILATOR_AUDIT_EVENT_TYPES = new String[]{
        VentilatorRecommendationToAuditEventConverter.EVENT_TYPE,
        VentilatorConfigurationChangeRequestToAuditEventConverter.EVENT_TYPE,
        VentilatorAckFio2toAuditEventConverter.EVENT_TYPE,
        VentilatorChangeModeAuditEventConverter.EVENT_TYPE,
        RecommenderControlTypetoAuditEventConverter.EVENT_TYPE
    };

    private final ExtendedPersistenceAuditEventRepository repository;

    public VentilatorAuditEventServiceImpl(ExtendedPersistenceAuditEventRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<PersistentAuditEvent> getVentilatorAuditEvents(long assignmentId) {
        return repository.findByDataKeyAndDataValue(VENTILATOR_AUDIT_EVENT_TYPES, VentilatorRecommendationToAuditEventConverter.DATA_ASSIGNMENT_ID_KEY, assignmentId + "");

    }

    @Override
    public List<PersistentAuditEvent> getVentilatorAuditEvents(long assignmentId, Date from, Date to) {
        return repository.findByDataKeyAndDataValueWithDateRange(VENTILATOR_AUDIT_EVENT_TYPES, from.toInstant(), to.toInstant(), VentilatorRecommendationToAuditEventConverter.DATA_ASSIGNMENT_ID_KEY, assignmentId + "");
    }

    @Override
    public String getVentilatorAuditEventsAsCSV(long assignmentId) throws IOException {
        List<PersistentAuditEvent> events = getVentilatorAuditEvents(assignmentId);
        return toCSV(events);
    }

    @Override
    public String getVentilatorAuditEventsAsCSV(long assignmentId, Date from, Date to) throws IOException {
        List<PersistentAuditEvent> events = getVentilatorAuditEvents(assignmentId, from, to);
        return toCSV(events);
    }

    private String toCSV(List<PersistentAuditEvent> events) throws IOException {
        String[] commonHeaders = createCSVCommonHeaders();
        String[] dataHeaders = createCSVDataHeaders();

        List<String> headers = new ArrayList<>();
        headers.addAll(Arrays.asList(commonHeaders));
        headers.addAll(Arrays.asList(dataHeaders));

        StringWriter buffer = new StringWriter();
        Appendable out = new BufferedWriter(buffer);
        CSVPrinter printer = CSVFormat.DEFAULT.withHeader(headers.toArray(new String[headers.size()])).print(out);

        for (PersistentAuditEvent event : events) {
            List<Object> data = new ArrayList<>();
            data.add(event.getAuditEventDate());
            data.add(event.getAuditEventType());
            data.add(event.getId());
            data.add(event.getPrincipal());
            data.addAll(Arrays.stream(dataHeaders).map(e -> {
                String v = event.getData().get(e);
                return StringUtils.isEmpty(v) ? "" : v;
            }).collect(toList()));

            printer.printRecord(data);
        }

        printer.flush();
        return buffer.getBuffer().toString();
    }

    private String[] createCSVCommonHeaders() {
        return new String[]{"date", "type", "id", "user"};
    }

    private String[] createCSVDataHeaders() {
        Set<String> headers = new LinkedHashSet<>();

        //VentilatorChangeModeAuditEventConverter
        headers.addAll(Arrays.asList(
            VentilatorChangeModeAuditEventConverter.DATA_ASSIGNMENT_ID_KEY,
            VentilatorChangeModeAuditEventConverter.DATA_DEVICE_HW_ID_KEY,
            VentilatorChangeModeAuditEventConverter.DATA_DEVICE_MODEL_KEY,
            VentilatorChangeModeAuditEventConverter.DATA_VENT_MODE_KEY
        ));

        //RecommenderControlTypetoAuditEventConverter
        headers.addAll(Arrays.asList(
            RecommenderControlTypetoAuditEventConverter.DATA_CONTROL_TYPE_KEY));

        //VentilatorRecommendationToAuditEventConverter
        headers.addAll(Arrays.asList(
            VentilatorRecommendationToAuditEventConverter.DATA_ASSIGNMENT_ID_KEY,
            VentilatorRecommendationToAuditEventConverter.DATA_REQ_ETCO2_KEY,
            VentilatorRecommendationToAuditEventConverter.DATA_REQ_FIO2_KEY,
            VentilatorRecommendationToAuditEventConverter.DATA_REQ_GENDER_KEY,
            VentilatorRecommendationToAuditEventConverter.DATA_REQ_HEIGHT_KEY,
            VentilatorRecommendationToAuditEventConverter.DATA_REQ_O2SAT_KEY,
            VentilatorRecommendationToAuditEventConverter.DATA_REQ_PEEP_KEY,
            VentilatorRecommendationToAuditEventConverter.DATA_REQ_PH_KEY,
            VentilatorRecommendationToAuditEventConverter.DATA_REQ_PIP_KEY,
            VentilatorRecommendationToAuditEventConverter.DATA_REQ_RATE_KEY,
            VentilatorRecommendationToAuditEventConverter.DATA_REQ_TIME_KEY,
            VentilatorRecommendationToAuditEventConverter.DATA_REQ_TV_KEY,
            VentilatorRecommendationToAuditEventConverter.DATA_RESP_FIO2_KEY,
            VentilatorRecommendationToAuditEventConverter.DATA_RESP_PEEP_KEY,
            VentilatorRecommendationToAuditEventConverter.DATA_RESP_PIP_KEY,
            VentilatorRecommendationToAuditEventConverter.DATA_RESP_TV_KEY,
            VentilatorRecommendationToAuditEventConverter.DATA_RESP_RATE_KEY,
            VentilatorRecommendationToAuditEventConverter.DATA_RESP_REASON_KEY
        ));

        //VentilatorConfigurationChangeRequestToAuditEventConverter
        headers.addAll(Arrays.asList(
            VentilatorConfigurationChangeRequestToAuditEventConverter.DATA_ASSIGNMENT_ID_KEY,
            VentilatorConfigurationChangeRequestToAuditEventConverter.DATA_DEVICE_HW_ID_KEY,
            VentilatorConfigurationChangeRequestToAuditEventConverter.DATA_DEVICE_MODEL_KEY,
            VentilatorConfigurationChangeRequestToAuditEventConverter.DATA_FIO2_KEY,
            VentilatorConfigurationChangeRequestToAuditEventConverter.DATA_IE_KEY,
            VentilatorConfigurationChangeRequestToAuditEventConverter.DATA_PEEP_KEY,
            VentilatorConfigurationChangeRequestToAuditEventConverter.DATA_PIP_KEY,
            VentilatorConfigurationChangeRequestToAuditEventConverter.DATA_TV_KEY,
            VentilatorConfigurationChangeRequestToAuditEventConverter.DATA_RATE_KEY
        ));

        return headers.toArray(new String[headers.size()]);
    }
}
