/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.gtx;

import com.athenagtx.framing.Frame;
import io.saperi.gtx.sample.SocketUtils;
import io.saperi.vcm.event.VentilatorConfigurationChangeRequestEvent;
import io.saperi.vcm.service.ext.ExtendedAssignmentService;
import io.saperi.vcm.service.ext.impl.DevicesDataCache;
import io.saperi.vcm.service.ext.recommender.VentilatorConfigurationChangeService;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

/**
 *
 * @author esteban
 */
@Service
public class GtxUdpVentilatorConfigurationChangeService extends GtxAbstractVentilatorConfigurationChangeService implements VentilatorConfigurationChangeService {

    private static final Logger log = LoggerFactory.getLogger(GtxUdpVentilatorConfigurationChangeService.class);

    @Value("${vcm.gtx.udp.enabled:false}")
    private boolean enabled;

    @Value("${vcm.gtx.udp.remote-port}")
    private int gtxRemotePort;

    @Value("${vcm.gtx.udp.send-using-broadcast:false}")
    private boolean sendUsingBroadcast;

    private DatagramSocket socket;

    public GtxUdpVentilatorConfigurationChangeService(
        ApplicationEventPublisher applicationEventPublisher,
        ExtendedAssignmentService extendedAssignmentService,
        GtxDevicesCache devicesCache,
        DevicesDataCache devicesDataCache
    ) {
        super(applicationEventPublisher, extendedAssignmentService, devicesCache, devicesDataCache, "UDP Ventilator Configuration Change Service");
    }

    @PostConstruct
    public void postConstruct() {
        if (!enabled) {
            log.debug("UDP Is not enabled for GTX Devices. No Ventilator Configuration Change Sevice for UDP devices will be configured.");
            return;
        }
        //Create the UDP socket to send data
        socket = SocketUtils.createSocket();
    }

    @PreDestroy
    public void preDestroy() {
        if (socket != null) {
            socket.close();
        }
    }

    @Override
    public boolean changeVentilator(GtxDeviceInfo info, Frame frame, VentilatorConfigurationChangeRequestEvent req) throws IOException {
        if (!enabled) {
            return false;
        }
        if (!(info instanceof GtxUdpDeviceInfo)) {
            log.trace("The message was not of type GtxUdpDeviceInfo. I'm not interested in it.");
            return false;
        }

        InetAddress targetAddress = ((GtxUdpDeviceInfo) info).getAddress();
        if (sendUsingBroadcast) {
            targetAddress = InetAddress.getByName("255.255.255.255");
        }
        byte[] data = frame.toByteArray();
        socket.send(new DatagramPacket(data, 0, data.length, targetAddress, gtxRemotePort));
        log.debug("Ventilator Change Message sent to Device {} at address {}", info.getHardwareId(), targetAddress);
        return true;
    }

}
