/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.recommender.impl;

import static io.saperi.vcm.constant.ExtendedErrorConstants.ERR_VENTILATOR_RECOMMENDER_MISSING_PARAMETER_ERROR;

import io.saperi.vcm.domain.AssignedDevice;
import io.saperi.vcm.domain.Assignment;
import io.saperi.vcm.domain.enumeration.VentilatorControlType;
import io.saperi.vcm.domain.ext.VentilatorRecommendationRequest;
import io.saperi.vcm.domain.ext.VentilatorRecommendationResponse;
import io.saperi.vcm.event.ScalarValue;
import io.saperi.vcm.service.ext.errors.BackendException;
import io.saperi.vcm.service.ext.impl.DevicesDataCache;
import io.saperi.vcm.service.ext.recommender.VentilatorRecommenderService;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class with common logic for Cloud Ventilator Recommender Services
 *
 * @author esteban
 */
public abstract class BaseCloudVentilatorRecommenderService implements VentilatorRecommenderService {

    private static final Logger log = LoggerFactory.getLogger(BaseCloudVentilatorRecommenderService.class);

    private final DevicesDataCache dataCache;

    public BaseCloudVentilatorRecommenderService(DevicesDataCache dataCache) {
        this.dataCache = dataCache;
    }

    private DecimalFormat df = new DecimalFormat("#.##");

    protected NumberFormat nf;

    @Override
    public VentilatorRecommendationResponse createRecommendation(Assignment assignment, String requester) {
        VentilatorRecommendationRequest request = this.createRequest(assignment, requester);
        if (request == null) {
            return null;
        }
        return createRecommendation(assignment, request);
    }

    protected VentilatorRecommendationRequest createRequest(Assignment assignment, String requester) {
        VentilatorRecommendationRequest request = new VentilatorRecommendationRequest();
        request.setRequester(requester);

        for (AssignedDevice device : assignment.getDevices()) {
            if (null != device.getCapability().getType()) {
                switch (device.getCapability().getType()) {
                    case DEMOGRAPHICS: {
                        DevicesDataCache.DeviceData d = dataCache.getData(device.getDevice().getHardwareId());
                        if (d != null && d.getLastDemographics() != null) {
                            request.setHeight(d.getLastDemographics().getHeight());
                            request.setGender(d.getLastDemographics().getGender());
                        } else {
                            log.warn("There is no Demographic Data for Device '{}' in oder to request a Ventilator Recommendation", device.getDevice().getHardwareId());
                            return null;
                        }
                        break;
                    }
                    case VENTILATOR: {
                        DevicesDataCache.DeviceData d = dataCache.getData(device.getDevice().getHardwareId());
                        if (d != null && d.getLastVentilatorData() != null) {
                            request.setPeep(d.getLastVentilatorData().getPeep());
                            request.setRate(d.getLastVentilatorData().getRate());
                            request.setFio2(d.getLastVentilatorData().getFio2());
                            // If the TV was not present on ventilator data will use the value set on vitals data
                            if (d.getLastVentilatorData().getTv()!=null) {
                                request.setTidalVolume(d.getLastVentilatorData().getTv());
                            }
                            request.setPip(d.getLastVentilatorData().getPip());
                        } else {
                            log.warn("There is no Ventilator Data for Device '{}' in oder to request a Ventilator Recommendation", device.getDevice().getHardwareId());
                            return null;
                        }
                        break;
                    }
                    case VITALS: {
                        DevicesDataCache.DeviceData d = dataCache.getData(device.getDevice().getHardwareId());
                        if (d != null && d.getLastVitals() != null) {
                            request.setO2sat(d.getLastVitals().getSat());
                            request.setTidalVolume(d.getLastVitals().getTv());
                            request.setEtco2(d.getLastVitals().getEtco2());
                        } else {
                            log.warn("There is no Vitals Data for Device '{}' in oder to request a Ventilator Recommendation", device.getDevice().getHardwareId());
                            return null;
                        }
                        break;
                    }
                    case BLOOD_GAS_ANALYZER: {
                        DevicesDataCache.DeviceData d = dataCache.getData(device.getDevice().getHardwareId());
                        if (d != null && d.getLastBloodGases() != null) {
                            request.setPh(d.getLastBloodGases().getPh());
                        } else {
                            log.warn("There is no Blood Gas Data for Device '{}' in oder to request a Ventilator Recommendation", device.getDevice().getHardwareId());
                            return null;
                        }
                        break;
                    }
                    default:
                        break;
                }
            }
        }

        return request;
    }

    protected double getDoubleValue(ScalarValue value, String propertyName) {
        if (value == null || value.getValue() == null) {
            log.error("Cannot invoke the Ventilator Recommender. Property '{}' is not set.", propertyName);
            throw new BackendException("", ERR_VENTILATOR_RECOMMENDER_MISSING_PARAMETER_ERROR, new BackendException.Param("param", propertyName));
        }
        // Formatting to 2 decimal values if it has
        return Double.valueOf(df.format(value.getValue()));
    }

    protected String getDoubleValueAsString(ScalarValue value, String propertyName) {
        return nf.format(getDoubleValue(value, propertyName));
    }

    protected String getStringValue(String value, String propertyName) {
        if (value == null || value.isBlank()) {
            log.error("Cannot invoke the Ventilator Recommender. Property '{}' is not set.", propertyName);
            throw new BackendException("", ERR_VENTILATOR_RECOMMENDER_MISSING_PARAMETER_ERROR, new BackendException.Param("param", propertyName));
        }

        return value;
    }

}
