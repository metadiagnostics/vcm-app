/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.pulse;

import com.kitware.physiology.datamodel.patient.actions.SEConsciousRespiration;
import com.kitware.physiology.datamodel.patient.actions.SEForcedExhale;
import com.kitware.physiology.datamodel.patient.actions.SEForcedInhale;
import com.kitware.physiology.datamodel.properties.CommonUnits;
import io.saperi.pulse.sim.api.model.PulseSimulation;
import io.saperi.vcm.event.HardwareStatusEvent;

/**
 *
 * @author esteban
 */
public class PulseUtils {

    public static HardwareStatusEvent toHardwareStatusEvent(String simulationName, PulseSimulation.State simState) {
        switch (simState) {
            case NON_EXISTING:
                return new HardwareStatusEvent(
                    simulationName,
                    HardwareStatusEvent.Status.WARNING,
                    "There is no Simulation with the given name.", simState);
            case INITIALIZED:
                return new HardwareStatusEvent(
                    simulationName,
                    HardwareStatusEvent.Status.WARNING,
                    "The simulation is initialized but paused.", simState);
            case RUNNING:
                return new HardwareStatusEvent(
                    simulationName,
                    HardwareStatusEvent.Status.CONNECTED,
                    simState);
            case STOPPED:
                return new HardwareStatusEvent(
                    simulationName,
                    HardwareStatusEvent.Status.WARNING,
                    "The simulation is stopped.",
                    simState);
            case TERMINATED:
                return new HardwareStatusEvent(
                    simulationName,
                    HardwareStatusEvent.Status.ERROR,
                    "The simulation is terminated.",
                    simState);
            case PATIENT_IRREVERSIBLE_STATE:
                return new HardwareStatusEvent(
                    simulationName,
                    HardwareStatusEvent.Status.ERROR,
                    "The simulation's patient is in irreversible state.",
                    simState);
            default:
                throw new IllegalArgumentException(String.format("Unssuported Simulation State '%s'", simState));
        }
    }

    public static SEConsciousRespiration createConsciousRespirationAction(double exhalePeriod, double releasePeriod1, double inhalePeriod, double releasePeriod2) {
        SEForcedExhale forcedExhaleCmd = new SEForcedExhale();
        forcedExhaleCmd.getExpiratoryReserveVolumeFraction().setValue(1.0);
        forcedExhaleCmd.getExhalePeriod().setValue(exhalePeriod, CommonUnits.TimeUnit.s);
        forcedExhaleCmd.getReleasePeriod().setValue(releasePeriod1, CommonUnits.TimeUnit.s);
        forcedExhaleCmd.getHoldPeriod().setValue(0, CommonUnits.TimeUnit.s);

        SEForcedInhale forcedInhaleCmd = new SEForcedInhale();
        forcedInhaleCmd.getInspiratoryCapacityFraction().setValue(1.0);
        forcedInhaleCmd.getInhalePeriod().setValue(inhalePeriod, CommonUnits.TimeUnit.s);
        forcedInhaleCmd.getReleasePeriod().setValue(releasePeriod2, CommonUnits.TimeUnit.s);
        forcedInhaleCmd.getHoldPeriod().setValue(0, CommonUnits.TimeUnit.s);

        SEConsciousRespiration action = new SEConsciousRespiration();
        action.getCommands().add(forcedExhaleCmd);
        action.getCommands().add(forcedInhaleCmd);

        return action;
    }
}
