/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.gtx;

import static io.saperi.vcm.constant.ExtendedErrorConstants.ERR_VENTILATOR_CHANGES_ERROR;
import static io.saperi.vcm.constant.MessageConstants.MSG_VENTILATOR_CHANGES_APPLIED;

import athena.admsandroid.Protobufs.Accs;
import com.athenagtx.framing.Frame;
import com.athenagtx.framing.HardwareId;
import com.google.protobuf.Message;
import io.saperi.gtx.sample.ACCSUtils;
import io.saperi.vcm.domain.Assignment;
import io.saperi.vcm.domain.Device;
import io.saperi.vcm.domain.enumeration.DeviceCapabilityType;
import io.saperi.vcm.domain.enumeration.DeviceType;
import io.saperi.vcm.domain.ext.util.AssignmentUtils;
import io.saperi.vcm.event.HardwareMessageEvent;
import io.saperi.vcm.event.HardwareVitalsEvent;
import io.saperi.vcm.event.ScalarValue;
import io.saperi.vcm.event.VentilatorConfigurationChangeRequestEvent;
import io.saperi.vcm.service.ext.ExtendedAssignmentService;
import io.saperi.vcm.service.ext.impl.DevicesDataCache;
import io.saperi.vcm.service.ext.recommender.VentilatorConfigurationChangeService;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;

/**
 *
 * @author esteban
 */
public abstract class GtxAbstractVentilatorConfigurationChangeService implements VentilatorConfigurationChangeService {

    private static final Logger log = LoggerFactory.getLogger(GtxAbstractVentilatorConfigurationChangeService.class);

    private final ApplicationEventPublisher applicationEventPublisher;
    private final ExtendedAssignmentService extendedAssignmentService;
    protected final GtxDevicesCache devicesCache;
    private final DevicesDataCache devicesDataCache;
    private final AtomicInteger dsn = new AtomicInteger();
    private final String SERVICE_NAME;

    public GtxAbstractVentilatorConfigurationChangeService(
        ApplicationEventPublisher applicationEventPublisher,
        ExtendedAssignmentService extendedAssignmentService,
        GtxDevicesCache devicesCache,
        DevicesDataCache devicesDataCache,
        String serviceName
    ) {
        this.applicationEventPublisher = applicationEventPublisher;
        this.extendedAssignmentService = extendedAssignmentService;
        this.devicesCache = devicesCache;
        this.devicesDataCache = devicesDataCache;
        this.SERVICE_NAME = serviceName;
    }

    public Class preSetupDeviceInfo(VentilatorConfigurationChangeRequestEvent request) {
        return GtxDeviceInfo.class;
    }

    @Override
    public void changeVentilator(VentilatorConfigurationChangeRequestEvent request) {

        if (request.getDevice().getModel().getType() != DeviceType.GTX) {
            //not ours
            return;
        }
        Class gtxDeviceInfoClass = preSetupDeviceInfo(request);
        SetGtxDeviceInfo devicesInfo = devicesCache.getInfo(request.getDevice().getHardwareId());
        if (devicesInfo!=null && !devicesInfo.getAllDevices().isEmpty()) {
            for (GtxDeviceInfo info: devicesInfo.getAllDevices()) {
                //Iterate for all the deviceInfoSources
                if (info == null) {
                    log.warn("We cannot send a command to GTX Device '{}' beacuse we don't have enough information about the device.", request.getDevice().getHardwareId());
                    return;
                }
                if (info.getRunId() == null) {
                    log.warn("A package with Run Id null can not be processed");
                    continue;
                }
                try {
                    if (changeVentilator(info, toGTXFrame(info.getHardwareId(), info.getRunId(), dsn.incrementAndGet(), Accs.AccsResources.VENTILATOR_CONTROL_VALUE, toGTXCommand(request)),request)) {
                        applicationEventPublisher.publishEvent(new HardwareMessageEvent(request.getDevice(), MSG_VENTILATOR_CHANGES_APPLIED));
                    }
                } catch (Exception ex) {
                    log.error(String.format("Exception sending command to GTX device '%s'.", request.getDevice().getHardwareId()), ex);
                    //Notify about the error
                    applicationEventPublisher.publishEvent(new HardwareMessageEvent(request.getDevice(), HardwareMessageEvent.Severity.ERROR, ERR_VENTILATOR_CHANGES_ERROR)
                        .addParameter("msg", ex.getMessage())
                    );
                }
            }
        } else {
            log.warn("Not enough information in cache to change the ventilator using ({})", SERVICE_NAME);
        }

    }

    public abstract boolean changeVentilator(GtxDeviceInfo info, Frame frame, VentilatorConfigurationChangeRequestEvent request) throws Exception;

    private Accs.AccsVentilatorControl toGTXCommand(VentilatorConfigurationChangeRequestEvent request) {


        //TODO: check unit conversion
        //intialize the content object
        int tv;
        try {
          tv = (int) (request.getChanges().getTv().getValue().doubleValue() * 1000.0);
        } catch (Exception e){
          tv = (int) (getTidalVolume(request) * 1000.0);
        }
        Accs.AccsVentilatorControl avc = Accs.AccsVentilatorControl.newBuilder()
            .setPowerState(true)
            .setPeep(request.getChanges().getPeep().getValue().intValue())
            .setPip(request.getChanges().getPip().getValue().intValue())
            .setRr(request.getChanges().getRate().getValue().intValue())
            .setTv(tv)
            .build();

        return avc;
    }

    private Frame toGTXFrame(HardwareId hardwareId, String runId, int dsn, int resourceValue, Message message) {
        //create the frame
        return ACCSUtils.createWriterRequestFrame(hardwareId, runId, dsn, resourceValue, message);
    }

    private double getTidalVolume(VentilatorConfigurationChangeRequestEvent request) {
        //Get the assignment associated to this change request
        Assignment assignment = extendedAssignmentService.findOneWithEagerRelationships(request.getAssignmentId())
            .orElseThrow(() -> new IllegalArgumentException(String.format("No Assignment with id '%d' was found in the system", request.getAssignmentId())));

        //Get the Device that is being used to provide information about VITALS for the assignment.
        Set<Device> assignmentVitals = AssignmentUtils.getDevicesByRequiredCapability(assignment).get(DeviceCapabilityType.VITALS);
        if (assignmentVitals == null || assignmentVitals.isEmpty()) {
            throw new IllegalStateException(String.format("The assignment '%d' has no Device configured to provide VITALS", request.getAssignmentId()));
        }

        //We take the first device (it should only be one)
        String hardwareId = assignmentVitals.iterator().next().getHardwareId();

        //We go to the cache and look for the Vitals data
        DevicesDataCache.DeviceData data = devicesDataCache.getData(hardwareId);
        if (data == null) {
            throw new IllegalStateException(String.format("There is no data in the cache for the Device '%s'", hardwareId));
        }
        HardwareVitalsEvent lastVitals = data.getLastVitals();
        if (lastVitals == null) {
            throw new IllegalStateException(String.format("There is no information about the Vitals in the cache for the Device '%s'", hardwareId));
        }

        //We finaly get the Tidal Volume
        ScalarValue tv = lastVitals.getTv();
        if (tv == null) {
            throw new IllegalStateException(String.format("There is no information about the Tidal Volume in the cache for the Device '%s'", hardwareId));
        }

        return tv.getValue();
    }

}
