/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.recommender;

import io.saperi.vcm.event.VentilatorRecommendationResponseEvent;
import io.saperi.vcm.repository.CustomAuditEventRepository;

import java.text.DecimalFormat;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 *
 * @author esteban
 */
@Component
public class VentilatorRecommendationToAuditEventConverter {

    public final static String EVENT_TYPE = "VENTILATOR_RECOMMENDATION";
    public final static String DATA_ASSIGNMENT_ID_KEY = "ASSIGNMENT_ID";

    public final static String DATA_REQ_TIME_KEY = "REQ_TIME";
    public final static String DATA_REQ_ETCO2_KEY = "REQ_ETCO2";
    public final static String DATA_REQ_FIO2_KEY = "REQ_FIO2";
    public final static String DATA_REQ_GENDER_KEY = "REQ_GENDER";
    public final static String DATA_REQ_HEIGHT_KEY = "REQ_HEIGHT";
    public final static String DATA_REQ_O2SAT_KEY = "REQ_O2SAT";
    public final static String DATA_REQ_PEEP_KEY = "REQ_PEEP";
    public final static String DATA_REQ_PH_KEY = "REQ_PH";
    public final static String DATA_REQ_PIP_KEY = "REQ_PIP";
    public final static String DATA_REQ_RATE_KEY = "REQ_RATE";
    public final static String DATA_REQ_TV_KEY = "REQ_TV";

    public final static String DATA_RESP_FIO2_KEY = "RESP_FIO2";
    public final static String DATA_RESP_PEEP_KEY = "RESP_PEEP";
    public final static String DATA_RESP_PIP_KEY = "RESP_PIP";
    public final static String DATA_RESP_RATE_KEY = "RESP_RATE";
    public final static String DATA_RESP_TV_KEY = "RESP_TV";
    public final static String DATA_RESP_REASON_KEY = "RESP_REASON";

    private final CustomAuditEventRepository eventRepository;
    private final DecimalFormat df = new DecimalFormat("#.##");

    public VentilatorRecommendationToAuditEventConverter(CustomAuditEventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    @EventListener
    public void handleVentilatorRecommendationResponseEvent(VentilatorRecommendationResponseEvent event) {
        AuditEvent audit = new AuditEvent(
            Instant.ofEpochMilli(event.getSource().getTimestamp()),
            event.getSource().getRequest().getRequester(),
            EVENT_TYPE,
            extractData(event)
        );

        eventRepository.add(audit);
    }

    protected Map<String, Object> extractData(VentilatorRecommendationResponseEvent event) {
        Map<String, Object> data = new HashMap<>();
        data.put(DATA_ASSIGNMENT_ID_KEY, event.getSource().getAssignmentId());

        data.put(DATA_REQ_PH_KEY, (event.getSource().getRequest().getPh()==null)?"":getFormattedValue(event.getSource().getRequest().getPh().getValue()) + " " + event.getSource().getRequest().getPh().getUnit());
        data.put(DATA_REQ_TIME_KEY, event.getSource().getRequest().getTimestamp());
        data.put(DATA_REQ_ETCO2_KEY, getFormattedValue(event.getSource().getRequest().getEtco2().getValue()) + " " + event.getSource().getRequest().getEtco2().getUnit());
        data.put(DATA_REQ_FIO2_KEY, getFormattedValue(event.getSource().getRequest().getFio2().getValue()) + " " + event.getSource().getRequest().getFio2().getUnit());
        data.put(DATA_REQ_GENDER_KEY, event.getSource().getRequest().getGender());
        data.put(DATA_REQ_HEIGHT_KEY, getFormattedValue(event.getSource().getRequest().getHeight().getValue()) + " " + event.getSource().getRequest().getHeight().getUnit());
        data.put(DATA_REQ_O2SAT_KEY, getFormattedValue(event.getSource().getRequest().getO2sat().getValue()) + " " + event.getSource().getRequest().getO2sat().getUnit());
        data.put(DATA_REQ_PEEP_KEY, getFormattedValue(event.getSource().getRequest().getPeep().getValue()) + " " + event.getSource().getRequest().getPeep().getUnit());
        data.put(DATA_REQ_PIP_KEY, getFormattedValue(event.getSource().getRequest().getPip().getValue()) + " " + event.getSource().getRequest().getPip().getUnit());
        data.put(DATA_REQ_RATE_KEY, getFormattedValue(event.getSource().getRequest().getRate().getValue()) + " " + event.getSource().getRequest().getRate().getUnit());
        data.put(DATA_REQ_TV_KEY, getFormattedValue(event.getSource().getRequest().getTidalVolume().getValue()) + " " + event.getSource().getRequest().getTidalVolume().getUnit());

        data.put(DATA_RESP_FIO2_KEY, getFormattedValue(event.getSource().getFio2().getValue()) + " " + event.getSource().getFio2().getUnit());
        data.put(DATA_RESP_PEEP_KEY, getFormattedValue(event.getSource().getPeep().getValue()) + " " + event.getSource().getPeep().getUnit());
        data.put(DATA_RESP_PIP_KEY, (event.getSource().getPip()==null)?"":getFormattedValue(event.getSource().getPip().getValue()) + " " + event.getSource().getPip().getUnit());
        data.put(DATA_RESP_TV_KEY, (event.getSource().getTv()==null)?"":getFormattedValue(event.getSource().getTv().getValue()) + " " + event.getSource().getTv().getUnit());
        data.put(DATA_RESP_RATE_KEY, getFormattedValue(event.getSource().getRate().getValue()) + " " + event.getSource().getRate().getUnit());
        data.put(DATA_RESP_REASON_KEY, event.getSource().getReason());

        return data;
    }

    private String getFormattedValue(Double value){
        return df.format(value);
    }
}
