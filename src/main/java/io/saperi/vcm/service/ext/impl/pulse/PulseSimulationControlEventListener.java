/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.pulse;

import io.saperi.pulse.sim.api.event.SimulationInitializedEvent;
import io.saperi.pulse.sim.api.event.SimulationStartedEvent;
import io.saperi.pulse.sim.api.event.SimulationStoppedEvent;
import io.saperi.pulse.sim.api.event.SimulationTerminatedEvent;
import io.saperi.pulse.sim.api.event.adapter.SimulationControlEventListenerAdapter;
import io.saperi.pulse.sim.api.model.PulseSimulation;
import io.saperi.vcm.event.HardwareStatusEvent;
import org.springframework.context.ApplicationEventPublisher;

/**
 * Broadcasts Pulse Control Events as {@link HardwareStatusEvent}.
 * <p>
 * The pulse events are converted according to the following logic:
 * <ul>
 * <li>Any {@link SimulationInitializedEvent} is published as a
 * {@link HardwareStatusEvent.Status#WARNING} event.
 * <li>Any {@link SimulationStartedEvent} is published as a
 * {@link HardwareStatusEvent.Status#CONNECTED} event.
 * <li>Any {@link SimulationStoppedEvent} is published as a
 * {@link HardwareStatusEvent.Status#WARNING} event.
 * <li>Any {@link SimulationTerminatedEvent} is published as a
 * {@link HardwareStatusEvent.Status#ERROR} event.
 * </ul>
 * </p>
 *
 * @author esteban
 */
public class PulseSimulationControlEventListener extends SimulationControlEventListenerAdapter {

    private final ApplicationEventPublisher applicationEventPublisher;

    public PulseSimulationControlEventListener(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    public void onSimulationTerminatedEvent(SimulationTerminatedEvent e) {
        applicationEventPublisher.publishEvent(PulseUtils.toHardwareStatusEvent(e.getSimulationName(), PulseSimulation.State.TERMINATED));
    }

    @Override
    public void onSimulationStoppedEvent(SimulationStoppedEvent e) {
        applicationEventPublisher.publishEvent(PulseUtils.toHardwareStatusEvent(e.getSimulationName(), PulseSimulation.State.STOPPED));
    }

    @Override
    public void onSimulationStartedEvent(SimulationStartedEvent e) {
        applicationEventPublisher.publishEvent(PulseUtils.toHardwareStatusEvent(e.getSimulationName(), PulseSimulation.State.RUNNING));
    }

    @Override
    public void onSimulationInitializedEvent(SimulationInitializedEvent e) {
        applicationEventPublisher.publishEvent(PulseUtils.toHardwareStatusEvent(e.getSimulationName(), PulseSimulation.State.INITIALIZED));
    }

}
