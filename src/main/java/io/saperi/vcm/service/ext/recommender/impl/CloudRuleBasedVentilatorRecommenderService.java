/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.recommender.impl;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.saperi.vcm.domain.Assignment;
import io.saperi.vcm.domain.enumeration.VentilatorControlType;
import io.saperi.vcm.domain.ext.VentilatorRecommendationRequest;
import io.saperi.vcm.domain.ext.VentilatorRecommendationResponse;
import io.saperi.vcm.event.ScalarValue;
import io.saperi.vcm.event.VentilatorRecommendationResponseEvent;
import io.saperi.vcm.service.ext.impl.DevicesDataCache;
import io.saperi.vcm.service.ext.recommender.VentilatorRecommenderService;

import java.text.NumberFormat;
import java.util.Collections;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;

/**
 * Concrete {@link VentilatorRecommenderService} that knows how to invoke
 * Saperi's Ventilator Cloud Recommender.
 *
 * @author esteban
 */
@Service
public class CloudRuleBasedVentilatorRecommenderService extends BaseCloudVentilatorRecommenderService {

    private static final Logger log = LoggerFactory.getLogger(CloudRuleBasedVentilatorRecommenderService.class);

    private ObjectMapper objectMapper;
    /**
     * Data structure used to send data to the rule-based recommender.
     *
     * @TODO: Enforce the required units when sending data to the recommender.
     */
    private static class RuleBasedRequestData {

        /**
         * Must be in cmH2O.
         */
        public Double vpValue;
        /**
         * Must be in cmH2O.
         */
        public Double peepValue;
        /**
         * Must be in 1/min.
         */
        public Double rateValue;
        /**
         * Must be a value between 0 and 100.
         */
        public Double fio2Value;
        public Double phValue;
        /**
         * Must be in mmHg.
         */
        public Double etco2Value;
        /**
         * Must be a value between 0 and 100.
         */
        public Double o2SatValue;
        /**
         * Must be in L.
         */
        public Double tvValue;
        /**
         * Must be in inches.
         */
        public Double heightValue;
        /**
         * Must be in "Male" or "Female".
         */
        public String genderValue;

        @Override
        public String toString() {
            return "RuleBasedRequestData{" + "vpValue=" + vpValue + ", peepValue=" + peepValue + ", rateValue=" + rateValue + ", fio2Value=" + fio2Value + ", phValue=" + phValue + ", etco2Value=" + etco2Value + ", o2SatValue=" + o2SatValue + ", tvValue=" + tvValue + ", heightValue=" + heightValue + ", genderValue=" + genderValue + '}';
        }

    }

    /**
     * Data structure used to deserialize the response of the recommender.
     */
    private static class RuleBasedResponseData {

        /**
         * A value between 0 and 100.
         */
        public double fio2Value;
        /**
         * In 1/min.
         */
        public double rrValue;
        /**
         * In cmH2O.
         */
        public double peepValue;
        /**
         * In cmH2O.
         */
        public double vpValue;
        /**
         * In L.
         */
        public double tvValue;
        public String reasonValue;
    }

    @Value("${vcm.ventilator-recommender.rb.pressure.url}")
    private String pressure_recommenderUrl;

    @Value("${vcm.ventilator-recommender.rb.pressure.recommend-endpoint}")
    private String pressure_recommendEndpoint;

    @Value("${vcm.ventilator-recommender.rb.pressure.api-key}")
    private String pressure_apiKey;

    @Value("${vcm.ventilator-recommender.rb.volume.url}")
    private String volume_recommenderUrl;

    @Value("${vcm.ventilator-recommender.rb.volume.recommend-endpoint}")
    private String volume_recommendEndpoint;

    @Value("${vcm.ventilator-recommender.rb.volume.api-key}")
    private String volume_apiKey;


    private final RestOperations restOperations;
    private final ApplicationEventPublisher applicationEventPublisher;

    public CloudRuleBasedVentilatorRecommenderService(
        RestOperations restOperations,
        DevicesDataCache dataCache,
        ApplicationEventPublisher applicationEventPublisher,
        ObjectMapper objectMapper) {
        super(dataCache);
        this.restOperations = restOperations;
        this.applicationEventPublisher = applicationEventPublisher;
        this.objectMapper = objectMapper;
        nf = NumberFormat.getInstance(Locale.US);
        nf.setGroupingUsed(false);
    }

    @Override
    public VentilatorRecommendationResponse createRecommendation(Assignment assignment, VentilatorRecommendationRequest request) {

        RuleBasedRequestData data = new RuleBasedRequestData();
        data.vpValue = getDoubleValue(request.getPip(), "PIP");
        data.peepValue = getDoubleValue(request.getPeep(), "PEEP");
        data.rateValue = getDoubleValue(request.getRate(), "Rate");
        data.fio2Value = getDoubleValue(request.getFio2(), "FiO2");
        //data.phValue = getDoubleValue(request.getPh(), "pH");
        //On RB model this value should be always 7.4
        data.phValue = 7.4;
        data.etco2Value = getDoubleValue(request.getEtco2(), "EtCO2");
        data.o2SatValue = getDoubleValue(request.getO2sat(), "Sat");
        data.tvValue = getDoubleValue(request.getTidalVolume(), "TV");
        data.heightValue = getDoubleValue(request.getHeight(), "Height");
        data.genderValue = getStringValue(request.getGender(), "Gender");

        VentilatorRecommendationResponse response = invokeRecommender(data, assignment);
        response.setAssignmentId(assignment.getId());
        response.setRequest(request);

        //Publish the results as an event.
        applicationEventPublisher.publishEvent(new VentilatorRecommendationResponseEvent(response));

        return response;
    }

    private VentilatorRecommendationResponse invokeRecommender(RuleBasedRequestData request, Assignment assignment) {

        if (request.etco2Value == null
            || request.fio2Value == null
            || request.genderValue == null
            || request.heightValue == null
            || request.o2SatValue == null
            || request.peepValue == null
            || request.phValue == null
            || request.rateValue == null
            || request.tvValue == null
            || request.vpValue == null) {

            log.warn("Incomplete Ventilator Recommender Data Request: {}", request);
            return null;
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        String apiKey = pressure_apiKey;
        if ( VentilatorControlType.VOLUME.equals(assignment.getVentilatorControlType()) ) {
            apiKey = volume_apiKey;
        }
        headers.set("x-api-key", apiKey);
        HttpEntity<RuleBasedRequestData> entity = new HttpEntity<>(request, headers);

        log.debug("Requesting Rule Based Ventilator Recommendation ({}) for {}", assignment.getVentilatorControlType().toString(), request);
        String recommenderUrl = pressure_recommenderUrl;
        String recommendEndpoint = pressure_recommendEndpoint;
        if ( VentilatorControlType.VOLUME.equals(assignment.getVentilatorControlType()) ) {
            recommenderUrl = volume_recommenderUrl;
            recommendEndpoint = volume_recommendEndpoint;
        }
        ResponseEntity<RuleBasedResponseData> httpResponse = restOperations.exchange(recommenderUrl + recommendEndpoint, HttpMethod.POST, entity, RuleBasedResponseData.class);

        if (!httpResponse.getStatusCode().is2xxSuccessful()) {
            throw new IllegalStateException("The Ventilator Recommender returned and error");
        }

        RuleBasedResponseData recommendation = httpResponse.getBody();
        if (recommendation == null) {
            return null;
        }

        VentilatorRecommendationResponse response = new VentilatorRecommendationResponse();
        response.setFio2(new ScalarValue(recommendation.fio2Value, "%", 0));
        response.setPeep(new ScalarValue(recommendation.peepValue, "cmH2O", 0));
        response.setRate(new ScalarValue(recommendation.rrValue, "1/min", 0));
        response.setPip(new ScalarValue(recommendation.vpValue, "cmH2O", 0));
        if (VentilatorControlType.VOLUME.equals(assignment.getVentilatorControlType())) {
            //This is Volume Control mode the TV is also in the response
            response.setTv(new ScalarValue(recommendation.tvValue, "L", 2));
        }
        response.setReason(recommendation.reasonValue);

        try {
            log.debug("Response Rule Based Ventilator Recommendation ({}): {}", assignment.getVentilatorControlType().toString(), objectMapper.writeValueAsString(response));
        } catch (JsonProcessingException e) {
            log.error("Error trying to serialize Recommendation Response into JSON format");
        }
        return response;
    }

}
