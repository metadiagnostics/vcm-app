/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl;

import io.saperi.vcm.domain.PersistentAuditEvent;
import io.saperi.vcm.repository.ext.ExtendedPersistenceAuditEventRepository;
import io.saperi.vcm.service.ext.ExtendedPersistenceAuditEventService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author esteban
 */
@Service
@Transactional
public class ExtendedPersistenceAuditEventServiceImpl implements ExtendedPersistenceAuditEventService {

    private final Logger log = LoggerFactory.getLogger(ExtendedPersistenceAuditEventServiceImpl.class);

    private final ExtendedPersistenceAuditEventRepository repository;

    public ExtendedPersistenceAuditEventServiceImpl(ExtendedPersistenceAuditEventRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<PersistentAuditEvent> findByDataKeyAndDataValue(String[] types, String dataKey, String dataValue) {
        return repository.findByDataKeyAndDataValue(types, dataKey, dataValue);
    }

}
