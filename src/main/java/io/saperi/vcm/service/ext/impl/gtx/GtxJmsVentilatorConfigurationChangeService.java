/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.gtx;

import com.athenagtx.framing.Frame;
import io.saperi.vcm.event.VentilatorConfigurationChangeRequestEvent;
import io.saperi.vcm.service.ext.ExtendedAssignmentService;
import io.saperi.vcm.service.ext.impl.DevicesDataCache;
import io.saperi.vcm.service.ext.recommender.VentilatorConfigurationChangeService;
import java.io.IOException;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import org.apache.activemq.command.ActiveMQBytesMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

/**
 *
 * @author esteban
 */
@Service
public class GtxJmsVentilatorConfigurationChangeService extends GtxAbstractVentilatorConfigurationChangeService implements VentilatorConfigurationChangeService {

    private static final Logger log = LoggerFactory.getLogger(GtxJmsVentilatorConfigurationChangeService.class);

    @Value("${vcm.gtx.jms.enabled:false}")
    private boolean enabled;

    @Value("${vcm.gtx.jms.command-topic}")
    private String gtxJmsTopic;

    private final ConnectionFactory connectionFactory;

    private Connection connection;
    private MessageProducer jmsProducer;

    public GtxJmsVentilatorConfigurationChangeService(
        ApplicationEventPublisher applicationEventPublisher,
        ExtendedAssignmentService extendedAssignmentService,
        GtxDevicesCache devicesCache,
        DevicesDataCache devicesDataCache,
        @Qualifier("amq-jms-factoy") ConnectionFactory connectionFactory
    ) {
        super(applicationEventPublisher, extendedAssignmentService, devicesCache, devicesDataCache,  "JMS Ventilator Configuration Change Service");
        this.connectionFactory = connectionFactory;
    }

    @PostConstruct
    public void postConstruct() {
        if (!enabled) {
            log.debug("JMS Is not enabled for GTX Devices. No Ventilator Configuration Change Sevice for JMS devices will be configured.");
            return;
        }
        log.debug("Starting GTX JMS Producer");
        try {
            this.connection = connectionFactory.createConnection();
            this.connection.setExceptionListener((JMSException exception) -> {
                log.error("Exception in GTX JMS Listener", exception);
            });
            this.connection.start();
            log.debug("JMS Connection with id '{}' CREATED", this.getConnectionId(this.connection));

            Session jmsSession = this.connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            jmsProducer = jmsSession
                .createProducer(jmsSession.createTopic(gtxJmsTopic));

        } catch (JMSException ex) {
            throw new IllegalStateException("Exception creating JMS Producer", ex);
        }
    }

    @PreDestroy
    public void preDestroy() {
        if (connection != null) {
            try {
                connection.close();
            } catch (JMSException ex) {
                log.error("Exception closing JMS Connection.", ex);
            }
        }
    }

    @Override
    public boolean changeVentilator(GtxDeviceInfo info, Frame frame, VentilatorConfigurationChangeRequestEvent req) throws IOException, JMSException {
        if (!enabled) {
            return false;
        }
        if (!(info instanceof GtxJmsDeviceInfo)) {
            log.trace("The message was not of type GtxJmsDeviceInfo. I'm not interested in it.");
            return false;
        }
        ActiveMQBytesMessage msg = new ActiveMQBytesMessage();
        msg.writeBytes(frame.toByteArray());
        jmsProducer.send(msg);
        return true;
    }

    private String getConnectionId(Connection c) {
        try {
            return c.getClientID();
        } catch (JMSException ex) {
            throw new IllegalStateException("Exception getting Id from JMS Connection", ex);
        }
    }
}
