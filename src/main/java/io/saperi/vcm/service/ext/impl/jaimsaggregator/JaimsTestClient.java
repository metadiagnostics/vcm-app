/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.jaimsaggregator;

import com.athenagtx.framing.FrameException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.saperi.virtualhealth.device.aggregators.common.*;
import io.saperi.virtualhealth.device.aggregators.common.events.CommandModeEnteredEvent;
import io.saperi.virtualhealth.device.aggregators.common.events.CommandModeExitedEvent;
import io.saperi.virtualhealth.device.aggregators.common.exceptions.ArgumentException;
import io.saperi.virtualhealth.device.aggregators.common.exceptions.ConnectionException;
import io.saperi.virtualhealth.device.aggregators.common.model.Command;
import io.saperi.virtualhealth.device.aggregators.common.model.ConnectionInfo;
import io.saperi.virtualhealth.device.aggregators.common.model.DeviceInfo;
import io.saperi.virtualhealth.device.aggregators.common.model.Frame;
import io.saperi.virtualhealth.device.aggregators.common.model.MeasureValue;
import io.saperi.virtualhealth.device.aggregators.common.model.MeasureWaveForm;
import io.saperi.virtualhealth.device.aggregators.common.model.Setting;

import java.io.*;
import java.lang.IllegalStateException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.activemq.command.ActiveMQBytesMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;

public class JaimsTestClient extends DeviceAggregatorClient {
    private static final Logger log = LoggerFactory.getLogger(io.saperi.virtualhealth.device.aggregators.common.JSONReplayClient.class);
    private boolean commandMode = false;
    private boolean connected = false;
    private IFrameManager frameManager = new FrameManager();
    private int waitTime = 950;
    private Duration baseDuration = null;

    private final ConnectionFactory connectionFactory;
    private final String testJmsTopic;
    private Connection connection;

    public JaimsTestClient(final ConnectionFactory connectionFactory,
                           final String testJmsTopic) {
        this.connectionFactory = connectionFactory;
        this.testJmsTopic = testJmsTopic;
    }

    public DeviceAggregatorClient close() {
        return this;
    }

    public DeviceAggregatorClient open(Properties props) throws ArgumentException, ConnectionException {
        try {
            this.connection = connectionFactory.createConnection();
            this.connection.setExceptionListener((JMSException exception) -> {
                log.error("Exception creating a test MQ connection", exception);
            });
            this.connection.start();
            log.debug("JMS Connection with id '{}' CREATED", this.getConnectionId(this.connection));

            Session jmsSession = this.connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageConsumer jmsConsumer = jmsSession
                .createConsumer(jmsSession.createTopic(testJmsTopic));
            jmsConsumer.setMessageListener((m) -> {
                if (!(m instanceof ActiveMQBytesMessage)) {
                    log.warn("Received a message in topic '{}' that was not an instance of ActiveMQBytesMessage. It was a '{}'", testJmsTopic, m.getClass().getName());
                    return;
                }

                ActiveMQBytesMessage amqMessage = (ActiveMQBytesMessage) m;
                try {
                    byte[] bytes = new byte[(int) amqMessage.getBodyLength()];
                    amqMessage.readBytes(bytes, bytes.length);
                    this.processFrame(framefromByteArray(bytes));
                } catch (JMSException ex) {
                    log.error("Exception deserializing JMS Message from Test Frame tool", ex);
                }
            });

        } catch (JMSException ex) {
            throw new IllegalStateException("Exception creating JMS Connection", ex);
        }

        return this;
    }

    private Frame framefromByteArray(byte[] bytes) {
        InputStream is = new ByteArrayInputStream(bytes);
        try (ObjectInputStream ois = new ObjectInputStream(is)) {
            return (Frame) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            log.error("Exception deserializing Frame Message", e);
        }
        return null;
    }


    private String getConnectionId(Connection c) {
        try {
            return c.getClientID();
        } catch (JMSException ex) {
            throw new IllegalStateException("Exception getting Id from JMS Connection", ex);
        }
    }

    public boolean connected() {
        return this.connected;
    }

    public DeviceAggregatorClient connect(Properties props) throws ArgumentException, ConnectionException {

        return this;
    }

    public DeviceAggregatorClient disconnect() {
        if (this.connected) {
            this.connected = false;
        }
        return this;
    }

    public void configure(Properties props) throws ArgumentException {
        if (props.containsKey("JaimsTestClient.waitMs")) {
            this.waitTime = Integer.valueOf((String)props.get("JaimsTestClient.waitMs"));
        }

    }

    public List<String> getRequiredProperties(PropertyScope scope) {
        ArrayList<String> out = new ArrayList();
        switch(scope) {
            case CONFIG:
                out.add("JaimsTestClient.waitMs");
                break;
            case CONNECT:
                out.add("JaimsTestClient.wssURL");
        }

        return out;
    }

    public boolean requestSettingChange(DeviceInfo device, List<Setting> newSetting) {
        return false;
    }

    public IFrameManager getFrameManager() {
        return this.frameManager;
    }

    public void setFrameManager(IFrameManager frameManager) {
        this.frameManager = frameManager;
    }

    public String getCurrentOperatingMode(String systemId) {
        return null;
    }

    public Set<String> getSettingsForOperatingMode(String system, String mode, boolean includeAll, boolean includeAlarms) {
        return null;
    }

    public String getOperatingModeFromFrame(String systemId, Frame frame) {
        return null;
    }

    public boolean postSettingChange(DeviceInfo device, List<Setting> newSettings) {
        return false;
    }

    public void requestCommandMode(String systemId) {
        this.commandMode = true;
        CommandModeEnteredEvent evt = new CommandModeEnteredEvent();
        evt.setSource("Client");
        evt.setSystemId(systemId);
        this.emit(CommandModeEnteredEvent.COMMAND_MODE_ACTIVE, new Object[]{evt});
    }

    public void exitCommandMode(String systemId) {
        this.commandMode = false;
        CommandModeExitedEvent evt = new CommandModeExitedEvent();
        evt.setReason("Requested");
        evt.setSystemId(systemId);
        this.emit(CommandModeExitedEvent.COMMAND_MODE_EXITED, new Object[]{evt});
    }

    public boolean isInCommandMode(String systemId) {
        return this.commandMode;
    }

    public void sendDeviceCommand(String systemId, Command cmd) {
    }
    /** Needed for upcoming versions uncomment on version 0.4.0 and above*/
    @Override
    public void setConnectionInfo(ConnectionInfo connectionInfo) {

    }

    @Override
    public DeviceAggregatorClient open(ConnectionInfo connectionInfo) throws ArgumentException, ConnectionException {
        return this;
    }

    @Override
    public DeviceAggregatorClient connect(ConnectionInfo connectionInfo) throws ArgumentException, ConnectionException {
        return this;
    }

    @Override
    public boolean isRawRecordingEnabled() {
        return false;
    }

    @Override
    public void setRawRecordingEnabled(boolean b) {

    }

    @Override
    public boolean runReplay(ConnectionInfo connectionInfo, String s) {
        return false;
    }

    @Override
    public void UpdateConnections() {

    }

    private int processFrame(Frame frame) {
        Set<String> names = frame.getDeviceNames();

        boolean measuresSeen = false;
        int wait = waitTime;

        if (baseDuration == null) {
            Instant now = Instant.now();
            baseDuration = Duration.between(now, frame.findEarliestInstant());
        }

        for (String devId : names) {
            DeviceCtx devCtx = frame.getDeviceContext(devId);

            fixTime(devCtx);
            if (devCtx.getDeviceInfo() != null) {
                frameManager.add(devCtx.getDeviceInfo());
            }
            Collection<Setting> settings = devCtx.getSettingValues().values();
            for (Setting setting : settings) {
                fixTime(setting);
                frameManager.add(devId, setting);
            }
            Collection<MeasureValue> measures = devCtx.getMeasureValues().values();
            if (measures.size() > 0) {
                measuresSeen = true;
            }
            for (MeasureValue measure : measures) {
                fixTime(measure);
                frameManager.add(devId, measure);
            }
            Collection<MeasureWaveForm> waveforms = devCtx.getMeasureWaveFormValues().values();
            for (MeasureWaveForm waveform : waveforms) {
                fixTime(waveform);
                frameManager.add(devId, waveform);
            }
        }
        frameManager.switchFrame();
        return measuresSeen ? waitTime : 80;
    }

    private void fixTime(Setting setting) {
        Instant source = setting.getSource_timestamp();
        if (source != null) {
            setting.setSource_timestamp(source.plus(this.baseDuration));
        }

    }

    private void fixTime(MeasureWaveForm waveForm) {
        Instant source = waveForm.getSource_timestamp();
        if (source != null) {
            waveForm.setSource_timestamp(source.plus(this.baseDuration));
        }

        source = waveForm.getDevice_relative_timestamp();
        if (source != null) {
            waveForm.setDevice_relative_timestamp(source.plus(this.baseDuration));
        }

    }

    private void fixTime(MeasureValue value) {
        Instant source = value.getSource_timestamp();
        if (source != null) {
            value.setSource_timestamp(source.plus(this.baseDuration));
        }

        source = value.getDevice_relative_timestamp();
        if (source != null) {
            value.setDevice_relative_timestamp(source.plus(this.baseDuration));
        }

    }

    private void fixTime(DeviceCtx deviceCtx) {
    }
}

