/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.recommender;

import io.saperi.vcm.event.VentilatorConfigurationChangeRequestEvent;
import org.springframework.context.event.EventListener;

/**
 * Interface that acts as a listener of
 * {@link VentilatorConfigurationChangeRequestEvent} events.
 * <p>
 * It is just enough to implement this interface to start receiving the
 * mentioned events.
 * </p>
 * <p>
 * Concrete implementations of this interface are used to actually change the
 * values of a Ventilator based on the received request.
 * </p>
 *
 * @author esteban
 */

//TODO: Refactor Package and Name
public interface VentilatorConfigurationChangeService {

    @EventListener
    void changeVentilator(VentilatorConfigurationChangeRequestEvent request);

}
