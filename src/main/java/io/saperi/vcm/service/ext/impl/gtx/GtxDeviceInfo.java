/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.gtx;

import com.athenagtx.framing.HardwareId;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * @author esteban
 */
public abstract class GtxDeviceInfo {

    private HardwareId hardwareId;
    private String runId;

    public GtxDeviceInfo() {
    }

    public GtxDeviceInfo(HardwareId hardwareId, String runId) {
        this.hardwareId = hardwareId;
        this.runId = runId;
    }

    public HardwareId getHardwareId() {
        return hardwareId;
    }

    public void setHardwareId(HardwareId hardwareId) {
        this.hardwareId = hardwareId;
    }

    public String getRunId() {
        return runId;
    }

    public void setRunId(String runId) {
        this.runId = runId;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            append(hardwareId.hashCode()).
            append(runId).
            toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof GtxDeviceInfo))
            return false;
        if (obj == this)
            return true;

        GtxDeviceInfo gtxDeviceInfo = (GtxDeviceInfo) obj;
        return new EqualsBuilder().
            append(hardwareId, gtxDeviceInfo.hardwareId).
            append(runId, gtxDeviceInfo.runId).
            isEquals();
    }

}
