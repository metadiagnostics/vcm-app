/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl;

import io.saperi.vcm.domain.Assignment;
import io.saperi.vcm.event.Fio2AckRequestEvent;
import io.saperi.vcm.event.VentilatorModeChangeRequestEvent;
import io.saperi.vcm.repository.CustomAuditEventRepository;
import io.saperi.vcm.service.ext.recommender.VentilatorRecommendationToAuditEventConverter;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

/**
 * An event listener to store Ventilator Mode changes on the specified event Repository.
 *
 * @author sgroh@saperi.io
 */
@Component
public class VentilatorChangeModeAuditEventConverter {

    public final static String EVENT_TYPE = "VENTILATOR_MODE_CHANGE";
    public final static String DATA_ASSIGNMENT_ID_KEY = VentilatorRecommendationToAuditEventConverter.DATA_ASSIGNMENT_ID_KEY;

    public final static String DATA_DEVICE_MODEL_KEY = "DEVICE_MODEL";
    public final static String DATA_DEVICE_HW_ID_KEY = "DEVICE_HW_ID";
    public final static String DATA_VENT_MODE_KEY = "VENTILATOR_MODE";

    private final CustomAuditEventRepository eventRepository;

    public VentilatorChangeModeAuditEventConverter(CustomAuditEventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    @EventListener
    public void handleVentilatorConfigurationChangeRequestEvent(VentilatorModeChangeRequestEvent event) {
        AuditEvent audit = new AuditEvent(
            Instant.ofEpochMilli(event.getTimestamp()),
            event.getRequester(),
            EVENT_TYPE,
            extractData(event)
        );

        eventRepository.add(audit);
    }

    protected Map<String, Object> extractData(VentilatorModeChangeRequestEvent event) {
        Map<String, Object> data = new HashMap<>();

        data.put(DATA_ASSIGNMENT_ID_KEY, event.getAssignmentId());
        data.put(DATA_DEVICE_MODEL_KEY, event.getDevice().getModel().getName());
        data.put(DATA_DEVICE_HW_ID_KEY, event.getDevice().getHardwareId());
        addEntry(data, DATA_VENT_MODE_KEY, ((Assignment) event.getSource()).getVentilatorMode().name());

        return data;
    }

    private void addEntry(Map<String, Object> map, String key, String value) {
        if (value == null) {
            return;
        }
        map.put(key, value);
    }
}
