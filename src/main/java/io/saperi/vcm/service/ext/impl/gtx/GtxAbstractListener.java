/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.gtx;

import athena.admsandroid.Devices.Message;
import athena.admsandroid.Protobufs.Accs;
import com.athenagtx.framing.Crc32Wrapper;
import com.athenagtx.framing.Frame;
import com.athenagtx.framing.HardwareId;
import com.google.protobuf.InvalidProtocolBufferException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;

/**
 * Base class for listeners of GTX Device data containing common functionality.
 *
 * @author esteban
 */
public abstract class GtxAbstractListener {

    private static final Logger log = LoggerFactory.getLogger(GtxAbstractListener.class);

    private final ApplicationEventPublisher applicationEventPublisher;
    private final GtxDevicesCache cache;

    public GtxAbstractListener(
        ApplicationEventPublisher applicationEventPublisher,
        GtxDevicesCache cache) {
        this.applicationEventPublisher = applicationEventPublisher;
        this.cache = cache;
    }

    protected abstract GtxDeviceInfo createGtxDeviceInfo();

    protected void processFrame(Frame f) {
        String deviceType = com.athenagtx.framing.DeviceType.fromValue(f.getHeader().getDeviceTypeValue()).name();
        HardwareId hardwareId = f.getHeader().getHardwareId();

        log.trace("Frame received:");
        log.trace("\tDevice Type: {}", deviceType);
        log.trace("\tHardware: {}", hardwareId);

        GtxDeviceInfo deviceInfo = createGtxDeviceInfo();
        deviceInfo.setHardwareId(hardwareId);

        cache.addDevice(hardwareId.toString(), deviceInfo);
        if (!cache.isDeviceOfInterest(hardwareId.toString())) {
            //We are not interested in this HW
            log.trace("\tWe are not interested in this Hardware. The Frame was discarded");
            return;
        }

        if (f.getHeader().getDeviceTypeValue() == com.athenagtx.framing.DeviceType.ACCS.getValue()) {
            //TODO: make this configurable
            Crc32Wrapper wrapper = new Crc32Wrapper();
            try {

                byte[] data = wrapper.unwrap(f.getContent());
                Message message = new Message(data);
                String runId = message.getHeader().getRunId();

                //Update the entry in the cache to include the runId.
                cache.getInfo(hardwareId.toString(), deviceInfo.getClass()).setRunId(runId);

                int dsn = message.getHeader().getDsn();
                String messageType = message.getHeader().getType().name();

                log.trace("\tRunId: {}", runId);
                log.trace("\tDSN: {}", dsn);
                log.trace("\tMessage Type: {}", messageType);

                processMessage(hardwareId.toString(), message);
            } catch (Exception e) {
                log.error("Exception processing GTX Frame", e);
            }

        } else {
            //do nothing
        }
    }

    private void processMessage(String hardwareId, Message message) throws InvalidProtocolBufferException {
        Accs.AccsResources resource = Accs.AccsResources.forNumber(message.getHeader().getResource());
        if (resource == null) {
            return;
        }

        log.trace("\tResource: {}", resource.name());
        switch (message.getHeader().getType()) {
            case BROADCAST: {
                switch (resource) {
                    case LIVE_DATA:
                        Accs.AccsLiveData data = Accs.AccsLiveData.parseFrom(message.getBody());
                        //publish the data
                        applicationEventPublisher.publishEvent(new GtxAccsLiveDataEvent(hardwareId, data));
                        break;

                    case MULTI_WAVEFORM:
                        Accs.AccsMultiWaveform multiWave = Accs.AccsMultiWaveform.parseFrom(message.getBody());
                        //do nothing
                        break;

                    default:
                        //do nothing
                        break;
                }
                break;
            }
            case WRITE_RESPONSE: {
                log.trace("\tStatus: {}", message.getHeader().getStatus());
                switch (resource.getNumber()) {
                    case Accs.AccsResources.VENTILATOR_CONTROL_VALUE:
                        //TODO: validate that the command was succesfully applied.
                        break;
                    default:
                        //do nothing
                        break;
                }
                break;
            }
            default:
                //do nothing
                break;
        }
    }

}
