/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.gtx;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.net.InetAddress;

/**
 *
 * @author esteban
 */
public class GtxUdpDeviceInfo extends GtxDeviceInfo {

    private final InetAddress address;

    public GtxUdpDeviceInfo(InetAddress address) {
        this.address = address;
    }

    public InetAddress getAddress() {
        return address;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
                appendSuper(super.hashCode()).
                append(address.hashCode()).
                toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof GtxUdpDeviceInfo))
            return false;
        if (obj == this)
            return true;

        GtxUdpDeviceInfo gtxDeviceInfo = (GtxUdpDeviceInfo) obj;
        return new EqualsBuilder().
            appendSuper(super.equals(obj)).
            append(address, gtxDeviceInfo.address).
            isEquals();
    }
}
