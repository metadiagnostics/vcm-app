/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext;

import io.saperi.vcm.event.HardwareListenerActivationRequestEvent;
import io.saperi.vcm.event.HardwareListenerDeactivationRequestEvent;
import io.saperi.vcm.event.HardwareStatusEvent;
import org.springframework.context.event.EventListener;

/**
 * Interface that acts as a listener of
 * {@link HardwareListenerActivationRequestEvent} and
 * {@link HardwareListenerActivationRequestEvent} events.
 * <p>
 * It is just enough to implement this interface to start receiving the
 * mentioned events.
 * </p>
 * <p>
 * Because the invocation of this interface will happen synchronously, and
 * because more than one listener could be involved, the implementation of these
 * interfaces should NEVER throw an exception. In the case of failure it is
 * recommended to publish an {@link HardwareStatusEvent}.
 * </p>
 *
 * @author esteban
 */
public interface HardwareEventListenerService {

    @EventListener
    void onHardwareListenerActivationRequestEvent(HardwareListenerActivationRequestEvent request);

    @EventListener
    void onHardwareListenerDeactivationRequestEvent(HardwareListenerDeactivationRequestEvent request);

}
