/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.pulse;

import com.kitware.physiology.cdm.Patient;
import com.kitware.physiology.datamodel.patient.assessments.SEComprehensiveMetabolicPanel;
import com.kitware.physiology.datamodel.properties.CommonUnits;
import io.saperi.pulse.sim.api.event.SimulationDataEvent;
import io.saperi.pulse.sim.api.event.adapter.SimulationDataEventListenerAdapter;
import io.saperi.pulse.sim.api.service.SimulationService;
import io.saperi.vcm.event.HardwareBloodGasDataEvent;
import io.saperi.vcm.event.HardwareDemographicsEvent;
import io.saperi.vcm.event.HardwareEvent;
import io.saperi.vcm.event.HardwareVentilatorEvent;
import io.saperi.vcm.event.HardwareVitalsEvent;
import io.saperi.vcm.event.ScalarPointValue;
import io.saperi.vcm.event.ScalarValue;
import io.saperi.vcm.util.ConvexHull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;

import javax.cache.Cache;

/**
 * Converts Pulse Data ({@link SimulationDataEvent}) into VCM format
 * ({@link HardwareEvent}).
 *
 * @author esteban
 */
public class PulseSimulationDataEventListener extends SimulationDataEventListenerAdapter {

    private static final Logger log = LoggerFactory.getLogger(PulseSimulationDataEventListener.class);

    private static final int MAX_RECURRENT_LOGS = 50;
    private int logCount = 0;

    private static final long HARDWARE_EVENT_RATE = -1;
    private long lastHardwareEventTimestamp = 0;

    private final String simulationName;
    private final SimulationService pulseSimulationService;
    private final ApplicationEventPublisher applicationEventPublisher;
    private final long bloodGasCalculationPeriod;
    private long lastBloodGasCalculationTimestamp = 0;

    private final long loopsCalculationPeriod = 40000;
    private long lastLoopsCalculationTimestamp = 0;
    private List<ScalarPointValue> pvLoopValues = new ArrayList<>();
    private List<ScalarPointValue> fvLoopValues = new ArrayList<>();
    private Cache<String, List<HardwareEvent>> eventsCache;

    private class HardwareDataEvents {

        boolean forcePushToClient;
        List<HardwareEvent> events;
    }

    public PulseSimulationDataEventListener(
        String simulationName,
        SimulationService pulseSimulationService,
        ApplicationEventPublisher applicationEventPublisher,
        Cache<String, List<HardwareEvent>> eventsCache) {

        this(simulationName, pulseSimulationService, applicationEventPublisher, 15000, eventsCache);
    }

    public PulseSimulationDataEventListener(
        String simulationName,
        SimulationService pulseSimulationService,
        ApplicationEventPublisher applicationEventPublisher,
        long bloodGasCalculationPeriod,
        Cache<String, List<HardwareEvent>> eventsCache) {

        this.simulationName = simulationName;
        this.pulseSimulationService = pulseSimulationService;
        this.applicationEventPublisher = applicationEventPublisher;
        this.bloodGasCalculationPeriod = bloodGasCalculationPeriod;
        this.eventsCache =  eventsCache;

    }

    public HardwareBloodGasDataEvent toHardwareBloodGasDataEvent(String simulationName, SimulationDataEvent e) {

        SEComprehensiveMetabolicPanel assessment = pulseSimulationService.performPatientAssessment(simulationName, new SEComprehensiveMetabolicPanel());

        HardwareBloodGasDataEvent event = new HardwareBloodGasDataEvent(simulationName, e.getSimulationName());
        event.setDataTimestamp(e.getSimulationTimestamp());
        event.setPh(getScalarValue(e.getData(), "BloodPH", "", 2));
        event.setPao2(getScalarValue(e.getData(), "ArterialOxygenPressure", "mmHg", 1));
        event.setPaco2(getScalarValue(e.getData(), "ArterialCarbonDioxidePressure", "mmHg", 1));
        event.setFio2(getPercentageValue(e.getData(), "OxygenFraction"));
        event.setO2sat(getPercentageValue(e.getData(), "OxygenSaturation"));
        //Warning: there is a bug in Pulse that prevent us to use a unit other thatn mol/L for the CO2. If we need to change it, we will have
        //to do the conversion here.
        event.setCo2(new ScalarValue(assessment.getCO2().getValue(CommonUnits.AmountPerVolumeUnit.mol_Per_L), "mol/L", 1));
        event.putInCache(eventsCache);
        return event;
    }

    @Override
    public void onSimulationDataEvent(SimulationDataEvent e) {
        long now = System.currentTimeMillis();
        HardwareDataEvents hardwareEvents = toHardwareDataEvents(simulationName, e);
        if (HARDWARE_EVENT_RATE < 0 || hardwareEvents.forcePushToClient || (now - lastHardwareEventTimestamp >= HARDWARE_EVENT_RATE)) {
            lastHardwareEventTimestamp = now;
            for (HardwareEvent hardwareDataEvent : hardwareEvents.events) {
                applicationEventPublisher.publishEvent(hardwareDataEvent);
            }
        }
    }

    /**
     * Converts a {@link SimulationDataEvent} from Pulse into multiple
     * {@link HardwareEvent HardwareEvents}.
     *
     * @param simulationName the name of the simulation
     * @param e              the event to be converted.
     * @return the converted events based on the requested capabilities.
     */
    private HardwareDataEvents toHardwareDataEvents(String simulationName, SimulationDataEvent e) {

        HardwareDataEvents results = new HardwareDataEvents();
        results.events = new ArrayList<>();

        if ((e.getSimulationTimestamp() - lastBloodGasCalculationTimestamp) < -2000) {
            //TODO: find a better way to restart these counters when the simulation is
            //restarted.
            lastBloodGasCalculationTimestamp = 0;
            lastLoopsCalculationTimestamp = 0;
        }

        //DeviceCapabilityType.BLOOD_GAS_ANALYZER
        if (lastBloodGasCalculationTimestamp == 0 || (e.getSimulationTimestamp() - lastBloodGasCalculationTimestamp) >= bloodGasCalculationPeriod) {
            lastBloodGasCalculationTimestamp = e.getSimulationTimestamp();
            results.events.add(toHardwareBloodGasDataEvent(simulationName, e));
            results.forcePushToClient = true;
        }

        //DeviceCapabilityType.DEMOGRAPHICS
        HardwareDemographicsEvent demographicsEvent = new HardwareDemographicsEvent(simulationName, e.getSimulationName());
        ScalarValue height = getScalarValue(e.getData(), "Height", "in", 0);
        ScalarValue weight = getScalarValue(e.getData(), "Weight", "lb", 0);
        String gender = getStringValue(e.getData(), "Sex");
        if (gender == null) {
            warn("The gender of the Patient could not be determined. We are assuming it is a female.");
            gender = Patient.PatientData.eSex.Female.toString();
        }
        ScalarValue pbw = getPbw(gender, height);
        demographicsEvent.setDataTimestamp(e.getSimulationTimestamp());
        demographicsEvent.setHeight(height);
        demographicsEvent.setGender(gender);
        demographicsEvent.setPbw(pbw);
        demographicsEvent.setWeight(weight);
        demographicsEvent.putInCache(eventsCache);
        results.events.add(demographicsEvent);

        //DeviceCapabilityType.VITALS
        HardwareVitalsEvent vitalsEvent = new HardwareVitalsEvent(simulationName, e.getSimulationName());
        vitalsEvent.setDataTimestamp(e.getSimulationTimestamp());
        vitalsEvent.setRate(getScalarValue(e.getData(), "RespirationRate", "1/min", 0));
        vitalsEvent.setSat(getPercentageValue(e.getData(), "OxygenSaturation"));
        vitalsEvent.setHr(getScalarValue(e.getData(), "HeartRate", "1/min", 0));
        vitalsEvent.setSbp(getScalarValue(e.getData(), "SystolicArterialPressure", "mmHg", 0));
        vitalsEvent.setDbp(getScalarValue(e.getData(), "DiastolicArterialPressure", "mmHg", 0));
        vitalsEvent.setCvp(getScalarValue(e.getData(), "CentralVenousPressure", "mmHg", 0));
        vitalsEvent.setTv(getScalarValue(e.getData(), "TidalVolume", "L", 2));
        vitalsEvent.setEtco2(getScalarValue(e.getData(), "EndTidalCarbonDioxidePressure", "mmHg", 0));
        // I need to send this value in this set of values because I need TV and weight at same time through WS
        vitalsEvent.setWeight(weight);
        vitalsEvent.setTemp(getScalarValue(e.getData(), "CoreTemperature", "C", 1));
        vitalsEvent.putInCache(eventsCache);
        results.events.add(vitalsEvent);

        //DeviceCapabilityType.VENTILATOR
        List<ScalarPointValue> pvLoopValuesToSend = new ArrayList<>();
        List<ScalarPointValue> fvLoopValuesToSend = new ArrayList<>();

        // PV and FV Loops calculation
        if (lastLoopsCalculationTimestamp == 0 || (e.getSimulationTimestamp() - lastLoopsCalculationTimestamp) >= loopsCalculationPeriod) {
            lastLoopsCalculationTimestamp = e.getSimulationTimestamp();

            // Is time to send whatever data we have collected so far and to ask the
            // engine for a full Exhale/Inhale loop.
            if (!pvLoopValues.isEmpty()) {
                pvLoopValuesToSend = ConvexHull.convexHull(pvLoopValues, true);
                results.forcePushToClient = true;
            }
            pvLoopValues = new ArrayList<>();

            if (!fvLoopValues.isEmpty()) {
                fvLoopValuesToSend = ConvexHull.convexHull(fvLoopValues, true);
                results.forcePushToClient = true;
            }
            fvLoopValues = new ArrayList<>();

            //pulseSimulationService.applyAction(simulationName, PulseUtils.createConsciousRespirationAction(10, 10, 10, 10));

        } else {
            //Collect data for the PV and FV Loops
            pvLoopValues.add(new ScalarPointValue(
                getScalarValue(e.getData(), "IntrapleuralPressure", "cmH2O", 2),
                getScalarValue(e.getData(), "TotalLungVolume", "L", 2)
            ));

            fvLoopValues.add(new ScalarPointValue(
                getScalarValue(e.getData(), "TotalLungVolume", "L", 2),
                getScalarValue(e.getData(), "ExpiratoryFlow", "L/s", 2)
            ));
        }

        HardwareVentilatorEvent ventilatorDataEvent = new HardwareVentilatorEvent(simulationName, e.getSimulationName());
        ventilatorDataEvent.setDataTimestamp(e.getSimulationTimestamp());
        ventilatorDataEvent.setPip(getScalarValue(e.getData(), "VentilatorPressure", "cmH2O", 0));
        ventilatorDataEvent.setPeep(getScalarValue(e.getData(), "PositiveEndExpiredPressure", "cmH2O", 0));
        ventilatorDataEvent.setRate(getScalarValue(e.getData(), "RespiratoryRate", "1/min", 0));
        ventilatorDataEvent.setIt(null);
        ventilatorDataEvent.setFio2(getPercentageValue(e.getData(), "OxygenFraction"));
        ventilatorDataEvent.setFlow(getScalarValue(e.getData(), "InletFlow", "L/min", 0));
        ventilatorDataEvent.setPvLoop(pvLoopValuesToSend);
        ventilatorDataEvent.setFvLoop(fvLoopValuesToSend);
        ventilatorDataEvent.setIp(getScalarValue(e.getData(), "IntrapleuralPressure", "cmH2O", 2));
        ventilatorDataEvent.setEf(getScalarValue(e.getData(), "ExpiratoryFlow", "L/s", 2));
        results.events.add(ventilatorDataEvent);
        ventilatorDataEvent.putInCache(eventsCache);
        return results;
    }

    private static ScalarValue getPbw(String gender, ScalarValue height) {
        double value = 2.3 * (height.getValue() - 60.0);
        if (Patient.PatientData.eSex.Male.toString().equals(gender)) {
            value += 50.0;
        } else if (Patient.PatientData.eSex.Female.toString().equals(gender)) {
            value += 45.5;
        } else {
            throw new IllegalArgumentException(String.format("Unknown gender '%s'", gender));
        }
        return new ScalarValue(value, "Kg", 2);
    }

    private static ScalarValue getScalarValue(Map<String, Object> data, String key, String unit, int precision) {
        Object value = data.get(key);
        if (value == null) {
            return null;
        }
        return new ScalarValue((double) value, unit, precision);
    }

    private static ScalarValue getPercentageValue(Map<String, Object> data, String key) {
        Object value = data.get(key);
        if (value == null) {
            return null;
        }
        return new ScalarValue(((double) value * 100), "%", 0);
    }

    private static String getStringValue(Map<String, Object> data, String key) {
        Object value = data.get(key);
        return value == null ? null : value.toString();
    }

    private void warn(String message, Object... arguments) {
        if (logCount == 0 || logCount++ % MAX_RECURRENT_LOGS == 0) {
            log.warn(message, arguments);
            logCount = 1;
        }
    }

}
