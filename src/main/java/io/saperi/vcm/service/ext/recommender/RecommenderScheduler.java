/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.recommender;

import io.saperi.vcm.domain.AssignedDevice;
import io.saperi.vcm.domain.Assignment;
import io.saperi.vcm.domain.enumeration.DeviceCapabilityType;
import io.saperi.vcm.domain.enumeration.VentilatorControlType;
import io.saperi.vcm.domain.enumeration.VentilatorModeType;
import io.saperi.vcm.domain.ext.VentilatorConfigurationChangeRequest;
import io.saperi.vcm.domain.ext.VentilatorRecommendationResponse;
import io.saperi.vcm.event.VentilatorConfigurationChangeRequestEvent;
import io.saperi.vcm.service.ext.ExtendedAssignmentService;
import io.saperi.vcm.service.ext.recommender.impl.CloudAiVentilatorRecommenderService;
import io.saperi.vcm.service.ext.recommender.impl.CloudRuleBasedVentilatorRecommenderService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author esteban
 */
@Component
public class RecommenderScheduler {

    public static final String REQUESTER = "1";

    private static final Logger log = LoggerFactory.getLogger(RecommenderScheduler.class);

    private final ExtendedAssignmentService extendedAssignmentService;
    private final CloudRuleBasedVentilatorRecommenderService ruleBasedrecommenderService;
    private final CloudAiVentilatorRecommenderService aIrecommenderService;
    private final ApplicationEventPublisher applicationEventPublisher;

    public RecommenderScheduler(
        ExtendedAssignmentService extendedAssignmentService,
        CloudRuleBasedVentilatorRecommenderService ruleBasedrecommenderService,
        CloudAiVentilatorRecommenderService aIrecommenderService,
        ApplicationEventPublisher applicationEventPublisher) {
        this.extendedAssignmentService = extendedAssignmentService;
        this.ruleBasedrecommenderService = ruleBasedrecommenderService;
        this.aIrecommenderService = aIrecommenderService;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Scheduled(cron = "${vcm.ventilator-recommender.rate}")
    public void schedule() {

        log.debug("Checking for Assignments configured for Ventilator Recommendations");
        List<Assignment> assignments = this.extendedAssignmentService.findAllActiveWithVentilatorModeTypes(new VentilatorModeType[]{VentilatorModeType.OPEN, VentilatorModeType.CLOSED});
        log.debug("Assignments found: {}", assignments == null ? 0 : assignments.size());
        if (assignments != null) {

            //TODO: use an executor?
            assignments.parallelStream().forEach(assignment -> {
                log.trace("Asking for a Ventilation Recommendation for Assignment {}", assignment.getId());

                VentilatorRecommendationResponse recommendation;
                switch (assignment.getVentilatorRecommenderType()) {
                    case AI:
                        recommendation = aIrecommenderService.createRecommendation(assignment, REQUESTER);
                        break;
                    case RULE_BASED:
                        recommendation = ruleBasedrecommenderService.createRecommendation(assignment, REQUESTER);
                        break;
                    default:
                        return;
                }

                if (recommendation == null) {
                    log.warn("No recommendation could be created for Assignment {}", assignment.getId());
                    return;
                }

                if (assignment.getVentilatorMode() == VentilatorModeType.CLOSED) {
                    log.trace("The Ventilator Mode in Assignment {} was CLOSED LOOP. The recommendation will be applied", assignment.getId());
                    //TODO: base this in a Strategy
                    VentilatorConfigurationChangeRequest changeRequest = new VentilatorConfigurationChangeRequest();
                    changeRequest.setFio2(recommendation.getFio2());
                    //changeRequest.setInspiratoryExpiratoryRatio(recommendation.get);
                    changeRequest.setPeep(recommendation.getPeep());
                    changeRequest.setRate(recommendation.getRate());
                    changeRequest.setPip(recommendation.getPip());
                    if (VentilatorControlType.VOLUME.equals(assignment.getVentilatorControlType())){
                        changeRequest.setTv(recommendation.getTv());
                    }
                    for (AssignedDevice device : assignment.getDevices()) {
                        if (device.getCapability().getType() == DeviceCapabilityType.VENTILATOR_CONTROL) {
                            applicationEventPublisher.publishEvent(new VentilatorConfigurationChangeRequestEvent(device.getDevice(), changeRequest, assignment.getId(), REQUESTER));
                        }
                    }
                }
            });
        }
    }

}
