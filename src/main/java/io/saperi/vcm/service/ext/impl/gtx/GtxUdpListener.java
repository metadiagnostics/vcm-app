/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.gtx;

import com.athenagtx.framing.FrameReader;
import io.saperi.gtx.sample.SocketUtils;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;

/**
 * Listener for GTX Device data that is coming from UDP. This class keeps a
 * cache with the information about the devices.
 *
 * @author esteban
 */
public class GtxUdpListener extends GtxAbstractListener implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(GtxUdpListener.class);

    private final DatagramSocket socket;

    private boolean running;
    private ThreadLocal<InetAddress> currentPacketAddress = new ThreadLocal<>();

    public GtxUdpListener(
        ApplicationEventPublisher applicationEventPublisher,
        GtxDevicesCache cache,
        DatagramSocket socket) {
        super(applicationEventPublisher, cache);
        this.socket = socket;
    }

    @Override
    public void run() {

        log.debug("Starting GTX UDP Listener");

        running = true;

        while (running) {
            DatagramPacket packet = SocketUtils.readDatagram(socket);

            log.trace("Read {} bytes from {}: {}", packet.getLength(), packet.getAddress(), packet.getPort());
            currentPacketAddress.set(packet.getAddress());

            FrameReader reader = new FrameReader(this::processFrame);
            reader.read(packet.getData(), packet.getOffset(), packet.getLength());
        }
        log.debug("GTX UDP Listener was Stopped");
    }

    @Override
    protected GtxDeviceInfo createGtxDeviceInfo() {
        return new GtxUdpDeviceInfo(currentPacketAddress.get());
    }

    public void stop() {
        running = false;
    }

}
