/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.mock;

import static io.saperi.vcm.constant.MessageConstants.MSG_VENTILATOR_CHANGES_APPLIED;

import io.saperi.vcm.domain.enumeration.DeviceType;
import io.saperi.vcm.event.HardwareMessageEvent;
import io.saperi.vcm.event.VentilatorConfigurationChangeRequestEvent;
import io.saperi.vcm.service.ext.recommender.VentilatorConfigurationChangeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

/**
 * This class is in charge of listen to
 * {@link VentilatorConfigurationChangeRequestEvent} events and change the
 * configuration of a MOCK Ventilator. Given that a MOCK ventilator cannot be
 * configured, this class does nothing.
 *
 * @author esteban
 */
@Service
public class MockVentilatorConfigurationChangeService implements VentilatorConfigurationChangeService {

    private static final Logger log = LoggerFactory.getLogger(MockVentilatorConfigurationChangeService.class);

    private final ApplicationEventPublisher applicationEventPublisher;

    public MockVentilatorConfigurationChangeService(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    public void changeVentilator(VentilatorConfigurationChangeRequestEvent request) {
        if (request.getDevice().getModel().getType() != DeviceType.MOCK) {
            //not mine
            return;
        }

        applicationEventPublisher.publishEvent(new HardwareMessageEvent(request.getDevice(), MSG_VENTILATOR_CHANGES_APPLIED));
    }

}
