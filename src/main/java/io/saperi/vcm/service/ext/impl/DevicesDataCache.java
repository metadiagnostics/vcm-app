/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */


package io.saperi.vcm.service.ext.impl;

import io.saperi.vcm.event.DeviceModificationEvent;
import io.saperi.vcm.event.HardwareBloodGasDataEvent;
import io.saperi.vcm.event.HardwareDemographicsEvent;
import io.saperi.vcm.event.HardwareStatusEvent;
import io.saperi.vcm.event.HardwareVentilatorEvent;
import io.saperi.vcm.event.HardwareVitalsEvent;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * Class that serves as an in-memory cache for information about Devices. This
 * class is not a cache of Devices themselves, but about data that is associated
 * with them.
 * This class listens to different events in the system to try to
 * keep the information about devices updated.
 *
 * @author esteban
 */
@Component
public class DevicesDataCache {

    private static final Logger log = LoggerFactory.getLogger(DevicesDataCache.class);

    /**
     * This class keeps the latest data events for a specific Device.
     */
    public static class DeviceData {

        private final String hardwareId;
        private HardwareStatusEvent lastStatus;
        private HardwareDemographicsEvent lastDemographics;
        private HardwareVitalsEvent lastVitals;
        private HardwareVentilatorEvent lastVentilatorData;
        private HardwareBloodGasDataEvent lastBloodGases;

        public DeviceData(String hardwareId) {
            this.hardwareId = hardwareId;
        }

        public String getHardwareId() {
            return hardwareId;
        }

        public HardwareStatusEvent getLastStatus() {
            return lastStatus;
        }

        public void setLastStatus(HardwareStatusEvent lastStatus) {
            this.lastStatus = lastStatus;
        }

        public HardwareDemographicsEvent getLastDemographics() {
            return lastDemographics;
        }

        public void setLastDemographics(HardwareDemographicsEvent lastDemographics) {
            this.lastDemographics = lastDemographics;
        }

        public HardwareVitalsEvent getLastVitals() {
            return lastVitals;
        }

        public void setLastVitals(HardwareVitalsEvent lastVitals) {
            this.lastVitals = lastVitals;
        }

        public HardwareVentilatorEvent getLastVentilatorData() {
            return lastVentilatorData;
        }

        public void setLastVentilatorData(HardwareVentilatorEvent lastVentilatorData) {
            this.lastVentilatorData = lastVentilatorData;
        }

        public HardwareBloodGasDataEvent getLastBloodGases() {
            return lastBloodGases;
        }

        public void setLastBloodGases(HardwareBloodGasDataEvent lastBloodGases) {
            this.lastBloodGases = lastBloodGases;
        }
    }

    private final Map<String, DeviceData> data = new ConcurrentHashMap<>();

    public DeviceData getData(String hardwareId) {
        return data.get(hardwareId);
    }

    public DeviceData clearCache(String hardwareId) {
        DeviceData d = data.remove(hardwareId);
        log.trace("Data Cache for Device '{}' was cleaned up.", hardwareId);
        return d;
    }

    /**
     * When a Device is deleted, we remove any data we may hold for it.
     *
     * @param event the event.
     */
    @EventListener
    public void onDeviceModificationEvent(DeviceModificationEvent event) {
        if (event.getOperation() == DeviceModificationEvent.Operation.DELETE) {
            clearCache(event.getOldHardwareId());
        }
    }

    @EventListener
    public void onHardwareStatusEvent(HardwareStatusEvent event) {
        DeviceData d = getDataOrDefault(event.getHardwareId());
        HardwareStatusEvent lastStatus = d.getLastStatus();
        if (lastStatus == null || lastStatus.getTimestamp() < event.getTimestamp()) {
            d.setLastStatus(event);
            log.trace("Status Data for Device '{}' was updated.", event.getHardwareId());
        }
    }

    @EventListener
    public void onHardwareDemographicsEvent(HardwareDemographicsEvent event) {
        DeviceData d = getDataOrDefault(event.getHardwareId());
        HardwareDemographicsEvent lastData = d.getLastDemographics();
        if (lastData == null || lastData.getTimestamp() < event.getTimestamp()) {
            d.setLastDemographics(event);
            log.trace("Demographics Data for Device '{}' was updated.", event.getHardwareId());
        }
    }

    @EventListener
    public void onHardwareVitalsEvent(HardwareVitalsEvent event) {
        DeviceData d = getDataOrDefault(event.getHardwareId());
        HardwareVitalsEvent lastData = d.getLastVitals();
        if (lastData == null || lastData.getTimestamp() < event.getTimestamp()) {
            d.setLastVitals(event);
            log.trace("Vitals Data for Device '{}' was updated.", event.getHardwareId());
        }
    }

    @EventListener
    public void onHardwareVentilatorEvent(HardwareVentilatorEvent event) {
        DeviceData d = getDataOrDefault(event.getHardwareId());
        HardwareVentilatorEvent lastData = d.getLastVentilatorData();
        if (lastData == null || lastData.getTimestamp() < event.getTimestamp()) {
            d.setLastVentilatorData(event);
            log.trace("Ventilator Data for Device '{}' was updated.", event.getHardwareId());
        }
    }

    @EventListener
    public void onHardwareBloodGasDataEvent(HardwareBloodGasDataEvent event) {
        DeviceData d = getDataOrDefault(event.getHardwareId());
        HardwareBloodGasDataEvent lastData = d.getLastBloodGases();
        if (lastData == null || lastData.getTimestamp() < event.getTimestamp()) {
            d.setLastBloodGases(event);
            log.trace("Blood Gas Data for Device '{}' was updated.", event.getHardwareId());
        }
    }

    private DeviceData getDataOrDefault(String hardwareId) {
        return data.computeIfAbsent(hardwareId, k -> new DeviceData(k));
    }
}
