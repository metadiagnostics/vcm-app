/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.pulse;

import static io.saperi.vcm.constant.ExtendedErrorConstants.ERR_VENTILATOR_CHANGES_ERROR;
import static io.saperi.vcm.constant.MessageConstants.MSG_VENTILATOR_CHANGES_APPLIED;

import com.kitware.physiology.datamodel.properties.SEScalar;
import com.kitware.physiology.datamodel.properties.SEScalar0To1;
import com.kitware.physiology.datamodel.system.equipment.anesthesia.actions.SEAnesthesiaMachineConfiguration;
import io.saperi.pulse.sim.api.service.SimulationService;
import io.saperi.vcm.domain.enumeration.DeviceType;
import io.saperi.vcm.event.HardwareMessageEvent;
import io.saperi.vcm.event.ScalarValue;
import io.saperi.vcm.event.VentilatorConfigurationChangeRequestEvent;
import io.saperi.vcm.service.ext.recommender.VentilatorConfigurationChangeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

/**
 * This class is in charge of listen to
 * {@link VentilatorConfigurationChangeRequestEvent} events and change the
 * configuration of a Ventilator in Pulse.
 *
 * @author esteban
 */
@Service
public class PulseVentilatorConfigurationChangeService implements VentilatorConfigurationChangeService {

    private static final Logger log = LoggerFactory.getLogger(PulseEventListenerService.class);

    private final SimulationService client;

    private final ApplicationEventPublisher applicationEventPublisher;

    public PulseVentilatorConfigurationChangeService(
        SimulationService client,
        ApplicationEventPublisher applicationEventPublisher) {
        this.client = client;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    public void changeVentilator(VentilatorConfigurationChangeRequestEvent request) {
        if (request.getDevice().getModel().getType() != DeviceType.PULSE) {
            //not mine
            return;
        }

        //Create a SEAnesthesiaMachineConfiguration and copy the values from the request.
        SEAnesthesiaMachineConfiguration config = new SEAnesthesiaMachineConfiguration();
        setSEScalar(request.getChanges().getFio2(), config.getConfiguration().getOxygenFraction());
        setSEScalar(request.getChanges().getInspiratoryExpiratoryRatio(), config.getConfiguration().getInspiratoryExpiratoryRatio());
        setSEScalar(request.getChanges().getPeep(), config.getConfiguration().getPositiveEndExpiredPressure());
        setSEScalar(request.getChanges().getPip(), config.getConfiguration().getVentilatorPressure());
        setSEScalar(request.getChanges().getRate(), config.getConfiguration().getRespiratoryRate());

        //Send the action to Pulse
        try {
            client.applyAction(request.getDevice().getHardwareId(), config);

            //Notify that the action was applied
            applicationEventPublisher.publishEvent(new HardwareMessageEvent(request.getDevice(), MSG_VENTILATOR_CHANGES_APPLIED));
        } catch (Exception e) {
            log.error("Error sending ventilator action to Pulse", e);
            //Notify about the error
            applicationEventPublisher.publishEvent(new HardwareMessageEvent(request.getDevice(), HardwareMessageEvent.Severity.ERROR, ERR_VENTILATOR_CHANGES_ERROR)
                .addParameter("msg", e.getMessage())
            );
        }
    }

    private void setSEScalar(ScalarValue src, SEScalar tgt) {
        if (src == null || src.getValue() == null || tgt == null) {
            //do not modify tgt
            return;
        }
        tgt.setValue(src.getValue(), src.getUnit());
    }

    private void setSEScalar(ScalarValue src, SEScalar0To1 tgt) {
        if (src == null || src.getValue() == null || tgt == null) {
            //do not modify tgt
            return;
        }

        double value = src.getValue() > 1 ? src.getValue() / 100.0 : src.getValue();
        tgt.setValue(value);
    }

}
