/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.mock;

import io.saperi.vcm.domain.enumeration.DeviceType;
import io.saperi.vcm.event.*;
import io.saperi.vcm.service.ext.ExtendedDeviceService;
import io.saperi.vcm.service.ext.HardwareEventListenerServiceAdapter;
import io.saperi.vcm.util.CacheUtil;
import io.saperi.vcm.util.ConvexHull;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import javax.annotation.PostConstruct;
import javax.cache.Cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Class that creates random values (without any clinical value whatsoever) and
 * makes them available to listeners. This class is used during development to
 * avoid having to contact real hardware or external services.
 *
 * @author esteban
 */
@Service
public class MockHardwareEventListenerService extends HardwareEventListenerServiceAdapter {
    @Value("${vcm.ventilator-recommender.mock.height.value:70.0}")
    private String height;

    @Value("${vcm.ventilator-recommender.mock.height.enabled:true}")
    private Boolean heightEnabled;

    private static final Logger log = LoggerFactory.getLogger(MockHardwareEventListenerService.class);

    private final ApplicationEventPublisher applicationEventPublisher;
    private final Random random = new Random();

    private final Set<String> knownHardwareIds = new CopyOnWriteArraySet<>();

    private final long bloodGasCalculationPeriod = 15000;
    private long lastBloodGasCalculationTimestamp = 0;

    private final long loopsCalculationPeriod = 40000;
    private long lastLoopsCalculationTimestamp = 0;
    private List<ScalarPointValue> pvLoopValues = new ArrayList<>();
    private List<ScalarPointValue> fvLoopValues = new ArrayList<>();

    public MockHardwareEventListenerService(ExtendedDeviceService extendedDeviceService,
                                            ApplicationEventPublisher applicationEventPublisher) {
        super(extendedDeviceService, applicationEventPublisher);
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @PostConstruct
    public void postConstruct() {
        super.eventsCache = CacheUtil.eventsCache();
        registerExistingDevices();
    }

    @Override
    protected void internalHardwareListenerActivationRequestEvent(HardwareListenerActivationRequestEvent request) {
        //A Mock device is always connected
        applicationEventPublisher.publishEvent(new HardwareStatusEvent(request.getHardwareId(), HardwareStatusEvent.Status.CONNECTED, ""));
    }

    @Override
    protected void internalOnHardwareListenerDeactivationRequestEvent(HardwareListenerDeactivationRequestEvent request) {
    }

    @Override
    protected DeviceType[] getTypes() {
        return new DeviceType[] {DeviceType.MOCK};
    }

    @Override
    protected void registerListener(String hardwareId, DeviceType deviceType) {
        knownHardwareIds.add(hardwareId);
    }

    @Override
    protected void unregisterListener(String hardwareId) {
        knownHardwareIds.remove(hardwareId);
    }

    @Scheduled(initialDelay = 1000, fixedRate = 1000)
    public void createEvents() {
        boolean calculateBloodGases = false;
        long now = System.currentTimeMillis();
        if (lastBloodGasCalculationTimestamp == 0 || (now - lastBloodGasCalculationTimestamp) >= bloodGasCalculationPeriod) {
            lastBloodGasCalculationTimestamp = now;
            calculateBloodGases = true;
        }
        boolean includeLoops = false;
        if (lastLoopsCalculationTimestamp == 0 || (now - lastLoopsCalculationTimestamp) >= loopsCalculationPeriod) {
            lastLoopsCalculationTimestamp = now;
            includeLoops = true;
        }
        for (String hardwareId : knownHardwareIds) {
            // BLOOD_GAS_ANALYZER:
            if (calculateBloodGases) {
                applicationEventPublisher.publishEvent(createRandomHardwareBloodGasDataEvent(hardwareId));
            }
            // DEMOGRAPHICS:
            applicationEventPublisher.publishEvent(createRandomHardwareDemographicsEvent(hardwareId));
            // VITALS:
            applicationEventPublisher.publishEvent(createRandomHardwareVitalsEvent(hardwareId));
            // VENTILATOR:
            applicationEventPublisher.publishEvent(createRandomHardwareVentilatorEvent(hardwareId, includeLoops));

        }

        if (includeLoops) {
            pvLoopValues = new ArrayList<>();
            fvLoopValues = new ArrayList<>();
        }
    }

    private HardwareDemographicsEvent createRandomHardwareDemographicsEvent(String hardwareId) {
        HardwareDemographicsEvent event = new HardwareDemographicsEvent(hardwareId, hardwareId);
        event.setDataTimestamp(System.currentTimeMillis());
        String defHeight = "70.4724";
        if (heightEnabled) {
            defHeight = height;
        }
        event.setHeight(new ScalarValue(Double.valueOf(defHeight), "in", 0));
        event.setGender("Female");
        event.setPbw(new ScalarValue(69.59, "Kg", 2));
        return event;
    }

    private HardwareVitalsEvent createRandomHardwareVitalsEvent(String hardwareId) {
        HardwareVitalsEvent event = new HardwareVitalsEvent(hardwareId, hardwareId);
        event.setDataTimestamp(System.currentTimeMillis());
        event.setRate(getIntValue(12, 18, "1/min"));
        event.setSat(getIntValue(94, 98, "%"));
        event.setHr(getIntValue(70, 80, "1/min")); //
        event.setSbp(getIntValue(130, 140, "mmHg"));
        event.setDbp(getIntValue(80, 90, "mmHg"));
        event.setCvp(getIntValue(0, 4, "mmHg"));
        event.setTv(getIntValue(400, 450, "cc"));
        event.setEtco2(getIntValue(40, 48, "mmHg"));
        event.setWeight(new ScalarValue(70.5, "Kg", 2));
        event.setTemp(getDoubleValue(36.0, 38.9, "C", 1));
        return event;
    }

    private HardwareVentilatorEvent createRandomHardwareVentilatorEvent(String hardwareId, boolean includeLoops) {
        HardwareVentilatorEvent event = new HardwareVentilatorEvent(hardwareId, hardwareId);
        event.setDataTimestamp(System.currentTimeMillis());
        event.setPip(getIntValue(0, 40, "cmH2O"));
        event.setPeep(getIntValue(0, 24, "cmH2O"));
        event.setRate(getIntValue(10, 20, "1/min"));
        event.setIt(null);
        event.setFio2(getIntValue(21, 100, "%"));
        event.setFlow(getIntValue(60, 120, "L/min"));
        event.setIp(getIntValue(10, 55, "cmH2O"));
        event.setEf(getIntValue(60, 100, "L/s"));
        event.setVentMode(new ScalarValue(3.0, "", 0));

        List<ScalarPointValue> pvLoopValuesToSend = new ArrayList<>();
        List<ScalarPointValue> fvLoopValuesToSend = new ArrayList<>();

        // PV and FV Loops calculation
        if (includeLoops) {
            lastLoopsCalculationTimestamp = event.getDataTimestamp();

            // Is time to send whatever data we have collected so far and to ask the
            // engine for a full Exhale/Inhale loop.
            if (!pvLoopValues.isEmpty()) {
                pvLoopValuesToSend = ConvexHull.convexHull(pvLoopValues, true);
            }
            if (!fvLoopValues.isEmpty()) {
                fvLoopValuesToSend = ConvexHull.convexHull(fvLoopValues, true);
            }
        } else {
            //Collect data for the PV and FV Loops
            pvLoopValues.add(new ScalarPointValue(
                getIntValue(1, 10, ""),
                getIntValue(1, 10, "")
            ));

            fvLoopValues.add(new ScalarPointValue(
                getIntValue(1, 10, ""),
                getIntValue(1, 10, "")
            ));
        }

        event.setPvLoop(pvLoopValuesToSend);
        event.setFvLoop(fvLoopValuesToSend);
        return event;
    }

    private HardwareBloodGasDataEvent createRandomHardwareBloodGasDataEvent(String hardwareId) {
        HardwareBloodGasDataEvent event = new HardwareBloodGasDataEvent(hardwareId, hardwareId);
        event.setDataTimestamp(System.currentTimeMillis());
        event.setPh(getDoubleValue(6.00, 8.00, ""));
        event.setPao2(getIntValue(80, 100, "mmHg"));
        event.setPaco2(getIntValue(35, 45, "mmHg"));
        event.setFio2(getIntValue(21, 100, "%"));
        event.setO2sat(getIntValue(2, 100, "%"));
        event.setCo2(getIntValue(30, 100, "mmHg"));
        return event;
    }

    private ScalarValue getIntValue(int min, int max, String unit) {
        return new ScalarValue(random.nextInt(max - min + 1) + min, unit, 0);
    }

    private ScalarValue getDoubleValue(double min, double max, String unit) {
        return getDoubleValue(min, max, unit, 2);
    }

    private ScalarValue getDoubleValue(double min, double max, String unit, int precision) {
        double value = min + random.nextDouble() * (max - min);
        return new ScalarValue(value, unit, precision);
    }
}
