/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.jaimsaggregator;

import io.saperi.virtualhealth.device.aggregators.common.managers.DataSetManager;

import java.util.*;

public class Mapping {

    public static HashMap<MapTargetType, HashMap<MapDataType, Set<String>>> targetMapping = new HashMap<>();
    public static HashMap<MapTargetType, HashMap<MapDataType, Set<String>>> targetSettingMapping = new HashMap<>();
    public static HashMap<String, MapData> metricMap = new HashMap<>();
    public static HashMap<String, MapData> settingMap = new HashMap<>();

    // Frames values do not need to be mapped here
    static {
        //Known Vitals
        addMetric("PRESS_BLD_NONINV_SYS", new MapData(MapDataType.SCALAR, MapTargetType.VITAL, "mmHg", 0, "Sbp"));
        addMetric("PRESS_BLD_NONINV_DIA", new MapData(MapDataType.SCALAR, MapTargetType.VITAL, "mmHg", 0, "Dbp"));
        addMetricByName("CONC_AWAY_CO2_ET", new MapData(MapDataType.SCALAR, MapTargetType.VITAL, "mmHg", 0, "Etco2"));
        addMetricByName("TEMP_GEN_1", new MapData(MapDataType.SCALAR, MapTargetType.VITAL, "C", 0, "Temp"));
        addSettingByName("VENT_VOL_TIDAL_INSP", new MapData(MapDataType.SCALAR, MapTargetType.VITAL, "L", 2, "Tv"));
        addMetricByName("PULS_OXIM_SAT_O2", new MapData(MapDataType.SCALAR, MapTargetType.VITAL, "%", 0, "Sat"));
        addMetricByName("PULS_OXIM_PULS_RATE", new MapData(MapDataType.SCALAR, MapTargetType.VITAL, "1/min", 0, "Hr"));


        //Not sure where we have the following
        //addMetricByName("PRESS_AWAY_MAX", new MapData(MapDataType.SCALAR, MapTargetType.VITAL, "cmH2O", 0, "Unknown"));


        //Vent Metric

        addMetricByName("VENT_FLOW_INSP", new MapData(MapDataType.SCALAR, MapTargetType.VENT, "1/min", 0, "Rate"));
        //Overlapping mapping - VENT_O2 TBI by DocBox
        addMetricByName("CONC_GASDLV_O2_INSP", new MapData(MapDataType.SCALAR, MapTargetType.VENT, "1/min", 0, "Fio2"));
        addMetricByName("MDCX_CONC_AWAY_O2_BASELINE", new MapData(MapDataType.SCALAR, MapTargetType.VENT, "1/min", 0, "Fio2"));


        //Known VENT
               /*
        event.setPip(new ScalarValue(data.getVentControl().getPip(), "cmH2O", 0));
        event.setPeep(new ScalarValue(data.getVentControl().getPeep(), "cmH2O", 0));
        event.setRate(new ScalarValue(data.getVentControl().getRr(), "1/min", 0));
        event.setIt(null);
        event.setFio2(new ScalarValue(data.getVitals().getEtco2().getFio2Percent(), "%", 0));
        event.setFlow(null);
        event.setPvLoop(null);
        event.setFvLoop(null);
        event.setIp(null); // No IntrapleuralPressure information on GTX device
        event.setEf(null); // No ExpiratoryFlow information on GTX device
        event.setTv(new ScalarValue(((double) data.getVentControl().getTv()) / 1000.0, "L", 2));

         */
        // Ventilator Settings


        addSettingByName("PRESS_AWAY_INSP_MAX", new MapData(MapDataType.SETTING, MapTargetType.VENT, "cmH2O", 0, "Pip"));

        //Temporary metric
        addSettingByName("PRESS_AWAY_END_EXP_POS_INTRINSIC", new MapData(MapDataType.SETTING, MapTargetType.VENT, "cmH2O", 0, "Peep"));
        addSettingByName("VENT_PRESS_AWAY_END_EXP_POS", new MapData(MapDataType.SETTING, MapTargetType.VENT, "cmH2O", 0, "Peep"));
        addSettingByName("VENT_RESP_RATE", new MapData(MapDataType.SETTING, MapTargetType.VENT, "1/min", 0, "Rate"));
        addSettingByName("CONC_GASDLV_O2_INSP", new MapData(MapDataType.SETTING, MapTargetType.VENT, "%", 0, "Fio2"));
        // Ventilator mode
        addSettingByName("VENT_MODE", new MapData(MapDataType.SETTING, MapTargetType.VENT, "", 0, "ventMode"));

        addSettingByName("VENT_VOL_TIDAL_INSP", new MapData(MapDataType.SETTING, MapTargetType.VENT, "L", 2, "Tv"));


    }

    public String metricId;
    public MapData data;


    public Mapping(String id, MapData data) {
        this.metricId = id;
        this.data = data;
    }

    private static void addSetting(String metricId, MapData mapping) {
        settingMap.put(metricId, mapping);

        if (!targetSettingMapping.containsKey(mapping.targetType)) {
            HashMap<MapDataType, Set<String>> dataTypeMap = new HashMap<>();
            HashSet<String> ids = new HashSet<>();
            ids.add(metricId);
            dataTypeMap.put(mapping.metricType, ids);
            targetSettingMapping.put(mapping.targetType, dataTypeMap);
        } else {
            HashMap<MapDataType, Set<String>> dataTypeMap = targetSettingMapping.get(mapping.targetType);
            if (!dataTypeMap.containsKey(mapping.metricType)) {
                HashSet<String> ids = new HashSet<>();
                ids.add(metricId);
                dataTypeMap.put(mapping.metricType, ids);
            } else {
                Set<String> ids = dataTypeMap.get(mapping.metricType);
                ids.add(metricId);
            }
        }
    }

    private static void addMetricByName(String metricName, MapData mapping) {
        String code = DataSetManager.getDataSetManager().getDataSet("ISO11073").getCode(metricName);
        if (code != null) {
            addMetric(code, mapping);
        }
        else
        {
            throw new RuntimeException("Bad Mapping, "+metricName);
        }
    }


    private static void addSettingByName(String metricName, MapData mapping) {
        String code = DataSetManager.getDataSetManager().getDataSet("ISO11073").getCode(metricName);
        if (code != null) {
            addSetting(code, mapping);
        }
        else
        {
            throw new RuntimeException("Bad Mapping, "+metricName);
        }
    }
    private static void addMetric(String metricId, MapData mapping) {
        metricMap.put(metricId, mapping);

        if (!targetMapping.containsKey(mapping.targetType)) {
            HashMap<MapDataType, Set<String>> dataTypeMap = new HashMap<>();
            HashSet<String> ids = new HashSet<>();
            ids.add(metricId);
            dataTypeMap.put(mapping.metricType, ids);
            targetMapping.put(mapping.targetType, dataTypeMap);
        } else {
            HashMap<MapDataType, Set<String>> dataTypeMap = targetMapping.get(mapping.targetType);
            if (!dataTypeMap.containsKey(mapping.metricType)) {
                HashSet<String> ids = new HashSet<>();
                ids.add(metricId);
                dataTypeMap.put(mapping.metricType, ids);
            } else {
                Set<String> ids = dataTypeMap.get(mapping.metricType);
                ids.add(metricId);
            }
        }
    }


    public static List<Mapping> getMappingForTargetAndType(MapTargetType target, MapDataType dataType) {
        ArrayList<Mapping> out = new ArrayList<>();
        HashMap<MapDataType, Set<String>> data = targetMapping.get(target);
        Set<String> ids = data.get(dataType);
        for (String id : ids) {
            out.add(new Mapping(id, metricMap.get(id)));
        }
        return out;

    }


}
