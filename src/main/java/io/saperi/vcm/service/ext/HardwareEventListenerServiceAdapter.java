/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext;

import static java.util.stream.Collectors.toSet;

import io.saperi.vcm.domain.Device;
import io.saperi.vcm.domain.enumeration.DeviceType;
import io.saperi.vcm.event.*;

import java.util.*;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.transaction.annotation.Transactional;

import javax.cache.Cache;

/**
 * Implementation of {@link HardwareEventListenerService} that wraps the methods
 * of the interface in a try/catch block and publishes error
 * {@link HardwareStatusEvent} when an exception is caught.
 *
 * @author esteban
 */
public abstract class HardwareEventListenerServiceAdapter implements HardwareEventListenerService {

    private static final Logger log = LoggerFactory.getLogger(HardwareEventListenerServiceAdapter.class);

    private final ExtendedDeviceService extendedDeviceService;
    private final ApplicationEventPublisher applicationEventPublisher;
    protected Cache<String, List<HardwareEvent>> eventsCache;
    private Map<DeviceType, String> deviceTypeUserIdMap = new HashMap<>();

    public HardwareEventListenerServiceAdapter(ExtendedDeviceService extendedDeviceService,
                                               ApplicationEventPublisher applicationEventPublisher                                               ) {
        this.extendedDeviceService = extendedDeviceService;
        this.applicationEventPublisher = applicationEventPublisher;
        // TODO: Move all this into the corresponding ConnectionMapper driver class ?
        deviceTypeUserIdMap.put(DeviceType.THORNHILL, "bridge");
        deviceTypeUserIdMap.put(DeviceType.NIHONKOHDEN, "NkWeb");
        deviceTypeUserIdMap.put(DeviceType.NEUROWAVE, "NeurowaveBridge");
    }

    public void registerExistingDevices() {
        //Register a Data Listener for each existing Device
        for (Device device: getExistingDevices()) {
            if (device.getAssignments()!=null) {
                long activeAssignments = device.getAssignments().stream().filter(assignedDevice -> assignedDevice.getAssignment().isActive()).count();
                if (activeAssignments > 0) {
                    this.registerListener(device.getHardwareId(), device.getModel().getType());
                } else {
                    log.info("\033[0;31mListener not added for {}, it is an inactive assignment\033[0m ", device.getHardwareId());
                }
            }
        }
    }

    private boolean typePresent(DeviceType type){
        return Arrays.stream(getTypes()).anyMatch(t -> t.equals(type));
    }

    @Override
    public void onHardwareListenerActivationRequestEvent(HardwareListenerActivationRequestEvent request) {
        try {
            if (typePresent(request.getDeviceType())) {
                internalHardwareListenerActivationRequestEvent(request);
            }
        } catch (Exception e) {
            log.error("Exception Registering to Device {}: {}", request.getHardwareId(), e.getMessage());
            applicationEventPublisher.publishEvent(new HardwareStatusEvent(request.getHardwareId(), HardwareStatusEvent.Status.ERROR, e.getMessage() == null ? "" : e.getMessage()));
        }
    }

    @Override
    public void onHardwareListenerDeactivationRequestEvent(HardwareListenerDeactivationRequestEvent request) {
        try {
            if (typePresent(request.getDeviceType())) {
                internalOnHardwareListenerDeactivationRequestEvent(request);
            }
        } catch (Exception e) {
            log.error("Exception Unregistering to Device {}: {}", request.getHardwareId(), e.getMessage());
            applicationEventPublisher.publishEvent(new HardwareStatusEvent(request.getHardwareId(), HardwareStatusEvent.Status.ERROR, e.getMessage() == null ? "" : e.getMessage()));
        }
    }

    /**
     * When a Device (of type X) is modified we must do the following:
     * <ul>
     * <li>If the Device was created and it is an X Device, then we need to
     * register a new Data Listener for it.</li>
     * <li>If the Device was modified and it type was changed to X, then we need
     * to register a new Data Listener for it.</li>
     * <li>If the Device was modified and it type is no longer X, then we need
     * to unregister the existing Data Listener for it.</li>
     * <li>If the Device was deleted and it was a X DEvice, then we need to
     * unregister the existing Data Listener for it.</li>
     * </ul>
     *
     * @param event the event.
     */
    @EventListener
    public void onDeviceModification(DeviceModificationEvent event) {

        if (event.getOperation() == DeviceModificationEvent.Operation.CREATE && event.getNewModel()!=null && typePresent(event.getNewModel().getType())) {
            log.debug("A new device of type {} was created. Creating a Data Listener for it.", event.getNewModel().getType());
            registerListener(event.getNewHardwareId(), event.getNewModel().getType());
            return;
        }
        if (event.getOperation() == DeviceModificationEvent.Operation.UPDATE) {
            if (event.getOldModel().getType() != event.getNewModel().getType()) {
                if (typePresent(event.getNewModel().getType())) {
                    log.debug("The Device '{}' is now a {} Device. Creating a Data Listener for it.", event.getNewHardwareId(), event.getNewModel().getType());
                    registerListener(event.getNewHardwareId(), event.getNewModel().getType());
                    return;
                }
                if ( typePresent(event.getOldModel().getType())) {
                    log.debug("The Device '{}' is no longer a {} Device. Removing its Data Listener.", event.getOldHardwareId(), event.getOldModel().getType());
                    unregisterListener(event.getOldHardwareId());
                }
            }

            if (!event.getNewHardwareId().equals(event.getOldHardwareId()) && typePresent(event.getOldModel().getType())) {
                log.debug("The Hardware Id for Device {} changed to {}. Removing old Data Listener.", event.getNewHardwareId(), event.getOldHardwareId());
                unregisterListener(event.getOldHardwareId());
                log.debug("The Hardware Id for Device {} changed to {}. Registering a new Data Listener.", event.getNewHardwareId(), event.getOldHardwareId());
                registerListener(event.getNewHardwareId(), event.getNewModel().getType());
            }
        }
        if (event.getOperation() == DeviceModificationEvent.Operation.DELETE && event.getOldModel()!=null && typePresent(event.getOldModel().getType())) {
            log.debug("The Device '{}' of type {} was deleted. Removing its Data Listener.", event.getOldHardwareId(), event.getOldModel().getType());
            unregisterListener(event.getOldHardwareId());
        }
        // Operation types to activate deactivate assignment should register/unregister the listener
        if (event.getOperation() == DeviceModificationEvent.Operation.ACTIVATION && typePresent(event.getOldModel().getType())) {
            log.debug("\033[0;32mThe Device with HWId '{}' will register a Listener.\033[0m", event.getNewHardwareId());
            registerListener(event.getOldHardwareId(), event.getOldModel().getType());
        }
        if (event.getOperation() == DeviceModificationEvent.Operation.DEACTIVATION && typePresent(event.getOldModel().getType())) {
            log.debug("\033[0;33mThe Device with HWId '{}' will unregister it's Listener.\033[0m", event.getOldHardwareId());
            unregisterListener(event.getOldHardwareId());
        }
    }

    @Transactional
    protected Set<Device> getExistingDevices() {
        return extendedDeviceService.findAllOfTypes(getTypes())
            .stream()
            .collect(toSet());
    }

    protected String getUserIdFromType(DeviceType deviceType){
       return deviceTypeUserIdMap.get(deviceType);
    }

    protected abstract DeviceType[] getTypes();

    protected abstract void registerListener(String hardwareId, DeviceType deviceType);

    protected abstract void unregisterListener(String hardwareId);

    protected abstract void internalHardwareListenerActivationRequestEvent(HardwareListenerActivationRequestEvent request);

    protected abstract void internalOnHardwareListenerDeactivationRequestEvent(HardwareListenerDeactivationRequestEvent request);

}
