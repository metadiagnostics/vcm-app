/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl;

import io.saperi.vcm.event.ScalarValue;
import io.saperi.vcm.event.VentilatorConfigurationChangeRequestEvent;
import io.saperi.vcm.repository.CustomAuditEventRepository;
import io.saperi.vcm.service.ext.recommender.VentilatorRecommendationToAuditEventConverter;

import java.text.DecimalFormat;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 *
 * @author esteban
 */
@Component
public class VentilatorConfigurationChangeRequestToAuditEventConverter {

    public final static String EVENT_TYPE = "VENTILATOR_CHANGE";
    public final static String DATA_ASSIGNMENT_ID_KEY = VentilatorRecommendationToAuditEventConverter.DATA_ASSIGNMENT_ID_KEY;

    public final static String DATA_DEVICE_MODEL_KEY = "DEVICE_MODEL";
    public final static String DATA_DEVICE_HW_ID_KEY = "DEVICE_HW_ID";
    public final static String DATA_FIO2_KEY = "APPLIED_FIO2";
    public final static String DATA_IE_KEY = "APPLIED_IE";
    public final static String DATA_PEEP_KEY = "APPLIED_PEEP";
    public final static String DATA_PIP_KEY = "APPLIED_PIP";
    public final static String DATA_TV_KEY = "APPLIED_TV";
    public final static String DATA_RATE_KEY = "APPLIED_RATE";

    private final CustomAuditEventRepository eventRepository;
    private final DecimalFormat df = new DecimalFormat("#.##");

    public VentilatorConfigurationChangeRequestToAuditEventConverter(CustomAuditEventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    @EventListener
    public void handleVentilatorConfigurationChangeRequestEvent(VentilatorConfigurationChangeRequestEvent event) {
        AuditEvent audit = new AuditEvent(
            Instant.ofEpochMilli(event.getTimestamp()),
            event.getRequester(),
            EVENT_TYPE,
            extractData(event)
        );

        eventRepository.add(audit);
    }

    protected Map<String, Object> extractData(VentilatorConfigurationChangeRequestEvent event) {
        Map<String, Object> data = new HashMap<>();

        data.put(DATA_ASSIGNMENT_ID_KEY, event.getAssignmentId());
        data.put(DATA_DEVICE_MODEL_KEY, event.getDevice().getModel().getName());
        data.put(DATA_DEVICE_HW_ID_KEY, event.getDevice().getHardwareId());
        addEntry(data, DATA_FIO2_KEY, event.getChanges().getFio2());
        addEntry(data, DATA_IE_KEY, event.getChanges().getInspiratoryExpiratoryRatio());
        addEntry(data, DATA_PEEP_KEY, event.getChanges().getPeep());
        addEntry(data, DATA_PIP_KEY, event.getChanges().getPip());
        addEntry(data, DATA_TV_KEY, event.getChanges().getTv());
        addEntry(data, DATA_RATE_KEY, event.getChanges().getRate());

        return data;
    }

    private void addEntry(Map<String, Object> map, String key, ScalarValue value) {
        if (value == null) {
            return;
        }
        map.put(key, getFormattedValue(value.getValue()) + " " + value.getUnit());
    }

    private String getFormattedValue(Double value){
        return df.format(value);
    }
}
