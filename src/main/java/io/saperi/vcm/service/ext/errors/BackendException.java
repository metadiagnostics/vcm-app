/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.errors;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class BackendException extends AbstractThrowableProblem {

    public static class Param {

        private final String key;
        private final String value;

        public Param(String key, String value) {
            this.key = key;
            this.value = value;
        }

    }

    private static final long serialVersionUID = 1L;

    private final String errorKey;

    public BackendException(String defaultMessage, String errorKey, Param... params) {
        this(ErrorConstants.DEFAULT_TYPE, defaultMessage, errorKey, params);
    }

    public BackendException(URI type, String defaultMessage, String errorKey, Param... params) {
        super(type, defaultMessage, Status.BAD_REQUEST, null, null, null, getAlertParameters(errorKey, params));
        this.errorKey = errorKey;
    }

    public String getErrorKey() {
        return errorKey;
    }

    private static Map<String, Object> getAlertParameters(String errorKey, Param... params) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("message", "error." + errorKey);
        if (params != null) {
            for (Param param : params) {
                parameters.put(param.key, param.value);
            }
        }
        return parameters;
    }
}
