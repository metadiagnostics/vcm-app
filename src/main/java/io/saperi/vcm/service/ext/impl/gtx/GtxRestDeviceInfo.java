/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.gtx;

import com.athenagtx.framing.Frame;
import com.athenagtx.framing.HardwareId;

/**
 * This Device Info class is used when a REST invocation in performed.
 * The class do not need to store more attributes, but is used to check instances on template method invocations.
 * See {@link GtxRESTVentilatorConfigurationChangeService#changeVentilator(GtxDeviceInfo, Frame)}
 *
 * @author sgroh@saperi.io
 */
public class GtxRestDeviceInfo extends GtxDeviceInfo {
    public GtxRestDeviceInfo(HardwareId hardwareId, String runId) {
        super(hardwareId, runId);
    }
}
