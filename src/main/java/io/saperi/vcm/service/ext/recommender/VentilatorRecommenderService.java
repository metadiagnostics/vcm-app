/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.recommender;

import io.saperi.vcm.domain.Assignment;
import io.saperi.vcm.domain.ext.VentilatorRecommendationRequest;
import io.saperi.vcm.domain.ext.VentilatorRecommendationResponse;

/**
 * Concrete implementations of this interface are used to actually get a
 * Ventilator recommendation based on the received request.
 *
 * @author esteban
 */
public interface VentilatorRecommenderService {

    /**
     * Creates a Ventilator Recommendation based on an Assignment. The latest
     * data available from the Assignment's Devices will be used.
     *
     * @param assignment the assignment.
     * @param requester  the requester (typically the user id).
     * @return the recommendation.
     */
    abstract VentilatorRecommendationResponse createRecommendation(Assignment assignment, String requester);

    /**
     * Creates a Ventilator Recommendation based on a specific
     * {@link VentilatorRecommendationRequest}.
     *
     * @param assignment the assignment this recommendation is for.
     * @param request    the request.
     * @return the recommendation.
     */
    abstract VentilatorRecommendationResponse createRecommendation(Assignment assignment, VentilatorRecommendationRequest request);

}
