/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.gtx;

import athena.admsandroid.Protobufs.Accs;
import org.springframework.context.ApplicationEvent;

/**
 *
 * @author esteban
 */
public class GtxAccsLiveDataEvent extends ApplicationEvent {

    private final String hardwareId;

    public GtxAccsLiveDataEvent(String hardwareId, Accs.AccsLiveData data) {
        super(data);
        this.hardwareId = hardwareId;
    }

    public String getHardwareId() {
        return hardwareId;
    }

    public Accs.AccsLiveData getData() {
        return (Accs.AccsLiveData) getSource();
    }
}
