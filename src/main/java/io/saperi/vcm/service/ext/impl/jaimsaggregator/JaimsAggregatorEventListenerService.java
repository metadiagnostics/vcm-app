/**
 * Copyright 2021 Saperi Systems.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.saperi.vcm.service.ext.impl.jaimsaggregator;

import io.saperi.vcm.domain.enumeration.DeviceType;
import io.saperi.vcm.domain.ext.VentilatorConfigurationChangeRequest;
import io.saperi.vcm.event.*;
import io.saperi.vcm.service.PumpService;
import io.saperi.vcm.service.VentilatorService;
import io.saperi.vcm.service.ext.ExtendedDeviceService;
import io.saperi.vcm.service.ext.HardwareEventListenerServiceAdapter;
import io.saperi.vcm.service.ext.recommender.VentilatorConfigurationChangeService;
import io.saperi.vcm.util.CacheUtil;
import io.saperi.virtualhealth.device.aggregators.common.DeviceAggregatorClient;
import io.saperi.virtualhealth.device.aggregators.common.DeviceCtx;
import io.saperi.virtualhealth.device.aggregators.common.FrameManager;
import io.saperi.virtualhealth.device.aggregators.common.JSONReplayClient;
import io.saperi.virtualhealth.device.aggregators.common.events.AggregatorEvent;
import io.saperi.virtualhealth.device.aggregators.common.events.DeviceStatusChangeEvent;
import io.saperi.virtualhealth.device.aggregators.common.events.NewDeviceEvent;
import io.saperi.virtualhealth.device.aggregators.common.exceptions.ArgumentException;
import io.saperi.virtualhealth.device.aggregators.common.exceptions.ConnectionException;
import io.saperi.virtualhealth.device.aggregators.common.managers.ConnectionMappingManager;
import io.saperi.virtualhealth.device.aggregators.common.model.*;
import io.saperi.virtualhealth.device.aggregators.jaims.JaimsAuth;
import io.saperi.virtualhealth.device.aggregators.jaims.JaimsClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.jms.ConnectionFactory;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class that handles activations and deactivations of Jaims Aggregator devices. This class
 * will use a DeviceAggregatorClient to listen to a specific Jaims Aggregator cloud instance. When data
 * from an active device is received by this class, it is transformed into the
 * corresponding {@link HardwareEvent event/s} and published.
 *
 * @author sgroh
 */
@Service
public class JaimsAggregatorEventListenerService extends HardwareEventListenerServiceAdapter implements VentilatorConfigurationChangeService, ApplicationListener<ApplicationReadyEvent> {
    private static final String REPLAY_SEPARATOR = "::";
    private static final String REPLAY = "Replay" + REPLAY_SEPARATOR;
    private static final String RAW_REPLAY = "RawReplay" + REPLAY_SEPARATOR;
    private static final String TEST = "Test" + REPLAY_SEPARATOR;

    private final Logger log = LoggerFactory.getLogger(JaimsAggregatorEventListenerService.class);

    private final ApplicationEventPublisher applicationEventPublisher;

    private ScheduledExecutorService scheduledService = Executors.newScheduledThreadPool(10);

    private Map<String, Runnable> runnableListeners = new HashMap<>();
    private Map<String, ScheduledFuture<?>> scheduledFutures = new HashMap<>();

    @Value("${vcm.jaimsaggregator.enabled:true}")
    private boolean enabled;
    @Value("${vcm.jaimsaggregator.config.check:true}")
    private boolean checkConfig;

    public long getValue_mask() {
        return value_mask;
    }

    public void setValue_mask(long value_mask) {
        this.value_mask = value_mask;
    }
    @Value("${vcm.jaimsaggregator.service.jwt_key:secure_secret}")
    private String jwt_key;
    @Value("${vcm.jaimsaggregator.service.userid:bridge}")
    private String userId;
    @Value("${vcm.jaimsaggregator.service.ip:130.211.12.234}")
    private String service_ip;
    @Value("${vcm.jaimsaggregator.socket.protocol:http}")
    private String socket_protocol;
    @Value("${vcm.jaimsaggregator.testtopic:testframetopic}")
    private String testJmsTopic;
    @Value("${vcm.jaimsaggregator.generate.genericFrame:true}")
    private Boolean boolGenerateGenericFrames=false;
    @Value("${vcm.jaimsaggregator.generate.oldSytleEvents:false}")
    private Boolean boolGenerateOldStyle = true;
    private AtomicBoolean applicationInitialized=new AtomicBoolean(false);


    private HashMap<String, DeviceAggregatorClient> clientMap = new HashMap<>();
    private HashMap<String, StateTrack> statusMap= new HashMap<>();

    private long value_mask = MeasurementStatus.DEMO_DATA.value | MeasurementStatus.TEST_DATA.value | MeasurementStatus.VALIDATED_DATA.value | MeasurementStatus.MSMT_STATE_IN_ALARM.value | MeasurementStatus.MSMT_STATE_AL_INHIBITED.value;
    private VentilatorService ventilatorService;
    private PumpService pumpService;
    private Map<String,Instant> hwId_lastSettingConfig = new HashMap<>();
    private final ConnectionFactory connectionFactory;

    static {
        JaimsClient.Initialize();
    }

    @Inject
    public JaimsAggregatorEventListenerService(
        ApplicationEventPublisher applicationEventPublisher,
        ExtendedDeviceService extendedDeviceService,
        VentilatorService ventilatorService,
        @Qualifier("amq-jms-factoy") ConnectionFactory connectionFactory,
        PumpService pumpService
    ) {
        super(extendedDeviceService, applicationEventPublisher);
        this.applicationEventPublisher = applicationEventPublisher;
        this.ventilatorService = ventilatorService;
        this.connectionFactory = connectionFactory;
        this.pumpService = pumpService;
    }

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        log.info("New devices can now be added");
        if (runnableListeners.size()>0){
            runnableListeners.entrySet().stream().forEach(entry -> {
                ScheduledFuture<?> scheduledFuture = scheduledService.schedule(entry.getValue(), 2, TimeUnit.SECONDS);
                scheduledFutures.put(entry.getKey(), scheduledFuture);
            });
        }
        runnableListeners.clear();
        this.applicationInitialized.set(true);
    }

    @Override
    public void changeVentilator(VentilatorConfigurationChangeRequestEvent request) {
        //Scan the metric map for mappings to settings for the request
        VentilatorConfigurationChangeRequest changes = request.getChanges();


        String hardware_id = request.getDevice().getHardwareId();
        if (clientMap.containsKey(hardware_id)){
            DeviceAggregatorClient client = clientMap.get(hardware_id);
            Frame aggregateFrame = client.getFrameManager().getAggregateFrame();
            //Ok Scan for the settable metrics
            List<Mapping> mappings = Mapping.getMappingForTargetAndType(MapTargetType.VENT, MapDataType.SETTING);
            HashMap<String, SettingAssoc> settings = new HashMap<>();
            for (Mapping mapping: mappings) {
                List<DeviceInfo> devices = aggregateFrame.getDevicesWithSetting(mapping.metricId);
                //Ideally we would have a stronger mapping to device here
                if (devices != null && !devices.isEmpty()) {
                    if (devices.size()>1)
                    {
                        log.warn("Multiple set options for metric {},  device {} being used", mapping.metricId, devices.get(0).getId());
                    }
                    DeviceInfo device = devices.get(0);
                    Setting lastValue = aggregateFrame.getDeviceContext(device.getId()).getSetting(mapping.metricId);
                    //TODO: Normalize the lastValue if required
                    //ScalarValue scalarValue = new ScalarValue((double) val.getValue(), mapping.unit, mapping.precision);
                    PropertyDescriptor pd = BeanUtils.getPropertyDescriptor(changes.getClass(), mapping.data.property);
                    if (pd != null)
                    {
                        Method method = pd.getReadMethod();
                        try {
                            Object val = method.invoke(changes);
                            if (val instanceof ScalarValue)
                            {
                                int scalarAsInt =  ((ScalarValue) val).getValue().intValue();

                                //TODO: Deal with non integer setting in the future
                                //In specific look and updating the whole setting change contract
                                if (lastValue.getValue() != scalarAsInt)
                                {
                                    //Ok the values differ - We should update this setting
                                    Setting newSetting = new Setting();
                                    newSetting.setValue(scalarAsInt);
                                    newSetting.setMetric_id(mapping.metricId);
                                    SettingAssoc sa;
                                    if (!settings.containsKey(device.getId()))
                                    {
                                        sa = new SettingAssoc();
                                        sa.deviceInfo = device;
                                        settings.put(device.getId(),sa);
                                    }
                                    else
                                    {
                                        sa = settings.get(device.getId());
                                    }
                                    sa.settings.add(newSetting);
                                 }
                            }
                            else
                            {
                                log.warn("Unsupported setting type {}",val.getClass().getName())
;                            }

                        } catch (Exception e) {
                            log.error("Exception reading Property", e);
                        }
                    }
                    //Place the in a map keyed by DeviceInfo
                    //  get the new setting and latest value seen
                    //  if they differ make the request
                }
                else
                {
                    //We have a issue here in that we have no idea which device supports the setting
                    log.warn("Unable to identify a device for setting {} ",mapping.metricId);
                    //As a fall back we could see if the MD info for the device
                }
            }

            if (!settings.isEmpty())
            {
                //OK there are somethings that have changed! We request updates
                for (String devId : settings.keySet())
                {
                    SettingAssoc sa = settings.get(devId);
                    client.requestSettingChange(sa.deviceInfo,sa.settings);
                }
            }
        }
        else
        {
            log.debug("Jaims aggregator ignoring vent change event for unknown hardware id {}", hardware_id);
        }

    }



    private void checkConfiguration(DeviceAggregatorClient client, Properties props) {
        List<String> unknownProps = new ArrayList<>();
        List<String> unusedProps = new ArrayList<>();
        HashSet<String> checkProps = new HashSet<>(client.getRequiredProperties(DeviceAggregatorClient.PropertyScope.CONFIG));
        Set<Object> keys = props.keySet();
        for (Object tempKey : keys) {
            String key = (String) tempKey;
            if (!checkProps.contains(key)) {
                unknownProps.add(key);
            }
        }
        for (String key : checkProps) {
            if (!props.containsKey(key)) {
                unknownProps.add(key);
            }
        }
        if (unknownProps.size() == 0 && unusedProps.size() == 0) {
            log.info("Configuration check passed with 100% coverage");
        } else {
            if (unknownProps.size() != 0) {
                for (String key : unknownProps) {
                    log.warn("Unknown property used (" + key + ")");
                }
            }
            if (unusedProps.size() != 0) {
                for (String key : unusedProps) {
                    log.warn("Configuration property (" + key + ") not passed to Device Aggregator");
                }
            }
        }
    }

    private void addReplayConfiguration(final String connectionId, final String connectionName, final String replayFileName){
        // Next line is a workaround to allow ConnectionMappingManager have idToConnectionmap metadata created on it.
        JaimsClient clientWorkaround = new JaimsClient(new JaimsAuth());
        ConnectionInfo info = ConnectionMappingManager.getConnectionMapper().getConnectionInfo(connectionId);
        ConnectionInfo replay = new ConnectionInfo(info);
        replay.setName(connectionName);
        String replayDriver= "io.saperi.virtualhealth.device.aggregators.jaims.RawDataReplayDriver";
        replay.setDriverClass(replayDriver);
        replay.getSupplementalProperties().put("ReplayFile", replayFileName);
        ConnectionMappingManager.getConnectionMapper().registerConnection(replay);
    }

    private void configureJaimsAggregator(DeviceAggregatorClient client, final String userId) {
        Properties props = new Properties();
        props.put("ConnectionId", userId);
        try {
            client.configure(props);
            if (checkConfig) {
                checkConfiguration(client, props);
            }
        } catch (ArgumentException e) {
            log.error("Error configuring Jaims Aggregator", e);
        }
    }

    private HashMap<MapTargetType, ApplicationEvent> createEventsFromFrames(String hardwareId, Frame currentFrame, Frame aggregateFrame, ArrayList<ApplicationEvent> exceptionEvents) {

        HashMap<MapTargetType, ApplicationEvent> evtMap = new HashMap<>();
        Set<String> devices = aggregateFrame.getDeviceNames();
        for (String device : devices) {
            //TODO: Add Mapping exception overlay based on the device type
            DeviceCtx aggregateCtx = aggregateFrame.getDeviceContext(device);
            if (aggregateCtx.getMeasureValues() != null && !aggregateCtx.getMeasureValues().isEmpty()) {
                log.info("Measures received: {}",aggregateCtx.getMeasureValues().size());
                processMeasureValues(aggregateCtx.getMeasureValues(), evtMap, aggregateCtx, hardwareId);
            }
            if (aggregateCtx.getSettingValues() !=  null && !aggregateCtx.getSettingValues().isEmpty())
            {
                log.info("Settings received: {}",aggregateCtx.getSettingValues().size());
                processSettings(aggregateCtx.getSettingValues(), evtMap, aggregateCtx, hardwareId);
            }
            if (aggregateCtx.getMeasureWaveFormValues() !=  null && !aggregateCtx.getMeasureWaveFormValues().isEmpty())
            {
                log.info("Waveforms received: {}",aggregateCtx.getMeasureWaveFormValues().size());
                processWaveforms(aggregateCtx.getMeasureWaveFormValues(), evtMap, aggregateCtx, hardwareId);
            }
            DeviceCtx currentCtx = currentFrame.getDeviceContext(device);
            String id = (currentCtx == null || currentCtx.getDeviceInfo() == null) ? hardwareId : currentCtx.getDeviceInfo().getId();

            StateTrack status = statusMap.get(hardwareId);
            if ( currentCtx == null || currentCtx.isEmpty() )
            {
                boolean generateUnknown = false;
                if (status.devStatus.containsKey(id))
                {
                    HardwareStatusEvent.Status curStatus = status.devStatus.get(id);
                    if (curStatus != HardwareStatusEvent.Status.UNKNOWN)
                    {
                        generateUnknown = true;
                    }
                }
                else
                {
                    generateUnknown = true;
                }

                if (generateUnknown)
                {
                    HardwareStatusEvent evt = new HardwareStatusEvent(hardwareId, HardwareStatusEvent.Status.UNKNOWN,id);
                    status.devStatus.put(id, HardwareStatusEvent.Status.UNKNOWN);
                    exceptionEvents.add(evt);
                }
            }
            else
            {
                if (status.devStatus.containsKey(id))
                {
                    HardwareStatusEvent.Status curStatus = status.devStatus.get(id);
                    if (curStatus == HardwareStatusEvent.Status.UNKNOWN)
                    {
                        HardwareStatusEvent evt = new HardwareStatusEvent(hardwareId, HardwareStatusEvent.Status.ONLINE,id);
                        status.devStatus.put(id, HardwareStatusEvent.Status.ONLINE);
                        exceptionEvents.add(evt);
                    }
                }
            }
        }

        return evtMap;
    }

    private void processMeasureValues(HashMap<String, MeasureValue> values, HashMap<MapTargetType, ApplicationEvent> evtMap, DeviceCtx ctx, String hardwareId  )
    {
        HashMap<String, MapData> metricMap = Mapping.metricMap;
        for (String key : values.keySet()) {
            if (metricMap.containsKey(key)) {
                MapData mapping = metricMap.get(key);
                ApplicationEvent evt;
                if (evtMap.containsKey(mapping.targetType)) {
                    evt = evtMap.get(mapping.targetType);
                } else {
                    evt = createEventOfType(mapping.targetType, hardwareId,ctx);
                    evtMap.put(mapping.targetType, evt);
                }
                MeasureValue val = values.get(key);
                if ((val.getState() ^ value_mask) != 0)
                {
                    switch (mapping.metricType) {
                        case SCALAR: {
                            ScalarValue scalarValue = new ScalarValue(val.getValue(), mapping.unit, mapping.precision);
                            PropertyDescriptor pd = BeanUtils.getPropertyDescriptor(evt.getClass(), mapping.property);
                            Method method = pd.getWriteMethod();
                            try {
                                method.invoke(evt, scalarValue);
                                break;
                            } catch (Exception e) {
                                log.error("Exception setting Event Property", e);
                            }
                            break;
                        }
                        default:
                            log.warn("Invalid data found for what is expected to be a scaler value, mapping {}, key = {}",mapping.metricType, key);
                            break;
                    }
                }
                else {
                    log.info("Metric {} skipped state {}",val.getMetric_id(),val.getState());
                }

            }
        }
    }

    private void processSettings(HashMap<String, Setting>  values, HashMap<MapTargetType, ApplicationEvent> evtMap, DeviceCtx ctx, String hardwareId  )
    {
        HashMap<String, MapData> metricMap = Mapping.settingMap;
        for (String key : values.keySet()) {
            if (metricMap.containsKey(key)) {
                MapData mapping = metricMap.get(key);
                ApplicationEvent evt;
                if (evtMap.containsKey(mapping.targetType)) {
                    evt = evtMap.get(mapping.targetType);
                } else {
                    evt = createEventOfType(mapping.targetType, hardwareId,ctx);
                    evtMap.put(mapping.targetType, evt);
                }
                Setting val = values.get(key);
                if ((val.getState() ^ value_mask) != 0) {
                    switch (mapping.metricType) {
                        case SETTING: {
                            ScalarValue scalarValue = new ScalarValue(val.getValue(), mapping.unit, mapping.precision);
                            if (val.getStringValue()!=null) {
                                try {
                                    scalarValue.setValue(Double.valueOf(val.getStringValue()));
                                } catch (NumberFormatException nfe) {
                                    log.warn("Could not set a numeric value from the string value for key: {} ", key);
                                }
                            }
                            PropertyDescriptor pd = BeanUtils.getPropertyDescriptor(evt.getClass(), mapping.property);
                            Method method = pd.getWriteMethod();
                            try {
                                method.invoke(evt, scalarValue);
                            } catch (Exception e) {
                                log.error("Exception setting Event Property", e);
                            }
                            break;
                        }
                        default:
                            log.warn("Invalid data found for what is expected to be a setting value, mapping {}, key = {}", mapping.metricType, key);
                            break;
                    }
                }
                else
                {
                    log.info("Setting {} skipped state {}",val.getMetric_id(),val.getState());
                }

            }
        }
    }

    private void processWaveforms(HashMap<String, MeasureWaveForm>  values, HashMap<MapTargetType, ApplicationEvent> evtMap, DeviceCtx ctx, String hardwareId  )
    {
        HashMap<String, MapData> metricMap = Mapping.metricMap;
        for (String key : values.keySet()) {
            if (metricMap.containsKey(key)) {
                MapData mapping = metricMap.get(key);
                ApplicationEvent evt;
                if (evtMap.containsKey(mapping.targetType)) {
                    evt = evtMap.get(mapping.targetType);
                } else {
                    evt = createEventOfType(mapping.targetType, hardwareId,ctx);
                    evtMap.put(mapping.targetType, evt);
                }
                MeasureWaveForm val = values.get(key);
                if ((val.getState() ^ value_mask) != 0) {
                    switch (mapping.metricType) {
                        case WAVEFORM: {
                            //Todo prep value
                            //ScalarValue scalarValue = new ScalarValue((double) val.getValue(), mapping.unit, mapping.precision);
                            PropertyDescriptor pd = BeanUtils.getPropertyDescriptor(evt.getClass(), mapping.property);
                            Method method = pd.getWriteMethod();
                            try {
                               // method.invoke(evt, scalarValue);
                            } catch (Exception e) {
                                log.error("Exception setting Event Property", e);
                            }
                            break;
                        }
                        default:
                            log.warn("Invalid data found for what is expected to be a Measure Wave value, mapping {}, key = {}",mapping.metricType, key);
                            break;
                    }
                }
                else {
                    log.info("Measure Waveform {} skipped state {}",val.getMetric_id(),val.getState());
                }


            }
        }
    }
    private ApplicationEvent createEventOfType(MapTargetType type, String hardwareId, DeviceCtx ctx)
    {
        ApplicationEvent event = null;
        DeviceInfo devinfo = ctx.getDeviceInfo();
        switch (type)
        {
            case VENT:
            {
                HardwareVentilatorEvent evt = new HardwareVentilatorEvent(hardwareId, hardwareId);
                evt.setDataTimestamp(System.currentTimeMillis());
                event = evt;
                break;
            }
            case VITAL:
            {

                HardwareVitalsEvent evt = new HardwareVitalsEvent(hardwareId, hardwareId);
                evt.setDataTimestamp(System.currentTimeMillis());
                event = evt;
                break;
            }
            default:
               break;
        }
        return event;
    }

    @Override
    protected DeviceType[] getTypes() {
        return new DeviceType[] {DeviceType.THORNHILL, DeviceType.NIHONKOHDEN, DeviceType.NEUROWAVE};
    }

    @Override
    protected void registerListener(String hardwareId, DeviceType deviceType) {
        Runnable runnableListener = () -> {
            log.info("Registering Jaims Aggregator Listener, HW: {}", hardwareId);
            DeviceAggregatorClient client;
            if (enabled) {
                if (!clientMap.containsKey(hardwareId)) {
                    Properties openProps = new Properties();
                    openProps.put("ConnectionId", hardwareId);
                    String userId = getUserIdFromType(deviceType);
                    String newUserId = userId;
                    if (hardwareId.startsWith(REPLAY)){
                        String[] hwIdParts = hardwareId.split(REPLAY_SEPARATOR);
                        String filename = hwIdParts[1];
                        openProps.put(JSONReplayClient.FILE_KEY, filename);
                        client = new JSONReplayClient();
                    } else if (hardwareId.startsWith(TEST)) {
                        client = new JaimsTestClient(connectionFactory, testJmsTopic);
                    } else {
                        openProps.put("ConnectionId", userId);
                        if (hardwareId.startsWith(RAW_REPLAY)){
                            String[] hwIdParts = hardwareId.split(REPLAY_SEPARATOR);
                            if (hwIdParts.length==3) {
                                String connectionName = hwIdParts[1];
                                newUserId = connectionName;
                                String filename = hwIdParts[2];
                                this.addReplayConfiguration(userId, connectionName, filename);
                                openProps.put("ConnectionId", newUserId);
                            } else {
                                log.error("To use a Raw Replay connection the Format used in Hardware id should be: RayReplay::ConnectionName::filename");
                            }
                        }
                        client = new JaimsClient(new JaimsAuth());
                    }
                    configureJaimsAggregator(client, newUserId);
                    try {
                        //Setup the message stream
                        client.getFrameManager().on(FrameManager.NEW_FRAME, args -> {
                            Frame currentFrame = (Frame) args[0];
                            Frame aggregateFrame = (Frame) args[1];
                            handleNewFrameEvent(hardwareId, currentFrame, aggregateFrame);
                        }).on(FrameManager.EVENT, args -> {
                            AggregatorEvent evt = (AggregatorEvent)  args[0];
                            handleEvents(hardwareId, evt);
                        }).on(FrameManager.NEW_DEVICE, args -> {
                            NewDeviceEvent evt = (NewDeviceEvent) args[0];
                            handleNewDeviceEvent(hardwareId, evt);
                        });
                        client.open(openProps);
                        if (client.connected() == false)
                            client.connect(openProps);
                        clientMap.put(hardwareId, client);
                        applicationEventPublisher.publishEvent(new HardwareStatusEvent(hardwareId, HardwareStatusEvent.Status.CONNECTED, hardwareId));
                        statusMap.put(hardwareId,new StateTrack());
                        if (deviceType.equals(DeviceType.NEUROWAVE)) {
                           this.pumpService.updateClientMap(clientMap);
                        }

                    } catch (ArgumentException e) {
                        log.error("Error on open configuration", e);
                    } catch (ConnectionException e) {
                        log.error("Connection issue", e);
                    }

                }
            } else {
                log.info("Jaims Aggregator is disabled.");
            }
        };
        runnableListeners.put(hardwareId, runnableListener);
        // If the application is up run it immediately, if not this will be executed on
        // the ApplicationReadyEvent handler
        if (Boolean.TRUE.equals(this.applicationInitialized.get())){
            ScheduledFuture<?> scheduledFuture = scheduledService.schedule(runnableListener, 2, TimeUnit.SECONDS);
            scheduledFutures.put(hardwareId, scheduledFuture); // call on unregisterlistener
            runnableListeners.clear();
        }
    }

    public void handleNewFrameEvent(String hardwareId, Frame currentFrame, Frame aggregateFrame)
    {
        if (boolGenerateOldStyle) {
            ArrayList<ApplicationEvent> exceptionEvents = new ArrayList<>();
            HashMap<MapTargetType, ApplicationEvent> events = this.createEventsFromFrames(hardwareId, currentFrame, aggregateFrame, exceptionEvents);
            for (ApplicationEvent evt : events.values()) {
                applicationEventPublisher.publishEvent(evt);
                if (evt instanceof CacheableEvent) {
                    CacheableEvent cevt = (CacheableEvent) evt;
                    cevt.putInCache(eventsCache);
                }
            }
            if (!exceptionEvents.isEmpty()) {
                for (ApplicationEvent evt : exceptionEvents) {
                    applicationEventPublisher.publishEvent(evt);
                }
            }
        }
        if (boolGenerateGenericFrames)
        {
            DeviceFrameEvent evt = new DeviceFrameEvent(hardwareId, this, currentFrame, aggregateFrame);
            applicationEventPublisher.publishEvent(evt);
            // We are going to send the VentSettings and Hardware Status each 5 seconds to reduce the traffic on Websockets
            // First 10 frames are sent to speed the setup process
            Instant lastSettingConfig = hwId_lastSettingConfig.get(hardwareId);
            if (lastSettingConfig==null || lastSettingConfig.plus(5, ChronoUnit.SECONDS).isBefore(Instant.now())){
                ventilatorService.sendVentilatorSettingsDefinitions(hardwareId, aggregateFrame);
                ventilatorService.sendVentilatorRODefinitions(hardwareId, aggregateFrame);
                hwId_lastSettingConfig.put(hardwareId, Instant.now());
                applicationEventPublisher.publishEvent(new HardwareStatusEvent(hardwareId, HardwareStatusEvent.Status.CONNECTED, hardwareId));
            }
        }
    }

    public void handleNewDeviceEvent(String hardwareId,NewDeviceEvent evt)
    {
        log.info("Event for New Device, HWID: {}, Device {}", hardwareId, evt);
        NewHardwareDetectedEvent localEvent = new NewHardwareDetectedEvent(hardwareId, this, evt.getNewDevice());
        applicationEventPublisher.publishEvent(localEvent);
    }

    public void handleEvents(String hardwareId, AggregatorEvent evt)
    {
        //Ok here we handle the different types of events
        if (evt instanceof DeviceStatusChangeEvent)
        {
            DeviceStatusChangeEvent dcevt = (DeviceStatusChangeEvent) evt;
            handleDeviceStatusChangeEvent(hardwareId,dcevt);
        }
    }

    public void handleDeviceStatusChangeEvent(String hardwareId, DeviceStatusChangeEvent evt)
    {
        HardwareStatusEvent.Status status = HardwareStatusEvent.Status.DISCONNECTED;

        DeviceStatus x = evt.getStatus().getStatus();
        switch (evt.getStatus().getStatus())
        {
            case OPERATING:
            {
                status = HardwareStatusEvent.Status.ONLINE;
                break;
            }
            case UNASSOCIATED:
            case DISASSOCIATED:
            case DISASSOCIATING:
            case TERMINATING:
            case DISCONNECTED:
            {
                status = HardwareStatusEvent.Status.DISCONNECTED;
                break;
            }
            case RE_CONFIGURING:
            case RE_INITIALIZING:
            case CONFIGURED:
            case CONFIGURING:
            case ASSOCIATING:
            case ASSOCIATED:
            {
                status = HardwareStatusEvent.Status.OFFLINE;
                break;
            }
            default:
            {
                status = HardwareStatusEvent.Status.UNKNOWN;
                break;
            }
        }
        HardwareStatusEvent hsEvt = new HardwareStatusEvent(hardwareId,status,evt.getStatus().getId());
        applicationEventPublisher.publishEvent(evt);
    }

    @Override
    protected void unregisterListener(String hardwareId) {

        // This is the disconnect
        if (clientMap.containsKey(hardwareId)) {
            //TODO: Look into dynamic loading
            DeviceAggregatorClient client = clientMap.get(hardwareId);
            client.close();
            clientMap.remove(hardwareId);
            applicationEventPublisher.publishEvent(new HardwareStatusEvent(hardwareId, HardwareStatusEvent.Status.DISCONNECTED, hardwareId));
            //devicesCache.addDeviceOfInterest(hardwareId);
            //TODO: Connect and service the client
            statusMap.remove(hardwareId);
            try {
                if (scheduledFutures.get(hardwareId)!=null){
                    if (scheduledFutures.get(hardwareId).cancel(false) == true) {
                        scheduledFutures.remove(hardwareId);
                    }

                }
            } catch (Exception e) {
                log.error("not possible to kill the runnable inside the listener");
            }

        }
        //devicesCache.removeDeviceOfInterest(hardwareId);
    }

    @Override
    protected void internalHardwareListenerActivationRequestEvent(HardwareListenerActivationRequestEvent request) {
    }

    @Override
    protected void internalOnHardwareListenerDeactivationRequestEvent(HardwareListenerDeactivationRequestEvent request) {
    }

    @PostConstruct
    public void postConstruct() {
        registerExistingDevices();
        super.eventsCache = CacheUtil.eventsCache();
    }

    @PreDestroy
    public void preDestroy() {
        if (!clientMap.isEmpty()) {
            Collection<DeviceAggregatorClient> clients = clientMap.values();
            for (DeviceAggregatorClient client : clients) {
                client.close();
            }
            clientMap.clear();
        }

    }

    public DeviceAggregatorClient getDeviceAggregatorClient(final String hwId){
        return clientMap.get(hwId);
    }

    class SettingAssoc {
        public DeviceInfo deviceInfo;
        public ArrayList<Setting> settings = new ArrayList<>();


    }

    class StateTrack
    {
        public HashMap<String ,HardwareStatusEvent.Status> devStatus = new HashMap<>();
    }

}
