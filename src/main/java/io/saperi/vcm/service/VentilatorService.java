/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.saperi.vcm.domain.Device;
import io.saperi.vcm.service.dto.Label;
import io.saperi.vcm.service.dto.MonitorDefinition;
import io.saperi.vcm.service.dto.MonitorSettingDefinition;
import io.saperi.vcm.service.ext.impl.jaimsaggregator.MapData;
import io.saperi.vcm.service.ext.impl.jaimsaggregator.Mapping;
import io.saperi.virtualhealth.device.aggregators.common.DeviceCtx;
import io.saperi.virtualhealth.device.aggregators.common.managers.DeviceCapabilityManager;
import io.saperi.virtualhealth.device.aggregators.common.managers.EnumGroupManager;
import io.saperi.virtualhealth.device.aggregators.common.managers.UnitManager;
import io.saperi.virtualhealth.device.aggregators.common.model.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.MethodParameter;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Service class for managing ventilators.
 */
@Service
@Transactional
public class VentilatorService {
    private final Logger log = LoggerFactory.getLogger(VentilatorService.class);
    private final ObjectMapper mapper = new ObjectMapper();
    // TODO: Do not rely services on this mappings
    private HashMap<String, MapData> metricMap = Mapping.metricMap;
    private HashMap<String, MapData> metricSettingsMap = Mapping.settingMap;
    private final static String VENT_MODE = "STDVM";
    private MonitorService monitorService;

    @Autowired
    DeviceConfiguration deviceConfiguration;

    @Value("${vcm.ventilator}")
    private String ventilatorStr;

    @Value("${vcm.datasetName:ISO11073}")
    private String datasetName;

    @Autowired
    MonitorDefinitionConfig monitorDefinitionConfig;

    private final SimpMessageSendingOperations messagingTemplate;

    private Label ventilator;
    private Map<String,String> settingsOnSessions = new HashMap<>();

    @Autowired
    public VentilatorService(SimpMessageSendingOperations messagingTemplate,
                             MonitorService monitorService) {
        this.messagingTemplate = messagingTemplate;
        this.monitorService = monitorService;
    }

    @PostConstruct
    private void deserializeItems() throws JsonProcessingException {
        ventilator = mapper.readValue(ventilatorStr, Label.class);
    }

    public Label getLabels(){
        log.info("Obtaining ventilator labels");
        return ventilator;
    }

    @Deprecated
    public MonitorDefinition[] getReadOnlyDefinitions(String hardwareId) {
        ////
        return null;
    }

    public void sendVentilatorRODefinitions(String hardwareId, Frame aggregateFrame) {
        sendNewROConfiguration(hardwareId, aggregateFrame); // This line will send information thought WS
    }

    public void sendVentilatorSettingsDefinitions(String hardwareId, Frame aggregateFrame) {
        sendNewRWConfiguration(hardwareId, aggregateFrame); // This line will send information thought WS
    }

    /**
     * This method is going to be triggered from an event once the Frame containing setting is found
     * @param hwId
     */
    public void sendNewROConfiguration(final String hwId, Frame aggregateFrame) {
        MonitorDefinition[] newDefinitions = getVentilatorRoDefinitionsFromDeviceCapabilities(hwId, aggregateFrame);
        if (newDefinitions!= null) {
            messagingTemplate.convertAndSend("/topic/hardware/" + hwId + "/ventilator-data/settings-ro", newDefinitions);
        }
    }

    /**
     * This method is going to be triggered from an event once the Frame containing setting is found
     * @param hwId
     */
    public void sendNewRWConfiguration(final String hwId, Frame aggregateFrame) {
        MonitorSettingDefinition[] newDefinitions = getVentilatorRwDefinitionsFromAggregateFrame(hwId, aggregateFrame);
        if (newDefinitions!= null) {
            messagingTemplate.convertAndSend("/topic/hardware/" + hwId + "/ventilator-data/settings-rw", newDefinitions);
        }
    }

    private String getKeyFromValue(String value){
        return monitorDefinitionConfig.getUikey_devicekey().get(value);
    }

    /**
     * Obtaining Ventilator Read Only definitions from DeviceCapabilityManager
     *
     * @param hwId
     * @param aggregateFrame
     * @return an Array of MonitorDefinition
     */
    private MonitorDefinition[] getVentilatorRoDefinitionsFromDeviceCapabilities(final String hwId, final Frame aggregateFrame) {
        Set<MonitorDefinition> aggregatorVentilatorDefinitions = new TreeSet<>((o1, o2) -> o1.getOrder() - o2.getOrder());
        UnitManager unitManager = UnitManager.getUnitManager();
        UnitSet unitSet = unitManager.getUnitSet(datasetName);
        Map<String,String> idToMetricSetting = monitorService.getMappingSettingIds();
        Map<String, String> id_metric = monitorDefinitionConfig.getReverseMap(monitorDefinitionConfig.getUikey_devicekey());
        // Pre settings UI
        HashMap<String, MonitorDefinition> predefVentDefRo = monitorDefinitionConfig.getVentilator_definitions_ro();

        Set<String> devices = aggregateFrame.getDeviceNames();
        DeviceCtx aggregateCtx;
        DeviceCapabilityManager dc = DeviceCapabilityManager.getDeviceCapabilityManager();
        Integer[] expandedCount = new Integer[1];
        expandedCount[0] = 0;
        for (String device : devices) {
            aggregateCtx = aggregateFrame.getDeviceContext(device);
            if (aggregateCtx.getDeviceInfo()==null){
                log.warn("There is no device info on this frame");
                return null;
            }
            // Look into measures
            Map<String, MeasureValue> deviceMeasureMap = aggregateCtx.getMeasureValues();
            deviceMeasureMap.values().stream().forEach(measure -> {
                final String metricName = id_metric.get(measure.getMetric_id());
                // If we define this metric as a desired metric in VCM UI we should return it.
                MonitorDefinition predefSetting = predefVentDefRo.get(metricName);
                if (predefSetting != null) {
                    if (predefSetting.isExpanded()){
                        expandedCount[0]++;
                    }
                    aggregatorVentilatorDefinitions.add(new MonitorDefinition().toBuilder()
                        .name(predefSetting.getName())
                        .order(predefSetting.getOrder())
                        .unit(unitSet.getUnit(String.valueOf(measure.getUnit_code()))!=null?unitSet.getUnit(String.valueOf(measure.getUnit_code())).getAbbreviation():"")
                        .hasAlarmMin(predefSetting.isHasAlarmMin())
                        .hasAlarmMax(predefSetting.isHasAlarmMax())
                        .alarmMax(predefSetting.getAlarmMax())
                        .alarmMin(predefSetting.getAlarmMin())
                        .dataKeys(predefSetting.getDataKeys())
                        .selectors(predefSetting.getSelectors())
                        .expanded(predefSetting.isExpanded())
                        .selected(predefSetting.isSelected())
                        .physiologicMonitors(predefSetting.isPhysiologicMonitors())
                        .ventFlow(predefSetting.isVentFlow())
                        .ventSettings(predefSetting.isVentSettings())
                        .ventMonitors(predefSetting.isVentMonitors())
                        .reduced(predefSetting.isReduced())
                        .xlabel(predefSetting.getXlabel())
                        .ylabel(predefSetting.getYlabel())
                        .separator(predefSetting.getSeparator())
                        .chartMaxDataLength(predefSetting.getChartMaxDataLength())
                        .yaxisMin(predefSetting.getYaxisMin())
                        .yaxisMax(predefSetting.getYaxisMax())
                        .type(predefSetting.getType())
                        .hwId(hwId)
                        .build());
                }
            });

            //Look into settings
            Map<String, Setting> deviceSettingMap = aggregateCtx.getSettingValues();
            deviceSettingMap.values().stream().forEach(setting -> {
                final String metricName = id_metric.get(setting.getMetric_id());
                // If we define this metric as a desired metric in VCM UI we should return it.
                MonitorDefinition predefSetting = predefVentDefRo.get(metricName);
                if (predefSetting != null) {
                    if (predefSetting.isExpanded()){
                        expandedCount[0]++;
                    }
                    aggregatorVentilatorDefinitions.add(new MonitorDefinition().toBuilder()
                        .name(predefSetting.getName())
                        .order(predefSetting.getOrder())
                        .unit(unitSet.getUnit(String.valueOf(setting.getUnit_code()))!=null?unitSet.getUnit(String.valueOf(setting.getUnit_code())).getAbbreviation():"")
                        .hasAlarmMin(predefSetting.isHasAlarmMin())
                        .hasAlarmMax(predefSetting.isHasAlarmMax())
                        .alarmMax(predefSetting.getAlarmMax())
                        .alarmMin(predefSetting.getAlarmMin())
                        .dataKeys(predefSetting.getDataKeys())
                        .selectors(predefSetting.getSelectors())
                        .expanded(predefSetting.isExpanded())
                        .selected(predefSetting.isSelected())
                        .physiologicMonitors(predefSetting.isPhysiologicMonitors())
                        .ventFlow(predefSetting.isVentFlow())
                        .ventSettings(predefSetting.isVentSettings())
                        .ventMonitors(predefSetting.isVentMonitors())
                        .reduced(predefSetting.isReduced())
                        .xlabel(predefSetting.getXlabel())
                        .ylabel(predefSetting.getYlabel())
                        .separator(predefSetting.getSeparator())
                        .chartMaxDataLength(predefSetting.getChartMaxDataLength())
                        .yaxisMin(predefSetting.getYaxisMin())
                        .yaxisMax(predefSetting.getYaxisMax())
                        .type(predefSetting.getType())
                        .hwId(hwId)
                        .build());
                }
            });
            //Add waveforms
            Map<String, MeasureWaveForm> deviceWaveformValuesMap = aggregateCtx.getMeasureWaveFormValues();
            deviceWaveformValuesMap.values().stream().forEach(waveform -> {
                final String metricName = id_metric.get(waveform.getMetric_id());
                // If we define this metric as a desired metric in VCM UI we should return it.
                MonitorDefinition predefSetting = predefVentDefRo.get(metricName);
                if (predefSetting != null) {
                    if (predefSetting.isExpanded()){
                        expandedCount[0]++;
                    }
                    aggregatorVentilatorDefinitions.add(new MonitorDefinition().toBuilder()
                        .name(predefSetting.getName())
                        .order(predefSetting.getOrder())
                        // .unit(unitSet.getUnit(setting.getUnit())!=null?unitSet.getUnit(setting.getUnit()).getAbbreviation():"") // no unit specified for waveforms
                        .hasAlarmMin(predefSetting.isHasAlarmMin())
                        .hasAlarmMax(predefSetting.isHasAlarmMax())
                        .alarmMax(predefSetting.getAlarmMax())
                        .alarmMin(predefSetting.getAlarmMin())
                        .dataKeys(predefSetting.getDataKeys())
                        .selectors(predefSetting.getSelectors())
                        .selected(predefSetting.isSelected())
                        .expanded(predefSetting.isExpanded())
                        .physiologicMonitors(predefSetting.isPhysiologicMonitors())
                        .ventFlow(predefSetting.isVentFlow())
                        .ventSettings(predefSetting.isVentSettings())
                        .ventMonitors(predefSetting.isVentMonitors())
                        .reduced(predefSetting.isReduced())
                        .xlabel(predefSetting.getXlabel())
                        .ylabel(predefSetting.getYlabel())
                        .separator(predefSetting.getSeparator())
                        .chartMaxDataLength(predefSetting.getChartMaxDataLength())
                        .yaxisMin(predefSetting.getYaxisMin())
                        .yaxisMax(predefSetting.getYaxisMax())
                        .type(predefSetting.getType())
                        //.data(waveform.getArray().stream().map(d -> String.valueOf(d)).collect(Collectors.toList()).toArray(String[]::new)) //not needed to add at this stage
                        .hwId(hwId)
                        .build()
                    );
                }
            });
        }
        // Check if we have at least 1 selected and expanded
        // (by design should be at least three in the UI)
        if (aggregatorVentilatorDefinitions.size()>=1 && expandedCount[0]<1) {
            while (expandedCount[0]<1) {
                MonitorDefinition md = aggregatorVentilatorDefinitions.stream().filter(ds->!ds.isExpanded()).findFirst().get();
                md.setExpanded(true);
                md.setSelected(true);
                expandedCount[0]++;
            }
        }
        return aggregatorVentilatorDefinitions.toArray(MonitorDefinition[]::new);
    }

    /**
     * Obtaining ventilator Read Write definitions from DeviceCapabilityManager
     *
     * @param hwId
     * @param aggregateFrame
     * @return an Array of MonitorSettingDefinition
     */
    private MonitorSettingDefinition[] getVentilatorRwDefinitionsFromAggregateFrame(final String hwId, final Frame aggregateFrame) {
        Set<MonitorSettingDefinition> possibleVentilatorRwDefinitions = new TreeSet<>((o1, o2) -> o1.getOrder() - o2.getOrder());
        UnitManager unitManager = UnitManager.getUnitManager();
        UnitSet unitSet = unitManager.getUnitSet(datasetName);
        Map<String,String> idToMetricSetting = monitorService.getMappingSettingIds();
        Map<String,String> idToAlarmLimit = monitorService.getMappingAlarmLimitIds();
        // Pre settings UI
        HashMap<String, MonitorSettingDefinition> predefinedSettings = monitorDefinitionConfig.getVentilator_definitions_rw();

        Set<String> devices = aggregateFrame.getDeviceNames();
        DeviceCtx aggregateCtx;
        DeviceCapabilityManager dc = DeviceCapabilityManager.getDeviceCapabilityManager();

        for (String device : devices) {
            aggregateCtx = aggregateFrame.getDeviceContext(device);
            if (aggregateCtx.getDeviceInfo()==null){
                log.warn("There is no device info on this frame");
                return null;
            }
            DeviceCapabilities cap = dc.getDeviceCapabilities(aggregateCtx.getDeviceInfo().getManufacturer(), aggregateCtx.getDeviceInfo().getSystem_model_number());
            Map<String, Setting> deviceSettingsMap = aggregateCtx.getSettingValues();
            deviceSettingsMap.values().stream().forEach(setting -> {
                //MapData mapping = metricSettingsMap.get(setting.getMetric_id());
                final String metricName = getMetricNameFromSettingName(idToMetricSetting, setting.getMetric_id());
                // If we define this metric as a desired metric in VCM UI we should return it.
                MonitorSettingDefinition predefSetting = predefinedSettings.get(metricName);
                if (predefSetting != null) {
                    List<EnumerationValue> enumValues = null;
                    CapabilityInfo capabilityInfo = getCapabilityInfo(cap, setting.getMetric_id());
                    if (capabilityInfo!=null) {
                        String enumKey = capabilityInfo.getEnumerationKey();
                        if (enumKey!=null) {
                            EnumerationList enumList = EnumGroupManager.getEnumGroupManager().getEnumerationList(enumKey);
                            if (enumList!=null && enumList.getValues()!=null && enumList.getValues().size()>0) {
                                // Values should be set through predefined Constrained ValueSet
                                enumValues = enumList.getValues();
                            }
                        }
                    }
                    possibleVentilatorRwDefinitions.add(new MonitorSettingDefinition().toBuilder()
                        .order(predefSetting.getOrder())
                        .name(predefSetting.getName())
                        .unit(unitSet.getUnit(setting.getUnit())!=null?unitSet.getUnit(setting.getUnit()).getAbbreviation():"")
                        .dataKey(idToMetricSetting.get(setting.getMetric_id()))
                        .requestDataKey(idToMetricSetting.get(setting.getMetric_id()))
                        .responseDataKey(idToMetricSetting.get(setting.getMetric_id()))
                        .hasAlarmMin(predefSetting.isHasAlarmMin())
                        .hasAlarmMax(predefSetting.isHasAlarmMax())
                        .alarmMax(predefSetting.getAlarmMax())
                        .alarmMin(predefSetting.getAlarmMin())
                        .hwId(hwId)
                        .codeId(setting.getMetric_id())
                        .alarmMinCodeId(idToAlarmLimit.get(idToMetricSetting.get(setting.getMetric_id()) + "AlarmMin")) //should use the convention <MetricId>alarmMin
                        .alarmMaxCodeId(idToAlarmLimit.get(idToMetricSetting.get(setting.getMetric_id()) + "AlarmMax")) //should use the convention <MetricId>alarmMax
                        .selectors(predefSetting.getSelectors())
                        .enumValues(enumValues)
                        .type(enumValues==null?"number":"string")
                        .build());
                }
            });
        }
        return possibleVentilatorRwDefinitions.toArray(MonitorSettingDefinition[]::new);
    }

    private CapabilityInfo getCapabilityInfo(DeviceCapabilities cap, String metricId) {
        try {
            return cap.getSettingsList().stream().filter(setting -> setting.getCapabilityId().equals(metricId)).findFirst().get();
        } catch (Exception e) {
            log.error("There is no capability info for metricid: {}", metricId, e);
        }
        return null;
    }

    private String getMetricNameFromSettingName(Map<String,String> idToMetricSetting ,String metricId){
        String settingName = idToMetricSetting.get(metricId);
        if (settingName!= null && settingName.endsWith("setting")){
            return settingName.replace("setting","");
        }
        return null;
    }

    public Object[] getVentilatorModesFromDeviceCapabilities(final String hwId) {
        DeviceCapabilityManager dc = DeviceCapabilityManager.getDeviceCapabilityManager();
        EnumGroupManager enumGroupManager = EnumGroupManager.getEnumGroupManager();
        Device device = null;
        try {
            device =  deviceConfiguration.getHw_device().get(hwId);
        } catch (Exception e){
            if (device==null || (device!=null && StringUtils.isEmpty(device.getManufacturer()) && StringUtils.isEmpty(device.getModelName()))) {
                return monitorDefinitionConfig
                    .getVentilator_modes()
                    .toArray();
            }
        }
        if (device == null) {
            return null;
        }
        DeviceCapabilities cap = dc.getDeviceCapabilities(device.getManufacturer(), device.getModelName());
        List<CapabilityInfo> deviceSettingList = cap.getSettingsList();
        List<EnumerationList> enumsList = new ArrayList<>();
        Supplier<Stream<CapabilityInfo>> supplierCapabilities = () -> deviceSettingList.stream().filter(s -> VENT_MODE.equals(s.getCapabilityId()));
        if (supplierCapabilities.get().count() > 0 ) {
            CapabilityInfo capability = supplierCapabilities.get().filter(s -> StringUtils.isNotBlank(s.getEnumerationKey())).findFirst().get();
            if (capability != null) {
                String enumKey = capability.getEnumerationKey();
                List<EnumerationValue> enumValues = null;
                if (enumKey!=null) {
                    EnumerationList enumList = EnumGroupManager.getEnumGroupManager().getEnumerationList(enumKey);
                    if (enumList!=null && enumList.getValues()!=null && enumList.getValues().size()>0) {
                        // Values should be set through predefined Constrained ValueSet
                        enumValues = enumList.getValues();
                    }
                }
                if (enumValues!=null) {
                    EnumerationList eList = enumGroupManager.getEnumerationList(enumKey);
                    enumsList.add(eList);
                }
            }
        }
        if (enumsList.size()==0) {
            log.info("Obtaining ventilator Modes from VCM Configuration");
            return monitorDefinitionConfig
                .getVentilator_modes()
                .toArray();
        }
        // return EnumerationValue[]
        return enumsList.stream()
            .map(f->f.getValues())
            .flatMap(a -> a.stream())
            .toArray();
    }

    public void settingsReceivedForSession(final String sessionId) {
        settingsOnSessions.put(sessionId, "" + Instant.now().toEpochMilli());
    }

    public String getSettingReceived(final String sessionId) {
        return settingsOnSessions.get(sessionId);
    }
}
