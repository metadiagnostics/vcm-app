/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service;

import io.saperi.vcm.event.AlarmEvent;
import io.saperi.vcm.event.DeviceFrameEvent;
import io.saperi.vcm.service.dto.MonitorDefinition;
import io.saperi.vcm.service.dto.MonitorSettingDefinition;
import io.saperi.vcm.util.FrameUtil;
import io.saperi.virtualhealth.device.aggregators.common.DeviceCtx;
import io.saperi.virtualhealth.device.aggregators.common.model.Frame;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.spring5.processor.SpringOptionFieldTagProcessor;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service class for managing alarms.
 */
@Service
@Transactional
@Slf4j
public class AlarmService {
    private static String NON_MEANINGFUL_VALUE = "-999";
    private ApplicationEventPublisher applicationEventPublisher;
    private MonitorService monitorService;
    MonitorDefinitionConfig monitorDefinitionConfig;
    Map<String,String> metricKeyToMetricId;
    Map<String,String> idToAlarmLimit;
    HashMap<String, MonitorDefinition> predefinedMonitorDefs;

    @Inject
    public AlarmService(ApplicationEventPublisher applicationEventPublisher,
                        MonitorService monitorService,
                        MonitorDefinitionConfig monitorDefinitionConfig) {
        this.applicationEventPublisher = applicationEventPublisher;
        this.monitorService = monitorService;
        this.monitorDefinitionConfig = monitorDefinitionConfig;
    }

    @PostConstruct
    void setup(){
        metricKeyToMetricId = monitorDefinitionConfig.getUikey_devicekey();
        idToAlarmLimit = monitorService.getMappingAlarmLimitIds();
        // Pre settings UI
        predefinedMonitorDefs = monitorDefinitionConfig.getMonitor_definitions();

    }

    @EventListener
    public void handleDeviceFrameEvent(final DeviceFrameEvent event) {
        Frame frame = event.getFrame(); //TODO: check if this can be aggregate
        DeviceCtx deviceCtx = FrameUtil.getMasterDeviceCtx(frame);
        if (deviceCtx != null && deviceCtx.getSettingValues().size() >0){
            List<AlarmEvent> alarms = new ArrayList<>();
            // Using the alarms defined on configuration file check if they exists on current frame
            predefinedMonitorDefs.entrySet().stream().forEach(entry -> {
                    String key = entry.getKey();
                    MonitorDefinition mdef = entry.getValue();
                    String idAlarmMin = idToAlarmLimit.get(key+"AlarmMin");
                    String idAlarmMax = idToAlarmLimit.get(key+"AlarmMax");
                    String idAlarmMinSetting = metricKeyToMetricId.get(key+"AlarmMinsetting");
                    String idAlarmMaxSetting = metricKeyToMetricId.get(key+"AlarmMaxsetting");
                    if (idAlarmMin!=null || idAlarmMax!=null || idAlarmMinSetting!=null  || idAlarmMaxSetting!=null ){
                        AlarmEvent alarm = new AlarmEvent(event.getHardwareId(), this);
                        alarm.setId(key);
                        alarm.setName(mdef.getDataKeys()[0]);
                        alarm.setMax(NON_MEANINGFUL_VALUE);
                        alarm.setMin(NON_MEANINGFUL_VALUE);
                        int propSet = 0;
                        if (idAlarmMin!=null && deviceCtx.getSettingValues().containsKey(idAlarmMin)) {
                            alarm.setMin(deviceCtx.getSetting(idAlarmMin).getStringValue()); //here we put the value not the metric id
                            propSet++;
                        }
                        if (idAlarmMinSetting!=null) {
                            alarm.setMinSetting(idAlarmMinSetting); // metric id
                        }
                        if (idAlarmMax!=null && deviceCtx.getSettingValues().containsKey(idAlarmMax)) {
                            alarm.setMax(deviceCtx.getSetting(idAlarmMax).getStringValue()); //here we put the value not the metric id
                            propSet++;
                        }
                        if (idAlarmMaxSetting!=null) {
                            alarm.setMaxSetting(idAlarmMaxSetting); // metric id
                        }
                        if ( propSet > 0) {
                            alarms.add(alarm);
                        }
                    }
                }
            );
            if (alarms.size() > 0) {
                this.applicationEventPublisher.publishEvent(alarms.toArray(AlarmEvent[]::new));
            }
        }
    }


}
