/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.mapper;


import io.saperi.virtualhealth.device.aggregators.common.managers.DataSetManager;
import io.saperi.virtualhealth.device.aggregators.common.model.DataSet;

/**
 * Class for ISO11073 identifier constants - Should be moved either the aggregator library or an independent libray.
 */
public class ISO11073 {

    public static final String NONINV_SYS_BP;  // Non-invasive Systolic Blood Pressure
    public static final String NONINV_DIA_BP; //  Non-invasive Diastolic Blood Pressure
    public static final String PUlSE_OXIM_SAT_O2; //Pulse Oximeter Sat O2
    public static final String END_TIDAL_CO2;   // EtcO2 (End Tidal Carbon Diaoxide)

    public static final String TEMP; // Patient Temperature (TEMP 1, TEMP 2) //19272
    public static final String TEMP_GEN_1; // 61639
    public static final String TEMP_GEN_2; // 61640
    public static final String PULS_OXIM_PULS_RATE; // Heart Rate (HR) from SpO2 (18458)


    public static final String PRESS_AWAY; // 20720 (Waveform)
    public static final String PRESS_AWAY_MAX; // 20721 (Numeric)
    public static final String PRESS_INTRA_CRAN_DIA; // 22538 (Numeric)



    public static final String VENT_PRESS_AWAY_MAX; //= "20271";  // Vent pressure Airway Maximum (PIP)
    public static final String VENT_FREQUENCY; // = "21418";  // Rate
    public static final String VENT_TIDAL_VOLUME_EXP; // = "21600";  // Expired Tidal Volume
    //TBD
    public static final String PIP; //= "20885";
    public static final String PIP_AUX; //= "21536"; added only until we have a correct setting PIP
    public static final String PEEP; //= "21644";
    public static final String RATE; //= "20514";
    public static final String FI02; //= "22072";
    public static final String VENT_PEEP;

    public static final String VENT_VOL_TIDAL; // = "20908";   /// Setting for VOLUME Tidal (Tidal Volume)

    public static final String VENT_PSISP; //Setting Pinsp (Pressure Control)
    public static final String VENT_MODE; // Vent Mode 53280

    public static final String HEART_RATE_ECG; // = "16770";   // Heart Rate for ECG

    public static final String HEART_RATE_PLETH; // = "18458";   // Heart Rate for ECG

    public static final String HEART_RATE_IBP1; // = "65207";   // Heart Rate for ECG
    public static final String HEART_RATE_IBP2; // = "65206";   // Heart Rate for ECG
    public static final String HEART_RATE_IBP3; // = "65205";   // Heart Rate for ECG

    private static DataSet dataSet;

    static
    {
        dataSet = DataSetManager.getDataSetManager().getDataSet("ISO11073");
        NONINV_SYS_BP = dataSet.getCode("PRESS_BLD_NONINV_SYS");
        NONINV_DIA_BP = dataSet.getCode("PRESS_BLD_NONINV_DIA");
        PUlSE_OXIM_SAT_O2 = dataSet.getCode("PULS_OXIM_SAT_O2");
        END_TIDAL_CO2 = dataSet.getCode("AWAY_CO2_ET");
        TEMP = dataSet.getCode("TEMP");
        TEMP_GEN_1 = dataSet.getCode("TEMP_GEN_1");
        TEMP_GEN_2 = dataSet.getCode("TEMP_GEN_2");
        PULS_OXIM_PULS_RATE = dataSet.getCode("PULS_OXIM_PULS_RATE");
        VENT_PRESS_AWAY_MAX = dataSet.getCode("VENT_PRESS_AWAY_MAX");
        PRESS_AWAY  = dataSet.getCode("PRESS_AWAY");
        PRESS_AWAY_MAX  = dataSet.getCode("PRESS_AWAY_MAX");
        PRESS_INTRA_CRAN_DIA  = dataSet.getCode("PRESS_INTRA_CRAN_DIA");
        VENT_FREQUENCY = dataSet.getCode("VENT_FREQUENCY");
        VENT_TIDAL_VOLUME_EXP = dataSet.getCode("VENT_TIDAL_VOLUME_EXP");
        VENT_PEEP = dataSet.getCode("VENT_PEEP");
        VENT_MODE = dataSet.getCode("VENT_MODE");
        PIP = dataSet.getCode("VENT_PRESS_MAX");
        PIP_AUX = dataSet.getCode("VENT_INSPTIME_SIMV");
        PEEP = dataSet.getCode("VENT_PRESSURE_SUPPORT");
        RATE = dataSet.getCode("VENT_RESP_RATE");
        FI02 = dataSet.getCode("VENT_O2");
        VENT_VOL_TIDAL = dataSet.getCode("VENT_VOL_TIDAL");
        //SETTING_PSISP  = dataSet.getCode("VENT_PSISP");
        VENT_PSISP = dataSet.getCode("VENT_PSISP");
        HEART_RATE_ECG = dataSet.getCode("HEART_RATE_ECG");
        HEART_RATE_IBP1 = dataSet.getCode("HEART_RATE_IBP1");
        HEART_RATE_IBP2 = dataSet.getCode("HEART_RATE_IBP2");
        HEART_RATE_IBP3 = dataSet.getCode("HEART_RATE_IBP3");
        HEART_RATE_PLETH = dataSet.getCode("HEART_RATE_PLETH");

    }



    public static DataSet getDataSet()
    {
        return dataSet;
    }


}
