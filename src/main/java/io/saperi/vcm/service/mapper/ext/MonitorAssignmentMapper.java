/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.mapper.ext;

import io.saperi.vcm.domain.AssignedDevice;
import io.saperi.vcm.domain.Assignment;
import io.saperi.vcm.domain.Device;
import io.saperi.vcm.service.dto.ext.MonitorAssignedDeviceDTO;
import io.saperi.vcm.service.dto.ext.MonitorAssignmentDTO;
import io.saperi.vcm.service.dto.ext.MonitorDeviceDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 *
 * @author esteban
 */
@Mapper(componentModel = "spring", uses = {})
public interface MonitorAssignmentMapper {

    @Mapping(source = "assignment.id", target = "assignmentId")
    @Mapping(source = "assignment.patientId", target = "patientId")
    @Mapping(source = "assignment.ventilatorMode", target = "ventilatorMode")
    @Mapping(source = "assignment.ventilatorRecommenderType", target = "ventilatorRecommenderType")
    @Mapping(source = "assignment.ventilatorControlType", target = "ventilatorControlType")
    @Mapping(source = "patientName", target = "patientName")
    MonitorAssignmentDTO assignmentToMonitorAssignmentDTO(Assignment assignment, String patientName);

    @Mapping(source = "ad.capability.type", target = "capability")
    MonitorAssignedDeviceDTO assignedDeviceToMonitorAssignedDeviceDTO(AssignedDevice ad);

    @Mapping(source = "id", target = "deviceId")
    @Mapping(source = "hardwareId", target = "hardwareId")
    @Mapping(source = "model.name", target = "model")
    @Mapping(source = "model.type", target = "type")
    MonitorDeviceDTO deviceToMonitorDeviceDTO(Device device);

}
