/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.dto;

import io.saperi.virtualhealth.device.aggregators.common.model.EnumerationValue;
import lombok.*;

import java.util.List;

/**
 * @author sgroh@saperi.io
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode
public class MonitorSettingDefinition {
    private int order;
    private String name;
    private String unit;
    private boolean hasAlarmMax;
    private boolean hasAlarmMin;
    private int alarmMax;
    private int alarmMin;
    private String dataKey;
    private String requestDataKey;
    private String responseDataKey;
    private String[] selectors;
    private int step;
    private String hwId;
    private String codeId;
    private String alarmMinCodeId;
    private String alarmMaxCodeId;
    private List<EnumerationValue> enumValues;
    private String displayString;
    private String type;
}
