/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.dto;

public class Label {
    private int batteryPercentage;
    private boolean powerSupplyConnected;
    private String cds;
    private String locusOfControl;

    public int getBatteryPercentage() {
        return batteryPercentage;
    }

    public void setBatteryPercentage(int batteryPercentage) {
        this.batteryPercentage = batteryPercentage;
    }

    public boolean isPowerSupplyConnected() {
        return powerSupplyConnected;
    }

    public void setPowerSupplyConnected(boolean powerSupplyConnected) {
        this.powerSupplyConnected = powerSupplyConnected;
    }

    public String getCds() {
        return cds;
    }

    public void setCds(String cds) {
        this.cds = cds;
    }

    public String getLocusOfControl() {
        return locusOfControl;
    }

    public void setLocusOfControl(String locusOfControl) {
        this.locusOfControl = locusOfControl;
    }
}
