/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */


package io.saperi.vcm.service.dto.ext;

import io.saperi.vcm.domain.AssignedDevice;
import io.saperi.vcm.domain.enumeration.DeviceCapabilityType;

/**
 * DTO class for an {@link AssignedDevice} in the context of a monitor.
 *
 * @author esteban
 */
public class MonitorAssignedDeviceDTO {
    private MonitorDeviceDTO device;
    private DeviceCapabilityType capability;

    public MonitorDeviceDTO getDevice() {
        return device;
    }

    public void setDevice(MonitorDeviceDTO device) {
        this.device = device;
    }

    public DeviceCapabilityType getCapability() {
        return capability;
    }

    public void setCapability(DeviceCapabilityType capability) {
        this.capability = capability;
    }

}
