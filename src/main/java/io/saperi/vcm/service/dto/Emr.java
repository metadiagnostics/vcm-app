/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.dto;

public class Emr {
    private String[] conditions;
    private String[] mechanismOfInjury;
    private String[] location;
    private String[] allergies;
    private String locusOfControl;

    public Emr(String[] conditions, String[] mechanismOfInjury, String[] location, String[] allergies, String locusOfControl) {
        this.conditions = conditions;
        this.mechanismOfInjury = mechanismOfInjury;
        this.location = location;
        this.allergies = allergies;
        this.locusOfControl = locusOfControl;
    }

    public String[] getLocation() {
        return location;
    }

    public void setLocation(String[] location) {
        this.location = location;
    }

    public String[] getAllergies() {
        return allergies;
    }

    public void setAllergies(String[] allergies) {
        this.allergies = allergies;
    }

    public String getLocusOfControl() {
        return locusOfControl;
    }

    public void setLocusOfControl(String locusOfControl) {
        this.locusOfControl = locusOfControl;
    }

    public String[] getMechanismOfInjury() {
        return mechanismOfInjury;
    }

    public void setMechanismOfInjury(String[] mechanismOfInjury) {
        this.mechanismOfInjury = mechanismOfInjury;
    }

    public String[] getConditions() {
        return conditions;
    }

    public void setConditions(String[] conditions) {
        this.conditions = conditions;
    }
}
