/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.dto;

import lombok.Data;

@Data
public class PatientDemographics {
    private String patientId;
    private long timestamp;
    private String name;
    private String gender;
    private String medicalRecordNumber;
    private String estimatedGestationalAge;
    private String correctedGestationalAge;
    private String birthDate;
    private Integer daysOfLife;
    private String birthWeight;
    private String todayWeight;
    private String calculatedWeight;
}
