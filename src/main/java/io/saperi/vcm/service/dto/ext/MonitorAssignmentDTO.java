/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.dto.ext;

import io.saperi.vcm.domain.Assignment;
import io.saperi.vcm.domain.enumeration.VentilatorControlType;
import io.saperi.vcm.domain.enumeration.VentilatorModeType;
import io.saperi.vcm.domain.enumeration.VentilatorRecommenderType;
import java.util.List;

/**
 * DTO class for an {@link Assignment} in the context of a monitor.
 *
 * @author esteban
 */
public class MonitorAssignmentDTO {
    private long assignmentId;
    private String patientId;
    private String patientName;
    private VentilatorModeType ventilatorMode;
    private VentilatorRecommenderType ventilatorRecommenderType;
    private VentilatorControlType ventilatorControlType;
    private List<MonitorAssignedDeviceDTO> devices;

    public long getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(long assignmentId) {
        this.assignmentId = assignmentId;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public VentilatorModeType getVentilatorMode() {
        return ventilatorMode;
    }

    public void setVentilatorMode(VentilatorModeType ventilatorMode) {
        this.ventilatorMode = ventilatorMode;
    }

    public VentilatorRecommenderType getVentilatorRecommenderType() {
        return ventilatorRecommenderType;
    }

    public void setVentilatorRecommenderType(VentilatorRecommenderType ventilatorRecommenderType) {
        this.ventilatorRecommenderType = ventilatorRecommenderType;
    }

    public List<MonitorAssignedDeviceDTO> getDevices() {
        return devices;
    }

    public void setDevices(List<MonitorAssignedDeviceDTO> devices) {
        this.devices = devices;
    }

    public VentilatorControlType getVentilatorControlType() {
        return ventilatorControlType;
    }

    public void setVentilatorControlType(VentilatorControlType ventilatorControlType) {
        this.ventilatorControlType = ventilatorControlType;
    }
}
