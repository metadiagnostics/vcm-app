/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.dto;

public class Pump {
    private int id;
    private String name;
    private Boolean plugged;
    private int infusionRate1;
    private int infusionRate2;
    private int currentRate1;
    private int currentRate2;
    private int vi1;
    private int vi2;
    private int vtbi1;
    private int vtbi2;
    private String timeRemaining1;
    private String timeRemaining2;
    private String medicine1;
    private String medicine2;
    private String medicineBgClass1;
    private String medicineFntClass1;
    private String medicineBgClass2;
    private String medicineFntClass2;
    private String cds1;
    private String cds2;
    private String locusOfControl1;
    private String locusOfControl2;
    private int batteryPercentage; //this should be between 0 and 100

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getPlugged() {
        return plugged;
    }

    public void setPlugged(Boolean plugged) {
        this.plugged = plugged;
    }

    public int getInfusionRate1() {
        return infusionRate1;
    }

    public void setInfusionRate1(int infusionRate1) {
        this.infusionRate1 = infusionRate1;
    }

    public int getInfusionRate2() {
        return infusionRate2;
    }

    public void setInfusionRate2(int infusionRate2) {
        this.infusionRate2 = infusionRate2;
    }

    public int getCurrentRate1() {
        return currentRate1;
    }

    public void setCurrentRate1(int currentRate1) {
        this.currentRate1 = currentRate1;
    }

    public int getCurrentRate2() {
        return currentRate2;
    }

    public void setCurrentRate2(int currentRate2) {
        this.currentRate2 = currentRate2;
    }

    public int getVi1() {
        return vi1;
    }

    public void setVi1(int vi1) {
        this.vi1 = vi1;
    }

    public int getVi2() {
        return vi2;
    }

    public void setVi2(int vi2) {
        this.vi2 = vi2;
    }

    public int getVtbi1() {
        return vtbi1;
    }

    public void setVtbi1(int vtbi1) {
        this.vtbi1 = vtbi1;
    }

    public int getVtbi2() {
        return vtbi2;
    }

    public void setVtbi2(int vtbi2) {
        this.vtbi2 = vtbi2;
    }

    public String getTimeRemaining1() {
        return timeRemaining1;
    }

    public void setTimeRemaining1(String timeRemaining1) {
        this.timeRemaining1 = timeRemaining1;
    }

    public String getTimeRemaining2() {
        return timeRemaining2;
    }

    public void setTimeRemaining2(String timeRemaining2) {
        this.timeRemaining2 = timeRemaining2;
    }

    public String getMedicine1() {
        return medicine1;
    }

    public void setMedicine1(String medicine1) {
        this.medicine1 = medicine1;
    }

    public String getMedicine2() {
        return medicine2;
    }

    public void setMedicine2(String medicine2) {
        this.medicine2 = medicine2;
    }

    public String getMedicineBgClass1() {
        return medicineBgClass1;
    }

    public void setMedicineBgClass1(String medicineBgClass1) {
        this.medicineBgClass1 = medicineBgClass1;
    }

    public String getMedicineFntClass1() {
        return medicineFntClass1;
    }

    public void setMedicineFntClass1(String medicineFntClass1) {
        this.medicineFntClass1 = medicineFntClass1;
    }

    public String getMedicineBgClass2() {
        return medicineBgClass2;
    }

    public void setMedicineBgClass2(String medicineBgClass2) {
        this.medicineBgClass2 = medicineBgClass2;
    }

    public String getMedicineFntClass2() {
        return medicineFntClass2;
    }

    public void setMedicineFntClass2(String medicineFntClass2) {
        this.medicineFntClass2 = medicineFntClass2;
    }

    public int getBatteryPercentage() {
        return batteryPercentage;
    }

    public void setBatteryPercentage(int batteryPercentage) {
        this.batteryPercentage = batteryPercentage;
    }

    public String getCds1() {
        return cds1;
    }

    public void setCds1(String cds1) {
        this.cds1 = cds1;
    }

    public String getCds2() {
        return cds2;
    }

    public void setCds2(String cds2) {
        this.cds2 = cds2;
    }

    public String getLocusOfControl1() {
        return locusOfControl1;
    }

    public void setLocusOfControl1(String locusOfControl1) {
        this.locusOfControl1 = locusOfControl1;
    }

    public String getLocusOfControl2() {
        return locusOfControl2;
    }

    public void setLocusOfControl2(String locusOfControl2) {
        this.locusOfControl2 = locusOfControl2;
    }
}
