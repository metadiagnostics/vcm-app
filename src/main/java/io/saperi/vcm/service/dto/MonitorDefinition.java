/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.dto;

import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.*;
import lombok.extern.jackson.Jacksonized;

/**
 * @author sgroh@saperi.io
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@EqualsAndHashCode
public class MonitorDefinition {
    private int order;
    private String name;
    private String unit;
    private boolean hasAlarmMax;
    private boolean hasAlarmMin;
    private int alarmMax;
    private int alarmMin;
    private String[] dataKeys;
    private String[] selectors;
    private String ddScope;
    private String[] data;
    private boolean expanded;
    private boolean physiologicMonitors;
    private boolean ventFlow;
    private boolean ventSettings;
    private boolean ventMonitors;
    private boolean reduced;
    private boolean selected;
    private String type;
    private String xlabel;
    private String ylabel;
    private String separator;
    private int chartMaxDataLength;
    private String alarmMinCodeId;
    private String alarmMaxCodeId;
    private String hwId;
    private int yaxisMin;
    private int yaxisMax;
}
