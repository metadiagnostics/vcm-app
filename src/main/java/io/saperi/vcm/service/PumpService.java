/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.saperi.vcm.event.DeviceFrameEvent;
import io.saperi.vcm.event.PumpEvent;
import io.saperi.vcm.service.dto.Pump;
import io.saperi.vcm.service.ext.impl.jaimsaggregator.JaimsAggregatorEventListenerService;
import io.saperi.vcm.util.FrameUtil;
import io.saperi.virtualhealth.device.aggregators.common.DeviceAggregatorClient;
import io.saperi.virtualhealth.device.aggregators.common.DeviceCtx;
import io.saperi.virtualhealth.device.aggregators.common.model.BatteryInfo;
import io.saperi.virtualhealth.device.aggregators.common.model.Command;
import io.saperi.virtualhealth.device.aggregators.common.model.DeviceInfo;
import io.saperi.virtualhealth.device.aggregators.common.model.Frame;
import io.saperi.virtualhealth.device.aggregators.common.model.MeasureValue;
import io.saperi.virtualhealth.device.aggregators.common.model.Setting;
import liquibase.pro.packaged.D;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service class for managing pump.
 */
@Slf4j
@Service
@Transactional
public class PumpService {
   private final ObjectMapper mapper = new ObjectMapper();

    @Value("${vcm.pump}")
    private String pumps;

    private List<Pump> existingPumps;
    private ApplicationEventPublisher applicationEventPublisher;
    private static final DecimalFormat df=new DecimalFormat("0.00");
    private MonitorDefinitionConfig monitorDefinitionConfig;
    private HashMap<String, DeviceAggregatorClient> clientMap = new HashMap<>();
    private HashMap<String, String> masterId_hwId = new HashMap<>();
    private Map<String, String> id_mappings = null;
    private static String NEUROWAVE = "Neurowave";
    private static String PUMP_1 = "PUMP1";
    private static String PUMP_2 = "PUMP2";
    // CONST USED TO Retrieve correct Pump Infusion codes from frames
    private static String PUMP_VI = "pi_vi";
    private static String PUMP_VTBI = "pi_vtbi";
    private static String PUMP_MODE = "pi_mode";
    private static String PUMP_BOLUS_RATE = "pi_bolusrate";
    private static String PUMP_MEDICATION = "pi_medicationsetting";
    private static String PUMP_BOLUS_PROG = "pi_bolusprog";
    private static String PUMP_BOLUS_PROG_RATE = "pi_bolusprograte";
    private static String PUMP_1_PAUSE_COMMAND = "pi_pausep1setting";
    private static String PUMP_2_PAUSE_COMMAND = "pi_pausep2setting";
    private static String PUMP_1_RESUME_COMMAND = "pi_resumep1setting";
    private static String PUMP_2_RESUME_COMMAND = "pi_resumep2setting";
    private static String PUMP_DEV_PROG_MODE = "pi_devprogmode";

    @Autowired
    public PumpService(ApplicationEventPublisher applicationEventPublisher,
                       MonitorDefinitionConfig monitorDefinitionConfig) {
        this.applicationEventPublisher = applicationEventPublisher;
        this.monitorDefinitionConfig = monitorDefinitionConfig;
        this.id_mappings = monitorDefinitionConfig.getUikey_devicekey();
    }

    @PostConstruct
    private void deserializeItems() throws JsonProcessingException {
        Pump[] localItemsArray = mapper.readValue(pumps, Pump[].class);
        existingPumps = Arrays.asList(localItemsArray);
    }

    public List<Pump> gePumps(){
        log.info("Obtaining local team members");
        return existingPumps;
    }

    private Double getRate(Double vi, Double vtbi){
        Double rate = Double.valueOf(100);
        // Volume to be infuse is a decreasing value. The pump infuse 100% when this is 0.
        if (vtbi<=0) return rate;
        rate = (vi * 100 / vtbi+vi);
        if (rate > 100 ){
            return Double.valueOf(100);
        }
        return rate;
    }

    @EventListener
    public void handleDeviceFrameEvent(DeviceFrameEvent event) {
        Frame frame = event.getFrame();
        DeviceCtx masterPumpCtx = FrameUtil.getMasterDeviceCtx(frame);
        if (masterPumpCtx != null && masterPumpCtx.getDeviceInfo()!=null && NEUROWAVE.equals(masterPumpCtx.getDeviceInfo().getManufacturer())){
            try {
                log.trace("Found Neurowave frame for HW_ID: {}", event.getHardwareId());
                HashMap<String, DeviceCtx> devicesMap = frame.getDevices();
                String masterDeviceName =  masterPumpCtx.getDeviceInfo().getId();
                // update this map that is used on sendDeviceCommands
                masterId_hwId.put(masterDeviceName, event.getHardwareId());
                PumpEvent pe = new PumpEvent(event.getHardwareId(), event.getHardwareId());
                DeviceCtx pump1Ctx = devicesMap.get(masterDeviceName + ":1");
                DeviceCtx pump2Ctx = devicesMap.get(masterDeviceName + ":2");
                DeviceCtx pumpMasterCtx = devicesMap.get(masterDeviceName);
                pe = populatePumpData(pe, pump1Ctx.getMeasureValues(), pump1Ctx.getSettingValues(), PUMP_1);
                pe = populatePumpData(pe, pump2Ctx.getMeasureValues(), pump2Ctx.getSettingValues(), PUMP_2);
                pe.setName(pumpMasterCtx.getDeviceInfo().getManufacturer()+" "+pumpMasterCtx.getDeviceInfo().getSystem_model_number());
                // Set battery status
                ArrayList<BatteryInfo> batteriesInfo = pumpMasterCtx.getDeviceInfo().getBatteryInfo();
                if (batteriesInfo.size() > 0) {
                    BatteryInfo batteryInfo = batteriesInfo.get(0);
                    pe.setOnMainsPower(pumpMasterCtx.getDeviceInfo().isOnMainsPower());
                    pe.setBatteryPercentage(batteryInfo.getPowerLevel());
                    pe.setBatteryPresent(batteryInfo.isPresent());
                }
                this.applicationEventPublisher.publishEvent(pe);
            } catch (Exception e) {
               log.error("Not possible to handle Device Frame Event to populate pumps", e);
            }
        }
    }

    private PumpEvent populatePumpData(PumpEvent pe,
                                       HashMap<String, MeasureValue> measureValues,
                                       HashMap<String, Setting> settingValues,
                                       String pumpId) {
        pe.setName("NeuroWave AccuPump");
        // pump1_pos1 = getNewRate(pump1_ch1_remaining_ms, pump1Ch1InfusionTime); // Not based on time?
        Double vi = getDoubleFromMeasure(id_mappings.get(PUMP_VI), measureValues,1);
        Double vtbi = getDoubleFromMeasure(id_mappings.get(PUMP_VTBI), measureValues, 1);
        Double bolusRate = getDoubleFromMeasure(id_mappings.get(PUMP_BOLUS_RATE), measureValues, 1);
        String devProgMode = getStringFromSetting(id_mappings.get(PUMP_DEV_PROG_MODE), settingValues);
        Double remainingTime = Double.valueOf(-1);
        if (bolusRate != null) {
            remainingTime = ((vtbi) * 3600000) / bolusRate;
        } else {
            bolusRate = Double.valueOf(0);
        }
        String medicine = settingValues.get(id_mappings.get(PUMP_MEDICATION)).getStringValue();
        Double bolusProg = getDoubleFromMeasure(id_mappings.get(PUMP_BOLUS_PROG), measureValues, 1);
        Double bolusProgRate = getDoubleFromSetting(id_mappings.get(PUMP_BOLUS_PROG_RATE), settingValues, 1);
        if (PUMP_1.equals(pumpId)) {
            pe.setVi1(vi);
            pe.setVtbi1(vtbi);
            pe.setInfusionRate1(getRate(vi, vtbi));
            pe.setCurrentRate1(bolusRate);
            if (remainingTime==-1){
                pe.setTimeRemaining1(""); // We do not have information
            } else {
                pe.setTimeRemaining1(DurationFormatUtils.formatDuration(remainingTime.intValue(),"H'h 'm'm 's'sec'"));
            }
            pe.setMedicine1(medicine);
            pe.setBolusProg1(bolusProg);
            pe.setBolusProgRate1(bolusProgRate);
            pe.setDevProgMode1(devProgMode);
        } else {
            pe.setVi2(vi);
            pe.setVtbi2(vtbi);
            pe.setInfusionRate2(getRate(vi, vtbi));
            pe.setCurrentRate2(bolusRate);
            if (remainingTime==-1){
                pe.setTimeRemaining2(""); // We do not have information
            } else {
                pe.setTimeRemaining2(DurationFormatUtils.formatDuration(remainingTime.intValue(),"H'h 'm'm 's'sec'"));
            }
            pe.setTimeRemaining2(DurationFormatUtils.formatDuration(remainingTime.intValue(),"H'h 'm'm 's'sec'"));
            pe.setMedicine2(medicine);
            pe.setBolusProg2(bolusProg);
            pe.setBolusProgRate2(bolusProgRate);
            pe.setDevProgMode2(devProgMode);
        }
        return pe;
    }

    private Double getDoubleFromMeasure(final String pumpKey, final HashMap<String, MeasureValue> measureValues, int precision) {
        Double value = null;
        try {
            value = new BigDecimal(measureValues.get(pumpKey).getStringValue()).setScale(precision, RoundingMode.HALF_UP).doubleValue();
        } catch (Exception e) {
            log.error("Could not get a Pump value from frame measures", e);
        }
        return value;
    }

    private Double getDoubleFromSetting(final String pumpKey, final HashMap<String, Setting> settingValues, int precision) {
        Double value = null;
        try {
            value = new BigDecimal(settingValues.get(pumpKey).getStringValue()).setScale(precision, RoundingMode.HALF_UP).doubleValue();
        } catch (Exception e) {
            log.error("Could not get a Pump value from frame settings", e);
        }
        return value;
    }

    private String getStringFromSetting(final String pumpKey, final HashMap<String, Setting> settingValues) {
        String value = "";
        try {
            value = settingValues.get(pumpKey).getStringValue();
        } catch (Exception e) {
            log.error("Could not get a String Pump value from frame settings", e);
        }
        return value;
    }

    public void pausePump(String masterDeviceId, String pumpId) {
        String commandId = PUMP_1.equals(pumpId)? PUMP_1_PAUSE_COMMAND: PUMP_2_PAUSE_COMMAND;
        String deviceId = masterDeviceId;
        Command c = new Command();
        c.setCommandId(id_mappings.get(commandId));
        String hwId = masterId_hwId.get(masterDeviceId);
        this.clientMap.get(hwId).sendDeviceCommand(deviceId, c);
        log.info("The pump {} for MasterDeviceID {} was paused", pumpId, deviceId);
    }

    public void resumePump(String masterDeviceId, String pumpId) {
        String commandId = PUMP_1.equals(pumpId)? PUMP_1_RESUME_COMMAND: PUMP_2_RESUME_COMMAND;
        String deviceId = masterDeviceId;
        Command c = new Command();
        c.setCommandId(id_mappings.get(commandId));
        String hwId = masterId_hwId.get(masterDeviceId);
        this.clientMap.get(hwId).sendDeviceCommand(deviceId, c);
        log.info("The pump {} for MasterDeviceID {} was resumed", pumpId, deviceId);
    }

    public void changePumpSetting(String masterDeviceId, String deviceId, String metricId, String data) {
        log.debug("Change Pump id {}, metricId: {}, data: {}", deviceId, metricId, data);
        String hwId = masterId_hwId.get(masterDeviceId);
        DeviceAggregatorClient daClient = this.clientMap.get(hwId);
        Frame aggregateFrame = daClient.getFrameManager().getAggregateFrame();
        List<DeviceInfo> devices = aggregateFrame.getDevicesWithSetting(metricId);
        if (devices.size()>0) {
            DeviceInfo deviceInfo = devices.stream().filter(di -> di.getId().equals(deviceId))
                .reduce((a, b) -> {
                    throw new IllegalStateException("Multiple devices with same id: " + a + ", " + b);
                })
                .get();
            Setting setting = new Setting();
            setting.setMetric_id(metricId);
            try {
                setting.setValue(Float.valueOf(data));
                setting.setStringValue(data);
            } catch (NumberFormatException e) {
                log.warn("Could not set ventilator value: {} on metric id: {}", data, metricId);
            }
            setting.setStringValue(data);
            daClient.postSettingChange(deviceInfo, List.of(setting));
        }
    }
    //
    public void changeSyncPumpSetting(String masterDeviceId, String deviceId, String metricIdBolusVolume, String metricIdBolusRate, String data) {
        log.debug("Change Pump id {}, metricId1: {}, metricId2: {}, data: {}", deviceId, metricIdBolusVolume, metricIdBolusRate, data);
        String hwId = masterId_hwId.get(masterDeviceId);
        DeviceAggregatorClient daClient = this.clientMap.get(hwId);
        Frame aggregateFrame = daClient.getFrameManager().getAggregateFrame();
        List<DeviceInfo> devices = aggregateFrame.getDevicesWithSetting(metricIdBolusVolume);
        if (devices.size()>0) {
            DeviceInfo deviceInfo = devices.stream().filter(di -> di.getId().equals(deviceId))
                .reduce((a, b) -> {
                    throw new IllegalStateException("Multiple devices with same id: " + a + ", " + b);
                })
                .get();
            Setting setting1 = new Setting();
            setting1.setMetric_id(metricIdBolusVolume);
            Setting setting2 = new Setting();
            setting2.setMetric_id(metricIdBolusRate);
            Double volusVolume = Double.valueOf(0);
            Double volusRate = Double.valueOf(0);

            try {
                String[] values = data.split("::");
                setting1.setStringValue(values[0]);
                setting2.setStringValue(values[1]);
                volusVolume = Double.valueOf(values[0]);
                volusRate = Double.valueOf(values[1]);
            } catch (Exception e) {
                log.warn("Could not set ventilator value: {} on metric id: {} and metric id {}", data, metricIdBolusVolume, metricIdBolusRate);
            }
            if (volusVolume.doubleValue() >= volusRate.doubleValue() ) {
                log.error("We can not send a Bolus Volume greater than Bolus Rate value");
            } else {
                daClient.postSettingChange(deviceInfo, List.of(setting1, setting2));
            }
        }
    }

    public void updateClientMap(HashMap<String, DeviceAggregatorClient> clientMap) {
        this.clientMap = clientMap;
    }
}
