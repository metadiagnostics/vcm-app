/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service;

import liquibase.pro.packaged.M;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service class for managing monitor.
 */
@Service
@Transactional
public class MonitorService {
    private final Logger log = LoggerFactory.getLogger(MonitorService.class);

    @Autowired
    MonitorDefinitionConfig monitorDefinitionConfig;

    public MonitorService() { }

    // Return a map of device key -> metric code id

    /***
     * Return a map of device key -> metric code id for settings
     * @return
     */
    public Map<String, String> getMappingSettingIds(){
        return getIdsMapping(true);
    }


    /***
     * Return a map of device key -> Limit id for alarms
     * @return
     */
    public Map<String, String> getMappingAlarmLimitIds(){
        return getLimitToIds();
    }
    /***
     * Return a map of device key -> metric code id for settings
     * @return
     */
    public Map<String, String> getMappingMeasureIds(){
        return getIdsMapping(false);
    }

    /***
     * Return a map of device key -> metric code id for settings or measures.
     * Alarm limits also are considered settings if we should split this just create another
     * @return
     */
    private Map<String, String> getIdsMapping(boolean setting){
        return monitorDefinitionConfig.getReverseMap(monitorDefinitionConfig.getUikey_devicekey()
            // Set<Map<K,V>.Entry<String,String>>
            .entrySet()
            .stream()
            .filter(entry -> entry.getKey().endsWith("setting") == setting)
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
    }

    /***
     * Return a map of device key -> metric code id for limts.
     * Alarm limits also are considered settings if we should split this just create another
     * @return
     */
    private Map<String, String> getIdsLimit(){
        return monitorDefinitionConfig.getReverseMap(monitorDefinitionConfig.getUikey_devicekey()
            // Set<Map<K,V>.Entry<String,String>>
            .entrySet()
            .stream()
            .filter(entry -> entry.getKey().endsWith("AlarmMin") || entry.getKey().endsWith("AlarmMax"))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
    }

    /***
     * Return a map of device key -> metric code id for limts.
     * Alarm limits also are considered settings if we should split this just create another
     * @return
     */
    private Map<String, String> getLimitToIds(){
        return monitorDefinitionConfig.getUikey_devicekey()
            // Set<Map<K,V>.Entry<String,String>>
            .entrySet()
            .stream()
            .filter(entry -> entry.getKey().endsWith("AlarmMin") || entry.getKey().endsWith("AlarmMax"))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

}
