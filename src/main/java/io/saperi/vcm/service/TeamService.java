/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.saperi.vcm.service.dto.TeamMember;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

/**
 * Service class for managing care teams.
 */
@Service
@Transactional
public class TeamService {
    private final Logger log = LoggerFactory.getLogger(TeamService.class);
    private final ObjectMapper mapper = new ObjectMapper();

    @Value("${vcm.care-team.local}")
    private String localItems;

    @Value("${vcm.care-team.remote}")
    private String remoteItems;

    private List<TeamMember> localTeamMembers;
    private List<TeamMember> remoteTeamMembers;

    public TeamService() { }

    @PostConstruct
    private void deserializeItems() throws JsonProcessingException {
        TeamMember[] localItemsArray = mapper.readValue(localItems, TeamMember[].class);
        localTeamMembers = Arrays.asList(localItemsArray);
        TeamMember[] remoteItemsArray = mapper.readValue(remoteItems, TeamMember[].class);
        remoteTeamMembers = Arrays.asList(remoteItemsArray);
    }

    public List<TeamMember> getLocalTeamMembers(){
        log.info("Obtaining local team members");
        return localTeamMembers;
    }

    public List<TeamMember> getRemoteTeamMembers(){
        log.info("Obtaining remote team members");
        return remoteTeamMembers;
    }

}
