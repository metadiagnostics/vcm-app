/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service;

import io.saperi.vcm.event.AlarmWarningEvent;
import io.saperi.vcm.event.DeviceFrameEvent;
import io.saperi.vcm.service.dto.Alarm;
import io.saperi.vcm.service.dto.AlarmWarning;
import io.saperi.vcm.util.FrameUtil;
import io.saperi.virtualhealth.device.aggregators.common.DeviceCtx;
import io.saperi.virtualhealth.device.aggregators.common.model.Frame;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Service class for managing alarm warnings.
 */
@Service
@Transactional
@Slf4j
public class AlarmWarningService {
    private ApplicationEventPublisher applicationEventPublisher;
    private Map<String, Pair<Integer, Instant>> alarmHashes = new HashMap<>();

    @Value("${vcm.alarm-threshold-sec:15}")
    private Integer threshold_secs;

    private Duration alarmThresholdSecs;

    @Autowired
    public AlarmWarningService(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @PostConstruct
    public void postConstructSetup(){
        alarmThresholdSecs = Duration.ofSeconds(threshold_secs);
    }

    /**
     * Deprecated should be reimplemented if alarm warning metadata is needed.
     */
    @Deprecated
    public void resetAllOccurences() {

    }

    /**
     * Deprecated, the alarms will be always be pushed from Frame (server) to client
     * In case we should keep them in memory to do some kind of individual calculation
     * We should store alarms warnings with some kind of metadata on each entry.
     * @param patientId
     * @return
     */
    @Deprecated
    public List<AlarmWarning> getAlarmWarnings(String patientId){
        return null;
    }

    /**
     * Since we are not storing alarm warnings this method is totally deprecated
     * @param id
     * @param patientId
     * @return
     */
    @Deprecated
    public synchronized AlarmWarning getAlarmWarning(String id, String patientId){
        return null;
    }

    /**
     * Since we are not storing alarm warnings this method is totally deprecated
     * @param patientId
     * @return
     */
    @Deprecated
    public synchronized List<AlarmWarning> ackAlarmWarning(String patientId) {
        return null;
    }


    /**
     * Since we are not storing alarm warnings this method is totally deprecated
     * @param patientId
     * @return
     */
    @Deprecated
    public synchronized void ackAlarmWarning(String id, String patientId) {}

    @EventListener
    public void handleDeviceFrameEvent(final DeviceFrameEvent event) {
        Frame frame = event.getFrame(); // TODO: check if this can be aggregate
        DeviceCtx deviceCtx = FrameUtil.getMasterDeviceCtx(frame);
        if (deviceCtx != null){
            // masterId_hwId.put(masterDeviceName, event.getHardwareId());
            HashMap<String, io.saperi.virtualhealth.device.aggregators.common.model.Alarm> alarmsMap= deviceCtx.getActiveAlarms();
            AlarmWarningEvent[] alarmWarnings = new AlarmWarningEvent[alarmsMap.size()];
            int i=0;
            int hashCode =1;
            for (io.saperi.virtualhealth.device.aggregators.common.model.Alarm alarm: alarmsMap.values()) {
                AlarmWarningEvent alarmWarning= new AlarmWarningEvent(event.getHardwareId(),this);
                alarmWarning.setId(UUID.randomUUID().toString());
                alarmWarning.setTitle(alarm.getUniform_id());
                alarmWarning.setSeverity(alarm.getPriority().toLowerCase());
                // Saperi extension
                alarmWarning.setBody(alarm.getText());
                alarmWarning.setTitle(alarm.getTitle());
                alarmWarning.setAlarmClass(alarm.getAlarmClass());
                alarmWarnings[i++] = alarmWarning;
                hashCode = 31*hashCode + alarmWarning.hashCode();
            }
            if (alarmWarnings.length > 0) {
                // store in a map hwId ->  hashcode of the alarms + timestamp
                Pair<Integer, Instant> alarmHash = alarmHashes.get(event.getHardwareId());
                if (alarmHash == null) {
                    this.updateAlarmMapAndSendEvent(event, hashCode, alarmWarnings);
                } else {
                    Integer oldHash = alarmHash.getLeft();
                    if (oldHash.intValue() == hashCode) {
                        Instant then = alarmHash.getRight();
                        // If has elapsed at least 5 seconds store new time and send
                        if (Duration.between(then, Instant.now()).toSeconds() > alarmThresholdSecs.toSeconds()) {
                            this.updateAlarmMapAndSendEvent(event, hashCode, alarmWarnings);
                        }
                    } else {
                        this.updateAlarmMapAndSendEvent(event, hashCode, alarmWarnings);
                    }
                }
            }
        }
    }

    private void updateAlarmMapAndSendEvent(final DeviceFrameEvent event, int hashCode, final  AlarmWarningEvent[] alarmWarnings){
        alarmHashes.put(event.getHardwareId(), new ImmutablePair<>(hashCode, Instant.now()));
        this.applicationEventPublisher.publishEvent(alarmWarnings);
    }


}
