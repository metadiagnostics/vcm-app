/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.impl;

import io.saperi.vcm.domain.AssignedDevice;
import io.saperi.vcm.domain.Assignment;
import io.saperi.vcm.domain.Device;
import io.saperi.vcm.domain.DeviceModel;
import io.saperi.vcm.domain.enumeration.DeviceCapabilityType;
import io.saperi.vcm.domain.enumeration.DeviceType;
import io.saperi.vcm.event.DeviceModificationEvent;
import io.saperi.vcm.event.Fio2AckRequestEvent;
import io.saperi.vcm.event.VentilatorModeChangeRequestEvent;
import io.saperi.vcm.repository.AssignmentRepository;
import io.saperi.vcm.service.AssignmentService;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static java.util.stream.Collectors.toSet;

/**
 * Service Implementation for managing {@link Assignment}.
 */
@Service
@Transactional
public class AssignmentServiceImpl implements AssignmentService {

    private final Logger log = LoggerFactory.getLogger(AssignmentServiceImpl.class);

    private final AssignmentRepository assignmentRepository;

    private final ApplicationEventPublisher applicationEventPublisher;

    public AssignmentServiceImpl(AssignmentRepository assignmentRepository, final ApplicationEventPublisher applicationEventPublisher) {
        this.assignmentRepository = assignmentRepository;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    /**
     * Save a assignment.
     *
     * @param assignment the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Assignment save(Assignment assignment) {
        return this.save(assignment,null);
    }


    /**
     * Save a assignment.
     *
     * @param assignment the entity to save.
     * @param username the username saving the entity.
     * @return the persisted entity.
     */
    @Override
    public Assignment save(Assignment assignment, String username) {
        log.debug("Request to save Assignment : {}", assignment);

        if (assignment.getId() == null) {
            Set<AssignedDevice> devices = assignment.getDevices();
            assignment.setDevices(new HashSet<>());
            assignment = assignmentRepository.save(assignment);

            if (devices != null && !devices.isEmpty()) {
                for (AssignedDevice device : devices) {
                    device.setAssignment(assignment);
                }
                assignment.setDevices(devices);
                assignment = assignmentRepository.save(assignment);
            }
            // Publish Assignment Change when we create it.
            publishAssignmentChange(assignment);
            return assignment;
        } else {
            Assignment existingAssignment = assignmentRepository.getOne(assignment.getId());
            if ( existingAssignment.getVentilatorMode() != null && !existingAssignment.getVentilatorMode().equals(assignment.getVentilatorMode()) ) {
                //When the ventilator mode changes
                Set<Device> devices = existingAssignment.getDevices().stream()
                    .filter(d -> d.getCapability().getType() == DeviceCapabilityType.VENTILATOR_CONTROL)
                    .map(ad -> ad.getDevice())
                    .collect(toSet());
                for (Device device: devices) {
                    applicationEventPublisher.publishEvent(new VentilatorModeChangeRequestEvent(device, assignment,  existingAssignment.getId(), username));
                }
            }
            publishAssignmentChange(assignment);
            return assignmentRepository.save(assignment);
        }
    }

    /**
     * Propagates a Device Modification Event.
     * @param assignment
     */
    private void publishAssignmentChange(Assignment assignment) {
        this.publishAssignmentChange(assignment, false, DeviceModificationEvent.Operation.ACTIVATION);
    }

        /**
         * Propagates a Device Modification Event.
         * @param assignment
         */
    private void publishAssignmentChange(Assignment assignment, boolean forceOperation, DeviceModificationEvent.Operation operation) {
        Set<String> hwIds = new HashSet<>();
        Map<String, DeviceModel> devicesModel = new HashMap<>();
        if (assignment.getDevices()!=null) {
            Set<Device> devices = assignment.getDevices().stream().map(AssignedDevice::getDevice).collect(Collectors.toSet());
            if (devices != null) {
                for (Device dev: devices) {
                    if (hwIds.add(dev.getHardwareId())) {
                        devicesModel.put(dev.getHardwareId(), dev.getModel());
                    }
                }
            }
        }
        DeviceModificationEvent deviceModificationEvent;
        for (String hwId: hwIds) {
            if ((assignment.isActive() && !forceOperation) || (forceOperation && operation == DeviceModificationEvent.Operation.ACTIVATION)) {
                deviceModificationEvent =
                    new DeviceModificationEvent(hwId, hwId, DeviceModificationEvent.Operation.ACTIVATION, devicesModel.get(hwId), devicesModel.get(hwId), this);
            } else {
                deviceModificationEvent =
                    new DeviceModificationEvent(hwId, hwId, DeviceModificationEvent.Operation.DEACTIVATION, devicesModel.get(hwId), devicesModel.get(hwId), this);
            }
            applicationEventPublisher.publishEvent(deviceModificationEvent);
        }
    }

    /**
     * Get all the assignments.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<Assignment> findAll() {
        log.debug("Request to get all Assignments");
        return assignmentRepository.findAll();
    }

    /**
     * Get one assignment by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Assignment> findOne(Long id) {
        log.debug("Request to get Assignment : {}", id);
        return assignmentRepository.findById(id);
    }

    /**
     * Delete the assignment by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Assignment : {}", id);
        Optional<Assignment> optionalAssignment = this.findOne(id);
        if (optionalAssignment.isPresent()) {
            Assignment assignment = optionalAssignment.get();
            if (assignment.getDevices()!=null) {
                Optional<AssignedDevice> optionalAssignedDevice = assignment.getDevices().stream().findFirst();
                if (optionalAssignedDevice.isPresent()) {
                    AssignedDevice assignedDevice = optionalAssignedDevice.get();
                    if (assignedDevice.getDevice()!= null) {
                        String hwId = assignedDevice.getDevice().getHardwareId();
                        DeviceModificationEvent deviceModificationEvent =
                            new DeviceModificationEvent(hwId, hwId, DeviceModificationEvent.Operation.DEACTIVATION, assignedDevice.getDevice().getModel(), assignedDevice.getDevice().getModel(), this);
                        applicationEventPublisher.publishEvent(deviceModificationEvent);
                    }
                }
            }
            assignmentRepository.deleteById(id);
        }
    }

    public void publishRefreshAssignmentChange(Assignment assignment) {
        this.publishAssignmentChange(assignment, true, DeviceModificationEvent.Operation.DEACTIVATION);
        try {
            Thread.sleep(500);
            if (assignment.getDevices()!=null &&
                assignment.getDevices().stream()
                    .filter(ad -> ad.getDevice()!=null && ad.getDevice().getModel()!=null && ad.getDevice().getModel().getType()!=null && ad.getDevice().getModel().getType().equals(DeviceType.NIHONKOHDEN)).count() >= 1) {
                Thread.sleep(2000);
            }
        } catch (InterruptedException e) {
            log.error("Error waiting for half a second on refresh assignment", e);
        }
        this.publishAssignmentChange(assignment, true, DeviceModificationEvent.Operation.ACTIVATION);
    }
}
