/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.impl;

import io.saperi.vcm.service.DeviceCapabilityService;
import io.saperi.vcm.domain.DeviceCapability;
import io.saperi.vcm.repository.DeviceCapabilityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link DeviceCapability}.
 */
@Service
@Transactional
public class DeviceCapabilityServiceImpl implements DeviceCapabilityService {

    private final Logger log = LoggerFactory.getLogger(DeviceCapabilityServiceImpl.class);

    private final DeviceCapabilityRepository deviceCapabilityRepository;

    public DeviceCapabilityServiceImpl(DeviceCapabilityRepository deviceCapabilityRepository) {
        this.deviceCapabilityRepository = deviceCapabilityRepository;
    }

    /**
     * Save a deviceCapability.
     *
     * @param deviceCapability the entity to save.
     * @return the persisted entity.
     */
    @Override
    public DeviceCapability save(DeviceCapability deviceCapability) {
        log.debug("Request to save DeviceCapability : {}", deviceCapability);
        return deviceCapabilityRepository.save(deviceCapability);
    }

    /**
     * Get all the deviceCapabilities.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<DeviceCapability> findAll() {
        log.debug("Request to get all DeviceCapabilities");
        return deviceCapabilityRepository.findAll();
    }

    /**
     * Get one deviceCapability by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DeviceCapability> findOne(Long id) {
        log.debug("Request to get DeviceCapability : {}", id);
        return deviceCapabilityRepository.findById(id);
    }

    /**
     * Delete the deviceCapability by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DeviceCapability : {}", id);
        deviceCapabilityRepository.deleteById(id);
    }
}
