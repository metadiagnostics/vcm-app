/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.impl;

import io.saperi.vcm.service.DeviceModelService;
import io.saperi.vcm.domain.DeviceModel;
import io.saperi.vcm.repository.DeviceModelRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link DeviceModel}.
 */
@Service
@Transactional
public class DeviceModelServiceImpl implements DeviceModelService {

    private final Logger log = LoggerFactory.getLogger(DeviceModelServiceImpl.class);

    private final DeviceModelRepository deviceModelRepository;

    public DeviceModelServiceImpl(DeviceModelRepository deviceModelRepository) {
        this.deviceModelRepository = deviceModelRepository;
    }

    /**
     * Save a deviceModel.
     *
     * @param deviceModel the entity to save.
     * @return the persisted entity.
     */
    @Override
    public DeviceModel save(DeviceModel deviceModel) {
        log.debug("Request to save DeviceModel : {}", deviceModel);
        return deviceModelRepository.save(deviceModel);
    }

    /**
     * Get all the deviceModels.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<DeviceModel> findAll() {
        log.debug("Request to get all DeviceModels");
        return deviceModelRepository.findAllWithEagerRelationships();
    }

    /**
     * Get all the deviceModels with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<DeviceModel> findAllWithEagerRelationships(Pageable pageable) {
        return deviceModelRepository.findAllWithEagerRelationships(pageable);
    }

    /**
     * Get one deviceModel by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DeviceModel> findOne(Long id) {
        log.debug("Request to get DeviceModel : {}", id);
        return deviceModelRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the deviceModel by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DeviceModel : {}", id);
        deviceModelRepository.deleteById(id);
    }
}
