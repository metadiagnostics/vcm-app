/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.impl;

import io.saperi.vcm.service.AssignedDeviceService;
import io.saperi.vcm.domain.AssignedDevice;
import io.saperi.vcm.repository.AssignedDeviceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link AssignedDevice}.
 */
@Service
@Transactional
public class AssignedDeviceServiceImpl implements AssignedDeviceService {

    private final Logger log = LoggerFactory.getLogger(AssignedDeviceServiceImpl.class);

    private final AssignedDeviceRepository assignedDeviceRepository;

    public AssignedDeviceServiceImpl(AssignedDeviceRepository assignedDeviceRepository) {
        this.assignedDeviceRepository = assignedDeviceRepository;
    }

    /**
     * Save a assignedDevice.
     *
     * @param assignedDevice the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AssignedDevice save(AssignedDevice assignedDevice) {
        log.debug("Request to save AssignedDevice : {}", assignedDevice);
        return assignedDeviceRepository.save(assignedDevice);
    }

    /**
     * Get all the assignedDevices.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<AssignedDevice> findAll() {
        log.debug("Request to get all AssignedDevices");
        return assignedDeviceRepository.findAll();
    }

    /**
     * Get one assignedDevice by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AssignedDevice> findOne(Long id) {
        log.debug("Request to get AssignedDevice : {}", id);
        return assignedDeviceRepository.findById(id);
    }

    /**
     * Delete the assignedDevice by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AssignedDevice : {}", id);
        assignedDeviceRepository.deleteById(id);
    }
}
