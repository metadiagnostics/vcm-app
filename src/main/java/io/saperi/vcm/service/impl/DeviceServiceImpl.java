/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.impl;

import static java.util.stream.Collectors.joining;

import io.saperi.vcm.domain.Device;
import io.saperi.vcm.domain.DeviceModel;
import io.saperi.vcm.domain.enumeration.DeviceType;
import io.saperi.vcm.event.DeviceModificationEvent;
import io.saperi.vcm.repository.DeviceRepository;
import io.saperi.vcm.service.DeviceService;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Device}.
 */
@Service
@Transactional
public class DeviceServiceImpl implements DeviceService {

    private final Logger log = LoggerFactory.getLogger(DeviceServiceImpl.class);

    private final DeviceRepository deviceRepository;
    private final ApplicationEventPublisher applicationEventPublisher;

    public DeviceServiceImpl(DeviceRepository deviceRepository, ApplicationEventPublisher applicationEventPublisher) {
        this.deviceRepository = deviceRepository;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    /**
     * Save a device.
     *
     * @param device the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Device save(Device device) {
        log.debug("Request to save Device : {}", device);

        DeviceModificationEvent.Operation operation = DeviceModificationEvent.Operation.CREATE;
        DeviceModel oldDeviceModel = null;
        String oldHardwareId = device.getHardwareId();

        if (device.getId() != null) {
            Optional<Device> oldDevice = deviceRepository.findById(device.getId());
            if (oldDevice.isPresent()) {
                operation = DeviceModificationEvent.Operation.UPDATE;
                oldDeviceModel = oldDevice.get().getModel();
                oldHardwareId = oldDevice.get().getHardwareId();
            }
        }

        if (device.getModel() != null && device.getModel().getType() == DeviceType.GTX && device.getHardwareId() != null && !device.getHardwareId().startsWith("0x")) {
            String hId = device.getHardwareId().chars().mapToObj(c -> String.format("%02x", c)).collect(joining()).toUpperCase();
            device.setHardwareId("0x" + hId + " 0x00000000 0x00000000 0x00000000");
        }

        device = deviceRepository.save(device);

        applicationEventPublisher.publishEvent(new DeviceModificationEvent(device.getHardwareId(), oldHardwareId, operation, oldDeviceModel, device.getModel(), ""));

        return device;
    }

    /**
     * Get all the devices.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<Device> findAll() {
        log.debug("Request to get all Devices");
        return deviceRepository.findAll();
    }

    /**
     * Get one device by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Device> findOne(Long id) {
        log.debug("Request to get Device : {}", id);
        return deviceRepository.findById(id);
    }

    /**
     * Delete the device by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Device : {}", id);

        Optional<Device> oldDevice = deviceRepository.findById(id);

        deviceRepository.deleteById(id);

        if (oldDevice.isPresent()) {
            applicationEventPublisher.publishEvent(new DeviceModificationEvent(
                oldDevice.get().getHardwareId(),
                oldDevice.get().getHardwareId(),
                DeviceModificationEvent.Operation.DELETE,
                oldDevice.get().getModel(),
                null, ""));
        }
    }
}
