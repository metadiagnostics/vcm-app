/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service;

import io.saperi.vcm.domain.DeviceCapability;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link DeviceCapability}.
 */
public interface DeviceCapabilityService {

    /**
     * Save a deviceCapability.
     *
     * @param deviceCapability the entity to save.
     * @return the persisted entity.
     */
    DeviceCapability save(DeviceCapability deviceCapability);

    /**
     * Get all the deviceCapabilities.
     *
     * @return the list of entities.
     */
    List<DeviceCapability> findAll();

    /**
     * Get the "id" deviceCapability.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DeviceCapability> findOne(Long id);

    /**
     * Delete the "id" deviceCapability.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
