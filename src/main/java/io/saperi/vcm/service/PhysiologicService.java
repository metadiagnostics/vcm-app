/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.saperi.vcm.domain.Device;
import io.saperi.vcm.service.dto.Label;
import io.saperi.vcm.service.dto.MonitorDefinition;
import io.saperi.vcm.service.ext.impl.jaimsaggregator.MapData;
import io.saperi.vcm.service.ext.impl.jaimsaggregator.Mapping;
import io.saperi.virtualhealth.device.aggregators.common.managers.DeviceCapabilityManager;
import io.saperi.virtualhealth.device.aggregators.common.managers.EnumGroupManager;
import io.saperi.virtualhealth.device.aggregators.common.managers.UnitManager;
import io.saperi.virtualhealth.device.aggregators.common.model.CapabilityInfo;
import io.saperi.virtualhealth.device.aggregators.common.model.DeviceCapabilities;
import io.saperi.virtualhealth.device.aggregators.common.model.UnitSet;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service class for managing physiologic monitors.
 */
@Slf4j
@Service
@Transactional
public class PhysiologicService {

    private final ObjectMapper mapper = new ObjectMapper();
    // TODO: Do not rely services on this mappings
    private HashMap<String, MapData> metricMap = Mapping.metricMap;

    @Value("${vcm.datasetName:ISO11073}")
    private String datasetName;

    @Value("${vcm.physiologic}")
    private String physiologicStr;

    @Autowired
    MonitorDefinitionConfig monitorDefinitionConfig;

    @Autowired
    DeviceConfiguration deviceConfiguration;

    private final SimpMessageSendingOperations messagingTemplate;

    private MonitorService monitorService;


    private Label physiologic;

    @Autowired
    public PhysiologicService(SimpMessageSendingOperations messagingTemplate,
                              MonitorService monitorService){
        this.messagingTemplate = messagingTemplate;
        this.monitorService = monitorService;
    }

    @PostConstruct
    private void deserializeItems() throws JsonProcessingException {
        physiologic = mapper.readValue(physiologicStr, Label.class);
    }

    public Label getLabels(){
        log.info("Obtaining physiologic labels");
        return physiologic;
    }

    public MonitorDefinition[] getMonitorDefinitions(String hwId){
        log.info("Obtaining physiologic monitor Definitions");
        sendNewConfiguration(hwId); // This line will send information thought WS
        return monitorDefinitionConfig
            .getMonitor_definitions()
            .values()
            .toArray(MonitorDefinition[]::new);
    }

    /**
     * This method is going to be triggered from an event once the Frame containing setting is found
     * @param hwId
     */
    public void sendNewConfiguration(final String hwId) {
        Map<String, Device> hwDevice = deviceConfiguration.getHw_device();
        Device device =  hwDevice!=null?hwDevice.get(hwId):null;
        if (device!=null && StringUtils.isNotEmpty(device.getManufacturer()) && StringUtils.isNotEmpty(device.getModelName())) {
            MonitorDefinition[] newDefinitions = getMonitorDefinitionsFromDeviceCapabilities(hwId, device.getManufacturer(), device.getModelName());
            messagingTemplate.convertAndSend("/topic/hardware/" + hwId + "/vitals-data/settings", newDefinitions);
        } else {
            log.error("There is no device registered for this HardwareID {}", hwId);
        }
    }

    /**
     * Obtaining monitor definitions from DeviceCapabilityManager
     *
     * @param hwId
     * @param manufacturer
     * @param model
     * @return an Array of onitorDefinition
     */
    private MonitorDefinition[] getMonitorDefinitionsFromDeviceCapabilities(final String hwId, final String manufacturer, final String model) {
        List<MonitorDefinition> aggregatorVitalsDefinitions;
        DeviceCapabilityManager dc = DeviceCapabilityManager.getDeviceCapabilityManager();
        UnitManager unitManager = UnitManager.getUnitManager();
        UnitSet unitSet = unitManager.getUnitSet(datasetName);
        DeviceCapabilities cap = dc.getDeviceCapabilities(manufacturer, model);
        List<CapabilityInfo> deviceNumericList = cap.getNumericMeasureList();
        aggregatorVitalsDefinitions = new ArrayList<>();
        Map<String,String> widgetMappings = getMonitorDefinitionsFromConfig();
        HashMap<String, MonitorDefinition> monitorDefinitions = monitorDefinitionConfig.getMonitor_definitions();
        Map<String,String> idToAlarmLimit = monitorService.getMappingAlarmLimitIds();

        deviceNumericList.stream().forEach(s -> {
            String widgetId = widgetMappings.get(s.getCapabilityId());
            MonitorDefinition md = monitorDefinitions.get(widgetId);
            if (md != null) {
                aggregatorVitalsDefinitions.add(new MonitorDefinition().toBuilder()
                    .name(widgetId.toLowerCase())
                    .order(md.getOrder())
                    .unit(unitSet.getUnit(s.getCapabilityId())!=null?unitSet.getUnit(s.getCapabilityId()).getAbbreviation():null)
                    .hasAlarmMin(s.isHasLowLimit())
                    .hasAlarmMax(s.isHasHighLimit())
                    .alarmMax((int) s.getHighLimit())
                    .alarmMin((int) s.getLowLimit())
                    .alarmMinCodeId(idToAlarmLimit.get(widgetId + "AlarmMin")) //should use the convention <MetricId>alarmMin
                    .alarmMaxCodeId(idToAlarmLimit.get(widgetId + "AlarmMax")) //should use the convention <MetricId>alarmMax
                    .hwId(hwId)
                    .dataKeys(md.getDataKeys())
                    .build());
            }
        });
        // Pre settings UI

        Set<MonitorDefinition> definitionSet = new TreeSet<>((o1, o2) -> o1.getOrder() - o2.getOrder());
        int expandedCount = 0;
        for (MonitorDefinition md: aggregatorVitalsDefinitions) {
            MonitorDefinition userDefinedMonitorDef = monitorDefinitions.get(md.getName());
            if (userDefinedMonitorDef == null) {
                log.warn("The Monitor Definition for {} was not found on settings", md.getName());
            } else {
                // Override the values if they are defined on settings
                if (md.isHasAlarmMin()) {
                    userDefinedMonitorDef.setAlarmMin(md.getAlarmMin());
                }
                if (md.isHasAlarmMax()) {
                    userDefinedMonitorDef.setAlarmMax(md.getAlarmMax());
                }
                if (md.getUnit() == null) {
                    userDefinedMonitorDef.setUnit(md.getUnit());
                }
                userDefinedMonitorDef.setAlarmMaxCodeId(md.getAlarmMaxCodeId());
                userDefinedMonitorDef.setAlarmMinCodeId(md.getAlarmMinCodeId());
                userDefinedMonitorDef.setHwId(md.getHwId());
                definitionSet.add(userDefinedMonitorDef);
                if (userDefinedMonitorDef.isExpanded()){
                    expandedCount++;
                }
            }
        }
        // Check if we have at least 3 selected and expanded
        // (by design should be at least three in the UI)
        if (definitionSet.size()>=3 && expandedCount<3) {
            while (expandedCount<3) {
                MonitorDefinition md = definitionSet.stream().filter(ds->!ds.isExpanded()).findFirst().get();
                md.setExpanded(true);
                md.setSelected(true);
                expandedCount++;
            }
        }
        return definitionSet.toArray(MonitorDefinition[]::new);
    }

    private Map<String,String> getMonitorDefinitionsFromConfig() {
        return monitorDefinitionConfig.getReverseMap(monitorDefinitionConfig.getUikey_devicekey()
        .entrySet()
        .stream()
        .filter(e -> !e.getKey().contains("setting"))
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,(k1, k2) -> k1)));
    }

}
