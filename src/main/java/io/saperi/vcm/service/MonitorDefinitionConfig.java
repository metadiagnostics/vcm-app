/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service;

import io.saperi.vcm.service.dto.MonitorDefinition;
import io.saperi.vcm.service.dto.MonitorSettingDefinition;
import io.saperi.vcm.service.dto.PatientDefinition;
import io.saperi.virtualhealth.device.aggregators.common.model.EnumerationValue;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author sgroh@saperi.io
 */
@ConfigurationProperties("vcm")
@Setter
@Getter
public class MonitorDefinitionConfig {
    private HashMap<String, MonitorDefinition> monitor_definitions;
    private HashMap<String, MonitorDefinition> ventilator_definitions_ro;
    private HashMap<String, MonitorSettingDefinition> ventilator_definitions_rw;
    private ArrayList<EnumerationValue> ventilator_modes;
    private HashMap<String, String> uikey_devicekey;
    private HashMap<String, PatientDefinition> emr;
    private HashMap<String, String[]> menu;
    public Map<String, String> getReverseMap(Map<String, String> toReverse) {
        return toReverse.entrySet()
            .stream()
            .collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey, (s1, s2) -> s1));
    }
}
