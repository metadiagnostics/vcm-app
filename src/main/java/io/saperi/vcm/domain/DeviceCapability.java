/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

import io.saperi.vcm.domain.enumeration.DeviceCapabilityType;

/**
 * This Entity exists only to have the DeviceCapabilityType\nas an Entity because JHipster doesn't support relationships\nwith enums.
 */
@ApiModel(description = "This Entity exists only to have the DeviceCapabilityType\nas an Entity because JHipster doesn't support relationships\nwith enums.")
@Entity
@Table(name = "device_capability")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DeviceCapability implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private DeviceCapabilityType type;

    @ManyToMany(mappedBy = "capabilities")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<DeviceModel> deviceModels = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DeviceCapabilityType getType() {
        return type;
    }

    public DeviceCapability type(DeviceCapabilityType type) {
        this.type = type;
        return this;
    }

    public void setType(DeviceCapabilityType type) {
        this.type = type;
    }

    public Set<DeviceModel> getDeviceModels() {
        return deviceModels;
    }

    public DeviceCapability deviceModels(Set<DeviceModel> deviceModels) {
        this.deviceModels = deviceModels;
        return this;
    }

    public DeviceCapability addDeviceModels(DeviceModel deviceModel) {
        this.deviceModels.add(deviceModel);
        deviceModel.getCapabilities().add(this);
        return this;
    }

    public DeviceCapability removeDeviceModels(DeviceModel deviceModel) {
        this.deviceModels.remove(deviceModel);
        deviceModel.getCapabilities().remove(this);
        return this;
    }

    public void setDeviceModels(Set<DeviceModel> deviceModels) {
        this.deviceModels = deviceModels;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DeviceCapability)) {
            return false;
        }
        return id != null && id.equals(((DeviceCapability) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "DeviceCapability{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            "}";
    }
}
