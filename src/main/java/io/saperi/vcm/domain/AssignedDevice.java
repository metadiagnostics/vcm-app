/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * When a Device is assigned to an Assignment, it fulfills\none or more roles (capabilities). This entity holds\nthat information. This entity is basically a N-M with extra data.
 */
@ApiModel(description = "When a Device is assigned to an Assignment, it fulfills\none or more roles (capabilities). This entity holds\nthat information. This entity is basically a N-M with extra data.")
@Entity
@Table(name = "assigned_device")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AssignedDevice implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JsonIgnoreProperties("devices")
    private Assignment assignment;

    @ManyToOne
    @JsonIgnoreProperties("assignments")
    private Device device;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("assignedDevices")
    private DeviceCapability capability;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Assignment getAssignment() {
        return assignment;
    }

    public AssignedDevice assignment(Assignment assignment) {
        this.assignment = assignment;
        return this;
    }

    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }

    public Device getDevice() {
        return device;
    }

    public AssignedDevice device(Device device) {
        this.device = device;
        return this;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public DeviceCapability getCapability() {
        return capability;
    }

    public AssignedDevice capability(DeviceCapability deviceCapability) {
        this.capability = deviceCapability;
        return this;
    }

    public void setCapability(DeviceCapability deviceCapability) {
        this.capability = deviceCapability;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AssignedDevice)) {
            return false;
        }
        return id != null && id.equals(((AssignedDevice) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "AssignedDevice{" +
            "id=" + getId() +
            "}";
    }
}
