/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import io.saperi.vcm.domain.enumeration.DeviceType;

/**
 * A DeviceModel.
 */
@Entity
@Table(name = "device_model")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DeviceModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private DeviceType type;

    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @NotNull
    @JoinTable(name = "device_model_capabilities",
               joinColumns = @JoinColumn(name = "device_model_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "capabilities_id", referencedColumnName = "id"))
    private Set<DeviceCapability> capabilities = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public DeviceModel name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DeviceType getType() {
        return type;
    }

    public DeviceModel type(DeviceType type) {
        this.type = type;
        return this;
    }

    public void setType(DeviceType type) {
        this.type = type;
    }

    public Set<DeviceCapability> getCapabilities() {
        return capabilities;
    }

    public DeviceModel capabilities(Set<DeviceCapability> deviceCapabilities) {
        this.capabilities = deviceCapabilities;
        return this;
    }

    public DeviceModel addCapabilities(DeviceCapability deviceCapability) {
        this.capabilities.add(deviceCapability);
        deviceCapability.getDeviceModels().add(this);
        return this;
    }

    public DeviceModel removeCapabilities(DeviceCapability deviceCapability) {
        this.capabilities.remove(deviceCapability);
        deviceCapability.getDeviceModels().remove(this);
        return this;
    }

    public void setCapabilities(Set<DeviceCapability> deviceCapabilities) {
        this.capabilities = deviceCapabilities;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DeviceModel)) {
            return false;
        }
        return id != null && id.equals(((DeviceModel) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "DeviceModel{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
