/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A Device.
 */
@Entity
@Table(name = "device")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Device implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "hardware_id", nullable = false)
    private String hardwareId;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("devices")
    private DeviceModel model;

    @OneToMany(mappedBy = "device", fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<AssignedDevice> assignments = new HashSet<>();

    @Transient
    private String manufacturer;

    @Transient
    private String modelName;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHardwareId() {
        return hardwareId;
    }

    public Device hardwareId(String hardwareId) {
        this.hardwareId = hardwareId;
        return this;
    }

    public void setHardwareId(String hardwareId) {
        this.hardwareId = hardwareId;
    }

    public DeviceModel getModel() {
        return model;
    }

    public Device model(DeviceModel deviceModel) {
        this.model = deviceModel;
        return this;
    }

    public void setModel(DeviceModel deviceModel) {
        this.model = deviceModel;
    }

    public Set<AssignedDevice> getAssignments() {
        return assignments;
    }

    public Device assignments(Set<AssignedDevice> assignedDevices) {
        this.assignments = assignedDevices;
        return this;
    }

    public Device addAssignments(AssignedDevice assignedDevice) {
        this.assignments.add(assignedDevice);
        assignedDevice.setDevice(this);
        return this;
    }

    public Device removeAssignments(AssignedDevice assignedDevice) {
        this.assignments.remove(assignedDevice);
        assignedDevice.setDevice(null);
        return this;
    }

    public void setAssignments(Set<AssignedDevice> assignedDevices) {
        this.assignments = assignedDevices;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Device)) {
            return false;
        }
        return id != null && id.equals(((Device) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Device{" +
            "id=" + getId() +
            ", hardwareId='" + getHardwareId() + "'" +
            "}";
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public void setModelName(String model) {
        this.modelName = model;
    }

    public String getModelName() {
        return this.modelName;
    }
}
