/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.domain;

import io.saperi.vcm.domain.enumeration.VentilatorControlType;
import io.saperi.vcm.domain.enumeration.VentilatorModeType;
import io.saperi.vcm.domain.enumeration.VentilatorRecommenderType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Assignment.
 */
@Entity
@Table(name = "assignment")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Assignment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "patient_id", nullable = false)
    private String patientId;

    @NotNull
    @Column(name = "mrn", nullable = false)
    private String mrn;

    @Column(name = "active")
    private Boolean active;

    @Enumerated(EnumType.STRING)
    @Column(name = "ventilator_mode")
    private VentilatorModeType ventilatorMode;

    @Enumerated(EnumType.STRING)
    @Column(name = "ventilator_recommender_type")
    private VentilatorRecommenderType ventilatorRecommenderType;

    @Enumerated(EnumType.STRING)
    @Column(name = "ventilator_control_type")
    private VentilatorControlType ventilatorControlType;

    @OneToMany(mappedBy = "assignment", cascade = CascadeType.ALL, orphanRemoval = true)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<AssignedDevice> devices = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPatientId() {
        return patientId;
    }

    public Assignment patientId(String patientId) {
        this.patientId = patientId;
        return this;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getMrn() {
        return mrn;
    }

    public Assignment mrn(String mrn) {
        this.mrn = mrn;
        return this;
    }

    public void setMrn(String mrn) {
        this.mrn = mrn;
    }


    public Boolean isActive() {
        return active;
    }

    public Assignment active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public VentilatorModeType getVentilatorMode() {
        return ventilatorMode;
    }

    public Assignment ventilatorMode(VentilatorModeType ventilatorMode) {
        this.ventilatorMode = ventilatorMode;
        return this;
    }

    public void setVentilatorMode(VentilatorModeType ventilatorMode) {
        this.ventilatorMode = ventilatorMode;
    }

    public VentilatorRecommenderType getVentilatorRecommenderType() {
        return ventilatorRecommenderType;
    }

    public Assignment ventilatorRecommenderType(VentilatorRecommenderType ventilatorRecommenderType) {
        this.ventilatorRecommenderType = ventilatorRecommenderType;
        return this;
    }

    public void setVentilatorRecommenderType(VentilatorRecommenderType ventilatorRecommenderType) {
        this.ventilatorRecommenderType = ventilatorRecommenderType;
    }

    public VentilatorControlType getVentilatorControlType() {
        return ventilatorControlType;
    }

    public void setVentilatorControlType(VentilatorControlType ventilatorControlType) {
        this.ventilatorControlType = ventilatorControlType;
    }

    public Set<AssignedDevice> getDevices() {
        return devices;
    }

    public Assignment devices(Set<AssignedDevice> assignedDevices) {
        this.devices = assignedDevices;
        return this;
    }

    public Assignment addDevices(AssignedDevice assignedDevice) {
        this.devices.add(assignedDevice);
        assignedDevice.setAssignment(this);
        return this;
    }

    public Assignment removeDevices(AssignedDevice assignedDevice) {
        this.devices.remove(assignedDevice);
        assignedDevice.setAssignment(null);
        return this;
    }

    public void setDevices(Set<AssignedDevice> assignedDevices) {
        this.devices = assignedDevices;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Assignment)) {
            return false;
        }
        return id != null && id.equals(((Assignment) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Assignment{" +
            "id=" + getId() +
            ", patientId='" + getPatientId() + "'" +
            ", active='" + isActive() + "'" +
            ", ventilatorMode='" + getVentilatorMode() + "'" +
            ", ventilatorControl='" + (getVentilatorControlType()==null?null:getVentilatorControlType().toString()) + "'" +
            "}";
    }
}
