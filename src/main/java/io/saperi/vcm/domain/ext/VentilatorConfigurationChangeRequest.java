/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.domain.ext;

import io.saperi.vcm.event.ScalarValue;

/**
 * Class containing all the necessary data to request the modification of the
 * parameters of a ventilator.
 *
 * @author esteban
 */
public class VentilatorConfigurationChangeRequest {

    private ScalarValue peep;
    private ScalarValue pip;
    private ScalarValue rate;
    private ScalarValue fio2;
    private ScalarValue tv;
    private ScalarValue inspiratoryExpiratoryRatio;

    public ScalarValue getPeep() {
        return peep;
    }

    public void setPeep(ScalarValue peep) {
        this.peep = peep;
    }

    public ScalarValue getPip() {
        return pip;
    }

    public void setPip(ScalarValue pip) {
        this.pip = pip;
    }

    public ScalarValue getRate() {
        return rate;
    }

    public void setRate(ScalarValue rate) {
        this.rate = rate;
    }

    public ScalarValue getFio2() {
        return fio2;
    }

    public void setFio2(ScalarValue fio2) {
        this.fio2 = fio2;
    }

    public ScalarValue getInspiratoryExpiratoryRatio() {
        return inspiratoryExpiratoryRatio;
    }

    public void setInspiratoryExpiratoryRatio(ScalarValue inspiratoryExpiratoryRatio) {
        this.inspiratoryExpiratoryRatio = inspiratoryExpiratoryRatio;
    }

    public ScalarValue getTv() {
        return tv;
    }

    public void setTv(ScalarValue tv) {
        this.tv = tv;
    }
}
