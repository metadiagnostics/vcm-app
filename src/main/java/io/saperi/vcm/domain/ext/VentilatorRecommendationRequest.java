/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */



package io.saperi.vcm.domain.ext;

import io.saperi.vcm.event.ScalarValue;

/**
 * Class containing all the necessary data to trigger a Ventilator
 * Recommendation.
 *
 * @author esteban
 */
public class VentilatorRecommendationRequest {

    private ScalarValue peep;
    private ScalarValue pip;
    private ScalarValue rate;
    private ScalarValue fio2;
    private ScalarValue ph;
    private ScalarValue etco2;
    private ScalarValue o2sat;
    private ScalarValue tidalVolume;
    private ScalarValue height;
    private String gender;
    private long timestamp;
    private String requester;

    public VentilatorRecommendationRequest() {
        timestamp = System.currentTimeMillis();
    }

    public ScalarValue getPeep() {
        return peep;
    }

    public void setPeep(ScalarValue peep) {
        this.peep = peep;
    }

    public ScalarValue getPip() {
        return pip;
    }

    public void setPip(ScalarValue pip) {
        this.pip = pip;
    }

    public ScalarValue getRate() {
        return rate;
    }

    public void setRate(ScalarValue rate) {
        this.rate = rate;
    }

    public ScalarValue getFio2() {
        return fio2;
    }

    public void setFio2(ScalarValue fio2) {
        this.fio2 = fio2;
    }

    public ScalarValue getPh() {
        return ph;
    }

    public void setPh(ScalarValue ph) {
        this.ph = ph;
    }

    public ScalarValue getEtco2() {
        return etco2;
    }

    public void setEtco2(ScalarValue etco2) {
        this.etco2 = etco2;
    }

    public ScalarValue getO2sat() {
        return o2sat;
    }

    public void setO2sat(ScalarValue o2sat) {
        this.o2sat = o2sat;
    }

    public ScalarValue getTidalVolume() {
        return tidalVolume;
    }

    public void setTidalVolume(ScalarValue tidalVolume) {
        this.tidalVolume = tidalVolume;
    }

    public ScalarValue getHeight() {
        return height;
    }

    public void setHeight(ScalarValue height) {
        this.height = height;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getRequester() {
        return requester;
    }

    public void setRequester(String requester) {
        this.requester = requester;
    }

}
