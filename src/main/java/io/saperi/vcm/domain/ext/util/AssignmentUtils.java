/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.domain.ext.util;

import io.saperi.vcm.domain.Assignment;
import io.saperi.vcm.domain.Device;
import io.saperi.vcm.domain.enumeration.DeviceCapabilityType;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Utility methods for the {@link Assignment} class.
 *
 * @author esteban
 */
public class AssignmentUtils {

    /**
     * Given an {@link Assignment}, this method returns a Map containing a Set
     * of the {@link DeviceCapabilityType} of each of the {@link Device Devices}
     * being referenced by the assignment.
     *
     * @param assignment the assignment.
     * @return a Map containing a Set of the {@link DeviceCapabilityType} of
     *         each of the {@link Device Devices} being referenced by the
     *         assignment.
     */
    public static Map<Device, Set<DeviceCapabilityType>> getRequiredCapabilitiesByDevice(Assignment assignment) {
        Map<Device, Set<DeviceCapabilityType>> results = new HashMap<>();
        assignment.getDevices().forEach(da -> {
            results.computeIfAbsent(da.getDevice(), k -> new HashSet<>()).add(da.getCapability().getType());
        });
        return results;
    }

    /**
     * Given an {@link Assignment}, this method returns a Map containing a Set
     * of the {@link Device Devices} grouped by the {@link DeviceCapabilityType}
     * assigned to it.
     *
     * @param assignment the assignment.
     * @return Map containing a Set of the {@link Device Devices} grouped by the
     *         {@link DeviceCapabilityType} assigned to the assignment.
     */
    public static Map<DeviceCapabilityType, Set<Device>> getDevicesByRequiredCapability(Assignment assignment) {
        Map<DeviceCapabilityType, Set<Device>> results = new HashMap<>();
        assignment.getDevices().forEach(da -> {
            results.computeIfAbsent(da.getCapability().getType(), k -> new HashSet<>()).add(da.getDevice());
        });
        return results;
    }
}
