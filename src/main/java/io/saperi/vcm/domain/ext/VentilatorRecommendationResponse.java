/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.domain.ext;

import io.saperi.vcm.event.ScalarValue;

/**
 * Class containing the response of a Ventilator Recommendation.
 *
 * @author esteban
 */
public class VentilatorRecommendationResponse {

    private Long assignmentId;
    private ScalarValue peep;
    private ScalarValue pip;
    private ScalarValue rate;
    private ScalarValue fio2;
    private ScalarValue tv;
    private String reason;
    private long timestamp;

    public VentilatorRecommendationResponse() {
        this.timestamp = System.currentTimeMillis();
    }

    /**
     * The request that originated the response.
     */
    private VentilatorRecommendationRequest request;

    public Long getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(Long assignmentId) {
        this.assignmentId = assignmentId;
    }

    public ScalarValue getPeep() {
        return peep;
    }

    public void setPeep(ScalarValue peep) {
        this.peep = peep;
    }

    public ScalarValue getPip() {
        return pip;
    }

    public void setPip(ScalarValue pip) {
        this.pip = pip;
    }

    public ScalarValue getRate() {
        return rate;
    }

    public void setRate(ScalarValue rate) {
        this.rate = rate;
    }

    public ScalarValue getFio2() {
        return fio2;
    }

    public void setFio2(ScalarValue fio2) {
        this.fio2 = fio2;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public VentilatorRecommendationRequest getRequest() {
        return request;
    }

    public void setRequest(VentilatorRecommendationRequest request) {
        this.request = request;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public ScalarValue getTv() {
        return tv;
    }

    public void setTv(ScalarValue tv) {
        this.tv = tv;
    }
}
