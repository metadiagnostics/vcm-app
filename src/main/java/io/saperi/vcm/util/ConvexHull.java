/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.util;

import static java.util.Collections.emptyList;

import io.saperi.vcm.event.ScalarPointValue;
import java.util.ArrayList;
import java.util.List;

public class ConvexHull {

    public static List<ScalarPointValue> convexHull(List<ScalarPointValue> p, boolean closePoints) {
        if (p.isEmpty()) {
            return emptyList();
        }
        p.sort(ScalarPointValue::compareTo);
        List<ScalarPointValue> h = new ArrayList<>();

        // lower hull
        for (ScalarPointValue pt : p) {
            while (h.size() >= 2 && !ccw(h.get(h.size() - 2), h.get(h.size() - 1), pt)) {
                h.remove(h.size() - 1);
            }
            h.add(pt);
        }

        // upper hull
        int t = h.size() + 1;
        for (int i = p.size() - 1; i >= 0; i--) {
            ScalarPointValue pt = p.get(i);
            while (h.size() >= t && !ccw(h.get(h.size() - 2), h.get(h.size() - 1), pt)) {
                h.remove(h.size() - 1);
            }
            h.add(pt);
        }

        if (!closePoints) {
            h.remove(h.size() - 1);
        }
        return h;
    }

    // ccw returns true if the three points make a counter-clockwise turn
    private static boolean ccw(ScalarPointValue a, ScalarPointValue b, ScalarPointValue c) {
        return ((b.getX().getValue() - a.getX().getValue()) * (c.getY().getValue() - a.getY().getValue())) > ((b.getY().getValue() - a.getY().getValue()) * (c.getX().getValue() - a.getX().getValue()));
    }
}
