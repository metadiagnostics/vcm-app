/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.util;

import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.StringType;

import java.util.List;

public class HumanNameParser {

    public String toString(HumanName name) {
        StringBuilder sb = new StringBuilder();
        append(sb, name.getPrefix());
        sb.append(name.getFamily());
        sb.append(",");
        append(sb, name.getGiven());
        append(sb, name.getSuffix());
        return sb.toString();
    }

    private void append(StringBuilder sb, List<StringType> components) {
        for (StringType component : components) {
            if (sb.length() > 0) {
                sb.append(" ");
            }

            sb.append(component);
        }
    }

}

