/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */


package io.saperi.vcm.util;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import org.hl7.fhir.r4.model.Bundle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class FhirServiceUtil {

    public static <T> List<T> buildAndRetrieveAllResources(IGenericClient client, Bundle initialBundle, Class<T> resourceType) {
        if (initialBundle.isEmpty()) {
            return Collections.EMPTY_LIST;
        }

        List<T> resourceList = new ArrayList<>(getResourceFromBundle(initialBundle, resourceType));

        boolean hasLink = initialBundle.hasLink() && initialBundle.getLink("next") != null;
        while (hasLink) {
            initialBundle = fetchNextPage(client, initialBundle.getLink("next").getUrl());
            resourceList.addAll(getResourceFromBundle(initialBundle, resourceType));
            hasLink = initialBundle.hasLink() && initialBundle.getLink("next") != null;
        }

        return resourceList;
    }

    private static Bundle fetchNextPage(IGenericClient client, String url) {
        return client.search().byUrl(url)
            .returnBundle(Bundle.class)
            .execute();
    }

    public static <T> List<T> getResourceFromBundle(Bundle bundle, Class<T> resourceType) {
        return bundle.getEntry().stream()
            .map(e -> e.getResource())
            .filter(resourceType::isInstance)
            .map(resourceType::cast)
            .collect(toList());
    }
}
