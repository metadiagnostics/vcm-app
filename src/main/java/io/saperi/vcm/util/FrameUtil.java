/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.util;

import io.saperi.virtualhealth.device.aggregators.common.DeviceCtx;
import io.saperi.virtualhealth.device.aggregators.common.model.Frame;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;

@Slf4j
public class FrameUtil {

    public static DeviceCtx getMasterDeviceCtx(final Frame frame){
        if (frame!=null) {
            HashMap<String, DeviceCtx> devicesMap = frame.getDevices();
            if ( devicesMap!= null && devicesMap.size() > 0 && devicesMap.size() % 3 == 0){
                //TODO: iterate here for all the master pumps, obtaining first now
                DeviceCtx masterPumpCtx  = devicesMap.values().stream().filter(ctx -> ctx.getDeviceInfo()!=null && ctx.getDeviceInfo().getParent_device_id()==null).findFirst().get();
                return masterPumpCtx;
            }
            else if ( devicesMap!= null && devicesMap.size() == 1) {
                 return devicesMap.values().stream().findFirst().get();
            }
        }
        return null;
    }
}
