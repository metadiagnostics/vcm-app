/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.util;

import ca.uhn.fhir.rest.api.MethodOutcome;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.OperationOutcome;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class FhirUtil {

    public FhirUtil() { }

    public static <T extends IBaseResource> T processMethodOutcome(MethodOutcome outcome, T resource) {
        checkOutcome(outcome.getOperationOutcome());
        IIdType id = outcome.getId();
        IBaseResource newResource = outcome.getResource();
        if (id != null) {
            resource.setId(id);
        } else if (newResource != null && newResource.getClass() == resource.getClass()) {
            resource = (T) newResource;
        }

        return resource;
    }

    public static boolean checkOutcome(IBaseResource resource) {
        if (!(resource instanceof OperationOutcome)) {
            return false;
        } else {
            OperationOutcome outcome = (OperationOutcome)resource;
            OperationOutcome.IssueSeverity severity = OperationOutcome.IssueSeverity.INFORMATION;
            if (outcome.getIssue() != null && !outcome.getIssue().isEmpty()) {
                StringBuilder sb = new StringBuilder();
                Iterator var4 = outcome.getIssue().iterator();

                while(true) {
                    OperationOutcome.OperationOutcomeIssueComponent issue;
                    OperationOutcome.IssueSeverity theSeverity;
                    do {
                        if (!var4.hasNext()) {
                            if (sb.length() != 0) {
                                throw new FhirUtil.OperationOutcomeException(sb.toString(), severity, outcome);
                            }

                            return true;
                        }

                        issue = (OperationOutcome.OperationOutcomeIssueComponent)var4.next();
                        theSeverity = (OperationOutcome.IssueSeverity)issue.getSeverityElement().getValue();
                    } while(theSeverity != OperationOutcome.IssueSeverity.ERROR && theSeverity != OperationOutcome.IssueSeverity.FATAL);

                    sb.append(issue.getDiagnostics()).append(" (").append(theSeverity.getDefinition()).append(")\n");
                    severity = theSeverity.ordinal() < severity.ordinal() ? theSeverity : severity;
                }
            } else {
                return true;
            }
        }
    }

    public static String stripVersion(String id) {
        int i = id.lastIndexOf("/_history");
        return i == -1 ? id : id.substring(0, i);
    }

    public static <T extends IBaseResource> T stripVersion(T resource) {
        IIdType id = resource.getIdElement();
        if (id.hasVersionIdPart()) {
            id.setValue(stripVersion(id.getValue()));
            resource.setId(id);
        }

        return resource;
    }

    public static String getResourceIdPath(IBaseResource resource) {
        return getResourceIdPath(resource, true);
    }

    public static String getResourceIdPath(IBaseResource resource, boolean stripVersion) {
        String id = resource.getIdElement().getResourceType() + "/" + resource.getIdElement().getIdPart();
        return stripVersion ? stripVersion(id) : id;
    }

    public static Identifier createIdentifier(String system, String value) {
        Identifier identifier = new Identifier();
        identifier.setSystem(system);
        identifier.setValue(value);
        return identifier;
    }

    public static <T extends IBaseResource> List<T> getEntries(Bundle bundle, Class<T> clazz) {
        return getEntries(bundle, Collections.singletonList(clazz), (List)null);
    }

    public static <T extends IBaseResource> List<IBaseResource> getEntries(Bundle bundle, List<Class<T>> inclusions, List<Class<T>> exclusions) {
        List<IBaseResource> entries = new ArrayList();
        if (bundle != null) {
            Iterator var4 = bundle.getEntry().iterator();

            while(var4.hasNext()) {
                Bundle.BundleEntryComponent entry = (Bundle.BundleEntryComponent)var4.next();
                IBaseResource resource = entry.getResource();
                boolean exclude = exclusions != null && classMatches(exclusions, resource);
                boolean include = !exclude && (inclusions == null || classMatches(inclusions, resource));
                if (include) {
                    entries.add(resource);
                }
            }
        }

        return entries;
    }

    private static <T extends IBaseResource> boolean classMatches(List<Class<T>> classes, IBaseResource resource) {
        Iterator var2 = classes.iterator();

        Class clazz;
        do {
            if (!var2.hasNext()) {
                return false;
            }

            clazz = (Class)var2.next();
        } while(!clazz.isInstance(resource));

        return true;
    }

    public static String concatPath(String root, String fragment) {
        while(root.endsWith("/")) {
            root = root.substring(0, root.length() - 1);
        }

        while(fragment.startsWith("/")) {
            fragment = fragment.substring(1);
        }

        return root + "/" + fragment;
    }

    public static class OperationOutcomeException extends RuntimeException {
        private static final long serialVersionUID = 1L;
        private final OperationOutcome operationOutcome;
        private final OperationOutcome.IssueSeverity severity;

        private OperationOutcomeException(String message, OperationOutcome.IssueSeverity severity, OperationOutcome operationOutcome) {
            super(message);
            this.severity = severity;
            this.operationOutcome = operationOutcome;
        }

        public OperationOutcome getOperationOutcome() {
            return this.operationOutcome;
        }

        public OperationOutcome.IssueSeverity getSeverity() {
            return this.severity;
        }
    }
}
