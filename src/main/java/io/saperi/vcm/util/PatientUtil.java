/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.util;

import org.hl7.fhir.r4.model.ContactPoint;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Patient;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class PatientUtil {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM-dd-yyyy");

    private static final HumanNameParser HUMAN_NAME_PARSER = new HumanNameParser();
    private static final String MR_SYSTEM = "urn:oid:1.2.3.4";

    public static String getPatientBirthdate(Patient patient) {
        Date birthDate = patient.getBirthDate();
        if (birthDate != null) {
            return DATE_FORMAT.format(birthDate);
        }
        return "";
    }

    public static String getPatientName(Patient patient) {
        String name = "";
        if (patient.getName() != null && patient.getName().size() > 0) {
            name = PatientUtil.formatName(patient.getName());
        }

        return name;
    }

    /**
     * Formats a name using the active parser.
     *
     * @param name HumanName instance.
     * @return Formatted name.
     */
    public static String formatName(HumanName name) {
        return name == null ? "" : HUMAN_NAME_PARSER.toString(name);
    }

    /**
     * Format the "usual" name.
     *
     * @param names List of names
     * @return A formatted name.
     */
    public static String formatName(List<HumanName> names) {
        return formatName(names, HumanName.NameUse.USUAL, null);
    }

    /**
     * Format a name of the specified use category.
     *
     * @param names List of names
     * @param uses  Use categories (use categories to search).
     * @return A formatted name.
     */
    public static String formatName(List<HumanName> names, HumanName.NameUse... uses) {
        return formatName(getName(names, uses));
    }

    /**
     * Returns a name of the desired use category from a list.
     *
     * @param list List of names to consider.
     * @param uses One or more use categories. These are searched in order until
     *             one is found. A null value matches any use category.
     * @return A name with a matching use category, or null if none found.
     */
    public static HumanName getName(List<HumanName> list, HumanName.NameUse... uses) {
        for (HumanName.NameUse use : uses) {
            for (HumanName name : list) {
                if (use == null || use.equals(name.getUse())) {
                    return name;
                }
            }
        }

        return null;
    }

    public static String getMedicalRecordNumber(Patient patient) {
        String medicalRecordNumber = null;

        if (patient.hasIdentifier()) {
            for (Identifier id : patient.getIdentifier()) {
                if (id.getSystem() != null && id.getSystem().equals(MR_SYSTEM)) {
                    medicalRecordNumber = id.getValue();
                    break;
                }
            }
        }

        return medicalRecordNumber;
    }

    public static Integer getDaysOfLife(Patient patient) {
        Integer value = null;

        if (patient.hasBirthDate()) {
            Date today = new Date();
            value = (int) ((today.getTime() - patient.getBirthDate().getTime()) / (1000 * 60 * 60 * 24));
        }

        return value;
    }

    public static String getContactName(Patient.ContactComponent contact) {
        if (contact == null) {
            return null;
        }
        if (contact.getName() == null) {
            return null;
        }
        return contact.getName().getNameAsSingleString();
    }

    public static String getContactPhone(Patient.ContactComponent contact) {
        return getContactPoint(contact, ContactPoint.ContactPointSystem.PHONE);
    }

    public static String getContactEmail(Patient.ContactComponent contact) {
        return getContactPoint(contact, ContactPoint.ContactPointSystem.EMAIL);
    }

    public static String getContactPoint(Patient.ContactComponent contact, ContactPoint.ContactPointSystem type) {
        if (contact == null) {
            return null;
        }
        if (!contact.hasTelecom()) {
            return null;
        }
        for (ContactPoint contactPoint : contact.getTelecom()) {
            if (contactPoint.getSystem() == type) {
                return contactPoint.getValue();
            }
        }
        return null;
    }
}
