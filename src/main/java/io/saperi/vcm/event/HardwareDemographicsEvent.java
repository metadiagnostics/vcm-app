/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.event;

import javax.cache.Cache;
import java.util.ArrayList;
import java.util.List;

/**
 * Event containing Demographic data that is generated by a device (or external
 * service) and sent to the clients (usually through a websocket). Different
 * devices will have different ways of read their data, but before the data is
 * sent to a client, or used inside this application, it must be first
 * normalized into instances of this class.
 *
 * @author esteban
 */
public class HardwareDemographicsEvent extends HardwareEvent implements CacheableEvent{

    private long dataTimestamp;

    private ScalarValue height;
    private ScalarValue weight;
    private String gender;
    private ScalarValue pbw;

    public HardwareDemographicsEvent(String hardwareId, Object source) {
        super(hardwareId, source);
    }

    public long getDataTimestamp() {
        return dataTimestamp;
    }

    public void setDataTimestamp(long dataTimestamp) {
        this.dataTimestamp = dataTimestamp;
    }

    public ScalarValue getHeight() {
        return height;
    }

    public void setHeight(ScalarValue height) {
        this.height = height;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public ScalarValue getPbw() {
        return pbw;
    }

    public void setPbw(ScalarValue pbw) {
        this.pbw = pbw;
    }

    public ScalarValue getWeight() {
        return weight;
    }

    public void setWeight(ScalarValue weight) {
        this.weight = weight;
    }

    @Override
    public void putInCache(Cache<String, List<HardwareEvent>> eventsCache) {
        // Store the event on some cache mechanism
        // hardwareId --> [events]
        if (eventsCache.containsKey(this.getHardwareId())) {
            List<HardwareEvent> events = eventsCache.get(this.getHardwareId());
            events.add(this);
            eventsCache.put(this.getHardwareId(), events);
        } else {
            List<HardwareEvent> events = new ArrayList();
            events.add(this);
            eventsCache.put(this.getHardwareId(), events);
        }
    }
}
