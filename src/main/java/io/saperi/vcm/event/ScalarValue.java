/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.event;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * A class that holds a numeric value associated to a unit. For the time being,
 * the units are treated as strings.
 *
 * @author esteban
 */
public final class ScalarValue {

    public static transient final int DEFAULT_PRECISION = 5;

    private BigDecimal value;
    private String unit;
    private final int precision;

    public ScalarValue() {
        this.precision = DEFAULT_PRECISION;
    }

    public ScalarValue(double value, String unit, int precision) {
        this.unit = unit;
        this.precision = precision;
        this.setValue(value);
    }

    public Double getValue() {
        return value == null ? null : value.doubleValue();
    }

    public void setValue(double value) {
        this.value = new BigDecimal(value).setScale(precision, RoundingMode.CEILING);
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

}
