/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.event;

import io.saperi.virtualhealth.device.aggregators.common.model.DeviceInfo;

import java.util.UUID;

public class NewHardwareDetectedEvent extends HardwareEvent {

    public DeviceInfo getNewDevice() {
        return newDevice;
    }

    DeviceInfo newDevice;
    public NewHardwareDetectedEvent(String hardwareId, Object source, DeviceInfo newDevice) {
        super(hardwareId, source);
        this.newDevice = newDevice;
    }
}
