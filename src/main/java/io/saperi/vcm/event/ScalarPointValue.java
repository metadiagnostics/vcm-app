/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.event;

/**
 * A class that holds 2 {@link ScalarValue ScalarValues}.
 *
 * @author esteban
 */
public final class ScalarPointValue implements Comparable<ScalarPointValue> {

    public transient final int DEFAULT_PRECISION = 5;

    private ScalarValue x;
    private ScalarValue y;

    public ScalarPointValue() {
    }

    public ScalarPointValue(ScalarValue x, ScalarValue y) {
        this.x = x;
        this.y = y;
    }

    public ScalarPointValue(double x, String xUnit, int xPrecision, double y, String yUnit, int yPrecision) {
        this.x = new ScalarValue(x, xUnit, xPrecision);
        this.y = new ScalarValue(y, yUnit, yPrecision);
    }

    public ScalarValue getX() {
        return x;
    }

    public void setX(ScalarValue x) {
        this.x = x;
    }

    public ScalarValue getY() {
        return y;
    }

    public void setY(ScalarValue y) {
        this.y = y;
    }

    @Override
    public int compareTo(ScalarPointValue o) {
        return Double.compare(x.getValue(), o.x.getValue());
    }

}
