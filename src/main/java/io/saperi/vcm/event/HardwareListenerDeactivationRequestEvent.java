/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.event;

import io.saperi.vcm.domain.enumeration.DeviceType;

/**
 * Event that is sent by the application when a Hardware must be deactivated.
 * Note that multiple deactivations for a single Hardware can be notified. The
 * classes processing these events must be aware of that in order to avoid
 * duplicated deactivation logic if needed.
 *
 * @author esteban
 */
public class HardwareListenerDeactivationRequestEvent extends HardwareEvent {

    private final DeviceType deviceType;

    public HardwareListenerDeactivationRequestEvent(DeviceType deviceType, String hardwareId, Object source) {
        super(hardwareId, source);
        this.deviceType = deviceType;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

}
