/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.event;

import io.saperi.vcm.domain.enumeration.VentilatorControlType;
import org.springframework.context.ApplicationEvent;

/**
 * Event that is published when the Recommender control type changes.
 * The changes can be from VOLUME to PRESSURE or vice-versa.
 *
 * @author sgroh@saperi.io
 */
public class RecommenderControlTypeChangeEvent extends ApplicationEvent {
    private final Long assignmentId;
    private final String requester;
    private final VentilatorControlType controlType;

    public RecommenderControlTypeChangeEvent(Object controlType, Long assignmentId, String requester) {
        super(controlType);
        this.assignmentId = assignmentId;
        this.requester = requester;
        this.controlType = (VentilatorControlType) controlType;
    }

    public String getRequester() {
        return requester;
    }

    public Long getAssignmentId() {
        return assignmentId;
    }

    public VentilatorControlType controlType(){
        return controlType;
    }

}
