/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.event;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Setter
@Getter
public class AlarmWarningEvent extends HardwareEvent {
    private String id;
    private String title;
    private String body;
    private String severity;
    private String alarmClass;
    private boolean ack;
    private boolean muted;
    /**
     * Constructor needed by Jackson library to deserialize the object
     */
    public AlarmWarningEvent() {
        super(null,"");
    }
    public AlarmWarningEvent(String hardwareId, Object source) {
        super(hardwareId, source);
    }

    public int hashCode(){
        return new HashCodeBuilder(17, 37).
            append(title).
            append(body).
            append(severity).
            toHashCode();
    }

    public boolean equals(Object other){
        if (other == null) { return false; }
        if (other == this) { return true; }
        if (other.getClass() != getClass()) {
            return false;
        }
        AlarmWarningEvent awe = (AlarmWarningEvent) other;
        return new EqualsBuilder()
            .appendSuper(super.equals(other))
            .append(title, awe.title)
            .append(body, awe.body)
            .append(severity, awe.severity)
            .isEquals();
    }

}
