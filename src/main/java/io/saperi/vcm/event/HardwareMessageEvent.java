/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.event;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.saperi.vcm.domain.Device;
import java.util.HashMap;
import java.util.Map;

/**
 * Class used to send notifications related to a specific Device (Hardware).
 *
 * @author esteban
 */
@JsonTypeInfo(
    use = JsonTypeInfo.Id.CLASS,
    include = JsonTypeInfo.As.EXTERNAL_PROPERTY,
    property = "_class",
    defaultImpl = Void.class)
public class HardwareMessageEvent extends HardwareEvent {

    public static enum Severity {
        INFO,
        WARNING,
        ERROR
    }

    private final Severity severity;

    //For i18n
    private final String messageKey;

    //For i18n
    private final Map<String, String> parameters = new HashMap<>();

    private final String message;

    public HardwareMessageEvent(Device source, String messageKey) {
        this(source, Severity.INFO, messageKey);
    }

    public HardwareMessageEvent(Device source, Severity severity, String messageKey) {
        this(source, severity, messageKey, "");
    }

    public HardwareMessageEvent(Device source, Severity severity, String messageKey, String message) {
        super(source.getHardwareId(), source);
        this.severity = severity;
        this.messageKey = messageKey;
        this.message = message;
    }

    public Severity getSeverity() {
        return severity;
    }

    public String getMessageKey() {
        return messageKey;
    }

    public String getMessage() {
        return message;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public HardwareMessageEvent addParameter(String key, String value) {
        this.parameters.put(key, value);
        return this;
    }

    @Override
    public Device getSource() {
        return (Device) super.getSource();
    }

}
