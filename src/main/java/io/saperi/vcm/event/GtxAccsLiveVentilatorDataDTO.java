/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.event;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.saperi.vcm.service.ext.impl.gtx.GtxAbstractVentilatorConfigurationChangeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GtxAccsLiveVentilatorDataDTO {

    private HardwareVentilatorEvent hardwareVentilatorEvent;
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final Logger log = LoggerFactory.getLogger(GtxAccsLiveVentilatorDataDTO.class);


    public static GtxAccsLiveVentilatorDataDTO from(VentilatorConfigurationChangeRequestEvent req) {
        if (req==null) {
            return null;
        }
        GtxAccsLiveVentilatorDataDTO gtxAccsLiveVentilatorDataDTO = new GtxAccsLiveVentilatorDataDTO();
        HardwareVentilatorEvent hwVentilatorEvent = new HardwareVentilatorEvent();
        hwVentilatorEvent.setPip( new ScalarValue(req.getChanges().getPip().getValue(), "cmH2O", ScalarValue.DEFAULT_PRECISION));
        hwVentilatorEvent.setPeep(new ScalarValue(req.getChanges().getPeep().getValue(), "cmH2O", ScalarValue.DEFAULT_PRECISION));
        hwVentilatorEvent.setRate(new ScalarValue(req.getChanges().getRate().getValue(), "1/min", ScalarValue.DEFAULT_PRECISION));
        //For Volume control we need to send also TV on Volume control
        hwVentilatorEvent.setTv((req.getChanges().getTv()==null)?null:new ScalarValue(req.getChanges().getTv().getValue(), "L", ScalarValue.DEFAULT_PRECISION));
        //We can send the Fio2 but it will not change on the device.
        hwVentilatorEvent.setFio2(new ScalarValue(req.getChanges().getFio2().getValue(), "%", ScalarValue.DEFAULT_PRECISION));
        gtxAccsLiveVentilatorDataDTO.setHardwareVentilatorEvent(hwVentilatorEvent);
        return gtxAccsLiveVentilatorDataDTO;
    }

    public HardwareVentilatorEvent getHardwareVentilatorEvent() {
        return hardwareVentilatorEvent;
    }

    public void setHardwareVentilatorEvent(HardwareVentilatorEvent hardwareVentilatorEvent) {
        this.hardwareVentilatorEvent = hardwareVentilatorEvent;
    }

    public String getHardwareId() {
        if (hardwareVentilatorEvent !=null) {
            return hardwareVentilatorEvent.getHardwareId();
        }
        return null;
    }

    public String toString() {
        if (hardwareVentilatorEvent!=null){
            try {
                objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
                return objectMapper.writeValueAsString(hardwareVentilatorEvent);
            } catch (JsonProcessingException e) {
                log.error("Could not convert DTO into JSON");
            }
        }
        return "";
    }

}
