/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.event;

import io.saperi.vcm.domain.enumeration.DeviceCapabilityType;
import io.saperi.vcm.domain.enumeration.DeviceType;
import java.util.Set;

/**
 * Event that is sent by the application when a Hardware must be activated.
 * Note
 * that multiple activations for a single Hardware can be notified. The classes
 * processing these events must be aware of that in order to avoid duplicated
 * activation logic if needed.
 *
 * @author esteban
 */
public class HardwareListenerActivationRequestEvent extends HardwareEvent {

    private final DeviceType deviceType;
    private final Set<DeviceCapabilityType> requiredCapabilities;

    public HardwareListenerActivationRequestEvent(DeviceType deviceType, String hardwareId, Set<DeviceCapabilityType> requiredCapabilities, Object source) {
        super(hardwareId, source);
        this.deviceType = deviceType;
        this.requiredCapabilities = requiredCapabilities;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public Set<DeviceCapabilityType> getRequiredCapabilities() {
        return requiredCapabilities;
    }

}
