/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.event;

import io.saperi.vcm.domain.Device;
import io.saperi.vcm.domain.DeviceModel;
import java.util.UUID;
import org.springframework.context.ApplicationEvent;

/**
 * Event emitted every time a {@link Device} is created, modified or updated.
 *
 * @author esteban
 */
public class DeviceModificationEvent extends ApplicationEvent {

    public static enum Operation {
        CREATE,
        UPDATE,
        DELETE,
        ACTIVATION,
        DEACTIVATION
    }

    private final String id;
    private final String newHardwareId;
    private final String oldHardwareId;
    private final Operation operation;
    private final DeviceModel oldModel;
    private final DeviceModel newModel;

    public DeviceModificationEvent(String newHardwareId, String oldHardwareId, Operation operation, DeviceModel oldModel, DeviceModel newModel, Object source) {
        super(source);
        this.id = UUID.randomUUID().toString();
        this.newHardwareId = newHardwareId;
        this.oldHardwareId = oldHardwareId;
        this.operation = operation;
        this.oldModel = oldModel;
        this.newModel = newModel;
    }

    public String getId() {
        return id;
    }

    public String getNewHardwareId() {
        return newHardwareId;
    }

    public String getOldHardwareId() {
        return oldHardwareId;
    }

    public Operation getOperation() {
        return operation;
    }

    public DeviceModel getOldModel() {
        return oldModel;
    }

    public DeviceModel getNewModel() {
        return newModel;
    }

}
