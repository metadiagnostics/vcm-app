/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.event;

import io.saperi.vcm.domain.Device;
import io.saperi.vcm.domain.ext.*;
import org.springframework.context.ApplicationEvent;

/**
 * Event that is published when a request to change the values of a ventilator
 * is processed.
 *
 * @author esteban
 */
public class VentilatorConfigurationChangeRequestEvent extends VentilatorRequestEvent {

    public VentilatorConfigurationChangeRequestEvent(Device device, Object source, Long assignmentId, String requester) {
        super(device, source, assignmentId, requester);
    }

    //TODO: Refactor this - Use something other than source
    public VentilatorConfigurationChangeRequest getChanges() {
        return (VentilatorConfigurationChangeRequest) getSource();
    }

}
