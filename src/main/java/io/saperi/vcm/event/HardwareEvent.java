/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.event;

import java.util.UUID;
import org.springframework.context.ApplicationEvent;

/**
 * Parent class for the various hardware events supported by this application.
 * Instances of this class are also instances of {@link ApplicationEvent}
 * meaning that can be broadcasted as Spring events.
 *
 * @author esteban
 */
public abstract class HardwareEvent extends ApplicationEvent {

    private final String id;
    private final String hardwareId;

    public HardwareEvent(String hardwareId, Object source) {
        super(source);
        this.id = UUID.randomUUID().toString();
        this.hardwareId = hardwareId;
    }

    public String getId() {
        return id;
    }

    public String getHardwareId() {
        return hardwareId;
    }

}
