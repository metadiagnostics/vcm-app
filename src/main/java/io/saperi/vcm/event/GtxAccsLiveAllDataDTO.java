/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.event;

public class GtxAccsLiveAllDataDTO {

    private HardwareVitalsEvent hardwareVitalsEvent;
    private HardwareVentilatorEvent hardwareVentilatorEvent;
    private String hardwareId;


    public GtxAccsLiveAllDataDTO() {}

    public HardwareVitalsEvent getHardwareVitalsEvent() {
        return hardwareVitalsEvent;
    }

    public void setHardwareVitalsEvent(HardwareVitalsEvent hardwareVitalsEvent) {
        this.hardwareVitalsEvent = hardwareVitalsEvent;
    }

    public HardwareVentilatorEvent getHardwareVentilatorEvent() {
        return hardwareVentilatorEvent;
    }

    public void setHardwareVentilatorEvent(HardwareVentilatorEvent hardwareVentilatorEvent) {
        this.hardwareVentilatorEvent = hardwareVentilatorEvent;
    }

    public String getHardwareId() {
        if (hardwareVitalsEvent !=null) {
            return hardwareVitalsEvent.getHardwareId();
        }
        else if (hardwareVentilatorEvent !=null) {
            return hardwareVentilatorEvent.getHardwareId();
        }
        return null;
    }

    public void setHardwareId(String hardwareId) {
        this.hardwareId = hardwareId;
    }
}
