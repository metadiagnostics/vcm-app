/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.event;

import io.saperi.vcm.domain.Assignment;
import io.saperi.vcm.domain.Device;
import io.saperi.vcm.domain.ext.VentilatorConfigurationChangeRequest;
import org.springframework.context.ApplicationEvent;

/**
 * Event that is published when an user ack in the UI that Fio2 was entered in the Hardware device.
 *
 * @author sgroh@saperi.io
 */
public class Fio2AckRequestEvent extends VentilatorRequestEvent {

    public Fio2AckRequestEvent(Device device, Object source, Long assignmentId, String requester) {
        super(device, source, assignmentId, requester);
    }
}
