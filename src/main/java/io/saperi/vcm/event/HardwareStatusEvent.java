/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.event;

/**
 * Class used to send notifications related to the status of a specific Device
 * (Hardware).
 *
 * @author esteban
 */
public class HardwareStatusEvent extends HardwareEvent {

    public static enum Status {
        CONNECTED,
        UNKNOWN,
        WARNING,
        ERROR,
        DISCONNECTED,
        ONLINE,
        OFFLINE
    }

    private final Status status;
    private final String message;

    public HardwareStatusEvent(String hardwareId, Status status, Object source) {
        this(hardwareId, status, "", source);
    }

    public HardwareStatusEvent(String hardwareId, Status status, String message, Object source) {
        super(hardwareId, source);
        this.status = status;
        this.message = message;
    }

    public Status getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

}
