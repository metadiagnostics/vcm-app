/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.config;

import io.saperi.vcm.event.HardwareEvent;
import org.ehcache.config.CacheConfiguration;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.jsr107.Eh107Configuration;
import org.glassfish.hk2.api.TypeLiteral;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.cache.Cache;
import javax.cache.CacheManager;
import javax.cache.Caching;
import javax.cache.spi.CachingProvider;
import java.util.List;

//@Configuration
public class EventCacheConfiguration {
    private final String CACHE_NAME="VCM_EVENTS";
    private final long MAX_CACHE_ENTRIES=1000000; // this should support at least 24hs of information

    @Bean(name = "events-cache")
    public Cache<String, List<HardwareEvent>> eventsCache(){
        CachingProvider cachingProvider = Caching.getCachingProvider();
        CacheManager cacheManager = cachingProvider.getCacheManager();
        Cache<String, HardwareEvent>  eventsCache = cacheManager.getCache(CACHE_NAME);
        if (eventsCache != null) {
            if (eventsCache.isClosed()) {
                cacheManager.destroyCache(CACHE_NAME);
            } else {
                return null;
            }
        }
        CacheConfiguration<String, List<HardwareEvent>> config = CacheConfigurationBuilder.newCacheConfigurationBuilder(
            String.class,
            new TypeLiteral<List<HardwareEvent>>() {}.getRawType(),
            ResourcePoolsBuilder.heap(MAX_CACHE_ENTRIES))
            //.withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofMinutes(10)))
            .build();
        javax.cache.configuration.Configuration<String, List<HardwareEvent>> jCacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(config);

        return cacheManager.createCache(CACHE_NAME, jCacheConfiguration);
    }

}
