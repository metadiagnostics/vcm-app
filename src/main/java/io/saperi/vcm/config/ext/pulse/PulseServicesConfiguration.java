/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.saperi.vcm.config.ext.pulse;

import static io.saperi.pulse.sim.api.serialize.PulseObjectMapperFactory.createObjectMapper;
import static java.util.Objects.isNull;

import io.saperi.pulse.sim.api.service.SimulationService;
import io.saperi.pulse.sim.api.service.StateService;
import io.saperi.pulse.sim.api.service.SubstanceService;
import io.saperi.pulse.sim.client.rs.spring.SimulationServiceSpringClient;
import io.saperi.pulse.sim.client.rs.spring.StateServiceSpringClient;
import io.saperi.pulse.sim.client.rs.spring.SubstanceServiceSpringClient;
import java.io.IOException;
import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.client.AuthorizedClientServiceOAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.OAuth2AuthorizeRequest;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProvider;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProviderBuilder;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author esteban
 */
@Configuration
public class PulseServicesConfiguration {

    @Value("${vcm.pulse.ssl}")
    private boolean sslEnabled;

    @Value("${vcm.pulse.host}")
    private String pulseHost;

    @Value("${vcm.pulse.port}")
    private int pulsePort;

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Autowired
    private OAuth2AuthorizedClientService oAuth2AuthorizedClientService;

    @Autowired
    private ClientRegistrationRepository clientRegistrationRepository;

    @Bean
    public StateService pulseStateServiceClient(PulseConfigurationUtils pulseUtils, @Qualifier("pulse") RestTemplate restTemplate) {
        return new StateServiceSpringClient(restTemplate, pulseUtils.getPulseRestEndpoint());
    }

    @Bean
    public SimulationService pulseSimulationServiceClient(PulseConfigurationUtils pulseUtils, @Qualifier("pulse") RestTemplate restTemplate) {
        return new SimulationServiceSpringClient(restTemplate, pulseUtils.getPulseRestEndpoint());
    }

    @Bean
    public SubstanceService pulseSubstanceServiceClient(PulseConfigurationUtils pulseUtils, @Qualifier("pulse") RestTemplate restTemplate) {
        return new SubstanceServiceSpringClient(restTemplate, pulseUtils.getPulseRestEndpoint());
    }

    @Bean
    @Qualifier("pulse")
    public RestTemplate restTemplate() {

        ClientRegistration clientRegistration = clientRegistrationRepository.findByRegistrationId("oidc-client-credentials");

        return restTemplateBuilder
            .messageConverters(createMappingJacksonHttpMessageConverter())
            .additionalInterceptors(new OAuthClientCredentialsRestTemplateInterceptor(authorizedClientManager(), clientRegistration))
            .setReadTimeout(Duration.ofMinutes(2)) //needed for simulations
            .setConnectTimeout(Duration.ofSeconds(1))
            .build();
    }

    @Bean
    @Qualifier("pulse")
    public OAuth2AuthorizedClientManager authorizedClientManager() {
        OAuth2AuthorizedClientProvider authorizedClientProvider = OAuth2AuthorizedClientProviderBuilder.builder()
            .clientCredentials()
            .build();

        AuthorizedClientServiceOAuth2AuthorizedClientManager authorizedClientManager
            = new AuthorizedClientServiceOAuth2AuthorizedClientManager(clientRegistrationRepository, oAuth2AuthorizedClientService);
        authorizedClientManager.setAuthorizedClientProvider(authorizedClientProvider);

        return authorizedClientManager;
    }

    private MappingJackson2HttpMessageConverter createMappingJacksonHttpMessageConverter() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(createObjectMapper());
        return converter;
    }

}

class OAuthClientCredentialsRestTemplateInterceptor implements ClientHttpRequestInterceptor {

    private final OAuth2AuthorizedClientManager manager;
    private final Authentication principal;
    private final ClientRegistration clientRegistration;

    public OAuthClientCredentialsRestTemplateInterceptor(OAuth2AuthorizedClientManager manager, ClientRegistration clientRegistration) {
        this.manager = manager;
        this.clientRegistration = clientRegistration;
        this.principal = createPrincipal();
    }

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        OAuth2AuthorizeRequest oAuth2AuthorizeRequest = OAuth2AuthorizeRequest
            .withClientRegistrationId(clientRegistration.getRegistrationId())
            .principal(principal)
            .build();
        OAuth2AuthorizedClient client = manager.authorize(oAuth2AuthorizeRequest);
        if (isNull(client)) {
            throw new IllegalStateException("Client credentials flow on " + clientRegistration.getRegistrationId() + " failed, client is null");
        }

        request.getHeaders().add(HttpHeaders.AUTHORIZATION, "Bearer " + client.getAccessToken().getTokenValue());
        return execution.execute(request, body);
    }

    private Authentication createPrincipal() {
        return new Authentication() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                return Collections.emptySet();
            }

            @Override
            public Object getCredentials() {
                return null;
            }

            @Override
            public Object getDetails() {
                return null;
            }

            @Override
            public Object getPrincipal() {
                return this;
            }

            @Override
            public boolean isAuthenticated() {
                return false;
            }

            @Override
            public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
            }

            @Override
            public String getName() {
                return clientRegistration.getClientId();
            }
        };
    }
}
