/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.saperi.vcm.config.ext.gtx;

import io.github.jhipster.async.ExceptionHandlingAsyncTaskExecutor;
import java.util.concurrent.Executor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 *
 * @author esteban
 */
@Configuration
public class GtxServicesConfiguration {

    private final Logger log = LoggerFactory.getLogger(GtxServicesConfiguration.class);

    @Bean(name = "gtxTaskExecutor")
    public Executor getGtxAsyncExecutor() {
        log.debug("Creating GTX Async Task Executor");
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(2);
        executor.setThreadNamePrefix("GTX-");
        executor.setWaitForTasksToCompleteOnShutdown(false);
        return new ExceptionHandlingAsyncTaskExecutor(executor);
    }

}
