/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.config.ext.amq;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration related to JMS.
 *
 * @author esteban
 */
@Configuration
public class AmqAutoConfiguration {

    private final Logger log = LoggerFactory.getLogger(AmqAutoConfiguration.class);

    @Value("${vcm.jms.amq.url:vm://localhost}")
    private String url;

    @Value("${vcm.jms.amq.username:}")
    private String username;

    @Value("${vcm.jms.amq.password:}")
    private String password;

    @Bean
    @Qualifier("amq-jms-factoy")
    public ConnectionFactory jmsConnectionFactory() throws JMSException {
        log.debug("Configuring ActiveMQ client for '{}'", url);
        //return new ImqConnectionFactoryBuilder(properties, factoryCustomizer.getIfAvailable()).createFactory();

        ActiveMQConnectionFactory factory;
        if (StringUtils.isBlank(username) && StringUtils.isBlank(password)) {
            log.debug("Creating AMQ Connection without username/password");
            factory = new ActiveMQConnectionFactory(url);
        } else {
            log.debug("Creating AMQ Connection with username/password");
            factory = new ActiveMQConnectionFactory(username, password, url);
        }

        factory.setTrustAllPackages(true);

        return factory;
    }

}
