/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.saperi.vcm.config.ext.pulse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author esteban
 */
@Component
public class PulseConfigurationUtils {

    @Value("${vcm.pulse.ssl}")
    private boolean sslEnabled;

    @Value("${vcm.pulse.host}")
    private String pulseHost;

    @Value("${vcm.pulse.port}")
    private int pulsePort;

    @Value("${vcm.pulse.ws-endpoint}")
    private String pulseWsEndpoint;

    public String getPulseRestEndpoint() {
        return (sslEnabled ? "https" : "http") + "://" + pulseHost + ":" + pulsePort;
    }

    public String getPulseWebsocketEndpoint() {
        return (sslEnabled ? "wss" : "ws") + "://" + pulseHost + ":" + pulsePort + pulseWsEndpoint;
    }

}
