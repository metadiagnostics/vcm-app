/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.config.ext;

import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

/**
 * Configuration for a {@link RestOperations} bean that can be used to
 * communicate with external HTTP endpoints.
 *
 * @author esteban
 */
@Configuration
public class RestOperationsConfiguration {

    @Value("${vcm.http-client.disable-content-compression:false}")
    private boolean disableContentCompression;

    /**
     * The {@link #disableContentCompression} property can be used to enable or
     * disable content compression in the requests.
     *
     */
    @Bean
    public RestOperations restOperations(RestTemplateCustomizer customizer) {

        HttpClientBuilder builder = HttpClientBuilder.create();
        if (disableContentCompression) {
            builder.disableContentCompression();
        }

        RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory(
            builder.build()
        ));
        customizer.customize(restTemplate);
        return restTemplate;
    }
}
