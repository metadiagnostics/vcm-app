/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.config;

import io.github.jhipster.config.JHipsterProperties;
import io.saperi.vcm.security.AuthoritiesConstants;
import java.security.Principal;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.saperi.vcm.security.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.*;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.util.Assert;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.*;
import org.springframework.web.socket.server.HandshakeInterceptor;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

@Configuration
@EnableWebSocketMessageBroker
public class WebsocketConfiguration implements WebSocketMessageBrokerConfigurer {

    public static final String IP_ADDRESS = "IP_ADDRESS";

    private final JHipsterProperties jHipsterProperties;
    private final Pattern pattern = Pattern.compile("^access_token=([\\w-]*\\.[\\w-]*\\.[\\w-]*)$");
    private final JwtDecoder jwtDecoder;

    @Autowired
    public WebsocketConfiguration(JHipsterProperties jHipsterProperties, @Qualifier("jwtDecoderWithoutIssuer") JwtDecoder jwtDecoder) {
        this.jHipsterProperties = jHipsterProperties;
        this.jwtDecoder = jwtDecoder;
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic");
    }

    @Override
    public void configureWebSocketTransport(WebSocketTransportRegistration registration) {
        // Terminating 'WebSocketServerSockJsSession[id=d55abi1p]': Buffer size 3161699 bytes for session 'd55abi1p' exceeds the allowed limit 3145728
        int initialLimit = 1024 *1024;
        int capacity = 8;
        registration.setMessageSizeLimit(initialLimit * capacity); // Terminating 'WebSocketServerSockJsSession[id=d55abi1p]': Buffer size 3161699 bytes for session 'd55abi1p' exceeds the allowed limit 3145728
        registration.setSendBufferSizeLimit(initialLimit * (capacity +1));
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        String[] allowedOrigins = Optional.ofNullable(jHipsterProperties.getCors().getAllowedOrigins()).map(origins -> origins.toArray(new String[0])).orElse(new String[0]);
        registry.addEndpoint("/websocket/tracker")
            .setHandshakeHandler(defaultHandshakeHandler())
            .setAllowedOrigins(allowedOrigins)
            .withSockJS()
            .setInterceptors(httpSessionHandshakeInterceptor());
        registry.addEndpoint("/websocket/hardware")
            .setHandshakeHandler(defaultHandshakeHandler())
            .setAllowedOrigins(allowedOrigins)
            .withSockJS()
            .setInterceptors(httpSessionHandshakeInterceptor());
        registry.addEndpoint("/websocket/monitor")
            .setHandshakeHandler(defaultHandshakeHandler())
            .setAllowedOrigins(allowedOrigins)
            .withSockJS()
            .setInterceptors(httpSessionHandshakeInterceptor());
        registry.addEndpoint("/websocket/ventilator")
            .setHandshakeHandler(defaultHandshakeHandler())
            .setAllowedOrigins(allowedOrigins)
            .withSockJS()
            .setInterceptors(httpSessionHandshakeInterceptor());
    }

    @Bean
    public HandshakeInterceptor httpSessionHandshakeInterceptor() {
        return new HandshakeInterceptor() {

            @Override
            public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Map<String, Object> attributes) throws Exception {
                if (request instanceof ServletServerHttpRequest) {
                    ServletServerHttpRequest servletRequest = (ServletServerHttpRequest) request;
                    attributes.put(IP_ADDRESS, servletRequest.getRemoteAddress());
                    //attributes.put("token", .get("auth_token"));
                    /* List<String> cookie= servletRequest.getHeaders().get("cookie");
                    if (cookie!=null){
                        String cookieValue = cookie.get(0);
                        String[] cookieParts = cookieValue.split(";");
                        if (cookieParts.length == 2) {
                            String tokenPair = cookieParts[0].startsWith("XSRF")?cookieParts[0].strip():cookieParts[1].strip();
                            if (tokenPair.startsWith("XSRF")){
                                String[] tokenParts = tokenPair.split("=");
                                if (tokenParts.length ==2 ){
                                    servletRequest.getHeaders().add("X-XSRF-TOKEN", tokenParts[1]);
                                                                    }
                            }
                        }


                    }*/
                }
                return true;
            }

            @Override
            public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Exception exception) {

            }
        };
    }

    @Bean
    public HandshakeInterceptor httpSessionHandshakeInterceptor2() {
        return new HandshakeInterceptor() {

            @Override
            public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Map<String, Object> attributes) throws Exception {
                if (request instanceof ServletServerHttpRequest) {
                    ServletServerHttpRequest servletRequest = (ServletServerHttpRequest) request;
                    attributes.put(IP_ADDRESS, servletRequest.getRemoteAddress());

                }
                return true;
            }

            @Override
            public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Exception exception) {

            }
        };
    }

    private DefaultHandshakeHandler defaultHandshakeHandler() {
        return new DefaultHandshakeHandler() {
            @Override
            protected Principal determineUser(ServerHttpRequest request, WebSocketHandler wsHandler, Map<String, Object> attributes) {
                Principal principal = request.getPrincipal();
                if (principal == null) {
                    Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
                    authorities.add(new SimpleGrantedAuthority(AuthoritiesConstants.ADMIN));
                    String query = request.getURI().getQuery();
                    Matcher matcher = pattern.matcher(query);
                    if (matcher.find()) {
                        String token = matcher.group(1);
                        // validate JWT token
                        if (SecurityUtils.isValidToken(token, jwtDecoder)){
                            principal = new BearedTokenAuthenticationTokenAndAuthorities(token, authorities);
                        }
                    }
                }
                return principal;
            }
        };
    }

    private class BearedTokenAuthenticationTokenAndAuthorities extends AbstractAuthenticationToken {
        private static final long serialVersionUID = 520L;
        private String token;

        public BearedTokenAuthenticationTokenAndAuthorities(String token, Collection<SimpleGrantedAuthority> authorities) {
            super(authorities);
            Assert.hasText(token, "token cannot be empty");
            this.token = token;
            this.setAuthenticated(true);
        }

        public String getToken() {
            return this.token;
        }

        public Object getCredentials() {
            return this.getToken();
        }

        public Object getPrincipal() {
            return this.getToken();
        }
    }
}
