/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import org.hibernate.cache.jcache.ConfigSettings;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache = jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, io.saperi.vcm.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, io.saperi.vcm.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, io.saperi.vcm.domain.User.class.getName());
            createCache(cm, io.saperi.vcm.domain.Authority.class.getName());
            createCache(cm, io.saperi.vcm.domain.User.class.getName() + ".authorities");
            createCache(cm, io.saperi.vcm.domain.Assignment.class.getName());
            createCache(cm, io.saperi.vcm.domain.Assignment.class.getName() + ".devices");
            createCache(cm, io.saperi.vcm.domain.Device.class.getName());
            createCache(cm, io.saperi.vcm.domain.Device.class.getName() + ".assignments");
            createCache(cm, io.saperi.vcm.domain.DeviceModel.class.getName());
            createCache(cm, io.saperi.vcm.domain.DeviceModel.class.getName() + ".capabilities");
            createCache(cm, io.saperi.vcm.domain.DeviceCapability.class.getName());
            createCache(cm, io.saperi.vcm.domain.DeviceCapability.class.getName() + ".deviceModels");
            createCache(cm, io.saperi.vcm.domain.AssignedDevice.class.getName());
            createCache(cm, io.saperi.vcm.domain.AssignedDevice.class.getName() + ".capabilities");
            createCache(cm, io.saperi.vcm.domain.AssignedDevice.class.getName() + ".assignments");
            createCache(cm, io.saperi.vcm.domain.DeviceCapability.class.getName() + ".assignments");
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache == null) {
            cm.createCache(cacheName, jcacheConfiguration);
        }
    }

}
