<?xml version="1.0" encoding="utf-8"?>
<!--
 *    Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
-->
<databaseChangeLog
    xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
    xmlns:ext="http://www.liquibase.org/xml/ns/dbchangelog-ext"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.8.xsd
                        http://www.liquibase.org/xml/ns/dbchangelog-ext http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-ext.xsd">

        <property name="autoIncrement" value="true"/>

    <!--
        Added the entity AssignedDevice.
    -->
    <changeSet id="20200604064800-1" author="jhipster">
        <createTable tableName="assigned_device" remarks="When a Device is assigned to an Assignment, it fulfills\none or more roles (capabilities). This entity holds\nthat information. This entity is basically a N-M with extra data.">
            <column name="id" type="bigint" autoIncrement="${autoIncrement}">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="assignment_id" type="bigint">
                <constraints nullable="true" />
            </column>
            <column name="device_id" type="bigint">
                <constraints nullable="true" />
            </column>
            <column name="capability_id" type="bigint">
                <constraints nullable="false" />
            </column>
            <!-- jhipster-needle-liquibase-add-column - JHipster will add columns here, do not remove-->
        </createTable>
    </changeSet>

    <changeSet id="20200604064800-1-relations" author="jhipster">

    </changeSet>
    <!-- jhipster-needle-liquibase-add-changeset - JHipster will add changesets here, do not remove-->

    <!--
        Load sample data generated with Faker.js
        - This data can be easily edited using a CSV editor (or even MS Excel) and
          is located in the 'src/main/resources/config/liquibase/fake-data' directory
        - By default this data is applied when running with the JHipster 'dev' profile.
          This can be customized by adding or removing 'faker' in the 'spring.liquibase.contexts'
          Spring Boot configuration key.
    -->
    <changeSet id="20200604064800-1-data" author="jhipster" context="faker">
        <loadData
                  file="config/liquibase/fake-data/assigned_device.csv"
                  separator=";"
                  tableName="assigned_device">
            <column name="id" type="numeric"/>
            <column name="capability_id" type="numeric"/>
            <!-- jhipster-needle-liquibase-add-loadcolumn - JHipster (and/or extensions) can add load columns here, do not remove-->
        </loadData>
    </changeSet>
    <changeSet id="20220218150000-2-data" author="sgroh" runAlways="true" context="!test">
        <preConditions onFail="MARK_RAN">
            <sqlCheck expectedResult="0">
                SELECT COUNT(*) FROM device_model_capabilities WHERE device_model_id=(select id from device_model where name='Docbox Aggregator' LIMIT 1);
            </sqlCheck>
        </preConditions>
        <insert tableName="device_model_capabilities">
            <column name="device_model_id" valueComputed="(SELECT id FROM device_model WHERE name='Docbox Aggregator' LIMIT 1)"/>
            <column name="capabilities_id" valueComputed="(SELECT id FROM device_capability WHERE type='DEMOGRAPHICS')"/>
        </insert>
        <insert tableName="device_model_capabilities">
            <column name="device_model_id" valueComputed="(SELECT id FROM device_model WHERE name='Docbox Aggregator' LIMIT 1)"/>
            <column name="capabilities_id" valueComputed="(SELECT id FROM device_capability WHERE type='VITALS')"/>
        </insert>
        <insert tableName="device_model_capabilities">
            <column name="device_model_id" valueComputed="(SELECT id FROM device_model WHERE name='Docbox Aggregator' LIMIT 1)"/>
            <column name="capabilities_id" valueComputed="(SELECT id FROM device_capability WHERE type='VENTILATOR')"/>
        </insert>
        <insert tableName="device_model_capabilities">
            <column name="device_model_id" valueComputed="(SELECT id FROM device_model WHERE name='Docbox Aggregator' LIMIT 1)"/>
            <column name="capabilities_id" valueComputed="(SELECT id FROM device_capability WHERE type='BLOOD_GAS_ANALYZER')"/>
        </insert>
        <insert tableName="device_model_capabilities">
            <column name="device_model_id" valueComputed="(SELECT id FROM device_model WHERE name='Docbox Aggregator' LIMIT 1)"/>
            <column name="capabilities_id" valueComputed="(SELECT id FROM device_capability WHERE type='VENTILATOR_CONTROL')"/>
        </insert>
    </changeSet>
    <changeSet id="20220728150000-3-data" author="sgroh" runAlways="true" context="!test">
        <validCheckSum>8:102a438a51b7ae61b11d69a61e17eec9</validCheckSum>
        <preConditions onFail="MARK_RAN">
            <sqlCheck expectedResult="0">
                SELECT COUNT(*) FROM device_model_capabilities WHERE device_model_id=(select id from device_model where name='Thornhill Moves SLC' LIMIT 1);
            </sqlCheck>
        </preConditions>
        <insert tableName="device_model_capabilities">
            <column name="device_model_id" valueComputed="(SELECT id FROM device_model WHERE name='Thornhill Moves SLC' LIMIT 1)"/>
            <column name="capabilities_id" valueComputed="(SELECT id FROM device_capability WHERE type='DEMOGRAPHICS')"/>
        </insert>
        <insert tableName="device_model_capabilities">
            <column name="device_model_id" valueComputed="(SELECT id FROM device_model WHERE name='Thornhill Moves SLC' LIMIT 1)"/>
            <column name="capabilities_id" valueComputed="(SELECT id FROM device_capability WHERE type='VITALS')"/>
        </insert>
        <insert tableName="device_model_capabilities">
            <column name="device_model_id" valueComputed="(SELECT id FROM device_model WHERE name='Thornhill Moves SLC' LIMIT 1)"/>
            <column name="capabilities_id" valueComputed="(SELECT id FROM device_capability WHERE type='VENTILATOR')"/>
        </insert>
        <insert tableName="device_model_capabilities">
            <column name="device_model_id" valueComputed="(SELECT id FROM device_model WHERE name='Thornhill Moves SLC' LIMIT 1)"/>
            <column name="capabilities_id" valueComputed="(SELECT id FROM device_capability WHERE type='BLOOD_GAS_ANALYZER')"/>
        </insert>
        <insert tableName="device_model_capabilities">
            <column name="device_model_id" valueComputed="(SELECT id FROM device_model WHERE name='Thornhill Moves SLC' LIMIT 1)"/>
            <column name="capabilities_id" valueComputed="(SELECT id FROM device_capability WHERE type='VENTILATOR_CONTROL')"/>
        </insert>
    </changeSet>
    <changeSet id="20220906150000-1-data" author="sgroh" runAlways="true" context="!test">
        <preConditions onFail="MARK_RAN">
            <sqlCheck expectedResult="0">
                SELECT COUNT(*) FROM device_model_capabilities WHERE device_model_id=(select id from device_model where type='NIHONKOHDEN' LIMIT 1);
            </sqlCheck>
        </preConditions>
        <insert tableName="device_model_capabilities">
            <column name="device_model_id" valueComputed="(SELECT id FROM device_model WHERE type='NIHONKOHDEN' LIMIT 1)"/>
            <column name="capabilities_id" valueComputed="(SELECT id FROM device_capability WHERE type='DEMOGRAPHICS')"/>
        </insert>
        <insert tableName="device_model_capabilities">
            <column name="device_model_id" valueComputed="(SELECT id FROM device_model WHERE type='NIHONKOHDEN' LIMIT 1)"/>
            <column name="capabilities_id" valueComputed="(SELECT id FROM device_capability WHERE type='VITALS')"/>
        </insert>

        <insert tableName="device_model_capabilities">
            <column name="device_model_id" valueComputed="(SELECT id FROM device_model WHERE type='NIHONKOHDEN' LIMIT 1)"/>
            <column name="capabilities_id" valueComputed="(SELECT id FROM device_capability WHERE type='VENTILATOR')"/>
        </insert>
        <insert tableName="device_model_capabilities">
            <column name="device_model_id" valueComputed="(SELECT id FROM device_model WHERE type='NIHONKOHDEN' LIMIT 1)"/>
            <column name="capabilities_id" valueComputed="(SELECT id FROM device_capability WHERE type='BLOOD_GAS_ANALYZER')"/>
        </insert>
        <insert tableName="device_model_capabilities">
            <column name="device_model_id" valueComputed="(SELECT id FROM device_model WHERE type='NIHONKOHDEN' LIMIT 1)"/>
            <column name="capabilities_id" valueComputed="(SELECT id FROM device_capability WHERE type='VENTILATOR_CONTROL')"/>
        </insert>
    </changeSet>
    <changeSet id="20220906150000-2-data" author="sgroh" runAlways="true" context="!test">
        <preConditions onFail="MARK_RAN">
            <sqlCheck expectedResult="0">
                SELECT COUNT(*) FROM device_model_capabilities WHERE device_model_id=(select id from device_model where type='NEUROWAVE' LIMIT 1);
            </sqlCheck>
        </preConditions>
        <insert tableName="device_model_capabilities">
            <column name="device_model_id" valueComputed="(SELECT id FROM device_model WHERE type='NEUROWAVE' LIMIT 1)"/>
            <column name="capabilities_id" valueComputed="(SELECT id FROM device_capability WHERE type='DEMOGRAPHICS')"/>
        </insert>
        <insert tableName="device_model_capabilities">
            <column name="device_model_id" valueComputed="(SELECT id FROM device_model WHERE type='NEUROWAVE' LIMIT 1)"/>
            <column name="capabilities_id" valueComputed="(SELECT id FROM device_capability WHERE type='VITALS')"/>
        </insert>

        <insert tableName="device_model_capabilities">
            <column name="device_model_id" valueComputed="(SELECT id FROM device_model WHERE type='NEUROWAVE' LIMIT 1)"/>
            <column name="capabilities_id" valueComputed="(SELECT id FROM device_capability WHERE type='VENTILATOR')"/>
        </insert>
        <insert tableName="device_model_capabilities">
            <column name="device_model_id" valueComputed="(SELECT id FROM device_model WHERE type='NEUROWAVE' LIMIT 1)"/>
            <column name="capabilities_id" valueComputed="(SELECT id FROM device_capability WHERE type='BLOOD_GAS_ANALYZER')"/>
        </insert>
        <insert tableName="device_model_capabilities">
            <column name="device_model_id" valueComputed="(SELECT id FROM device_model WHERE type='NEUROWAVE' LIMIT 1)"/>
            <column name="capabilities_id" valueComputed="(SELECT id FROM device_capability WHERE type='VENTILATOR_CONTROL')"/>
        </insert>
    </changeSet>

</databaseChangeLog>
