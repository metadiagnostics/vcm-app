<?xml version="1.0" encoding="utf-8"?>
<!--
 *    Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
-->
<databaseChangeLog
    xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
    xmlns:ext="http://www.liquibase.org/xml/ns/dbchangelog-ext"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.8.xsd
                        http://www.liquibase.org/xml/ns/dbchangelog-ext http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-ext.xsd">

        <property name="autoIncrement" value="true"/>

    <!--
        Added the entity DeviceModel.
    -->
    <changeSet id="20200507080100-1" author="jhipster">
        <createTable tableName="device_model">
            <column name="id" type="bigint" autoIncrement="${autoIncrement}">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="name" type="varchar(255)">
                <constraints nullable="false" />
            </column>
            <column name="type" type="varchar(255)">
                <constraints nullable="false" />
            </column>
            <!-- jhipster-needle-liquibase-add-column - JHipster will add columns here, do not remove-->
        </createTable>
    </changeSet>

    <changeSet id="20200507080100-1-relations" author="jhipster">

        <createTable tableName="device_model_capabilities">
            <column name="capabilities_id" type="bigint">
                <constraints nullable="false"/>
            </column>
            <column name="device_model_id" type="bigint">
                <constraints nullable="false"/>
            </column>
        </createTable>

        <addPrimaryKey columnNames="device_model_id, capabilities_id" tableName="device_model_capabilities"/>

    </changeSet>
    <!-- jhipster-needle-liquibase-add-changeset - JHipster will add changesets here, do not remove-->

    <!--
        Load sample data generated with Faker.js
        - This data can be easily edited using a CSV editor (or even MS Excel) and
          is located in the 'src/main/resources/config/liquibase/fake-data' directory
        - By default this data is applied when running with the JHipster 'dev' profile.
          This can be customized by adding or removing 'faker' in the 'spring.liquibase.contexts'
          Spring Boot configuration key.
    -->
    <changeSet id="20200507080100-1-data" author="jhipster" context="faker">
        <loadData
                  file="config/liquibase/fake-data/device_model.csv"
                  separator=";"
                  tableName="device_model">
            <column name="id" type="numeric"/>
            <column name="name" type="string"/>
            <column name="type" type="string"/>
            <!-- jhipster-needle-liquibase-add-loadcolumn - JHipster (and/or extensions) can add load columns here, do not remove-->
        </loadData>
    </changeSet>

    <changeSet id="20220218080000-data" author="sgroh" runAlways="true" context="!test">
        <preConditions onFail="MARK_RAN">
            <sqlCheck expectedResult="0">
                SELECT COUNT(*) FROM device_model WHERE name='Docbox Aggregator';
            </sqlCheck>
        </preConditions>
        <insert tableName="device_model">
            <column name="name" value="Docbox Aggregator"/>
            <column name="type" value="DOCBOX"/>
        </insert>
    </changeSet>

    <!--Adding a Device insert here to guarantee that the device model already exists -->
    <changeSet id="20220414080000-data" author="sgroh" runAlways="true" context="!test">
        <preConditions onFail="MARK_RAN">
            <sqlCheck expectedResult="0">
                SELECT COUNT(*) FROM device WHERE hardware_id='Replay::Thornhill_mock_data.json' and model_id=(SELECT id FROM device_model WHERE name='Docbox Aggregator' LIMIT 1);
            </sqlCheck>
        </preConditions>
        <insert tableName="device">
            <column name="model_id" valueComputed="(SELECT id FROM device_model WHERE name='Docbox Aggregator' LIMIT 1)"/>
            <column name="hardware_id" value="Replay::Thornhill_mock_data.json"/>
        </insert>
    </changeSet>
    <changeSet id="20220728080100-data-update" author="sgroh" runAlways="true" context="!test">
        <preConditions onFail="MARK_RAN">
            <sqlCheck expectedResult="1">
                SELECT COUNT(*) FROM device_model WHERE name='Thornhill Aggregator' LIMIT 1;
            </sqlCheck>
        </preConditions>
        <update tableName="device_model">
            <column name="name" value="Thornhill Moves SLC"/>
            <where>name='Thornhill Aggregator'</where>
        </update>
    </changeSet>
    <changeSet id="20220728080100-data" author="sgroh" runAlways="true">
        <validCheckSum>8:a4a00a3a59e3a83e8f15083f2b844547</validCheckSum>
        <preConditions onFail="MARK_RAN">
            <sqlCheck expectedResult="0">
                SELECT COUNT(*) FROM device_model WHERE name='Thornhill Moves SLC';
            </sqlCheck>
        </preConditions>
        <insert tableName="device_model">
            <column name="id" value="5"/>
            <column name="name" value="Thornhill Moves SLC"/>
            <column name="type" value="THORNHILL"/>
        </insert>
    </changeSet>
    <!--Adding a Device insert here to guarantee that the device model already exists -->
    <changeSet id="202207284080000-data" author="sgroh" runAlways="true" context="!test">
        <validCheckSum>8:afe298c990227e373d5eb8b9295a5e7e</validCheckSum>
        <preConditions onFail="MARK_RAN">
            <sqlCheck expectedResult="0">
                SELECT COUNT(*) FROM device WHERE hardware_id='a2f9cf1567f402f14c82' and model_id=(SELECT id FROM device_model WHERE name='Thornhill Moves SLC' LIMIT 1);
            </sqlCheck>
        </preConditions>
        <insert tableName="device">
            <column name="model_id" valueComputed="(SELECT id FROM device_model WHERE name='Thornhill Moves SLC' LIMIT 1)"/>
            <column name="hardware_id" value="a2f9cf1567f402f14c82"/>
        </insert>
    </changeSet>

    <changeSet id="2022090640800000-data" author="sgroh" runAlways="true">
        <preConditions onFail="MARK_RAN">
            <sqlCheck expectedResult="0">
                SELECT COUNT(*) FROM device_model WHERE name='Nihon Kohden';
            </sqlCheck>
        </preConditions>

        <insert tableName="device_model">
            <column name="name" value="Nihon Kohden"/>
            <column name="type" value="NIHONKOHDEN"/>
        </insert>
    </changeSet>


    <!--Adding a Device insert here to guarantee that the device model already exists -->

    <changeSet id="202209064080001-data" author="sgroh" runAlways="true" context="!test">
        <preConditions onFail="MARK_RAN">
            <sqlCheck expectedResult="0">
                SELECT COUNT(*) FROM device WHERE hardware_id='Replay::NihonKohden550_20220909_102346.json' and model_id=(SELECT id FROM device_model WHERE type='NIHONKOHDEN' LIMIT 1);
            </sqlCheck>
        </preConditions>
        <insert tableName="device">
            <column name="model_id" valueComputed="(SELECT id FROM device_model WHERE type='NIHONKOHDEN' LIMIT 1)"/>
            <column name="hardware_id" value="Replay::NihonKohden550_20220909_102346.json"/>
        </insert>
    </changeSet>

    <changeSet id="2022090640800002-data" author="sgroh" runAlways="true">
        <preConditions onFail="MARK_RAN">
            <sqlCheck expectedResult="0">
                SELECT COUNT(*) FROM device_model WHERE name='Neurowave';
            </sqlCheck>
        </preConditions>

        <insert tableName="device_model">
            <column name="name" value="Neurowave"/>
            <column name="type" value="NEUROWAVE"/>
        </insert>
    </changeSet>


    <!--Adding a Device insert here to guarantee that the device model already exists -->

    <changeSet id="202209064080003-data" author="sgroh" runAlways="true" context="!test">
        <preConditions onFail="MARK_RAN">
            <sqlCheck expectedResult="0">
                SELECT COUNT(*) FROM device WHERE hardware_id='e5526c3c776655a542c' and model_id=(SELECT id FROM device_model WHERE type='NEUROWAVE' LIMIT 1);
            </sqlCheck>
        </preConditions>
        <insert tableName="device">
            <column name="model_id" valueComputed="(SELECT id FROM device_model WHERE type='NEUROWAVE' LIMIT 1)"/>
            <column name="hardware_id" value="e5526c3c776655a542c"/>
        </insert>
    </changeSet>

</databaseChangeLog>
