/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { browser, ExpectedConditions as ec } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { DeviceCapabilityComponentsPage } from './device-capability.page-object';

const expect = chai.expect;

describe('DeviceCapability e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let deviceCapabilityComponentsPage: DeviceCapabilityComponentsPage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.loginWithOAuth('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load DeviceCapabilities', async () => {
    await navBarPage.goToEntity('device-capability');
    deviceCapabilityComponentsPage = new DeviceCapabilityComponentsPage();
    await browser.wait(ec.visibilityOf(deviceCapabilityComponentsPage.title), 5000);
    expect(await deviceCapabilityComponentsPage.getTitle()).to.eq('vcmApp.deviceCapability.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(deviceCapabilityComponentsPage.entities), ec.visibilityOf(deviceCapabilityComponentsPage.noResult)),
      1000
    );
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
