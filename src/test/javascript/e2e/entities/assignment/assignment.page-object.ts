/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { element, by, ElementFinder } from 'protractor';

export class AssignmentComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-assignment div table .btn-danger'));
  title = element.all(by.css('jhi-assignment div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class AssignmentUpdatePage {
  pageTitle = element(by.id('jhi-assignment-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  patientIdInput = element(by.id('field_patientId'));
  activeInput = element(by.id('field_active'));
  ventilatorModeSelect = element(by.id('field_ventilatorMode'));
  ventilatorRecommenderTypeSelect = element(by.id('field_ventilatorRecommenderType'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setPatientIdInput(patientId: string): Promise<void> {
    await this.patientIdInput.sendKeys(patientId);
  }

  async getPatientIdInput(): Promise<string> {
    return await this.patientIdInput.getAttribute('value');
  }

  getActiveInput(): ElementFinder {
    return this.activeInput;
  }

  async setVentilatorModeSelect(ventilatorMode: string): Promise<void> {
    await this.ventilatorModeSelect.sendKeys(ventilatorMode);
  }

  async getVentilatorModeSelect(): Promise<string> {
    return await this.ventilatorModeSelect.element(by.css('option:checked')).getText();
  }

  async ventilatorModeSelectLastOption(): Promise<void> {
    await this.ventilatorModeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setVentilatorRecommenderTypeSelect(ventilatorRecommenderType: string): Promise<void> {
    await this.ventilatorRecommenderTypeSelect.sendKeys(ventilatorRecommenderType);
  }

  async getVentilatorRecommenderTypeSelect(): Promise<string> {
    return await this.ventilatorRecommenderTypeSelect.element(by.css('option:checked')).getText();
  }

  async ventilatorRecommenderTypeSelectLastOption(): Promise<void> {
    await this.ventilatorRecommenderTypeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class AssignmentDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-assignment-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-assignment'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
