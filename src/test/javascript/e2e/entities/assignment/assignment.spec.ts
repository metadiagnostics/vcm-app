/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  AssignmentComponentsPage,
  /* AssignmentDeleteDialog, */
  AssignmentUpdatePage
} from './assignment.page-object';

const expect = chai.expect;

describe('Assignment e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let assignmentComponentsPage: AssignmentComponentsPage;
  let assignmentUpdatePage: AssignmentUpdatePage;
  /* let assignmentDeleteDialog: AssignmentDeleteDialog; */

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.loginWithOAuth('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Assignments', async () => {
    await navBarPage.goToEntity('assignment');
    assignmentComponentsPage = new AssignmentComponentsPage();
    await browser.wait(ec.visibilityOf(assignmentComponentsPage.title), 5000);
    expect(await assignmentComponentsPage.getTitle()).to.eq('vcmApp.assignment.home.title');
    await browser.wait(ec.or(ec.visibilityOf(assignmentComponentsPage.entities), ec.visibilityOf(assignmentComponentsPage.noResult)), 1000);
  });

  it('should load create Assignment page', async () => {
    await assignmentComponentsPage.clickOnCreateButton();
    assignmentUpdatePage = new AssignmentUpdatePage();
    expect(await assignmentUpdatePage.getPageTitle()).to.eq('vcmApp.assignment.home.createOrEditLabel');
    await assignmentUpdatePage.cancel();
  });

  /* it('should create and save Assignments', async () => {
        const nbButtonsBeforeCreate = await assignmentComponentsPage.countDeleteButtons();

        await assignmentComponentsPage.clickOnCreateButton();

        await promise.all([
            assignmentUpdatePage.setPatientIdInput('patientId'),
            assignmentUpdatePage.ventilatorModeSelectLastOption(),
            assignmentUpdatePage.ventilatorRecommenderTypeSelectLastOption(),
        ]);

        expect(await assignmentUpdatePage.getPatientIdInput()).to.eq('patientId', 'Expected PatientId value to be equals to patientId');
        const selectedActive = assignmentUpdatePage.getActiveInput();
        if (await selectedActive.isSelected()) {
            await assignmentUpdatePage.getActiveInput().click();
            expect(await assignmentUpdatePage.getActiveInput().isSelected(), 'Expected active not to be selected').to.be.false;
        } else {
            await assignmentUpdatePage.getActiveInput().click();
            expect(await assignmentUpdatePage.getActiveInput().isSelected(), 'Expected active to be selected').to.be.true;
        }

        await assignmentUpdatePage.save();
        expect(await assignmentUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await assignmentComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last Assignment', async () => {
        const nbButtonsBeforeDelete = await assignmentComponentsPage.countDeleteButtons();
        await assignmentComponentsPage.clickOnLastDeleteButton();

        assignmentDeleteDialog = new AssignmentDeleteDialog();
        expect(await assignmentDeleteDialog.getDialogTitle())
            .to.eq('vcmApp.assignment.delete.question');
        await assignmentDeleteDialog.clickOnConfirmButton();

        expect(await assignmentComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
