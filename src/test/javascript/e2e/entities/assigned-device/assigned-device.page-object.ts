/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { element, by, ElementFinder } from 'protractor';

export class AssignedDeviceComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-assigned-device div table .btn-danger'));
  title = element.all(by.css('jhi-assigned-device div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class AssignedDeviceUpdatePage {
  pageTitle = element(by.id('jhi-assigned-device-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  assignmentSelect = element(by.id('field_assignment'));
  deviceSelect = element(by.id('field_device'));
  capabilitySelect = element(by.id('field_capability'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async assignmentSelectLastOption(): Promise<void> {
    await this.assignmentSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async assignmentSelectOption(option: string): Promise<void> {
    await this.assignmentSelect.sendKeys(option);
  }

  getAssignmentSelect(): ElementFinder {
    return this.assignmentSelect;
  }

  async getAssignmentSelectedOption(): Promise<string> {
    return await this.assignmentSelect.element(by.css('option:checked')).getText();
  }

  async deviceSelectLastOption(): Promise<void> {
    await this.deviceSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async deviceSelectOption(option: string): Promise<void> {
    await this.deviceSelect.sendKeys(option);
  }

  getDeviceSelect(): ElementFinder {
    return this.deviceSelect;
  }

  async getDeviceSelectedOption(): Promise<string> {
    return await this.deviceSelect.element(by.css('option:checked')).getText();
  }

  async capabilitySelectLastOption(): Promise<void> {
    await this.capabilitySelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async capabilitySelectOption(option: string): Promise<void> {
    await this.capabilitySelect.sendKeys(option);
  }

  getCapabilitySelect(): ElementFinder {
    return this.capabilitySelect;
  }

  async getCapabilitySelectedOption(): Promise<string> {
    return await this.capabilitySelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class AssignedDeviceDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-assignedDevice-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-assignedDevice'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
