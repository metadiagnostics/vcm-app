/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  AssignedDeviceComponentsPage,
  /* AssignedDeviceDeleteDialog, */
  AssignedDeviceUpdatePage
} from './assigned-device.page-object';

const expect = chai.expect;

describe('AssignedDevice e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let assignedDeviceComponentsPage: AssignedDeviceComponentsPage;
  let assignedDeviceUpdatePage: AssignedDeviceUpdatePage;
  /* let assignedDeviceDeleteDialog: AssignedDeviceDeleteDialog; */

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.loginWithOAuth('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load AssignedDevices', async () => {
    await navBarPage.goToEntity('assigned-device');
    assignedDeviceComponentsPage = new AssignedDeviceComponentsPage();
    await browser.wait(ec.visibilityOf(assignedDeviceComponentsPage.title), 5000);
    expect(await assignedDeviceComponentsPage.getTitle()).to.eq('vcmApp.assignedDevice.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(assignedDeviceComponentsPage.entities), ec.visibilityOf(assignedDeviceComponentsPage.noResult)),
      1000
    );
  });

  it('should load create AssignedDevice page', async () => {
    await assignedDeviceComponentsPage.clickOnCreateButton();
    assignedDeviceUpdatePage = new AssignedDeviceUpdatePage();
    expect(await assignedDeviceUpdatePage.getPageTitle()).to.eq('vcmApp.assignedDevice.home.createOrEditLabel');
    await assignedDeviceUpdatePage.cancel();
  });

  /* it('should create and save AssignedDevices', async () => {
        const nbButtonsBeforeCreate = await assignedDeviceComponentsPage.countDeleteButtons();

        await assignedDeviceComponentsPage.clickOnCreateButton();

        await promise.all([
            assignedDeviceUpdatePage.assignmentSelectLastOption(),
            assignedDeviceUpdatePage.deviceSelectLastOption(),
            assignedDeviceUpdatePage.capabilitySelectLastOption(),
        ]);


        await assignedDeviceUpdatePage.save();
        expect(await assignedDeviceUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await assignedDeviceComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last AssignedDevice', async () => {
        const nbButtonsBeforeDelete = await assignedDeviceComponentsPage.countDeleteButtons();
        await assignedDeviceComponentsPage.clickOnLastDeleteButton();

        assignedDeviceDeleteDialog = new AssignedDeviceDeleteDialog();
        expect(await assignedDeviceDeleteDialog.getDialogTitle())
            .to.eq('vcmApp.assignedDevice.delete.question');
        await assignedDeviceDeleteDialog.clickOnConfirmButton();

        expect(await assignedDeviceComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
