/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { browser, ExpectedConditions as ec /* , promise */ } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import {
  DeviceModelComponentsPage,
  /* DeviceModelDeleteDialog, */
  DeviceModelUpdatePage
} from './device-model.page-object';

const expect = chai.expect;

describe('DeviceModel e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let deviceModelComponentsPage: DeviceModelComponentsPage;
  let deviceModelUpdatePage: DeviceModelUpdatePage;
  /* let deviceModelDeleteDialog: DeviceModelDeleteDialog; */

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.loginWithOAuth('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load DeviceModels', async () => {
    await navBarPage.goToEntity('device-model');
    deviceModelComponentsPage = new DeviceModelComponentsPage();
    await browser.wait(ec.visibilityOf(deviceModelComponentsPage.title), 5000);
    expect(await deviceModelComponentsPage.getTitle()).to.eq('vcmApp.deviceModel.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(deviceModelComponentsPage.entities), ec.visibilityOf(deviceModelComponentsPage.noResult)),
      1000
    );
  });

  it('should load create DeviceModel page', async () => {
    await deviceModelComponentsPage.clickOnCreateButton();
    deviceModelUpdatePage = new DeviceModelUpdatePage();
    expect(await deviceModelUpdatePage.getPageTitle()).to.eq('vcmApp.deviceModel.home.createOrEditLabel');
    await deviceModelUpdatePage.cancel();
  });

  /* it('should create and save DeviceModels', async () => {
        const nbButtonsBeforeCreate = await deviceModelComponentsPage.countDeleteButtons();

        await deviceModelComponentsPage.clickOnCreateButton();

        await promise.all([
            deviceModelUpdatePage.setNameInput('name'),
            deviceModelUpdatePage.typeSelectLastOption(),
            // deviceModelUpdatePage.capabilitiesSelectLastOption(),
        ]);

        expect(await deviceModelUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');

        await deviceModelUpdatePage.save();
        expect(await deviceModelUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await deviceModelComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    }); */

  /* it('should delete last DeviceModel', async () => {
        const nbButtonsBeforeDelete = await deviceModelComponentsPage.countDeleteButtons();
        await deviceModelComponentsPage.clickOnLastDeleteButton();

        deviceModelDeleteDialog = new DeviceModelDeleteDialog();
        expect(await deviceModelDeleteDialog.getDialogTitle())
            .to.eq('vcmApp.deviceModel.delete.question');
        await deviceModelDeleteDialog.clickOnConfirmButton();

        expect(await deviceModelComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    }); */

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
