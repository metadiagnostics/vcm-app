/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { element, by, ElementFinder } from 'protractor';

export class DeviceComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-device div table .btn-danger'));
  title = element.all(by.css('jhi-device div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class DeviceUpdatePage {
  pageTitle = element(by.id('jhi-device-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  hardwareIdInput = element(by.id('field_hardwareId'));

  modelSelect = element(by.id('field_model'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setHardwareIdInput(hardwareId: string): Promise<void> {
    await this.hardwareIdInput.sendKeys(hardwareId);
  }

  async getHardwareIdInput(): Promise<string> {
    return await this.hardwareIdInput.getAttribute('value');
  }

  async modelSelectLastOption(): Promise<void> {
    await this.modelSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async modelSelectOption(option: string): Promise<void> {
    await this.modelSelect.sendKeys(option);
  }

  getModelSelect(): ElementFinder {
    return this.modelSelect;
  }

  async getModelSelectedOption(): Promise<string> {
    return await this.modelSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class DeviceDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-device-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-device'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
