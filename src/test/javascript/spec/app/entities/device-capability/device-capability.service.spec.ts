/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { DeviceCapabilityService } from 'app/entities/device-capability/device-capability.service';
import { IDeviceCapability, DeviceCapability } from 'app/shared/model/device-capability.model';
import { DeviceCapabilityType } from 'app/shared/model/enumerations/device-capability-type.model';

describe('Service Tests', () => {
  describe('DeviceCapability Service', () => {
    let injector: TestBed;
    let service: DeviceCapabilityService;
    let httpMock: HttpTestingController;
    let elemDefault: IDeviceCapability;
    let expectedResult: IDeviceCapability | IDeviceCapability[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(DeviceCapabilityService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new DeviceCapability(0, DeviceCapabilityType.BLOOD_GAS_ANALYZER);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should return a list of DeviceCapability', () => {
        const returnedFromService = Object.assign(
          {
            type: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
