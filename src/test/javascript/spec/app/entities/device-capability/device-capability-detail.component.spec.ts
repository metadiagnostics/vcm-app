/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { VcmTestModule } from '../../../test.module';
import { DeviceCapabilityDetailComponent } from 'app/entities/device-capability/device-capability-detail.component';
import { DeviceCapability } from 'app/shared/model/device-capability.model';

describe('Component Tests', () => {
  describe('DeviceCapability Management Detail Component', () => {
    let comp: DeviceCapabilityDetailComponent;
    let fixture: ComponentFixture<DeviceCapabilityDetailComponent>;
    const route = ({ data: of({ deviceCapability: new DeviceCapability(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [VcmTestModule],
        declarations: [DeviceCapabilityDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(DeviceCapabilityDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DeviceCapabilityDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load deviceCapability on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.deviceCapability).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
