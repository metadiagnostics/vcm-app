/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { VcmTestModule } from '../../../test.module';
import { DeviceCapabilityComponent } from 'app/entities/device-capability/device-capability.component';
import { DeviceCapabilityService } from 'app/entities/device-capability/device-capability.service';
import { DeviceCapability } from 'app/shared/model/device-capability.model';

describe('Component Tests', () => {
  describe('DeviceCapability Management Component', () => {
    let comp: DeviceCapabilityComponent;
    let fixture: ComponentFixture<DeviceCapabilityComponent>;
    let service: DeviceCapabilityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [VcmTestModule],
        declarations: [DeviceCapabilityComponent]
      })
        .overrideTemplate(DeviceCapabilityComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DeviceCapabilityComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DeviceCapabilityService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new DeviceCapability(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.deviceCapabilities && comp.deviceCapabilities[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
