/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { VcmTestModule } from '../../../test.module';
import { DeviceModelComponent } from 'app/entities/device-model/device-model.component';
import { DeviceModelService } from 'app/entities/device-model/device-model.service';
import { DeviceModel } from 'app/shared/model/device-model.model';

describe('Component Tests', () => {
  describe('DeviceModel Management Component', () => {
    let comp: DeviceModelComponent;
    let fixture: ComponentFixture<DeviceModelComponent>;
    let service: DeviceModelService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [VcmTestModule],
        declarations: [DeviceModelComponent]
      })
        .overrideTemplate(DeviceModelComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DeviceModelComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DeviceModelService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new DeviceModel(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.deviceModels && comp.deviceModels[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
