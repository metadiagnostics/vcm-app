/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { VcmTestModule } from '../../../test.module';
import { DeviceDetailComponent } from 'app/entities/device/device-detail.component';
import { Device } from 'app/shared/model/device.model';

describe('Component Tests', () => {
  describe('Device Management Detail Component', () => {
    let comp: DeviceDetailComponent;
    let fixture: ComponentFixture<DeviceDetailComponent>;
    const route = ({ data: of({ device: new Device(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [VcmTestModule],
        declarations: [DeviceDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(DeviceDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(DeviceDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load device on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.device).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
