/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { VcmTestModule } from '../../../test.module';
import { AssignedDeviceComponent } from 'app/entities/assigned-device/assigned-device.component';
import { AssignedDeviceService } from 'app/entities/assigned-device/assigned-device.service';
import { AssignedDevice } from 'app/shared/model/assigned-device.model';

describe('Component Tests', () => {
  describe('AssignedDevice Management Component', () => {
    let comp: AssignedDeviceComponent;
    let fixture: ComponentFixture<AssignedDeviceComponent>;
    let service: AssignedDeviceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [VcmTestModule],
        declarations: [AssignedDeviceComponent]
      })
        .overrideTemplate(AssignedDeviceComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AssignedDeviceComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AssignedDeviceService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new AssignedDevice(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.assignedDevices && comp.assignedDevices[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
