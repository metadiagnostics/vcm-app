/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { VcmTestModule } from '../../../test.module';
import { AssignmentComponent } from 'app/entities/assignment/assignment.component';
import { AssignmentService } from 'app/entities/assignment/assignment.service';
import { Assignment } from 'app/shared/model/assignment.model';

describe('Component Tests', () => {
  describe('Assignment Management Component', () => {
    let comp: AssignmentComponent;
    let fixture: ComponentFixture<AssignmentComponent>;
    let service: AssignmentService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [VcmTestModule],
        declarations: [AssignmentComponent]
      })
        .overrideTemplate(AssignmentComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AssignmentComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AssignmentService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Assignment(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.assignments && comp.assignments[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
