/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.rest.ext;

import static io.saperi.vcm.service.ext.ExtendedDeviceServiceImplTest.createAssignment;
import static io.saperi.vcm.service.ext.ExtendedDeviceServiceImplTest.createDevice;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import io.saperi.vcm.VcmApp;
import io.saperi.vcm.config.TestSecurityConfiguration;
import io.saperi.vcm.domain.Assignment;
import io.saperi.vcm.domain.Device;
import io.saperi.vcm.domain.DeviceModel;
import io.saperi.vcm.domain.enumeration.DeviceCapabilityType;
import io.saperi.vcm.service.AssignmentService;
import io.saperi.vcm.service.DeviceModelService;
import io.saperi.vcm.service.DeviceService;
import io.saperi.vcm.service.ext.ExtendedDeviceCapabilityService;
import io.saperi.vcm.service.ext.ExtendedDeviceServiceImplTest;
import io.saperi.vcm.web.rest.DeviceResource;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link DeviceResource} REST controller.
 */
@SpringBootTest(classes = { VcmApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class ExtendedDeviceResourceIT {

    @Autowired
    private AssignmentService assignmentService;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private ExtendedDeviceCapabilityService extendedDeviceCapabilityService;

    @Autowired
    private DeviceModelService deviceModelService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restExtendedDeviceMockMvc;

    private Device hw1;
    private Device hw2;
    private Device hw3;
    private Device hw4;
    private Device hw5;
    private Device hw6;
    private Device hw7;

    private Assignment a1;
    private Assignment a2;
    private Assignment a3;

    @BeforeEach
    @Transactional
    public void createData() {
        DeviceModel model = ExtendedDeviceServiceImplTest.createDeviceModel(deviceModelService, extendedDeviceCapabilityService, "Model A", DeviceCapabilityType.DEMOGRAPHICS);

        //Create a bunch of Devices
        this.hw1 = createDevice("hw-1", model, deviceService);
        this.hw2 = createDevice("hw-2", model, deviceService);
        this.hw3 = createDevice("hw-3", model, deviceService);
        this.hw4 = createDevice("hw-4", model, deviceService);
        this.hw5 = createDevice("hw-5", model, deviceService);
        this.hw6 = createDevice("hw-6", model, deviceService);
        this.hw7 = createDevice("hw-7", model, deviceService);

        //Create some Assignments using some of the devices
        this.a1 = createAssignment(hw1, hw1, hw1, assignmentService, deviceService, extendedDeviceCapabilityService);
        this.a2 = createAssignment(hw2, hw2, null, assignmentService, deviceService, extendedDeviceCapabilityService);
        this.a3 = createAssignment(hw5, hw7, null, assignmentService, deviceService, extendedDeviceCapabilityService);
    }

    @Test
    @Transactional
    public void getUnassignedDevicesTest() throws Exception {

        //Devices hw3, hw4 and hw6 are not assigned to any Assignment
        restExtendedDeviceMockMvc.perform(get("/api/ext/unassigned-devices"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$", hasSize(3)))
            .andExpect(jsonPath("$.[*].hardwareId", hasItems(hw3.getHardwareId(), hw4.getHardwareId(), hw6.getHardwareId())));
    }

    @Test
    @Transactional
    public void getAvailableTest() throws Exception {

        //When no assignmentId is provided, this operation returns all the unassigned
        //devices.
        restExtendedDeviceMockMvc.perform(get("/api/ext/available-devices"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$", hasSize(3)))
            .andExpect(jsonPath("$.[*].hardwareId", hasItems(hw3.getHardwareId(), hw4.getHardwareId(), hw6.getHardwareId())));

        //Assignment 1
        restExtendedDeviceMockMvc.perform(get("/api/ext/available-devices").param("assignmentId", a1.getId().toString()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$", hasSize(4)))
            .andExpect(jsonPath("$.[*].hardwareId", hasItems(hw1.getHardwareId(), hw3.getHardwareId(), hw4.getHardwareId(), hw6.getHardwareId())));

        //Assignment 2
        restExtendedDeviceMockMvc.perform(get("/api/ext/available-devices").param("assignmentId", a2.getId().toString()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$", hasSize(4)))
            .andExpect(jsonPath("$.[*].hardwareId", hasItems(hw2.getHardwareId(), hw3.getHardwareId(), hw4.getHardwareId(), hw6.getHardwareId())));

        //Assignment 3
        restExtendedDeviceMockMvc.perform(get("/api/ext/available-devices").param("assignmentId", a3.getId().toString()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$", hasSize(5)))
            .andExpect(jsonPath("$.[*].hardwareId", hasItems(hw3.getHardwareId(), hw4.getHardwareId(), hw5.getHardwareId(), hw6.getHardwareId(), hw7.getHardwareId())));
    }
}
