/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.rest;

import io.saperi.vcm.VcmApp;
import io.saperi.vcm.config.TestSecurityConfiguration;
import io.saperi.vcm.domain.Assignment;
import io.saperi.vcm.domain.AssignedDevice;
import io.saperi.vcm.repository.AssignmentRepository;
import io.saperi.vcm.service.AssignmentService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import io.saperi.vcm.domain.enumeration.VentilatorModeType;
import io.saperi.vcm.domain.enumeration.VentilatorRecommenderType;
/**
 * Integration tests for the {@link AssignmentResource} REST controller.
 */
@SpringBootTest(classes = { VcmApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class AssignmentResourceIT {

    private static final String DEFAULT_PATIENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_PATIENT_ID = "BBBBBBBBBB";
    private static final String MRN =  UUID.randomUUID().toString();
    private static final String UPDATED_MRN =  UUID.randomUUID().toString();

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    private static final VentilatorModeType DEFAULT_VENTILATOR_MODE = VentilatorModeType.MANUAL;
    private static final VentilatorModeType UPDATED_VENTILATOR_MODE = VentilatorModeType.OPEN;

    private static final VentilatorRecommenderType DEFAULT_VENTILATOR_RECOMMENDER_TYPE = VentilatorRecommenderType.AI;
    private static final VentilatorRecommenderType UPDATED_VENTILATOR_RECOMMENDER_TYPE = VentilatorRecommenderType.RULE_BASED;

    @Autowired
    private AssignmentRepository assignmentRepository;

    @Autowired
    private AssignmentService assignmentService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAssignmentMockMvc;

    private Assignment assignment;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Assignment createEntity(EntityManager em) {
        Assignment assignment = new Assignment()
            .patientId(DEFAULT_PATIENT_ID)
            .mrn(MRN)
            .active(DEFAULT_ACTIVE)
            .ventilatorMode(DEFAULT_VENTILATOR_MODE)
            .ventilatorRecommenderType(DEFAULT_VENTILATOR_RECOMMENDER_TYPE);
        // Add required entity
        AssignedDevice assignedDevice;
        if (TestUtil.findAll(em, AssignedDevice.class).isEmpty()) {
            assignedDevice = AssignedDeviceResourceIT.createEntity(em);
            em.persist(assignedDevice);
            em.flush();
        } else {
            assignedDevice = TestUtil.findAll(em, AssignedDevice.class).get(0);
        }
        assignment.getDevices().add(assignedDevice);
        return assignment;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Assignment createUpdatedEntity(EntityManager em) {
        Assignment assignment = new Assignment()
            .patientId(UPDATED_PATIENT_ID)
            .mrn(UPDATED_MRN)
            .active(UPDATED_ACTIVE)
            .ventilatorMode(UPDATED_VENTILATOR_MODE)
            .ventilatorRecommenderType(UPDATED_VENTILATOR_RECOMMENDER_TYPE);
        // Add required entity
        AssignedDevice assignedDevice;
        if (TestUtil.findAll(em, AssignedDevice.class).isEmpty()) {
            assignedDevice = AssignedDeviceResourceIT.createUpdatedEntity(em);
            em.persist(assignedDevice);
            em.flush();
        } else {
            assignedDevice = TestUtil.findAll(em, AssignedDevice.class).get(0);
        }
        assignment.getDevices().add(assignedDevice);
        return assignment;
    }

    @BeforeEach
    public void initTest() {
        assignment = createEntity(em);
    }

    @Test
    @Transactional
    public void createAssignment() throws Exception {
        int databaseSizeBeforeCreate = assignmentRepository.findAll().size();

        // Create the Assignment
        restAssignmentMockMvc.perform(post("/api/assignments").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assignment)))
            .andExpect(status().isCreated());

        // Validate the Assignment in the database
        List<Assignment> assignmentList = assignmentRepository.findAll();
        assertThat(assignmentList).hasSize(databaseSizeBeforeCreate + 1);
        Assignment testAssignment = assignmentList.get(assignmentList.size() - 1);
        assertThat(testAssignment.getPatientId()).isEqualTo(DEFAULT_PATIENT_ID);
        assertThat(testAssignment.isActive()).isEqualTo(DEFAULT_ACTIVE);
        assertThat(testAssignment.getVentilatorMode()).isEqualTo(DEFAULT_VENTILATOR_MODE);
        assertThat(testAssignment.getVentilatorRecommenderType()).isEqualTo(DEFAULT_VENTILATOR_RECOMMENDER_TYPE);
    }

    @Test
    @Transactional
    public void createAssignmentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = assignmentRepository.findAll().size();

        // Create the Assignment with an existing ID
        assignment.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAssignmentMockMvc.perform(post("/api/assignments").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assignment)))
            .andExpect(status().isBadRequest());

        // Validate the Assignment in the database
        List<Assignment> assignmentList = assignmentRepository.findAll();
        assertThat(assignmentList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkPatientIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = assignmentRepository.findAll().size();
        // set the field null
        assignment.setPatientId(null);

        // Create the Assignment, which fails.

        restAssignmentMockMvc.perform(post("/api/assignments").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assignment)))
            .andExpect(status().isBadRequest());

        List<Assignment> assignmentList = assignmentRepository.findAll();
        assertThat(assignmentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAssignments() throws Exception {
        // Initialize the database
        assignmentRepository.saveAndFlush(assignment);

        // Get all the assignmentList
        restAssignmentMockMvc.perform(get("/api/assignments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(assignment.getId().intValue())))
            .andExpect(jsonPath("$.[*].patientId").value(hasItem(DEFAULT_PATIENT_ID)))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].ventilatorMode").value(hasItem(DEFAULT_VENTILATOR_MODE.toString())))
            .andExpect(jsonPath("$.[*].ventilatorRecommenderType").value(hasItem(DEFAULT_VENTILATOR_RECOMMENDER_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getAssignment() throws Exception {
        // Initialize the database
        assignmentRepository.saveAndFlush(assignment);

        // Get the assignment
        restAssignmentMockMvc.perform(get("/api/assignments/{id}", assignment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(assignment.getId().intValue()))
            .andExpect(jsonPath("$.patientId").value(DEFAULT_PATIENT_ID))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.ventilatorMode").value(DEFAULT_VENTILATOR_MODE.toString()))
            .andExpect(jsonPath("$.ventilatorRecommenderType").value(DEFAULT_VENTILATOR_RECOMMENDER_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAssignment() throws Exception {
        // Get the assignment
        restAssignmentMockMvc.perform(get("/api/assignments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAssignment() throws Exception {
        // Initialize the database
        assignmentService.save(assignment);

        int databaseSizeBeforeUpdate = assignmentRepository.findAll().size();

        // Update the assignment
        Assignment updatedAssignment = assignmentRepository.findById(assignment.getId()).get();
        // Disconnect from session so that the updates on updatedAssignment are not directly saved in db
        em.detach(updatedAssignment);
        updatedAssignment
            .patientId(UPDATED_PATIENT_ID)
            .mrn(UPDATED_MRN)
            .active(UPDATED_ACTIVE)
            .ventilatorMode(UPDATED_VENTILATOR_MODE)
            .ventilatorRecommenderType(UPDATED_VENTILATOR_RECOMMENDER_TYPE);

        restAssignmentMockMvc.perform(put("/api/assignments").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAssignment)))
            .andExpect(status().isOk());

        // Validate the Assignment in the database
        List<Assignment> assignmentList = assignmentRepository.findAll();
        assertThat(assignmentList).hasSize(databaseSizeBeforeUpdate);
        Assignment testAssignment = assignmentList.get(assignmentList.size() - 1);
        assertThat(testAssignment.getPatientId()).isEqualTo(UPDATED_PATIENT_ID);
        assertThat(testAssignment.isActive()).isEqualTo(UPDATED_ACTIVE);
        assertThat(testAssignment.getVentilatorMode()).isEqualTo(UPDATED_VENTILATOR_MODE);
        assertThat(testAssignment.getVentilatorRecommenderType()).isEqualTo(UPDATED_VENTILATOR_RECOMMENDER_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingAssignment() throws Exception {
        int databaseSizeBeforeUpdate = assignmentRepository.findAll().size();

        // Create the Assignment

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAssignmentMockMvc.perform(put("/api/assignments").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assignment)))
            .andExpect(status().isBadRequest());

        // Validate the Assignment in the database
        List<Assignment> assignmentList = assignmentRepository.findAll();
        assertThat(assignmentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAssignment() throws Exception {
        // Initialize the database
        assignmentService.save(assignment);

        int databaseSizeBeforeDelete = assignmentRepository.findAll().size();

        // Delete the assignment
        restAssignmentMockMvc.perform(delete("/api/assignments/{id}", assignment.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Assignment> assignmentList = assignmentRepository.findAll();
        assertThat(assignmentList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
