/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.rest;

import io.saperi.vcm.VcmApp;
import io.saperi.vcm.config.TestSecurityConfiguration;
import io.saperi.vcm.domain.DeviceCapability;
import io.saperi.vcm.repository.DeviceCapabilityRepository;
import io.saperi.vcm.service.DeviceCapabilityService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import io.saperi.vcm.domain.enumeration.DeviceCapabilityType;
/**
 * Integration tests for the {@link DeviceCapabilityResource} REST controller.
 */
@SpringBootTest(classes = { VcmApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class DeviceCapabilityResourceIT {

    private static final DeviceCapabilityType DEFAULT_TYPE = DeviceCapabilityType.BLOOD_GAS_ANALYZER;
    private static final DeviceCapabilityType UPDATED_TYPE = DeviceCapabilityType.DEMOGRAPHICS;

    @Autowired
    private DeviceCapabilityRepository deviceCapabilityRepository;

    @Autowired
    private DeviceCapabilityService deviceCapabilityService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDeviceCapabilityMockMvc;

    private DeviceCapability deviceCapability;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DeviceCapability createEntity(EntityManager em) {
        DeviceCapability deviceCapability = new DeviceCapability()
            .type(DEFAULT_TYPE);
        return deviceCapability;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DeviceCapability createUpdatedEntity(EntityManager em) {
        DeviceCapability deviceCapability = new DeviceCapability()
            .type(UPDATED_TYPE);
        return deviceCapability;
    }

    @BeforeEach
    public void initTest() {
        deviceCapability = createEntity(em);
    }

    @Test
    @Transactional
    public void getAllDeviceCapabilities() throws Exception {
        // Initialize the database
        deviceCapabilityRepository.saveAndFlush(deviceCapability);

        // Get all the deviceCapabilityList
        restDeviceCapabilityMockMvc.perform(get("/api/device-capabilities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(deviceCapability.getId().intValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getDeviceCapability() throws Exception {
        // Initialize the database
        deviceCapabilityRepository.saveAndFlush(deviceCapability);

        // Get the deviceCapability
        restDeviceCapabilityMockMvc.perform(get("/api/device-capabilities/{id}", deviceCapability.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(deviceCapability.getId().intValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDeviceCapability() throws Exception {
        // Get the deviceCapability
        restDeviceCapabilityMockMvc.perform(get("/api/device-capabilities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }
}
