/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.web.rest;

import static io.saperi.vcm.web.rest.DeviceResourceIT.DEFAULT_HARDWARE_ID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import io.saperi.vcm.VcmApp;
import io.saperi.vcm.config.TestSecurityConfiguration;
import io.saperi.vcm.domain.AssignedDevice;
import io.saperi.vcm.domain.Device;
import io.saperi.vcm.domain.DeviceCapability;
import io.saperi.vcm.domain.DeviceModel;
import io.saperi.vcm.domain.enumeration.DeviceType;
import io.saperi.vcm.repository.AssignedDeviceRepository;
import io.saperi.vcm.service.AssignedDeviceService;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AssignedDeviceResource} REST controller.
 */
@SpringBootTest(classes = { VcmApp.class, TestSecurityConfiguration.class })

@AutoConfigureMockMvc
@WithMockUser
public class AssignedDeviceResourceIT {

    @Autowired
    private AssignedDeviceRepository assignedDeviceRepository;

    @Autowired
    private AssignedDeviceService assignedDeviceService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAssignedDeviceMockMvc;

    private AssignedDevice assignedDevice;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AssignedDevice createEntity(EntityManager em) {
        AssignedDevice assignedDevice = new AssignedDevice();
        // Add required entity
        DeviceCapability deviceCapability;
        if (TestUtil.findAll(em, DeviceCapability.class).isEmpty()) {
            deviceCapability = DeviceCapabilityResourceIT.createEntity(em);
            em.persist(deviceCapability);
            em.flush();
        } else {
            deviceCapability = TestUtil.findAll(em, DeviceCapability.class).get(0);
        }
        assignedDevice.setCapability(deviceCapability);
        Device device = new Device()
            .hardwareId(DEFAULT_HARDWARE_ID);
        // Add required entity
        DeviceModel deviceModel;
        if (TestUtil.findAll(em, DeviceModel.class).isEmpty()) {
            deviceModel = DeviceModelResourceIT.createEntity(em);
            em.persist(deviceModel);
            em.flush();
        } else {
            deviceModel = TestUtil.findAll(em, DeviceModel.class).stream().filter(d -> d.getType() != DeviceType.GTX).findFirst().orElseThrow(() -> new IllegalStateException("No DeviceModel to use"));
        }
        device.setModel(deviceModel);
        em.persist(device);
        assignedDevice.setDevice(device);

        em.persist(deviceCapability);
        em.flush();
        return assignedDevice;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AssignedDevice createUpdatedEntity(EntityManager em) {
        AssignedDevice assignedDevice = new AssignedDevice();
        // Add required entity
        DeviceCapability deviceCapability;
        if (TestUtil.findAll(em, DeviceCapability.class).isEmpty()) {
            deviceCapability = DeviceCapabilityResourceIT.createUpdatedEntity(em);
            em.persist(deviceCapability);
            em.flush();
        } else {
            deviceCapability = TestUtil.findAll(em, DeviceCapability.class).get(0);
        }
        assignedDevice.setCapability(deviceCapability);
        return assignedDevice;
    }

    @BeforeEach
    public void initTest() {
        assignedDevice = createEntity(em);
    }

    @Test
    @Transactional
    public void createAssignedDevice() throws Exception {
        int databaseSizeBeforeCreate = assignedDeviceRepository.findAll().size();

        // Create the AssignedDevice
        restAssignedDeviceMockMvc.perform(post("/api/assigned-devices").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assignedDevice)))
            .andExpect(status().isCreated());

        // Validate the AssignedDevice in the database
        List<AssignedDevice> assignedDeviceList = assignedDeviceRepository.findAll();
        assertThat(assignedDeviceList).hasSize(databaseSizeBeforeCreate + 1);
        AssignedDevice testAssignedDevice = assignedDeviceList.get(assignedDeviceList.size() - 1);
    }

    @Test
    @Transactional
    public void createAssignedDeviceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = assignedDeviceRepository.findAll().size();

        // Create the AssignedDevice with an existing ID
        assignedDevice.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAssignedDeviceMockMvc.perform(post("/api/assigned-devices").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assignedDevice)))
            .andExpect(status().isBadRequest());

        // Validate the AssignedDevice in the database
        List<AssignedDevice> assignedDeviceList = assignedDeviceRepository.findAll();
        assertThat(assignedDeviceList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAssignedDevices() throws Exception {
        // Initialize the database
        assignedDeviceRepository.saveAndFlush(assignedDevice);

        // Get all the assignedDeviceList
        restAssignedDeviceMockMvc.perform(get("/api/assigned-devices?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(assignedDevice.getId().intValue())));
    }

    @Test
    @Transactional
    public void getAssignedDevice() throws Exception {
        // Initialize the database
        assignedDeviceRepository.saveAndFlush(assignedDevice);

        // Get the assignedDevice
        restAssignedDeviceMockMvc.perform(get("/api/assigned-devices/{id}", assignedDevice.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(assignedDevice.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingAssignedDevice() throws Exception {
        // Get the assignedDevice
        restAssignedDeviceMockMvc.perform(get("/api/assigned-devices/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAssignedDevice() throws Exception {
        // Initialize the database
        assignedDeviceService.save(assignedDevice);

        int databaseSizeBeforeUpdate = assignedDeviceRepository.findAll().size();

        // Update the assignedDevice
        AssignedDevice updatedAssignedDevice = assignedDeviceRepository.findById(assignedDevice.getId()).get();
        // Disconnect from session so that the updates on updatedAssignedDevice are not directly saved in db
        em.detach(updatedAssignedDevice);

        restAssignedDeviceMockMvc.perform(put("/api/assigned-devices").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAssignedDevice)))
            .andExpect(status().isOk());

        // Validate the AssignedDevice in the database
        List<AssignedDevice> assignedDeviceList = assignedDeviceRepository.findAll();
        assertThat(assignedDeviceList).hasSize(databaseSizeBeforeUpdate);
        AssignedDevice testAssignedDevice = assignedDeviceList.get(assignedDeviceList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingAssignedDevice() throws Exception {
        int databaseSizeBeforeUpdate = assignedDeviceRepository.findAll().size();

        // Create the AssignedDevice

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAssignedDeviceMockMvc.perform(put("/api/assigned-devices").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(assignedDevice)))
            .andExpect(status().isBadRequest());

        // Validate the AssignedDevice in the database
        List<AssignedDevice> assignedDeviceList = assignedDeviceRepository.findAll();
        assertThat(assignedDeviceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAssignedDevice() throws Exception {
        // Initialize the database
        assignedDeviceService.save(assignedDevice);

        int databaseSizeBeforeDelete = assignedDeviceRepository.findAll().size();

        // Delete the assignedDevice
        restAssignedDeviceMockMvc.perform(delete("/api/assigned-devices/{id}", assignedDevice.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AssignedDevice> assignedDeviceList = assignedDeviceRepository.findAll();
        assertThat(assignedDeviceList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
