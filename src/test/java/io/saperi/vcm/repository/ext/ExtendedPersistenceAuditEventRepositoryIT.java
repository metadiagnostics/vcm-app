/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.repository.ext;

import static java.time.temporal.ChronoUnit.MICROS;
import static java.util.stream.Collectors.toSet;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;

import io.saperi.vcm.VcmApp;
import io.saperi.vcm.config.TestSecurityConfiguration;
import io.saperi.vcm.config.audit.AuditEventConverter;
import io.saperi.vcm.domain.PersistentAuditEvent;
import io.saperi.vcm.repository.CustomAuditEventRepository;
import io.saperi.vcm.repository.PersistenceAuditEventRepository;
import io.saperi.vcm.service.ext.impl.VentilatorConfigurationChangeRequestToAuditEventConverter;
import io.saperi.vcm.service.ext.recommender.VentilatorRecommendationToAuditEventConverter;
import java.time.Instant;
import java.time.temporal.TemporalUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author esteban
 */
@SpringBootTest(classes = {VcmApp.class, TestSecurityConfiguration.class})
@Transactional
public class ExtendedPersistenceAuditEventRepositoryIT {

    @Autowired
    private ExtendedPersistenceAuditEventRepository repository;

    @Autowired
    private PersistenceAuditEventRepository persistenceAuditEventRepository;

    @Autowired
    private AuditEventConverter auditEventConverter;

    private CustomAuditEventRepository customAuditEventRepository;

    private Instant oneHourAgo;
    private Instant twoHoursAgo;

    @BeforeEach
    public void setup() {
        customAuditEventRepository = new CustomAuditEventRepository(persistenceAuditEventRepository, auditEventConverter);
        persistenceAuditEventRepository.deleteAll();
        oneHourAgo = Instant.now().minusSeconds(3600);
        twoHoursAgo = oneHourAgo.minusSeconds(3600);

        Map<String, Object> data = new HashMap<>();
        data.put(VentilatorRecommendationToAuditEventConverter.DATA_ASSIGNMENT_ID_KEY, "1");
        data.put("PEEP", "1");
        AuditEvent testAssignment1VentilatorEvent1 = new AuditEvent(
            oneHourAgo,
            "test-user",
            VentilatorRecommendationToAuditEventConverter.EVENT_TYPE,
            data
        );
        customAuditEventRepository.add(testAssignment1VentilatorEvent1);

        data = new HashMap<>();
        data.put(VentilatorRecommendationToAuditEventConverter.DATA_ASSIGNMENT_ID_KEY, "1");
        data.put("PEEP", "2");
        AuditEvent testAssignment1VentilatorEvent2 = new AuditEvent(
            twoHoursAgo,
            "test-user",
            VentilatorRecommendationToAuditEventConverter.EVENT_TYPE,
            data
        );
        customAuditEventRepository.add(testAssignment1VentilatorEvent2);

        data = new HashMap<>();
        data.put(VentilatorRecommendationToAuditEventConverter.DATA_ASSIGNMENT_ID_KEY, "1");
        data.put("APPLIED_PEEP", "3");
        AuditEvent testAssignment1VentilatorConfigurationEvent2 = new AuditEvent(
            twoHoursAgo,
            "test-user",
            VentilatorConfigurationChangeRequestToAuditEventConverter.EVENT_TYPE,
            data
        );
        customAuditEventRepository.add(testAssignment1VentilatorConfigurationEvent2);

        data = new HashMap<>();
        data.put(VentilatorRecommendationToAuditEventConverter.DATA_ASSIGNMENT_ID_KEY, "2");
        data.put("PEEP", "4");
        AuditEvent testAssignment2VentilatorEvent1 = new AuditEvent(
            oneHourAgo,
            "test-user",
            VentilatorRecommendationToAuditEventConverter.EVENT_TYPE,
            data
        );
        customAuditEventRepository.add(testAssignment2VentilatorEvent1);

        data = new HashMap<>();
        data.put("Some-Key", "Some-Value");
        data.put("PEEP", "5");
        AuditEvent testOtherEvent = new AuditEvent(
            oneHourAgo,
            "test-user",
            "test-type",
            data
        );
        customAuditEventRepository.add(testOtherEvent);
    }

    @Test
    public void testSingleTypeQuery() {
        List<PersistentAuditEvent> events = repository.findByDataKeyAndDataValue(
            new String[]{
                VentilatorRecommendationToAuditEventConverter.EVENT_TYPE},
            VentilatorRecommendationToAuditEventConverter.DATA_ASSIGNMENT_ID_KEY,
            "1"
        );

        assertThat(events.size(), is(2));
        Set<String> retrievedIds = events.stream().map(e -> e.getData().get(VentilatorRecommendationToAuditEventConverter.DATA_ASSIGNMENT_ID_KEY)).collect(toSet());
        assertThat(retrievedIds.size(), is(1));
        assertThat(retrievedIds.iterator().next(), is("1"));

        Set<String> retrievedPeeps = events.stream().map(e -> e.getData().get("PEEP")).collect(toSet());
        assertThat(retrievedPeeps, containsInAnyOrder("1", "2"));

    }

    @Test
    public void testMultiTypeQuery() {
        List<PersistentAuditEvent> events = repository.findByDataKeyAndDataValue(
            new String[]{
                VentilatorRecommendationToAuditEventConverter.EVENT_TYPE,
                VentilatorConfigurationChangeRequestToAuditEventConverter.EVENT_TYPE
            },
            VentilatorRecommendationToAuditEventConverter.DATA_ASSIGNMENT_ID_KEY,
            "1"
        );

        assertThat(events.size(), is(3));
        Set<String> retrievedIds = events.stream().map(e -> e.getData().get(VentilatorRecommendationToAuditEventConverter.DATA_ASSIGNMENT_ID_KEY)).collect(toSet());
        assertThat(retrievedIds.size(), is(1));
        assertThat(retrievedIds.iterator().next(), is("1"));

        Map<String, List<PersistentAuditEvent>> byType = events.stream().collect(Collectors.groupingBy(PersistentAuditEvent::getAuditEventType));
        assertThat(byType.get(VentilatorRecommendationToAuditEventConverter.EVENT_TYPE).size(), is(2));
        assertThat(byType.get(VentilatorConfigurationChangeRequestToAuditEventConverter.EVENT_TYPE).size(), is(1));

    }

    @Test
    public void testDateRangeQuery() {
        Instant now = Instant.now();
        List<PersistentAuditEvent> events = repository.findByDataKeyAndDataValueWithDateRange(
            new String[]{
                VentilatorRecommendationToAuditEventConverter.EVENT_TYPE,
                VentilatorConfigurationChangeRequestToAuditEventConverter.EVENT_TYPE
            },
            oneHourAgo.truncatedTo(MICROS), // Having 6 milliseconds in the precision to solve this H2 rounding issue: https://github.com/h2database/h2database/issues/3111
            now,
            VentilatorRecommendationToAuditEventConverter.DATA_ASSIGNMENT_ID_KEY,
            "1"
        );
        assertThat(events.size(), is(1));
        assertThat(events.get(0).getData().get("PEEP"), is("1"));

    }
}
