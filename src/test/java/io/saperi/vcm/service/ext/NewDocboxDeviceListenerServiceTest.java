/**
 * Copyright 2022 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.saperi.vcm.service.ext;

import io.saperi.vcm.domain.Device;
import io.saperi.vcm.domain.DeviceModel;
import io.saperi.vcm.event.NewHardwareDetectedEvent;
import io.saperi.vcm.repository.DeviceModelRepository;
import io.saperi.vcm.repository.DeviceRepository;
import io.saperi.vcm.service.DeviceConfiguration;
import io.saperi.virtualhealth.device.aggregators.common.model.DeviceInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
/**
 * @author sgroh@saperi.io
 */
@ExtendWith(MockitoExtension.class)
class NewDocboxDeviceListenerServiceTest {

    private NewDeviceListenerService service;

    @Mock
    DeviceRepository deviceRepository;

    @Mock
    DeviceModelRepository deviceModelRepository;

    @Mock
    DeviceConfiguration deviceConfiguration;

    @BeforeEach
    void setUp() {
        service = new NewDeviceListenerService(deviceRepository, deviceModelRepository, deviceConfiguration);
    }

    @Test
    void invalidDeviceEvent() {
        String hwId = "some_hw_id";
        DeviceInfo deviceInfo = new DeviceInfo();
        NewHardwareDetectedEvent event = new NewHardwareDetectedEvent(hwId, this, deviceInfo);
        service.onApplicationEvent(event);
        verifyNoInteractions(deviceRepository, deviceModelRepository, deviceConfiguration);
    }

    @Test
    void validNewDeviceEventDBInconsistent() {
        String hwId = "some_hw_id";
        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setManufacturer("Prototype");
        deviceInfo.setSystem_model_number("Vent");
        NewHardwareDetectedEvent event = new NewHardwareDetectedEvent(hwId, this, deviceInfo);
        when(deviceRepository.findByHardwareId(anyString())).thenReturn(null);
        when(deviceModelRepository.findFirstByName(anyString())).thenReturn(null);
        service.onApplicationEvent(event);
        verifyNoInteractions(deviceConfiguration);
    }

    @Test
    void validNewDeviceEventDBConsistent() {
        String hwId = "some_hw_id";
        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setManufacturer("Prototype");
        deviceInfo.setSystem_model_number("Vent");
        NewHardwareDetectedEvent event = new NewHardwareDetectedEvent(hwId, this, deviceInfo);
        when(deviceRepository.findByHardwareId(anyString())).thenReturn(null);
        DeviceModel dm = new DeviceModel();
        dm.setName("Some Model Name");
        when(deviceModelRepository.findFirstByName(anyString())).thenReturn(dm);
        service.onApplicationEvent(event);
        verify(deviceRepository, times(1)).save(any());
        verifyNoInteractions(deviceConfiguration);
    }

    @Test
    void updateNewDeviceEventDBConsistent() {
        String hwId = "some_hw_id";
        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setManufacturer("Prototype");
        deviceInfo.setSystem_model_number("Vent");
        NewHardwareDetectedEvent event = new NewHardwareDetectedEvent(hwId, this, deviceInfo);
        Device device = new Device();
        when(deviceRepository.findByHardwareId(anyString())).thenReturn(device);
        DeviceModel dm = new DeviceModel();
        dm.setName("Some Model Name");
        when(deviceModelRepository.findFirstByName(anyString())).thenReturn(dm);
        when(deviceConfiguration.getHw_device()).thenReturn(null);
        service.onApplicationEvent(event);
        verify(deviceRepository, times(1)).save(device);

    }
}
