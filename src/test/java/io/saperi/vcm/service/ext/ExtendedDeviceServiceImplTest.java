/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import io.saperi.vcm.VcmApp;
import io.saperi.vcm.config.TestSecurityConfiguration;
import io.saperi.vcm.domain.AssignedDevice;
import io.saperi.vcm.domain.Assignment;
import io.saperi.vcm.domain.Device;
import io.saperi.vcm.domain.DeviceCapability;
import io.saperi.vcm.domain.DeviceModel;
import io.saperi.vcm.domain.enumeration.DeviceCapabilityType;
import io.saperi.vcm.domain.enumeration.DeviceType;
import io.saperi.vcm.service.AssignmentService;
import io.saperi.vcm.service.DeviceModelService;
import io.saperi.vcm.service.DeviceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

/**
 *
 * @author esteban
 */
@SpringBootTest(classes = {VcmApp.class, TestSecurityConfiguration.class})
@Transactional
public class ExtendedDeviceServiceImplTest {

    @Autowired
    private AssignmentService assignmentService;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private ExtendedDeviceCapabilityService extendedDeviceCapabilityService;

    @Autowired
    private DeviceModelService deviceModelService;

    @Autowired
    private ExtendedDeviceService extendsDeviceService;

    @Autowired
    private ExtendedDeviceCapabilityService extendsDeviceCapabilityService;

    public static DeviceModel createDeviceModel(DeviceModelService deviceModelService, ExtendedDeviceCapabilityService extendedDeviceCapabilityService,
        String name, DeviceCapabilityType... capabilities) {
        DeviceModel deviceModel = new DeviceModel();
        deviceModel.setName(name);
        deviceModel.setType(DeviceType.MOCK);

        if (capabilities != null) {
            for (int i = 0; i < capabilities.length; i++) {
                DeviceCapabilityType capability = capabilities[i];
                deviceModel.addCapabilities(extendedDeviceCapabilityService.findByTypeOrDie(capability));
            }
        }

        return deviceModelService.save(deviceModel);
    }

    public static Device createDevice(String hardwareId, DeviceModel model, DeviceService deviceService) {
        Device device = new Device();
        device.setHardwareId(hardwareId);
        device.setModel(model);
        return deviceService.save(device);
    }

    public static Assignment instantiateAssignment() {
        Assignment assignment = new Assignment();
        assignment.setPatientId("patient-1");
        assignment.setMrn(UUID.randomUUID().toString());
        return assignment;
    }

    public static void assignDevice(
        Assignment assignment,
        Device device,
        DeviceCapabilityType capability,
        AssignmentService assignmentService,
        DeviceService deviceService,
        ExtendedDeviceCapabilityService extendsDeviceCapabilityService) {

        if (assignment == null || device == null) {
            return;
        }

        AssignedDevice ad = createAssignedDevice(assignment, device, extendsDeviceCapabilityService.findByTypeOrDie(capability));

        assignment.addDevices(ad);
        device.addAssignments(ad);
    }

    public static Assignment createAssignment(Device demographics, Device vitals, Device ventilator,
        AssignmentService assignmentService,
        DeviceService deviceService,
        ExtendedDeviceCapabilityService extendsDeviceCapabilityService) {

        Assignment assignment = instantiateAssignment();
        assignment = assignmentService.save(assignment);

        assignDevice(assignment, demographics, DeviceCapabilityType.DEMOGRAPHICS, assignmentService, deviceService, extendsDeviceCapabilityService);
        assignDevice(assignment, vitals, DeviceCapabilityType.VITALS, assignmentService, deviceService, extendsDeviceCapabilityService);
        assignDevice(assignment, ventilator, DeviceCapabilityType.VENTILATOR, assignmentService, deviceService, extendsDeviceCapabilityService);

        return assignment;
    }

    public static AssignedDevice createAssignedDevice(Assignment assignment, Device device, DeviceCapability capability) {
        AssignedDevice ad = new AssignedDevice();
        ad.setAssignment(assignment);
        ad.setDevice(device);

        ad.setCapability(capability);

        return ad;
    }

    private Device hw1;
    private Device hw2;
    private Device hw3;
    private Device hw4;
    private Device hw5;
    private Device hw6;
    private Device hw7;
    private Device hw8;

    private Assignment a1;
    private Assignment a2;
    private Assignment a3;
    private Assignment a4;

    @BeforeEach
    @Transactional
    public void createData() {
        DeviceModel modelA = ExtendedDeviceServiceImplTest.createDeviceModel(
            deviceModelService,
            extendedDeviceCapabilityService,
            "Model A",
            DeviceCapabilityType.DEMOGRAPHICS);

        DeviceModel modelB = ExtendedDeviceServiceImplTest.createDeviceModel(
            deviceModelService,
            extendedDeviceCapabilityService,
            "Model B",
            DeviceCapabilityType.DEMOGRAPHICS, DeviceCapabilityType.VITALS);

        DeviceModel modelC = ExtendedDeviceServiceImplTest.createDeviceModel(
            deviceModelService,
            extendedDeviceCapabilityService,
            "Model C",
            DeviceCapabilityType.VENTILATOR, DeviceCapabilityType.VENTILATOR_CONTROL);

        //Create a bunch of Devices
        this.hw1 = createDevice("hw-1", modelA, deviceService);
        this.hw2 = createDevice("hw-2", modelA, deviceService);
        this.hw3 = createDevice("hw-3", modelB, deviceService);
        this.hw4 = createDevice("hw-4", modelB, deviceService);
        this.hw5 = createDevice("hw-5", modelB, deviceService);
        this.hw6 = createDevice("hw-6", modelC, deviceService);
        this.hw7 = createDevice("hw-7", modelC, deviceService);
        this.hw8 = createDevice("hw-8", modelC, deviceService);

        //Create some Assignments using some of the devices
        this.a1 = createAssignment(hw1, hw1, hw1, assignmentService, deviceService, extendsDeviceCapabilityService);
        this.a2 = createAssignment(hw2, hw2, null, assignmentService, deviceService, extendsDeviceCapabilityService);
        this.a3 = createAssignment(hw5, hw7, null, assignmentService, deviceService, extendsDeviceCapabilityService);
        this.a4 = createAssignment(hw8, null, hw4, assignmentService, deviceService, extendsDeviceCapabilityService);
    }

    @Test
    @Transactional
    public void isDeviceAssignedTest() {
        //Devices hw3 and hw6 are not assigned to any Assignment
        assertTrue(extendsDeviceService.isDeviceAssigned(hw1));
        assertTrue(extendsDeviceService.isDeviceAssigned(hw2));
        assertFalse(extendsDeviceService.isDeviceAssigned(hw3));
        assertTrue(extendsDeviceService.isDeviceAssigned(hw4));
        assertTrue(extendsDeviceService.isDeviceAssigned(hw5));
        assertFalse(extendsDeviceService.isDeviceAssigned(hw6));
        assertTrue(extendsDeviceService.isDeviceAssigned(hw7));
    }

    @Test
    @Transactional
    public void isDeviceAssignedToAnotherAssignmentTest() {
        //hw1 is assigned to a1
        assertFalse(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw1, a1));
        assertTrue(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw1, a2));
        assertTrue(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw1, a3));
        assertTrue(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw1, a4));

        //hw2 is assigned to a2
        assertTrue(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw2, a1));
        assertFalse(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw2, a2));
        assertTrue(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw2, a3));
        assertTrue(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw2, a4));

        //hw3 is not assigned
        assertFalse(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw3, a1));
        assertFalse(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw3, a2));
        assertFalse(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw3, a3));
        assertFalse(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw3, a4));

        //hw4 is assigned to a4
        assertTrue(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw4, a1));
        assertTrue(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw4, a2));
        assertTrue(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw4, a3));
        assertFalse(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw4, a4));

        //hw5 is assigned to a3
        assertTrue(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw5, a1));
        assertTrue(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw5, a2));
        assertFalse(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw5, a3));
        assertTrue(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw5, a4));

        //hw6 is not assigned
        assertFalse(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw6, a1));
        assertFalse(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw6, a2));
        assertFalse(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw6, a3));
        assertFalse(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw6, a4));

        //hw7 is assigned to a3
        assertTrue(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw7, a1));
        assertTrue(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw7, a2));
        assertFalse(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw7, a3));
        assertTrue(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw7, a4));

        //hw8 is assigned to a4
        assertTrue(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw8, a1));
        assertTrue(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw8, a2));
        assertTrue(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw8, a3));
        assertFalse(extendsDeviceService.isDeviceAssignedToAnotherAssignment(hw8, a4));
    }

    /**
     * Only unassigned devices and those devices that are already assigned to an
     * assignment are returned.
     */
    @Test
    @Transactional
    public void findAllAvailableTest() {
        assertThat(extendsDeviceService.findAllAvailable(a1.getId()))
            .hasSize(3).contains(hw1, hw3, hw6);

        assertThat(extendsDeviceService.findAllAvailable(a2.getId()))
            .hasSize(3).contains(hw2, hw3, hw6);

        assertThat(extendsDeviceService.findAllAvailable(a3.getId()))
            .hasSize(4).contains(hw3, hw5, hw6, hw7);

        assertThat(extendsDeviceService.findAllAvailable(a4.getId()))
            .hasSize(4).contains(hw3, hw4, hw6, hw8);
    }

    /**
     * Only unassigned devices and those devices that are already assigned to an
     * assignment are returned. When a specific {@link DeviceCapabilityType} is
     * specified, the results are filtered by it.
     */
    @Test
    @Transactional
    public void findAllAvailableWithCapabilityFilterTest() {

        //With not filter, we have 3 possible Devices
        assertThat(extendsDeviceService.findAllAvailable(a1.getId()))
            .hasSize(3).contains(hw1, hw3, hw6);

        //Only 1 and 3 have DEMOGRAPHICS capabilities
        assertThat(extendsDeviceService.findAllAvailable(a1.getId(), DeviceCapabilityType.DEMOGRAPHICS))
            .hasSize(2).contains(hw1, hw3);

        //Only 6 has VENTILATOR_CONTROL capabilities
        assertThat(extendsDeviceService.findAllAvailable(a1.getId(), DeviceCapabilityType.VENTILATOR_CONTROL))
            .hasSize(1).contains(hw6);

        //None has BLOOD_GAS_ANALYZER capabilities
        assertThat(extendsDeviceService.findAllAvailable(a1.getId(), DeviceCapabilityType.BLOOD_GAS_ANALYZER))
            .isEmpty();
    }
}
