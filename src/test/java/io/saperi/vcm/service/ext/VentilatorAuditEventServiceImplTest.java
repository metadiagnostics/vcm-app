/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.service.ext;

import io.saperi.vcm.VcmApp;
import io.saperi.vcm.config.TestSecurityConfiguration;
import io.saperi.vcm.config.audit.AuditEventConverter;
import io.saperi.vcm.repository.CustomAuditEventRepository;
import io.saperi.vcm.repository.PersistenceAuditEventRepository;
import io.saperi.vcm.service.ext.impl.VentilatorAuditEventServiceImpl;
import io.saperi.vcm.service.ext.impl.VentilatorConfigurationChangeRequestToAuditEventConverter;
import io.saperi.vcm.service.ext.recommender.VentilatorRecommendationToAuditEventConverter;
import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author esteban
 */
@SpringBootTest(classes = {VcmApp.class, TestSecurityConfiguration.class})
@Transactional
public class VentilatorAuditEventServiceImplTest {

    @Autowired
    private PersistenceAuditEventRepository persistenceAuditEventRepository;

    @Autowired
    private AuditEventConverter auditEventConverter;

    @Autowired
    private VentilatorAuditEventServiceImpl service;

    private CustomAuditEventRepository customAuditEventRepository;

    @BeforeEach
    public void setup() {
        customAuditEventRepository = new CustomAuditEventRepository(persistenceAuditEventRepository, auditEventConverter);
        persistenceAuditEventRepository.deleteAll();
        Instant oneHourAgo = Instant.now().minusSeconds(3600);

        Map<String, Object> data = new HashMap<>();
        data.put(VentilatorRecommendationToAuditEventConverter.DATA_ASSIGNMENT_ID_KEY, "1");
        data.put("PEEP", "1");
        AuditEvent testAssignment1VentilatorEvent1 = new AuditEvent(
            oneHourAgo,
            "test-user",
            VentilatorRecommendationToAuditEventConverter.EVENT_TYPE,
            data
        );
        customAuditEventRepository.add(testAssignment1VentilatorEvent1);

        data = new HashMap<>();
        data.put(VentilatorRecommendationToAuditEventConverter.DATA_ASSIGNMENT_ID_KEY, "1");
        data.put("PEEP", "2");
        AuditEvent testAssignment1VentilatorEvent2 = new AuditEvent(
            oneHourAgo.minusSeconds(10000),
            "test-user",
            VentilatorRecommendationToAuditEventConverter.EVENT_TYPE,
            data
        );
        customAuditEventRepository.add(testAssignment1VentilatorEvent2);

        data = new HashMap<>();
        data.put(VentilatorRecommendationToAuditEventConverter.DATA_ASSIGNMENT_ID_KEY, "1");
        data.put("APPLIED_PEEP", "3");
        AuditEvent testAssignment1VentilatorConfigurationEvent2 = new AuditEvent(
            oneHourAgo.minusSeconds(10000),
            "test-user",
            VentilatorConfigurationChangeRequestToAuditEventConverter.EVENT_TYPE,
            data
        );
        customAuditEventRepository.add(testAssignment1VentilatorConfigurationEvent2);

        data = new HashMap<>();
        data.put(VentilatorRecommendationToAuditEventConverter.DATA_ASSIGNMENT_ID_KEY, "2");
        data.put("PEEP", "3");
        AuditEvent testAssignment2VentilatorEvent1 = new AuditEvent(
            oneHourAgo,
            "test-user",
            VentilatorRecommendationToAuditEventConverter.EVENT_TYPE,
            data
        );
        customAuditEventRepository.add(testAssignment2VentilatorEvent1);
    }

    @Test
    public void doCSVTest() throws IOException {
        String csv = service.getVentilatorAuditEventsAsCSV(1L);
        System.out.println("csv = \n" + csv + "\n\n\n");
    }
}
