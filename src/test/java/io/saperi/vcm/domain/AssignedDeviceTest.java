/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import io.saperi.vcm.web.rest.TestUtil;

public class AssignedDeviceTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AssignedDevice.class);
        AssignedDevice assignedDevice1 = new AssignedDevice();
        assignedDevice1.setId(1L);
        AssignedDevice assignedDevice2 = new AssignedDevice();
        assignedDevice2.setId(assignedDevice1.getId());
        assertThat(assignedDevice1).isEqualTo(assignedDevice2);
        assignedDevice2.setId(2L);
        assertThat(assignedDevice1).isNotEqualTo(assignedDevice2);
        assignedDevice1.setId(null);
        assertThat(assignedDevice1).isNotEqualTo(assignedDevice2);
    }
}
