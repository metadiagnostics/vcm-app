/**
 * Copyright 2021 Saperi Systems.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.saperi.vcm.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import io.saperi.vcm.web.rest.TestUtil;

public class DeviceCapabilityTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DeviceCapability.class);
        DeviceCapability deviceCapability1 = new DeviceCapability();
        deviceCapability1.setId(1L);
        DeviceCapability deviceCapability2 = new DeviceCapability();
        deviceCapability2.setId(deviceCapability1.getId());
        assertThat(deviceCapability1).isEqualTo(deviceCapability2);
        deviceCapability2.setId(2L);
        assertThat(deviceCapability1).isNotEqualTo(deviceCapability2);
        deviceCapability1.setId(null);
        assertThat(deviceCapability1).isNotEqualTo(deviceCapability2);
    }
}
