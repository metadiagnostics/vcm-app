# Design Concepts

## Model

![Model](src/main/jdl/entities.png)

The main class of this application is the `Assignment` class. This class groups together information about a patient and devices providing real-time data for him/her. A device has capabilities that indicate the data that can be retrieved or sent to them. The supported capabilities are:

* Demographics: modeled as a device, but in most cases this is just a service. This capability indicates that a device can be queried for demographic information for a patient.
* Vitals: this capability indicates that a device can be queried for information about the vitals of a patient.
* Blood Gas Analyzer: this capability indicates that a device can be queried for information about the blood gases of a patient.
* Ventilator: this capability indicates that the device is a ventilator and that its parameters can be queried.
* Ventilator Control: this capability indicates that the device is a ventilator and that its configuration can be programmatically modified.

When an `Assignment` is defined in the application, the Device or Devices that will provide the different capabilities must be specified. At the moment, there is no restriction about the number of devices and capabilities that can be configured for an `Assignment`, but some restrictions will be added in the future.

For the time being, an `Assignment` is not associated to a `User`: any `User` can see and access any `Assignment`. In the future, we may want to add this capability so an `Assignment` really represents a relationship between a patient, a user and one or more devices.

A `Device` has a unique id (`hardwareId`) that unequivocally identifies it. This id is used when broadcasting data to let subscribers of this data to know the source of it. A `Device` also has a model (`DeviceModel`) containing information about that specific device. There are two important characteristics of a `DeviceModel`: its type (`DeviceType`) and its capabilities (`DeviceCapability`). 

The `DeviceType` of a `DeviceModel` tells the application the concrete way to interact with the device. There is a limited amount of supported types at the moment: 

 * *GTX*: used to communicate with [ACCS GTX](https://athenagtx.com/rd-2/accs/) devices.
 * *PULSE*: used to communicate with a [PULSE service](https://bitbucket.org/saperisys/pulse-sim/src/master/pulse-sim-service/) instance.
 * *MOCK*: used mostly for development.

The capabilities of a `DeviceModel` tells the application what the supported capabilities a specific model has. These capabilities will be used by the application to properly configure an `Assignment` (i.e. do not use a `Device` that doesn't provide vitals data as the vitals data source of an `Assignment`) and to understand how to interact with a device.

For simplicity sake--and given that that this is a regular constraint in real-life environments), a `Device` can only be used by a single `Assignment` at a time.

## Devices Data Flow

For all the registered `Devices`, the application -on startup- will configure the necessary listeners to start gathering data from them. The type of listener will depend on the type of `Device` (`DeviceType`). The data coming from the devices will be converted into a canonical model being the `HardwareEvent` class the superclass. This canonical model will then be sent through Websockets to any interested client. The canonical model is also used by the backend to keep an in-memory cache of the latest data snapshot of each `Device`. This data can then be used for tasks such as automatic recommendation calculation or automatic ventilator control.



## Ventilator Recommendation Mode

Each `Assignment` has an associated Ventilator Recommender (in the future, it will be possible to have multiple recommenders for different devices). Each `Assignment` also has a ventilator mode for the recommender and Ventilator it contains (assuming that the `Assignment` has a Ventilator in the first place). The supported modes are:

* MANUAL: in this case, the application will not do anything special. The user will be able to manually invoke the ventilator recommender using the UI. 
* OPEN LOOP: in this case, the application will periodically invoke the recommender using the latest data snapshot of the `Devices` associated to the `Assignment`. The recommendations will then be published into a Websocket for potential clients to consume them.
* CLOSED LOOP: in this case, the recommender will also be periodically invoked by the application without the intervention of any user. The recommendations will then be automatically applied to the ventilator. Both, the recommendation and the fact that the changes were applied are published into a Websocket for potential clients to consume.