# vcm

This application was generated using JHipster 6.8.0, you can find documentation and help at [https://www.jhipster.tech/documentation-archive/v6.8.0](https://www.jhipster.tech/documentation-archive/v6.8.0).

## Development

Before you can build this project, you must install and configure the following dependencies on your machine:

1. [Node.js][]: We use Node to run a development web server and build the project.
   Depending on your system, you can install Node either from source or as a pre-packaged bundle.

After installing Node, you should be able to run the following command to install development tools.
You will only need to run this command when dependencies change in [package.json](package.json).

    npm install

We use npm scripts and [Webpack][] as our build system.

Run the following commands in two separate terminals to create a blissful development experience where your browser
auto-refreshes when files change on your hard drive.

    ./mvnw
    npm start

Npm is also used to manage CSS and JavaScript dependencies used in this application. You can upgrade dependencies by
specifying a newer version in [package.json](package.json). You can also run `npm update` and `npm install` to manage dependencies.
Add the `help` flag on any command to see how you can use it. For example, `npm help update`.

The `npm run` command will list all of the scripts available to run for this project.

The project relies on some maven dependencies that are not available in any repository at the moment.
The following dependencies have to be manually built:

- https://bitbucket.org/saperisys/gtx-java/src/develop/

### Third party dependencies (Database / Authentication-authorization / Queue

VCM uses a Relational Database (MySQL), security through OAuth2 and AMQ to communicate with third-party 
dependencies such as Pulse simulator.
There is a happy path to have all this tools up and running in your local just running a Docker compose command.

```
docker-compose -f src/main/docker/services.yml up
```

Please ensure that you do not have another instance of MySQL on port 3306, if this is your scenario please shutdown that instance and then 
re-run the docker-compose command.

To run AWS locally using Keycloak and a local MySQL instance just run next command changing the <username> and <password>

```
./mvnw -P-webpack -Dspring-boot.run.jvmArguments="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5004 -Dspring.security.oauth2.client.registration.oidc.client-id=web_app -Dspring.security.oauth2.client.registration.oidc.client-secret=web_app -Dspring.profiles.active=prod,swagger -Dvcm.ventilator-recommender.rate='0 0 1 1 * ?'"
```

### Liquibase
The project uses liquibase to create the initial schema and changesets for incremental changes in the DB.
Using this tool we can version and keep isolated changes on the Relational model allowing us to undo a changeset or re-run it if necessary.
JHipster on VCM integrates liquibase as part of the runtime process for VCM, so when it starts liquibase plugin will check which changesets were already run or not in order to have the 
application running with the latest changes applied. All the changes are applied directly on the MySQL instance but there is a way to check if the liquibase process run smoothly and without failures in a non-invasive way, using an H2 (in memory instance) running
next command:

```
mvn liquibase:update
```

To set up the project differently or tune it please read next sections.

## OAuth 2.0 / OpenID Connect

Congratulations! You've selected an excellent way to secure your JHipster application. If you're not sure what OAuth and OpenID Connect (OIDC) are, please see [What the Heck is OAuth?](https://developer.okta.com/blog/2017/06/21/what-the-heck-is-oauth)

### Using Keycloak

To log in to your app, you'll need to have [Keycloak](https://keycloak.org) up and running. The JHipster Team has created a Docker container for you that has the default users and roles. Start Keycloak using the following command.

```
docker-compose -f src/main/docker/keycloak.yml up
```

The security settings in `src/main/resources/config/application.yml` are configured for this image.

```yaml
spring:
  ...
  security:
    oauth2:
      client:
        provider:
          oidc:
            issuer-uri: http://localhost:9080/auth/realms/jhipster
        registration:
          oidc:
            client-id: web_app
            client-secret: web_app
```

When using the default developer keyclock image the access information is **Username:** admin **Password:** admin.

### Using Okta

If you'd like to use Okta instead of Keycloak, you'll need to change a few things. First, you'll need to create a free developer account at <https://developer.okta.com/signup/>. After doing so, you'll get your own Okta domain, that has a name like `https://dev-123456.okta.com`.

Modify `src/main/resources/config/application.yml` to use your Okta settings.

```yaml
spring:
  ...
  security:
    oauth2:
      client:
        provider:
          oidc:
            issuer-uri: https://{yourOktaDomain}/oauth2/default
        registration:
          oidc:
            client-id: {clientId}
            client-secret: {clientSecret}
security:
```

Create an OIDC App in Okta to get a `{clientId}` and `{clientSecret}`. To do this, log in to your Okta Developer account and navigate to **Applications** > **Add Application**. Click **Web** and click the **Next** button. Give the app a name you’ll remember, specify `http://localhost:8080` as a Base URI, and `http://localhost:8080/login/oauth2/code/oidc` as a Login Redirect URI. Click **Done**, then Edit and add `http://localhost:8080` as a Logout redirect URI. Copy and paste the client ID and secret into your `application.yml` file.

Create a `ROLE_ADMIN` and `ROLE_USER` group and add users into them. Modify e2e tests to use this account when running integration tests. You'll need to change credentials in `src/test/javascript/e2e/account/account.spec.ts` and `src/test/javascript/e2e/admin/administration.spec.ts`.

Navigate to **API** > **Authorization Servers**, click the **Authorization Servers** tab and edit the default one. Click the **Claims** tab and **Add Claim**. Name it "groups", and include it in the ID Token. Set the value type to "Groups" and set the filter to be a Regex of `.*`.

After making these changes, you should be good to go! If you have any issues, please post them to [Stack Overflow](https://stackoverflow.com/questions/tagged/jhipster). Make sure to tag your question with "jhipster" and "okta".

### Using AWS Cognito

Source: https://blog.ippon.tech/aws-cognito-and-jhipster-for-the-love-of-oauth-2-0/

The first thing we will need if we want to use AWS' Cognito is to setup a User Pool and to configure it according to the blog post mentioned before: we will need to create at least 2 Groups: "ROLE_ADMIN" and "ROLE_USER" and at least 1 used having these 2 roles.
We will also need to create an "App Client" that uses OAuth2.

![Cognito App Client](./docs/assets/cognito-app-client.png)

It is important to configure the Callback and Sign out URLs.

The last thing we will need is to create a Domain name for the OAuth endpoint.

![Cognito App Client](./docs/assets/cognito-domain.png)

With all this information, we now need to modify the `src/main/resources/config/application.yml` to use your Cognito settings.

```yaml
spring:
  ...
  security:
    oauth2:
      client:
        provider:
          oidc:
            issuer-uri: https://cognito-idp.{REGION}.amazonaws.com/{POOL_ID}
            logout-uri: https://{DOMAIN}.auth.{REGION}.amazoncognito.com/logout
        registration:
          oidc:
            client-id: {clientId}
            client-secret: {clientSecret}
security:
```

It is recommended to set the concrete value of all these attributes when starting the application (as system properties or env variables) instead of having them in the yaml file.

The last step to enable AWS' Cognito as our Oauth2 server is to use a specific Spring profile called 'cognito' when starting the application. This can be done by either using `--spring.profiles.active=cognito`, `-Dspring.profiles.active=cognito`, etc.

## PWA Support

JHipster ships with PWA (Progressive Web App) support, and it's turned off by default. One of the main components of a PWA is a service worker.

The service worker initialization code is commented out by default. To enable it, uncomment the following code in `src/main/webapp/index.html`:

```html
<script>
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('./service-worker.js').then(function() {
      console.log('Service Worker Registered');
    });
  }
</script>
```

Note: [Workbox](https://developers.google.com/web/tools/workbox/) powers JHipster's service worker. It dynamically generates the `service-worker.js` file.

## Managing dependencies

For example, to add [Leaflet][] library as a runtime dependency of your application, you would run following command:

    npm install --save --save-exact leaflet

To benefit from TypeScript type definitions from [DefinitelyTyped][] repository in development, you would run following command:

    npm install --save-dev --save-exact @types/leaflet

Then you would import the JS and CSS files specified in library's installation instructions so that [Webpack][] knows about them:
Edit [src/main/webapp/app/vendor.ts](src/main/webapp/app/vendor.ts) file:

```
import 'leaflet/dist/leaflet.js';
```

Edit [src/main/webapp/content/scss/vendor.scss](src/main/webapp/content/scss/vendor.scss) file:

```
@import '~leaflet/dist/leaflet.css';
```

Note: There are still a few other things remaining to do for Leaflet that we won't detail here.

For further instructions on how to develop with JHipster, have a look at [Using JHipster in development][].

### Using Angular CLI

You can also use [Angular CLI][] to generate some custom client code.

For example, the following command:

    ng generate component my-component

will generate few files:

    create src/main/webapp/app/my-component/my-component.component.html
    create src/main/webapp/app/my-component/my-component.component.ts
    update src/main/webapp/app/app.module.ts

## Building for production

### Packaging as jar

To build the final jar and optimize the vcm application for production, run:

    ./mvnw -Pprod clean verify

This will concatenate and minify the client CSS and JavaScript files. It will also modify `index.html` so it references these new files.
To ensure everything worked, run:

    java -jar target/*.jar

Then navigate to [http://localhost:8080](http://localhost:8080) in your browser.

Refer to [Using JHipster in production][] for more details.

### Packaging as war

To package your application as a war in order to deploy it to an application server, run:

    ./mvnw -Pprod,war clean verify

### Packaging as docker image - Push to AWS ECR -

This section explains how to build VCM and prepare it to be pushed as an image into an AWS ECR container.
To do that we need three steps:

1.  Compile the code using a mvn wrapper command (mvnw)
2.  Push the image into ECR using a bash script that is under deploy/ecr called tagAndPublish.sh
3.  Deploy the image on Elastic Beanstalk.

#### Compile VCM as a docker image (using mvnw)

        ./mvnw -DskipTests -Pprod verify jib:dockerBuild

#### Push the pushed image on Elastic Beanstalk

The script initially will need a Saperi Google Account user with privileges to push to the ECR that is using the
bash script.
Please replace <username> with the user that will push the image.

        ./deploy/ecr/tagAndPublish.sh <username>

#### Deploy the image on Elastic Beanstalk.

On AWS go to Elastic Beanstalk services and follow next steps:

1. Go to Vcm-environment
2. Click on Upload and deploy
3. Use Application Versions page link
4. On Applications versions select vcm-env-cloudwatch-metrics
5. On Actions dropdown select deploy.

Please see images below

![VCM Deploy 1](./docs/assets/vcm-deploy-1.png)

![VCM Deploy 2](./docs/assets/vcm-deploy-2.png)

![VCM Deploy 3](./docs/assets/vcm-deploy-3.png)

## Testing

To launch your application's tests, run:

    ./mvnw verify

### Client tests

Unit tests are run by [Jest][] and written with [Jasmine][]. They're located in [src/test/javascript/](src/test/javascript/) and can be run with:

    npm test

For more information, refer to the [Running tests page][].

### Code quality

Sonar is used to analyse code quality. You can start a local Sonar server (accessible on http://localhost:9001) with:

```
docker-compose -f src/main/docker/sonar.yml up -d
```

You can run a Sonar analysis with using the [sonar-scanner](https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner) or by using the maven plugin.

Then, run a Sonar analysis:

```
./mvnw -Pprod clean verify sonar:sonar
```

If you need to re-run the Sonar phase, please be sure to specify at least the `initialize` phase since Sonar properties are loaded from the sonar-project.properties file.

```
./mvnw initialize sonar:sonar
```

or

For more information, refer to the [Code quality page][].

## Using Docker to simplify development (optional)

You can use Docker to improve your JHipster development experience. A number of docker-compose configuration are available in the [src/main/docker](src/main/docker) folder to launch required third party services.

For example, to start a mysql database in a docker container, run:

    docker-compose -f src/main/docker/mysql.yml up -d

To stop it and remove the container, run:

    docker-compose -f src/main/docker/mysql.yml down

### MySQL + AMQ + Keyclock

    docker-compose -f src/main/docker/services.yml up -d

To stop it and remove the container, run:

    docker-compose -f src/main/docker/services.yml down

You can also fully dockerize your application and all the services that it depends on.
To achieve this, first build a docker image of your app by running:

    ./mvnw -Pprod verify jib:dockerBuild

Then run:

    docker-compose -f src/main/docker/app.yml up -d

For more information refer to [Using Docker and Docker-Compose][], this page also contains information on the docker-compose sub-generator (`jhipster docker-compose`), which is able to generate docker configurations for one or several JHipster applications.

###Start the VCM application locally
`./mvnw -P-webpack -Dspring-boot.run.jvmArguments="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5004 -Dvcm.ventilator-recommender.rb.api-key=<aws-lamba-api-key> -Dspring.security.oauth2.client.registration.oidc.client-id=web_app -Dspring.security.oauth2.client.registration.oidc.client-secret=web_app -Dspring.security.oauth2.client.registration.oidc-client-credentials.client-id=vcm_pulse -Dspring.security.oauth2.client.registration.oidc-client-credentials.client-secret=f66451a9-61e9-4ccd-9606-bc10786c36f3 -Dspring.profiles.active=dev -Dvcm.pulse.host=localhost -Dvcm.pulse.port=8050 -Dvcm.ventilator-recommender.ai.pressure.api-key=<aws-lamba-api-key> -Dvcm.ventilator-recommender.ai.volume.api-key=<aws-lamba-api-key> -Dvcm.ventilator-recommender.rb.pressure.api-key=<aws-lamba-api-key> -Dvcm.ventilator-recommender.rb.volume.api-key=<aws-lamba-api-key>"`

#### Simple Version - Without Amazon Recommenders

`./mvnw -P-webpack -Dspring-boot.run.jvmArguments="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5004 -Dspring.security.oauth2.client.registration.oidc.client-id=web_app -Dspring.security.oauth2.client.registration.oidc.client-secret=web_app -Dspring.security.oauth2.client.registration.oidc-client-credentials.client-id=vcm_pulse -Dspring.security.oauth2.client.registration.oidc-client-credentials.client-secret=f66451a9-61e9-4ccd-9606-bc10786c36f3 -Dspring.profiles.active=dev -Dvcm.pulse.host=localhost"`

`./mvnw -P-webpack -Dspring-boot.run.jvmArguments="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5004 -Dspring.security.oauth2.client.registration.oidc-client-credentials.client-secret=f66451a9-61e9-4ccd-9606-bc10786c36f3 -Dspring.profiles.active=dev"`

Create a vcm*pulse client on keyclock (please have a look to the https://bitbucket.org/saperisys/pulse-sim/src/develop/README.MD) Section \*\*\_Create Keycloak VCM Pulse Client*\*\*

#### Simple Version that relies on the dev profile

Note that for the client-id vcm-pulse secret key (f66451a9-61e9-4ccd-9606-bc10786c36f3) that is on the parameter `spring.security.oauth2.client.registration.oidc-client-credentials.client-secret` should be taken from the Keycloak - Clients section - Credentials

![Keyclock clients](./docs/assets/keycloak-app-client.png)

## Continuous Integration (optional)

To configure CI for your project, run the ci-cd sub-generator (`jhipster ci-cd`), this will let you generate configuration files for a number of Continuous Integration systems. Consult the [Setting up Continuous Integration][] page for more information.

[jhipster homepage and latest documentation]: https://www.jhipster.tech
[jhipster 6.8.0 archive]: https://www.jhipster.tech/documentation-archive/v6.8.0
[using jhipster in development]: https://www.jhipster.tech/documentation-archive/v6.8.0/development/
[using docker and docker-compose]: https://www.jhipster.tech/documentation-archive/v6.8.0/docker-compose
[using jhipster in production]: https://www.jhipster.tech/documentation-archive/v6.8.0/production/
[running tests page]: https://www.jhipster.tech/documentation-archive/v6.8.0/running-tests/
[code quality page]: https://www.jhipster.tech/documentation-archive/v6.8.0/code-quality/
[setting up continuous integration]: https://www.jhipster.tech/documentation-archive/v6.8.0/setting-up-ci/
[node.js]: https://nodejs.org/
[yarn]: https://yarnpkg.org/
[webpack]: https://webpack.github.io/
[angular cli]: https://cli.angular.io/
[browsersync]: https://www.browsersync.io/
[jest]: https://facebook.github.io/jest/
[jasmine]: https://jasmine.github.io/2.0/introduction.html
[protractor]: https://angular.github.io/protractor/
[leaflet]: https://leafletjs.com/
[definitelytyped]: https://definitelytyped.org/
